PRES_FIGS      = /PETSc3/presentations-git/figures
PRESENTATIONS := $(wildcard Pres*.tex)
TUTORIALS     := $(wildcard Tutorial*.tex)
POSTERS       := $(wildcard Poster*.tex)
INTERVIEWS    := $(wildcard Interview*.tex)
LECTURES      := $(wildcard Lecture*.tex)
TEXIN          = ./tex:${TEXINPUTS}:${PRES_FIGS}
BIBIN          = .:/PETSc3/papers/bibtexjb:/PETSc3/petsc/petsc-dev/src/docs/tex:/Users/knepley/Documents:${BIBINPUTS}
ALLPDF         = ${PRESENTATIONS:.tex=.pdf} ${TUTORIALS:.tex=.pdf} ${POSTERS:.tex=.pdf} ${INTERVIEWS:.tex=.pdf} ${LECTURES:.tex=.pdf}

all: ${ALLPDF}

%.pdf: %.tex
	TEXINPUTS=${TEXIN} BIBINPUTS=${BIBIN} latexmk -pdf $^

clean::
	rm -f *.aux *.bbl *.log *.blg *.fdb_latexmk *.fls *.nav *.out *.pyg *.snm *.toc *.vrb *~

.PHONY: figures clean

include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules
