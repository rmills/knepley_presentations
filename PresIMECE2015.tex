\documentclass[dvipsnames]{beamer}

% 12-38-2 Mechanics and materials in biology and medicine
% 372A 3:30pm-5:00pm
%   4:06pm - 4:24pm

\input{tex/talkPreamble.tex}
\beamertemplatenavigationsymbolsempty

\title[BioElec]{Work/Precision Tradeoffs in Continuum Models of Biomolecular Electrostatics}
\author[M.~Knepley]{Matthew~Knepley and Jaydeep Bardhan}
\date[IMECE]{ASME International Mechanical Engineering\\Congress \& Exposition\\Houston, TX \qquad November 16, 2015}
% - Use the \inst command if there are several affiliations
\institute[Rice]{
  Computational and Applied Mathematics\\
  Rice University
}
\subject{BEM}

\begin{document}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.12]{figures/logos/anl-white-background-modern.jpg}\hspace{1.0in}
  \includegraphics[scale=0.10]{figures/logos/RiceLogo_TMCMYK300DPI.jpg}
  \end{center}
  \vskip0.4in
\end{frame}

%
\begin{frame}<testing>{Abstract}
The structure and function of biological molecules are strongly influenced by the water and dissolved ions that
surround them.  This aqueous solution (solvent) exerts significant electrostatic forces in response to the biomolecule's
ubiquitous atomic charges and polar chemical groups. In this work, we investigate a simple approach to numerical
calculation of this model using boundary-integral equation (BIE) methods and boundary-element methods (BEM).
Traditional BEM discretizes the protein--solvent boundary into a set of boundary elements, or panels, and the
approximate solution is defined as a weighted combination of basis functions with compact support.  The resulting BEM
matrix then requires integrating singular or near singular functions, which can be slow and challenging to compute.
Here we investigate the accuracy and convergence of a simpler representation, namely modeling the unknown surface charge
distribution as a set of discrete point charges on the surface. We find that at low resolution, point-based BEM is more
accurate than panel-based methods, due to the fact that the protein surface is sampled directly, and can be of
significant value for numerous important calculations that require only moderate accuracy, such as the preliminary
stages of rational drug design and protein engineering.
\end{frame}
%
\begin{frame}{Main Idea}\LARGE
Show work/accuracy tradeoff for\\
\quad boundary element variants, and\\
\pause
\medskip
quantify the dependence on\\
\quad mesh resolution and\\
\pause
\medskip
\quad the geometric complexity of\\
\quad\quad the molecular boundary
\end{frame}
%
%
\section{Model}
%
\begin{frame}{Bioelectrostatics}
\setlength{\tabcolsep}{0pt}
\begin{tabular}{ll}
\vbox{
  \hbox{\includegraphics[width=1.6in,height=1.6in]{figures/Collaborators/JayBardhan.png}}
  \smallskip
  \hbox{\Large Jaydeep Bardhan}
  \smallskip
  \hbox{\Large \ ME Northeastern}
  \vspace{6ex}
} & \includegraphics[width=4in]{figures/Bioelectrostatics/lysozymeSurfaceCharge.pdf}
\end{tabular}
\end{frame}
%
\input{slides/Bioelectrostatics/Lysozyme.tex}
\input{slides/Bioelectrostatics/ContinuumModel.tex}
\input{slides/Bioelectrostatics/SecondKindModel.tex}
\input{slides/Bioelectrostatics/ReactionPotentialDefinition.tex}
%
\subsection{Panel Discretization}
%
\begin{frame}{Galerkin}

\Large
Introduce test functions $\tau_i(\vr)$,
\smallskip
\begin{align*}
  \int_a d^2\vr\, \tau_i(\vr) \left( \sigma(\vr) + \hat\epsilon \int_a \frac{\partial}{\partial n(\vr)}
  \frac{\sigma(\vr') d^2\vr'}{4\pi|\vr-\vr'|} \right) &= \\
  \quad -\int_a d^2\vr\, \tau_i(\vr) \hat\epsilon \sum^Q_{k=1} \frac{\partial}{\partial n(\vr)} \frac{q_k}{4\pi|\vr-\vr_k|}
\end{align*}
\end{frame}
%
\begin{frame}{Qualocation}

\Large
\begin{align*}
  \sum_m w_m \tau_i(\vr_m) \left( \sigma(\vr_m) + \hat\epsilon \frac{\partial}{\partial n(\vr_m)}
  \frac{\sigma(\vr_j) a_j}{4\pi|\vr_m-\vr_j|} \right) &= \\
  \quad -\sum_m w_m \tau_i(\vr_m) \hat\epsilon \sum^Q_{k=1} \frac{\partial}{\partial n(\vr_m)} \frac{q_k}{4\pi|\vr_m-\vr_k|}.
\end{align*}
\medskip
\makebox[\widthof{\bf Outer:}][l]{\bf Inner:} One-point centroid quadrature\\
{\bf Outer:} High-order quadrature or analytics
\end{frame}
%
\begin{frame}{Computational Complexity}{Direct Method}

\Large
For evaluation and inversion of $A$, time is in
\bigskip
\begin{align*}
  \mathcal{O}(N_p N_c^2) + \mathcal{O}(N_c^3),
\end{align*}
\bigskip
where $N_p$ is the cost of integration vs. collocation.
\begin{center}
  $B$ and $C$ are lower order
\end{center}
\end{frame}
%
\begin{frame}{Computational Complexity}{Iterative Method}

\Large
For evaluation and inversion of $A$, time is in
\bigskip
\begin{align*}
  \mathcal{O}(N_p N_c^2) + \mathcal{O}(N_c^2),
\end{align*}
\bigskip
since the condition number and iterates are bounded.
\begin{center}
  $B$ and $C$ are lower order
\end{center}
\end{frame}
%
\begin{frame}{Computational Complexity}{Hierarchical Method}

\Large
For evaluation and inversion of $A$, time is in 
\bigskip
\begin{align*}
  \mathcal{O}(N_p N_c) + \mathcal{O}(N_c)
\end{align*}
\bigskip
using FMM or other hierarchical methods.
\begin{center}
  $B$ and $C$ are lower order
\end{center}
\end{frame}
%
\subsection{Point Discretization}
%
\begin{frame}{Collocation}

\Large
Discretize using the Nystr\"om method (quadrature)
\bigskip
\begin{align*}
  \sigma(\vr_i) + \hat\epsilon \sum_j \frac{\partial}{\partial n(\vr_i)} \frac{\sigma(\vr_j) w_j}{4\pi|\vr_i-\vr_j|} =
  -\hat\epsilon \sum^Q_{k=1} \frac{\partial}{\partial n(\vr_i)} \frac{q_k}{4\pi|\vr_i-\vr_k|}
\end{align*}
\bigskip
\makebox[\widthof{\bf Weights:}][l]{\bf Points:} Vertices of panel triangulations\\
{\bf Weights:} $\displaystyle\sum_{\mathrm{adj\ panels}} \frac{\mathrm{panel\ area}}{3}$
\end{frame}
%
\begin{frame}{Computational Complexity}{Iterative Method}

\Large
For evaluation and inversion of $A$, time is in 
\bigskip
\begin{align*}
  \mathcal{O}(N_c^2) + \mathcal{O}(N_c^2),
\end{align*}
\bigskip
since the condition number and iterates are bounded.
\begin{center}
  $B$ and $C$ are lower order
\end{center}
\end{frame}
%
%
\section{Experiments}
%
\subsection{Sphere}
%
\begin{frame}{Model}

\Large
\begin{itemize}
  \item Sphere of radius $R$
  \bigskip
  \item Dielectric constant $\epsilon_{II}$ inside
  \bigskip
  \item Dielectric constant $\epsilon_I$
  \bigskip
  \item Filled with $Q$ random charges
  \bigskip
  \item Charges at least $h$ from surface
\end{itemize}
\end{frame}
%
\begin{frame}{Mesh Convergence}{Random Charges in a Sphere}

% THE ANALYTIC SOLUTION IS CALCULATED TO MULTIPOLE ORDER 25
\begin{center}
%% ./meshConvergence.py --type=sphere --output=workPrecSphere
\includegraphics[width=0.65\textwidth]{/PETSc3/papers/biology/imece15-asme/figures/meshConvSphere_RelErr.png}\\
\Large
$R = 6.0$, $h = 1.0$, $\epsilon_I = 80$, $\epsilon_{II} = 4$, and $Q = 10$
\end{center}
% THE PANEL METHOD (PAN) CONVERGES LINEARLY, AND THE POINT METHOD SRF HAS ORDER 1/2.
\end{frame}
%
\begin{frame}{Work-Precision}{Random Charges in a Sphere}
%  THE PANEL METHOD IS SUPERIOR FOR ERROR BELOW 3 KCAL/MOL, ALTHOUGH WHEN
%  ONLY CONSIDERING THE SURFACE-TO-SURFACE (S2S) OPERATOR $A$, THE POINT METHOD IS SUPERIOR DOWN TO 1.5 KCAL/MOL.
\begin{center}
%% ./workPrecision.py --type=sphere --output=workPrecSphere
\includegraphics[width=0.75\textwidth]{/PETSc3/papers/biology/imece15-asme/figures/workPrecSphere.png}
\end{center}
\end{frame}
%
\subsection{Residues}
%
\begin{frame}{Model}{$\alpha$-amino acids}

\Large Look at aspartic acid (ASP) and arginine (ARG)
\bigskip
\begin{itemize}
  \item Geometry from the {\bf Protein Data Bank}
  \bigskip
  \item Atomic radii from the {\bf CHARMM} force field
  \bigskip
  \item Meshes created using {\bf meshmaker} and {\bf MSMS}
\end{itemize}
\end{frame}
%
\begin{frame}{Mesh Convergence}{$\alpha$-amino acids}

% THE PANEL METHOD (PAN) CONVERGES LINEARLY, AND THE POINT METHOD (SRF) HAS ORDER 1/2
\begin{overprint}
\onslide<1>
\begin{center}
%% ./meshConvergence.py --type=asp --output=meshConvASP
\includegraphics[width=0.5\textwidth]{/PETSc3/papers/biology/imece15-asme/figures/meshConvASP_RelErr.png}
%% ./meshConvergence.py --type=arg --output=meshConvARG
\includegraphics[width=0.5\textwidth]{/PETSc3/papers/biology/imece15-asme/figures/meshConvARG_RelErr.png}\\
\Large
$\epsilon_I = 80$ \qquad $\epsilon_{II} = 4$
\end{center}
\onslide<2>
\begin{center}
\includegraphics[width=0.5\textwidth]{figures/Bioelectrostatics/Aspartate-sphere-pymol.png}
\includegraphics[width=0.5\textwidth]{figures/Bioelectrostatics/Arginine-sphere-pymol.png}\\
\Large
$\epsilon_I = 80$ \qquad $\epsilon_{II} = 4$
\end{center}
\onslide<3>
\begin{center}
%% ./meshConvergence.py --type=asp --output=meshConvASP
\includegraphics[width=0.5\textwidth]{/PETSc3/papers/biology/imece15-asme/figures/meshConvASP_RelErr.png}
%% ./meshConvergence.py --type=arg --output=meshConvARG
\includegraphics[width=0.5\textwidth]{/PETSc3/papers/biology/imece15-asme/figures/meshConvARG_RelErr.png}\\
\Large
$\epsilon_I = 80$ \qquad $\epsilon_{II} = 4$
\end{center}
\end{overprint}
\end{frame}
%
\begin{frame}{Solvation Energy}{$\alpha$-amino acids}

%NOTE THAT INITIALLY THE POINT METHOD CONVERGES FASTER, BUT STAGNATES SOON AFTER.
\begin{center}
%% ./meshConvergence.py --type=asp --output=meshConvASP
\includegraphics[width=0.65\textwidth]{/PETSc3/papers/biology/imece15-asme/figures/meshConvASP_SolvEng.png}\\
\Large
$\epsilon_I = 80$ \qquad $\epsilon_{II} = 4$
\end{center}
\end{frame}
%
\begin{frame}{Work-Precision}{$\alpha$-amino acids}

% THE PANEL METHOD IS SUPERIOR FOR ERROR BELOW 0.25 KCAL/MOL.
% THE PANEL METHOD IS SUPERIOR FOR ERROR BELOW 0.75 KCAL/MOL, ALTHOUGH WHEN ONLY CONSIDERING THE SURFACE-TO-SURFACE (S2S) OPERATOR $A$, THE POINT METHOD IS SUPERIOR DOWN TO 0.25 KCAL/MOL.
\begin{center}
%% ./workPrecision.py --type=asp --output=workPrecASP
\includegraphics[width=0.5\textwidth]{/PETSc3/papers/biology/imece15-asme/figures/workPrecASP.png}
%% ./workPrecision.py --type=asp --output=workPrecARG
\includegraphics[width=0.5\textwidth]{/PETSc3/papers/biology/imece15-asme/figures/workPrecARG.png}\\
\Large
$\epsilon_I = 80$ \qquad $\epsilon_{II} = 4$
\end{center}
\end{frame}
%
%
\section{Further Questions}
%
%% Could optimize point placement to lower threshold for crossover
%% Same analysis for a fast method
\begin{frame}{Bioelectrostatics}
  \includegraphics[width=4.75in]{figures/Bioelectrostatics/asymmetry-sphere-fits.pdf}
\end{frame}
%
\input{slides/PETSc/MattInPETSc.tex}

\end{document}
