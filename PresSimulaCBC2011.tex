\documentclass{beamer}

\input{tex/talkPreamble.tex}

\title[GPU]{Developing GPU-Enabled Scientific Libraries}
\author[M.~Knepley]{Matthew~Knepley}
\date[CBC]{CBC Workshop on CBC Key Topics\\Simula Research Laboratory\\Oslo, Norway \quad August 25--26, 2011}
% - Use the \inst command if there are several affiliations
\institute[UC]{
  Computation Institute\\
  University of Chicago

  \smallskip

  Department of Molecular Biology and Physiology\\
  Rush University Medical Center
}
\subject{PETSc}

\begin{document}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.3]{figures/logos/rush_logo.png}\hspace{1.0in}
  \includegraphics[scale=0.24]{figures/logos/uc-logo-official.jpg}
  \end{center}
  \vskip0.4in
\end{frame}
%
\begin{frame}<testing>{Abstract}\small
A central challenge for the use of GPU hardware for computational science is the integration of code optimized for
manycore execution into existing scientific libraries. The most successful paradigm for several decades has been the
development of high quality libraries, such as the PETSc library for parallel linear algebra and scalable linear and
nonlinear solvers. We will demonstrate the steps taken to augment PETSc with GPU code for linear and nonlinear
solvers. However, the fragility of GPU with respect to performance necessitates the extension of the library approach
with code generation facilities. We will present both template metaprogramming and domain specific language (DSL)
generation schemes for code generation and discuss the tradeoffs for both library developers and users. We will discuss
the development of a library for finite element computations on the GPU, compatible with PETSc solvers, and also with
the FEniCS toolkit which allows flexible specification of weak forms using a DSL.
\end{frame}
%
%
\section{Scientific Libraries}
\input{slides/GPU/GPULibraries.tex}
\input{slides/GPU/LessonsFromClusters.tex}
\subsection{What is PETSc?}
\input{slides/PETSc/OriginalImpetus.tex}
\input{slides/PETSc/RoleOfPETSc.tex}
\input{slides/PETSc/WhatIsPETSc.tex}
\input{slides/PETSc/PETScDevelopers.tex}
\input{slides/PETSc/WhoUsesPETSc.tex}
\input{slides/PETSc/Limits.tex}
%
\begin{frame}{Interface Questions}

{\LARGE How should the user interact with\\
manycore systems?}
\pause

Through computational libraries
\pause
\bigskip

{\LARGE How should the user interact with the library?}
\pause
Strong, data structure-neutral API (\magenta{\href{http://portal.acm.org/citation.cfm?id=245883}{Smith and Gropp, 1996}})
\pause
\bigskip

{\LARGE How should the library interact with\\
manycore systems?}
\pause
\begin{itemize}
  \item Existing library APIs

  \item Code generation (CUDA, OpenCL, PyCUDA)

  \item Custom multi-language extensions
\end{itemize}
\end{frame}
%
\begin{frame}{Performance Analysis}
In order to understand and predict the performance of GPU code, we need:

\bigskip

\blue{good models for the computation}, which make it possible to evaluate the efficiency of an implementation;

\bigskip

\blue{a flop rate}, which tells us how well we are utilizing the hardware;

\bigskip

\blue{timing}, which is what users care about;
\end{frame}
%
%
\section{Linear Systems}
\begin{frame}{Performance Expectations}{Linear Systems}
\begin{center}\LARGE
  The Sparse Matrix-Vector product (SpMV)\\
  is limited by system \red{memory bandwidth},\\
  rather than by peak \blue{flop rate}.
\end{center}
\bigskip
\begin{itemize}
  \item We expect bandwidth ratio speedup (3x--6x for most systems)
  \medskip
  \item Memory movement is more important than minimizing flops
  \medskip
  \item Kernel is a vectorized, segmented sum
    (\magenta{\href{http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.51.1840&rep=rep1&type=pdf}{Blelloch, Heroux, and Zagha: CMU-CS-93-173}})
\end{itemize}
\end{frame}
%
\begin{frame}{Memory Bandwidth}

All computations in this presentation are memory bandwidth limited. We have a \textit{bandwidth peak}, the maximum flop rate
achievable given a bandwidth. This depends on $\beta$, the ratio of bytes transferred to flops done by the algorithm.

\bigskip

\begin{tabular}{lrrr}
Processor     & BW (GB/s) & Peak (GF/s) & BW Peak${}^*$ (GF/s) \\
\hline
Core 2 Duo    &         4 &          34 &   1 \\
GeForce 9400M &        21 &          54 &   5 \\
GTX 285       &       159 &        1062 &  40 \\
Tesla M2050   &       144 &        1030 &  36 \\
\end{tabular}

\smallskip

{\small ${}^*$Bandwidth peak is shown for $\beta = 4$}
\end{frame}
%
\input{slides/Performance/Streams.tex}
\input{slides/Performance/SMVAnalysis.tex}
\input{slides/PETSc/LinearAlgebraInterfaces.tex}
\input{slides/PETSc/GPU/VecCUDA.tex}
\input{slides/PETSc/GPU/MatCUDA.tex}
\input{slides/PETSc/GPU/Solvers.tex}
\input{slides/PETSc/GPU/Example.tex}
\input{slides/PETSc/GPU/PFLOTRANExample.tex}
\input{slides/GPU/Results/LinearSolve_GeForce9400M.tex}
\input{slides/GPU/Results/LinearSolve_TeslaM2050.tex}
%
%
\section{Assembly}
\begin{frame}{Performance Expectations}{Matrix Assembly}
\begin{center}\LARGE
  Matrix Assembly, aggregation of inputs,\\
  is also limited by \red{memory bandwidth},\\
  rather than by peak \blue{flop rate}.
\end{center}
\bigskip
\begin{itemize}
  \item We expect bandwidth ratio speedup (3x--6x for most systems)
  \medskip
  \item Input for FEM is a set of element matrices
  \medskip
  \item Kernel is dominated by sort (submission to TOMS)
\end{itemize}
\end{frame}
%
\begin{frame}[fragile]{Assembly Interface}

{\LARGE A single new method is added:}
\medskip
\begin{cprog}
MatSetValuesBatch(Mat J, PetscInt Ne, PetscInt Nl,
                  PetscInt *elemRows,
                  PetscScalar *elemMats)
\end{cprog}

\bigskip

\begin{center}\LARGE
Thus, a user just \blue{batches} his input to achieve massive \red{concurrency}.
\end{center}
\end{frame}
%
\begin{frame}{Serial Assembly Steps}
\begin{enumerate}
  \item<1-> Copy elemRows and elemMat to device
  \item<2-> Allocate storage for intermediate COO matrix
  \item<3-> Use repeat\&tile iterators to expand row input
\end{enumerate}
\end{frame}
%
\input{slides/GPU/ConvenienceIterators.tex}
%
\begin{frame}{Serial Assembly Steps}
\begin{enumerate}
  \item<1> Copy elemRows and elemMat to device
  \item<1> Allocate storage for intermediate COO matrix
  \item<1> Use repeat\&tile iterators to expand row input
  \item<2> Sort COO matrix by row and column
  \begin{enumerate}
    \item Get permutation from (stably) sorting columns
    \item Gather rows with this permutation
    \item Get permutation from (stably) sorting rows
    \item Gather columns with this permutation
    \item Gather values with this permutation
  \end{enumerate}
\end{enumerate}
\end{frame}
%
\input{slides/GPU/MultikeySort.tex}
%
\begin{frame}{Serial Assembly Steps}
\begin{enumerate}
  \item<1> Copy elemRows and elemMat to device
  \item<1> Allocate storage for intermediate COO matrix
  \item<1> Use repeat\&tile iterators to expand row input
  \item<1> Sort COO matrix by row and column
  \item<2> Compute number of unique (i,j) entries using \cinline{inner_product()}
\end{enumerate}
\end{frame}
%
\input{slides/GPU/MultikeyUniqueEntries.tex}
%
\begin{frame}{Serial Assembly Steps}
\begin{enumerate}
  \item<1-> Copy elemRows and elemMat to device
  \item<1-> Allocate storage for intermediate COO matrix
  \item<1-> Use repeat\&tile iterators to expand row input
  \item<1-> Sort COO matrix by row and column
  \item<1-> Compute number of unique (i,j) entries using \cinline{inner_product()}
  \item<2-> Allocate COO storage for final matrix
  \item<3-> Sum values with the same (i,j) index using \cinline{reduce_by_key()}
  \item<4-> Convert to AIJ matrix
  \item<4-> Copy from GPU (if necessary)
\end{enumerate}
\end{frame}
%
%% There are two result graphs in /PETSc3
%
\begin{frame}{Parallel Assembly Steps}
\begin{enumerate}
  \item<1-> Copy elemRows and elemMat to device
  \item<2-> Use repeat\&tile iterators to expand row input
  \item<3-> Communicate off-process entry sizes
  \begin{enumerate}
    \item Find number of off-process rows (serial)
    \item Map rows to processes (serial)
    \item Send number of rows to each process (collective)
  \end{enumerate}
\end{enumerate}
\end{frame}
%
\begin{frame}{Parallel Assembly Steps}
\begin{enumerate}
  \item<1> Copy elemRows and elemMat to device
  \item<1> Use repeat\&tile iterators to expand row input
  \item<1> Communicate off-process entry sizes
  \item<2-> Allocate storage for intermediate diagonal COO matrix
  \item<3-> Partition entries
  \begin{enumerate}
    \item Partition into diagonal and off-diagonal\&off-process using \cinline{partition_copy()}
    \item Partition again into off-diagonal and off-process using \cinline{stable_partition()}
  \end{enumerate}
\end{enumerate}
\end{frame}
%
\input{slides/GPU/PartitionEntries.tex}
%
\begin{frame}{Parallel Assembly Steps}
\begin{enumerate}
  \item<1-> Copy elemRows and elemMat to device
  \item<1-> Use repeat\&tile iterators to expand row input
  \item<1-> Communicate off-process entry sizes
  \item<1-> Allocate storage for intermediate diagonal COO matrix
  \item<1-> Partition entries
  \item<2-> Send off-process entries
  \item<3-> Allocate storage for intermediate off-diagonal COO matrix
  \item<4-> Repartition entries into diagonal and off-diagonal using \cinline{partition_copy()}
  \item<5-> Repeat serial assembly on both matrices
\end{enumerate}
\end{frame}
%
\input{slides/GPU/Results/Assembly_GTX285.tex}
%
%
\section{Integration}
% Needs fine-grained control over statements, loop nesting and unrolling, blocking
% No longer contrained only by memory bandwidth
\input{slides/FEM/GPU/Benefits.tex}
\subsection{Analytic Flexibility}
\input{slides/FEM/GPU/AnalyticFlexibility.tex}
\input{slides/FEM/FormDecomposition.tex}
\input{slides/FFC/WeakFormProcessing.tex}
\subsection{Computational Flexibility}
\input{slides/FEM/GPU/ComputationalFlexibility.tex}
\subsection{Efficiency}
\input{slides/FEM/GPU/Results2_GTX285.tex}
\input{slides/FEM/GPU/PricePerformance.tex}
%
%
\section{Yet To be Done}
\begin{frame}{Competing Models}
\begin{center}\Huge
 How should modern scientific computing be structured?
\end{center}

\bigskip

\begin{columns}
\begin{column}[T]{0.35\textwidth}
Current Model: \magenta{\href{http://www.mcs.anl.gov/petsc}{PETSC}}
\begin{itemize}
  \item Single language
  \item Hand optimized
  \item<2-> \red{3rd party libraries}
  \item<3-> \red{new hardware}
\end{itemize}
\end{column}
%
\begin{column}[T]{0.65\textwidth}
\visible<4->{
Alternative Model: \magenta{\href{http://numerics.kaust.edu.sa/pyclaw}{PetCLAW}}
\begin{itemize}
  \item Multiple language through Python
  \item Optimization through code generation
  \item 3rd party libaries through wrappers
  \item New hardware through code generation
\end{itemize}
}
\end{column}
\end{columns}
\end{frame}
%
\input{slides/ScientificComputing/PythonApplicationModel.tex}
%
\begin{frame}{What Do We Still Need?}
\begin{itemize}
  \item {\Large Better integration of code generation}
  \begin{itemize}
    \item Match CUDA driver interface to CUDA runtime interface
    \medskip
    \item Extend code generation to quadrature schemes
    \medskip
    \item Kernel fusion in assembly
  \end{itemize}
  \bigskip
  \item {\Large Better hierarchical parallelism}
  \begin{itemize}
    \item Larger scale parallel GPU tests
    \medskip
    \item Synchronization reduction in current algorithms
  \end{itemize}
\end{itemize}
\end{frame}

\end{document}
