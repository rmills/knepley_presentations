\documentclass[dvipsnames]{beamer}

\input{tex/talkPreamble.tex}
\usepackage{listings}

\title[Libraries]{Getting Modern Algorithms into the Hands of Working Scientists on Modern Hardware}
\author[M.~Knepley]{Matthew~Knepley}
\date[Monash]{Applied and Computational Mathematics seminar\\School of Mathematical Sciences, Monash University\\Victoria, Australia \quad October 15, 2012\vskip-0.15in}
% - Use the \inst command if there are several affiliations
\institute[UC]{
  Computation Institute\\
  University of Chicago

  \smallskip

  Department of Molecular Biology and Physiology\\
  Rush University Medical Center
}
\subject{PETSc}

\begin{document}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.3]{figures/logos/rush_logo.png}\hspace{1.0in}
  \includegraphics[scale=0.24]{figures/logos/uc-logo-official.jpg}
  \end{center}
  \vskip0.4in
\end{frame}
%
\begin{frame}<testing>{Introduction}

I want to begin with a digression on the types of talks one can give. When I was a young man, I gave only talks filled
with technical results. This is a good strategy for a young person setting out to demonstrate mastery of the material
and communicate their own contribution. Now that I am older, I can give talks which mix technical results with history and
advice. My former opinion of these additions was that they were entirely BS. However, and I think the physical sciences
recognize this better than mathematics, the value of experience can be great. The hallmark of physical science is its
interaction with the physical world, and it is long experience with this world that informs and guides theory, and now
computation.

Computational mathematics, or Computational Science, stands between the worlds of mathematics and physical science, and
thus must deal with these real world constraints. The speed of light, capacitive coupling, leakage current, and the
limits of photolithography all constrain the kinds of computations we can do efficiently. And these tradeoffs do change
depending on advances in materials science.

For pure mathematicians, the \textit{sine non qua} of technical communication is the journal paper, although people like
Terence Tao and Timothy Gowers have clearly shown that blogging and the polymath project can play a significant role.
However, more then 40 years ago, computational mathematicians created a new way to disseminate their results, namely
high quality numerical libraries. It is now a commonplace that a great part of your interaction with physical sciences,
engineering, and other fields can be mediated by software you produce and maintain. I will argue that the most effective
form of software communication is the library.

\end{frame}
%
\begin{frame}{Impact of Computational Mathematics}\LARGE
The main impact of\\\blue{computational mathematics} is in\\
\bigskip
\pause\Huge
\ design/analysis of algorithms\\
\medskip
\pause
\ for simulation \& data analysis\\
\pause
\bigskip\bigskip\LARGE
This is where CS comes in \ldots
% Code is the transmission mechanism from mathematicians/comp. scientisits to rest of the field
\end{frame}
%
%
\section{Computational Science}
\begin{frame}{Big Idea}\Huge
The best way to create robust,\\
\medskip\pause
efficient and scalable,\\
\medskip\pause
maintainable scientific codes,\\
\pause
\begin{center}
is to use \red{libraries}.
\end{center}
\end{frame}
%
\begin{frame}{Why Libraries?}
\begin{overprint}
\onslide<1-3>
\begin{itemize}\Large
  \item<1-> Hides Hardware Details
  \begin{itemize}
    \item \magenta{\href{http://www.mcs.anl.gov/mpi}{MPI}} does for this for machines and networks
  \end{itemize}
  \bigskip
  \item<2-> Hide Implementation Complexity
  \begin{itemize}
    \item \magenta{\href{http://www.mcs.anl.gov/petsc}{PETSc}} does for this Matrices and Krylov Solvers
  \end{itemize}
  \bigskip
  \item<3-> Accumulates Best Practices
  \begin{itemize}
    \item \magenta{\href{http://www.mcs.anl.gov/petsc}{PETSc}} defaults to classical Gram-Schmidt orthogonalization with
      selective reorthogonalization
  \end{itemize}
\end{itemize}
\onslide<4->
\begin{itemize}\Large
  \item<4-> Improvement without code changes
  \begin{itemize}
    \item \magenta{\href{http://www.mcs.anl.gov/petsc}{PETSc}} time integration library has expanded rapidly, e.g. IMEX
  \end{itemize}
  \bigskip
  \item<5-> Extensiblity
  \begin{itemize}
    \item<6-7> Q: Why is it not just good enough to make a fantastic working code?
    \item<7> A: {\bf Extensibility}\\Users need the ability to change your approach to fit their problem.
    \medskip
    \item<8-> \magenta{\href{http://www.mcs.anl.gov/petsc}{PETSc}} now does \magenta{\href{http://www.mcs.anl.gov/uploads/cels/papers/P2017-0112.pdf}{Multigrid+Block Solvers}}
    \medskip
    \item<9-> \magenta{\href{http://www.mcs.anl.gov/petsc}{PETSc}} now does \magenta{\href{https://bitbucket.org/dalcinl/petiga}{Isogeometric Analysis}}
  \end{itemize}
\end{itemize}
\end{overprint}
\end{frame}
%
\begin{frame}{Early Numerical Libraries}
\begin{itemize}
  \item[71] \magenta{\href{http://www.amazon.com/Handbook-Automatic-Computation-Vol-Mathematischen/dp/0387054146}{Handbook
    for Automatic Computation: Linear Algebra}},\\ \qquad J. H. Wilkinson and C. Reinch
  \begin{itemize}
    \item[73] \magenta{\href{http://www.netlib.org/blas/}{EISPACK}}, Brian Smith et.al.
  \end{itemize}
  \medskip
  \item[79] \magenta{\href{http://www.netlib.org/blas/}{BLAS}}, Lawson, Hanson, Kincaid and Krogh
  \medskip
  \item[90] \magenta{\href{http://www.netlib.org/lapack/}{LAPACK}}, many contributors
  \medskip
  \item[91] \magenta{\href{http://www.mcs.anl.gov/petsc/}{PETSc}}, Gropp and Smith
\end{itemize}
\begin{center}
  All of these packages had their genesis at\\ \magenta{\href{http://www.anl.gov}{Argonne National Laboratory}}/\magenta{\href{http://www.mcs.anl.gov}{MCS}}
\end{center}
\end{frame}
%
\input{slides/GPU/WhyGPU.tex}
%
\subsection{Linear Algebra}
\input{slides/PETSc/GPU/VecCUDA.tex}
\input{slides/PETSc/GPU/MatCUDA.tex}
\input{slides/PETSc/GPU/Solvers.tex}
\input{slides/PETSc/GPU/PFLOTRANExample.tex}
\input{slides/PETSc/GPU/Example.tex}
%
\subsection{FEM Integration}
\input{slides/FEM/InterfaceMaturity.tex}
\input{slides/FEM/IntegrationModel.tex}
\input{slides/PETSc/FEMIntegration.tex}
\begin{frame}{2D $P_1$ Laplacian Performance}
\begin{center}
\begin{tabular}{c}
  \includegraphics[width=0.8\textwidth]{/PETSc3/papers/gpu/gpu-fem-quadrature-paper/ex52_lap2d_block_gtx580.png} \\
  \Large Reaches \red{100} GF/s by 100K elements
\end{tabular}
\end{center}
\end{frame}
\begin{frame}{2D $P_1$ Laplacian Performance}
\begin{center}
\begin{tabular}{c}
  \includegraphics[width=0.8\textwidth]{/PETSc3/papers/gpu/gpu-fem-quadrature-paper/ex52_time.png} \\
  \Large Linear scaling for both GPU and CPU integration
\end{tabular}
\end{center}
\end{frame}
\begin{frame}{2D $P_1$ Laplacian Performance}{Configuring PETSc}
\$PETSC\_DIR/configure
\begin{itemize}
  \item<1->[] --download-triangle --download-chaco
  \item<2->[] --download-scientificpython --download-fiat --download-generator
  \item<3->[] --with-cuda
  \item<4>[]  --with-cudac='nvcc -m64' --with-cuda-arch=sm\_10
  \item<5>[]  --with-cusp-dir=/PETSc3/multicore/cusp
  \item<5>[]  --with-thrust-dir=/PETSc3/multicore/thrust
  \item<6>[]  --with-cuda-only
  \item<7>[] --with-precision=single
\end{itemize}
\end{frame}
\begin{frame}{2D $P_1$ Laplacian Performance}{Running the example}
\$PETSC\_DIR/src/benchmarks/benchmarkExample.py
\begin{itemize}
  \item[] -{}-daemon -{}-num 52 DMComplex
  \item[] -{}-events IntegBatchCPU IntegBatchGPU IntegGPUOnly
  \item[] -{}-refine 0.0625 0.00625 0.000625 0.0000625 0.00003125 0.000015625 0.0000078125 0.00000390625
  \item[] -{}-order=1 -{}-blockExp 4
  \item[] CPU='dm\_view show\_residual=0 compute\_function batch'
  \item[] GPU='dm\_view show\_residual=0 compute\_function batch gpu gpu\_batches=8'
\end{itemize}
\end{frame}
%
\begin{frame}{2D $P_1$ Rate-of-Strain Performance}
\begin{center}
\begin{tabular}{c}
  \includegraphics[width=0.8\textwidth]{figures/FEM/GPU/block_2D_P1_Elasticity} \\
  \Large Reaches \red{100} GF/s by 100K elements
\end{tabular}
\end{center}
\end{frame}
\begin{frame}{2D $P_1$ Rate-of-Strain Performance}{Running the example}
\$PETSC\_DIR/src/benchmarks/benchmarkExample.py
\begin{itemize}
  \item[] -{}-daemon -{}-num 52 DMComplex
  \item[] -{}-events IntegBatchCPU IntegBatchGPU IntegGPUOnly
  \item[] -{}-refine 0.0625 0.00625 0.000625 0.0000625 0.00003125 0.000015625 0.0000078125 0.00000390625
  \item[] -{}-operator=elasticity -{}-order=1 -{}-blockExp 4
  \item[] CPU='dm\_view op\_type=elasticity show\_residual=0 compute\_function batch'
  \item[] GPU='dm\_view op\_type=elasticity show\_residual=0 compute\_function batch gpu gpu\_batches=8'
\end{itemize}
\end{frame}
%
\begin{frame}{General Strategy}
\begin{itemize}\Large
  \item Vectorize
  \medskip
  \item Overdecompose
  \medskip
  \item Cover memory latency with computation
  \begin{itemize}
    \item Multiple cycles of writes in the kernel
  \end{itemize}
  \smallskip
  \item User must \blue{relinquish control of the layout}
\end{itemize}
\bigskip
\magenta{\href{http://arxiv.org/abs/1103.0066}{Finite Element Integration on GPUs}}, ACM TOMS,\\
\quad Andy Terrel and Matthew Knepley.\\
{\bf Finite Element Integration with Quadrature on the GPU}, to SISC,\\
\quad Robert Kirby, Matthew Knepley, Andreas Kl\"ockner, and Andy Terrel.
\end{frame}
%
%
\section{Mathematics}
\begin{frame}[fragile]{Composable System for Scalable Preconditioners}{Stokes and KKT}

The saddle-point matrix is a canonical form for handling constraints:
\begin{itemize}
  \item Incompressibility
  \item Contact
  \item Multi-constituent phase-field models
  \item Optimal control
  \item PDE constrained optimization
\end{itemize}

\smallskip

\includegraphics[width=0.4\textwidth]{./figures/Magma/ridgePlot.pdf}

\begin{textblock}{0.4}[0,1](0.35,0.92)
  \includegraphics[width=8cm]{./figures/Magma/ridgePorosity.jpg}\\
  {\tiny \qquad\qquad Courtesy R.~F. Katz}
\end{textblock}
\begin{textblock}{0.2}[1,0](0.9,0.3)
  {\tiny Courtesy M. Spiegelman}
  \includegraphics[width=3cm]{./figures/Magma/porousChannels.jpg}
\end{textblock}

% SAY: we are actively working on all these problems in PETSc.
\end{frame}
%
\begin{frame}{Composable System for Scalable Preconditioners}{Stokes and KKT}

There are \textit{many} approaches for saddle-point problems:
\begin{itemize}
  \item Block preconditioners % SAY: diagonal, triangular, physics-based
  \medskip
  \item Schur complement methods % SAY: many partial factorizations, different choices for $A$ and $S$ solvers
  \medskip
  \item Multigrid with special smoothers % SAY: also called seregated KKT smoothers
\end{itemize}
\begin{textblock}{0.2}[0.9,0.5](0.72,0.47)
  \begin{equation*}
    \begin{pmatrix} F & B & M \\ B^T & 0 & 0 \\ N & 0 & K\end{pmatrix} \begin{pmatrix} {\bf u}\\p\\T \end{pmatrix} = \begin{pmatrix} {\bf f}\\0\\q \end{pmatrix}
  \end{equation*}
\end{textblock}

\bigskip

However, today it is hard to \red{compare} \& \red{combine} them and combine in a {\bf hierarchical} manner.
\pause
For instance we might want,
\smallskip
\begin{overprint}
\onslide<3>
  \begin{tabular}{@{\hspace{3ex}}p{42em}}
  a Gauss-Siedel iteration between blocks of $({\bf u},p)$ and $T$,\\
  and a full Schur complement factorization for ${\bf u}$ and $p$.
  \end{tabular}
\onslide<4>
  \begin{tabular}{@{\hspace{3ex}}p{42em}}
  an upper triangular Schur complement factorization for ${\bf u}$ and $p$,\\
  and geometric multigrid for the ${\bf u}$ block.
  \end{tabular}
\onslide<5>
  \begin{tabular}{@{\hspace{3ex}}p{42em}}
  algebraic multigrid for the full $({\bf u},p)$ system,\\
  using a block triangular Gauss-Siedel smoother on each level,\\
  and use identity for the $(p,p)$ block.
  \end{tabular}
\end{overprint}

\end{frame}
%
\begin{frame}{Approach for efficient, robust, scalable linear solvers}

\vspace{-0.1in}
{\footnotesize
\begin{block}{\bf Need solvers to be:}
\begin{itemize}
  \item {\bf Composable}: separately developed solvers may be easily combined, by non-experts, to form a more powerful solver
  \smallskip
  \item {\bf Nested}: outer solvers call inner solvers
  \smallskip
  \item {\bf Hierarchical}: outer solvers may iterate over all variables for a global problem, while nested inner solvers handle smaller subsets of physics, smaller physical subdomains, or coarser meshes
  \smallskip
  \item {\bf Extensible}: users can easily customize/extend
\end{itemize}
\end{block}
}
\bigskip
\magenta{\href{http://www.mcs.anl.gov/uploads/cels/papers/P2017-0112.pdf}{Composable Linear Solvers for Multiphysics}}, IPDPS, 2012,\\
\quad J. Brown, M. G. Knepley, D. A. May, L. C. McInnes and B. F. Smith.
\end{frame}
%
%%\input{slides/PETSc/OrganizingPrinciple.tex}
%   Reusable pieces allow you to create algorithms not present in the package itself (Ex: Elman PCs, SIMPLE)
%   Add SIMPLE to Stokes Tour
\input{slides/MultiField/StokesOptionsTour.tex}
\input{slides/PETSc/ProgrammingWithOptions.tex}
%
\begin{frame}{Composability}
\Large \blue{Composable} interfaces allow the nested, hierarchical interaction of different components,
\pause
\begin{itemize}
  \item {\bf analysis} (discretization)
  \medskip
  \item {\bf topology} (mesh)
  \medskip
  \item {\bf algebra}  (solver)
\end{itemize}
\pause
so that \red{non-experts} can produce powerful simulations with modern algorithms.
\medskip
\pause
\begin{center}
  \magenta{\href{http://59a2.org/research/}{Jed Brown}} discusses this interplay\\
  in the context of \magenta{\href{}{multilevel solvers}}
\end{center}
\end{frame}
%
%
\section*{Conclusions}
\begin{frame}{Main Points}

\Large
\begin{itemize}
  \item<2-> Libraries encapsulate the Mathematics
  \begin{itemize}
    \item \large Users will give up more Control
  \end{itemize}

  \bigskip

  \item<3-> Multiphysics demands Composable Solvers
  \begin{itemize}
    \item \large Each piece will have to be Optimal
  \end{itemize}
\end{itemize}

\bigskip

\begin{center}
\visible<4>{\emph{Change alone is unchanging\\--- Heraclitus, 544--483 \sc{BC}}}
\end{center}
%
\end{frame}

\end{document}
