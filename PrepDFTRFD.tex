\documentclass{beamer}

\input{tex/talkPreamble.tex}

\title[GPU]{GPUs in Computational Science}
\author[M.~Knepley]{Matthew~Knepley and Peter Brune}
\institute[] % (optional, but mostly needed)
{
  Computation Institute\\
  University of Chicago
}

\date[7/27/10] % (optional)
{International Workshop on GPU Solutions to\\Multiscale Problems in Science and Engineering\\Harbin, China, July 27, 2010}

\subject{GPU Programming}

\begin{document}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.3]{figures/logos/rush_logo.png}\hspace{1.0in}
  \includegraphics[scale=0.24]{figures/logos/uc-logo-official.jpg}
  \end{center}
  \vskip0.4in
\end{frame}
%
%
\section{Molecular Biology}
\input{slides/IonChannel/Introduction.tex}
%
\subsection{Spectral Quadrature}
%
\begin{frame}{Electrostatic Reference Solution}
\begin{center}
  \includegraphics[width=2in]{figures/DFT/screening_radius.png} 
\end{center}  

\begin{equation}
  \rho^{\mathrm{ref}}(\vx) = \int \rho(\vx') \frac{\theta(|\vx' - \vx| - R(\vx))}{\frac{4\pi}{3} R^3(\vx)} d\vx'
\end{equation}

\begin{itemize}
  \item $\theta(\vx)$ is the Heaviside function, $R = \frac{\sum_i\ R_i \rho_i}{\sum_i \rho_i} + \frac{1}{2\Gamma(x)}$ 

  \item Real space quadrature is inaccurate ($\mathcal{O}(1)$ error in 3D)
\end{itemize}
\end{frame}
%
\begin{frame}{Spectral Quadrature}

\begin{eqnarray}
  \rho^{\mathrm{ref}}(x) &=& \int \rho(\vx') \frac{\theta(|\vx' - \vx| - R(\vx))}{\frac{4\pi}{3} R^3(\vx)} d\vx' \\
                       &=& \int \rho(\vx')\ \mathbb{K}^{\vx}(\vx') d\vx' \\
                       &=& \int \hat{\rho}(\vk)\ \hat{\mathbb{K}}^{\vx}(\vk) d\vk
\end{eqnarray}
where
\begin{equation}
  \hat{\mathbb{K}}^{\vx}(\vk) = 3 e^{i\vk\cdot\vx} \left\{ -\frac{1}{k^2 R^2} \cos kR + \frac{1}{k^3 R^3} \sin kR \right\}
\end{equation}

\begin{itemize}
\only<1>{
  \item For non-constant $R$, this is not a convolution
  \smallskip
  \item Inner product of $\hat{\rho}(\vk)$ and $\hat{\mathbb{K}}^{\vx}(\vk)$ for each $\vx \in \Omega$
}
\only<2>{
  \item Smooth Fourier space integrand can be accurately integrated
  \smallskip
  \item $\mathcal{O}(N^2)$ complexity $\Longrightarrow$ weeks of computer time
}
\end{itemize}
\end{frame}
%
\begin{frame}{Naive Algorithm}

\begin{center}\Large
The $\mathcal{O}(N^2)$ algorithm is much like the action of a \textbf{dense matrix},
\end{center}

except:
\begin{itemize}
  \item<1-> The matrix is never \red{assembled}

  \bigskip

  \item<2-> Data traffic is thus $\mathcal{O}(N)$ instead of $\mathcal{O}(N^2)$

  \bigskip

  \item<3-> Perfect for GPU SIMT paradigm
\end{itemize}
\end{frame}
%
\subsection{Algorithm}
\begin{frame}{Algorithm}

%MGK -- change the picture, LinAlg rather than spatial
\begin{center}
  \includegraphics[width=30mm]{figures/DFT/reductalg.png} 
\end{center}

\visible<2->{The algorithm is two-stage:}
\only<1>{
\begin{itemize}
  \item Minimize inter-thread communication
  \begin{itemize}
    \item Reductions
  \end{itemize}

  \medskip

  \item Maximize reuse of data stored in shared memory
  \begin{itemize}
    \item Common vectors, or sections of vectors
  \end{itemize}
\end{itemize}
}
\only<2->{
\begin{enumerate}
  \item Compute kernel \yellow{action} for a \red{real space block} on \blue{Fourier blocks}
  \begin{itemize}
    \item Compute limited
  \end{itemize}

  \medskip

  \item Compute sums for this \red{real space block} and write back
  \begin{itemize}
    \item Bandwidth limited
  \end{itemize}
\end{enumerate}
}
\visible<3->{Use two separate GPU kernels which martial the processors differently.}
\end{frame}
%
\begin{frame}{Step 1: Computation of $\mathbb{K}^{\vec{x}_i}(k_j)$}

%MGK -- Linalg picture here

\begin{center}\Large
Each kernel invocation operates\\
over some range of $\mathbb{R}^3$, $\{\vx_i\}$.
\end{center}

\only<1>{
{\bf Initialization}:
\begin{itemize}
  \item Thread block is assigned a range $\{\vk_j\}$ of Fourier space, $B$

  \bigskip

  \item Thread block precomputes quantities reused for each $\vk_j$

  \bigskip

  \item Size of $B$ limited by shared memory
\end{itemize}
}
\only<2>{
{\bf Computation}:
\begin{itemize}
  \item Thread is assigned a portion of $B$

  \bigskip

  \item Compute $\mathbb{K}^{\vx_i}(\vk_j)$ for each $\vx_i$

  \bigskip

  \item Sum results to form $\rho^{\mathrm{ref}}(\vx_i)_B$
\end{itemize}
}
\end{frame}
%
\begin{frame}{Step 2: Reduction over $B$ blocks}

\begin{center}\Large
  Reduction over blocks $B$ of Fourier space
\end{center}

\begin{itemize}
  \item Thread block is assigned a single $\vx_i$

  \bigskip

  \item Stream in partial results $\rho^{\mathrm{ref}}_B(\vx_i)$

  \bigskip

  \item Sum to form $\rho^{\mathrm{ref}}(\vx_i)$
\end{itemize}

\end{frame}
%
\subsection{Implementation}
\begin{frame}{PyCUDA}

\begin{center}\Large
  The best way to interact with the GPU through CUDA
\end{center}

\begin{itemize}
  \item Andreas Kl\"ockner at \href{http://mathema.tician.de/}{\magenta{Courant}}

  \medskip

  \item Great documentation, \href{http://documen.tician.de/pycuda}{\magenta{http://documen.tician.de/pycuda}}

  \medskip

  \item Automatic 
  \begin{itemize}
    \item Initialization and device management

    \item Compilation and dynamic loading

    \item Memory transfer and management
  \end{itemize}

  \medskip

  \item Easy \href{http://arxiv.org/abs/0911.3456}{\magenta{Code Generation and Auto-Tuning}}
  \begin{itemize}
    \item Combine with templating engine like \href{http://www.makotemplates.org}{\magenta{Mako}}
  \end{itemize}
\end{itemize}
\end{frame}
%
\begin{frame}{Implementation Details}

\begin{center}\Large
  Successful cross-language programming
\end{center}

\begin{itemize}
  \item Main DFT package is built on \href{http://www.mcs.anl.gov/petsc}{\magenta{PETSc}}

  \bigskip

  \item Reference density calculation in \href{http://www.python.org}{\magenta{Python}} using \href{http://mathema.tician.de/software/pycuda}{\magenta{PyCuda}}

  \bigskip

  \item Simple \href{http://cython.org}{\magenta{Cython}} wrapper allows DFT application to call Python

  \bigskip

  \item PyCuda enabled rapid prototyping and testing
  \begin{itemize}
    \item No real advantage to moving CUDA portion into C
  \end{itemize}
\end{itemize}
\end{frame}
%
\subsection{Results}
\begin{frame}{Experimental Setup}

{\Large The test system, \textit{BaconOst.cs.uchicago.edu}}
\begin{center}
  \includegraphics[width=1.5in]{figures/Hardware/Baconost.jpg} 
\end{center}

{\Large consists of}
\begin{itemize}
  \item GTX 285 (240 SPs, 1 GB Global Memory),
  \smallskip
  \item Intel Core 2 Duo E8400 3.0 GHz,
  \smallskip
  \item 4 GB RAM,
\end{itemize}
{\Large \visible<2->{and cost \$1000.}}
\end{frame}
%
\begin{frame}{Scaling}

We see very large speedups over the CPU implementation:
\begin{columns}
\begin{column}[c]{4in}
  \includegraphics[width=4in]{figures/DFT/ReferenceDensityTimingCPUvsGPU.png} 
\end{column}
\begin{column}[c]{1in}\LARGE
  \visible<2->{124 GF} % 667 GF peak
\end{column}
\end{columns}
\end{frame}
%
\begin{frame}{Problem}

\begin{center}\Large
  Two ion species of different radii close to a hard wall\\
\end{center}

\begin{columns}
\begin{column}[c]{3in}
  \includegraphics[width=3in]{figures/DFT/problem2RFD.png} 
\end{column}
\begin{column}[c]{2in}
\begin{itemize}
  \item Periodic domain

  \medskip

  \item $2 \times 2 \times 6$nm

  \medskip

  \item Hard wall at the origin

  \medskip

  \item Radius 1 \AA, 2.125 \AA

  \medskip

  \item Valence $+1$, $-1$
\end{itemize}
\end{column}
\end{columns}
\end{frame}
%
\begin{frame}{DFT Wall Problem Timings}

\Large
\begin{table}[H]
\center{
\begin{tabular}{|r|r|c|}
\hline
N                         & t (min) & Picard Iterations \\
\hline
$21  \times 21 \times 21$ &  4:05   & 60 \\
$41  \times 21 \times 21$ & 13:02   & 74 \\
$81  \times 21 \times 21$ & 36:47   & 71 \\
$161 \times 21 \times 21$ & 93:36   & 59 \\
\hline
\end{tabular}
}
\caption{Two species, hard wall problem}
\end{table}
\begin{center}
Contrasted with \red{weeks} on a CPU!
\end{center}
\end{frame}
%
%
\section{FEM}

\end{document}
