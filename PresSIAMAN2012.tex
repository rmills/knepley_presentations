\documentclass[dvipsnames]{beamer}

\input{tex/talkPreamble.tex}
\usepackage{listings}

\title[SIAM]{Incorporation of Multicore FEM Integration Routines into Scientific Libraries}
\author[M.~Knepley]{Matthew~Knepley}
\date[SIAM]{SIAM Annual Meeting 2012\\Minneapolis, MN \quad July 9--13, 2012}
% - Use the \inst command if there are several affiliations
\institute[UC]{
  Computation Institute\\
  University of Chicago

  \smallskip

  Department of Molecular Biology and Physiology\\
  Rush University Medical Center
}
\subject{PETSc}

\begin{document}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.3]{figures/logos/rush_logo.png}\hspace{1.0in}
  \includegraphics[scale=0.24]{figures/logos/uc-logo-official.jpg}
  \end{center}
  \vskip0.4in
\end{frame}
%
\begin{frame}<testing>{Abstract}\small
We have developed a system for integration of the finite element residual and Jacobian arising from a given weak
form. This system is based upon the Ignition code generation package and ideas from the FEniCS project, and generates
code targeting GPUs. Since this is merely a part of the overall PETSc library framework, we discuss the design decisions
made in order to incorporate these routines with a minimum of user input, generation of debuggable code, and smooth
integration into the PETSc build process.
\end{frame}
%
\begin{frame}{Collaborators}
\begin{center}\LARGE
\begin{tabular}{cc}
\includegraphics[width=4cm]{figures/PETSc/AndyTerrel.jpg} & \magenta{\href{http://andy.terrel.us/Professional/}{Andy R. Terrel}}
\end{tabular}
\end{center}
\bigskip
\begin{itemize}\Large
  \item \magenta{\href{http://mathema.tician.de/}{Andreas Kl\"ockner}}
  \medskip
  \item \magenta{\href{http://www.59a2.org/research/}{Jed Brown}}
  \medskip
  \item \magenta{\href{http://www.math.ttu.edu/~kirby/}{Robert Kirby}}
\end{itemize}
\end{frame}
%
\input{slides/GPU/GPULibraries.tex}
\input{slides/FEM/InterfaceMaturity.tex}
%
%%\input{slides/GPU/PETScGPU.tex}
%%\input{slides/PETSc/GPU/Example.tex}
%%\input{slides/PETSc/GPU/PFLOTRANExample.tex}
%%\input{slides/GPU/Results/LinearSolve_GeForce9400M.tex}
%%\input{slides/GPU/Results/LinearSolve_TeslaM2050.tex}
%
\input{slides/PETSc/FEMIntegration.tex}
\input{slides/FEM/IntegrationModel.tex}
%
\begin{frame}{Why Quadrature?}

{\Large Quadrature can handle}
\bigskip
\begin{itemize}
  \item many fields (linearization) % code size is important
  \medskip
  \item non-affine elements (Argyris)
  \medskip
  \item non-affine mappings (isoparametric)
  \medskip
  \item functions not in the FEM space
\end{itemize}
\bigskip
\begin{center}\scriptsize
  \magenta{\href{http://arxiv.org/abs/1104.0199}{Optimizations for Quadrature Representations of Finite Element Tensors
      through Automated Code Generation}}, ACM TOMS, Kristian B. {\O}lgaard and Garth N. Wells\\
  \medskip
  \magenta{\href{http://arxiv.org/abs/1103.0066}{Finite Element Integration on GPUs}}, ACM TOMS, Andy R. Terrel and Matthew G. Knepley
\end{center}
\end{frame}
%% Show code for Laplacian, elasticity, Laplacian with temp-dependent viscosity
\lstset{language=c,
  basicstyle=\fontfamily{cm}\normalfont\scriptsize,
  keywordstyle=\color{ForestGreen}\bfseries,
  %identifierstyle=\color{brown},
  morekeywords={realType,vecType,float2,float3,float4,PetscInt,PetscScalar},
  emph={__global__,__shared__,__device__},
  emphstyle=\color{Mulberry}\bfseries,
  emph={[2]integrateIdentityAction},
  emphstyle={[2]\color{RoyalBlue}\bfseries},
  texcl=true,
  commentstyle=\color{Red},
}
\begin{frame}[fragile]{Physics code}

\begin{overprint}
\onslide<1-2>
\begin{equation*}
  \nabla\phi_i \cdot \nabla u
\end{equation*}
\onslide<3-4>
\begin{equation*}
  \nabla\phi_i \cdot (\nabla u + {\nabla u}^T)
\end{equation*}
\onslide<5-6>
\begin{equation*}
  \nabla\phi_i \cdot \nabla u + \phi_i k^2 u
\end{equation*}
\onslide<7-8>
\begin{equation*}
  \nabla{\bf \phi}_i \cdot \nabla \vu - (\nabla\cdot{\bf \phi}) p
\end{equation*}
\onslide<9-10>
\begin{equation*}
  \nabla{\bf \phi}_i \cdot \nu_0 e^{-\beta T} \nabla \vu - (\nabla\cdot{\bf \phi}) p
\end{equation*}
\end{overprint}
\bigskip
\begin{overprint}
\onslide<2>
\begin{lstlisting}
__device__ vecType f1(realType u[], vecType gradU[], int comp) {
  return gradU[comp];
}
\end{lstlisting}
\onslide<4>
\begin{lstlisting}
__device__ vecType f1(realType u[], vecType gradU[], int comp) {
  vecType f1;

  switch(comp) {
  case 0:
    f1.x = 0.5*(gradU[0].x + gradU[0].x);
    f1.y = 0.5*(gradU[0].y + gradU[1].x);
    break;
  case 1:
    f1.x = 0.5*(gradU[1].x + gradU[0].y);
    f1.y = 0.5*(gradU[1].y + gradU[1].y);
  }
  return f1;
}
\end{lstlisting}
\onslide<6>
\begin{lstlisting}
__device__ vecType f1(realType u[], vecType gradU[], int comp) {
  return gradU[comp];
}

__device__ realType f0(realType u[], vecType gradU[], int comp) {
  return k*k*u[0];
}
\end{lstlisting}
\onslide<8>
\begin{lstlisting}
void f1(PetscScalar u[], const PetscScalar gradU[], PetscScalar f1[]) {
  const PetscInt dim   = SPATIAL_DIM_0;
  const PetscInt Ncomp = NUM_BASIS_COMPONENTS_0;
  PetscInt       comp, d;

  for(comp = 0; comp < Ncomp; ++comp) {
    for(d = 0; d < dim; ++d) {
      f1[comp*dim+d] = gradU[comp*dim+d];
    }
    f1[comp*dim+comp] -= u[Ncomp];
  }
}
\end{lstlisting}
\onslide<10>
\begin{lstlisting}
void f1(PetscScalar u[], const PetscScalar gradU[], PetscScalar f1[]) {
  const PetscInt dim   = SPATIAL_DIM_0;
  const PetscInt Ncomp = NUM_BASIS_COMPONENTS_0;
  PetscInt       comp, d;

  for(comp = 0; comp < Ncomp; ++comp) {
    for(d = 0; d < dim; ++d) {
      f1[comp*dim+d] = nu_0*exp(-beta*u[Ncomp+1])*gradU[comp*dim+d];
    }
    f1[comp*dim+comp] -= u[Ncomp];
  }
}
\end{lstlisting}
\end{overprint}
\end{frame}
%% Explain why quadrature is a problem, start with waht does not work for vectorization
\begin{frame}[fragile]{Why Not Quadrature?}

\begin{center}\LARGE Vectorization is a Problem\end{center}
\bigskip
\begin{tabular}{p{5cm}p{5.5cm}}
{\bf Strategy}  & {\bf Problem} \\\hline
\visible<2->{Vectorize over Quad Points}                    & \visible<2->{Reduction needed to compute Basis Coefficients} \\
\visible<3->{Vectorize over Basis Coef for each Quad Point} & \visible<3->{Too many passes through global memory} \\
\visible<4->{Vectorize over Basis Coef and Quad Points}     & \visible<4->{Some threads idle when sizes are different}
\end{tabular}
\end{frame}
%% Explain idea of thread transpose (2 slides)
\begin{frame}{Thread Transposition}
\begin{figure}
\centering
\tikzstyle{work group} = [draw,green,rounded corners]
\tikzstyle{thread}     = [fill,red]
\tikzstyle{operation}  = [dashed,rounded corners]
\tikzstyle{notation}   = [anchor=south,text width=4cm,text centered]
\begin{tikzpicture}[scale=0.35]
% Draw the basis function evaluation
\draw[operation] (-0.5,-0.5) rectangle +(9,12)
  ++(4.5,12) node[notation] {\scriptsize Map values at quadrature points to coefficients};
% Draw the basis function evaluation breakdown
\foreach \x in {0, 3, 6}
  \foreach \y/\j in {0/1, 6/0}
{
  % draw a cell work unit
  \path[work group] (\x,\y) rectangle +(2,5);
  % draw a 3 thread configuration
 \path[thread] (\x,\y)
             ++(0.5,0.5)
              +(0,0) rectangle +(1,1) +(0.5,0.5) node[anchor=mid,text=white] {\pgfmathtruncatemacro{\t}{\j*3+2}$t_{\t}$}
             ++(0.0,1.5)
              +(0,0) rectangle +(1,1) +(0.5,0.5) node[anchor=mid,text=white] {\pgfmathtruncatemacro{\t}{\j*3+1}$t_{\t}$}
             ++(0.0,1.5)
              +(0,0) rectangle +(1,1) +(0.5,0.5) node[anchor=mid,text=white] {\pgfmathtruncatemacro{\t}{\j*3+0}$t_{\t}$};
}
% Draw arrow for kernel continuation
\draw[<-,ultra thick] (8.5,5.5) -- node[anchor=south] {\scriptsize Continue with kernel} (17.5,5.5);
\draw[dashed] (8.5,11.5) -- (17.5,14.5);
\draw[dashed] (8.5,-0.5) -- (17.5,-3.5);
% Draw the quadrature point evaluation
\draw[operation] (17.5,-3.5) rectangle +(6,18)
  ++(3,18) node[notation] {\scriptsize Evaluate basis and process values at quadrature points};
% Draw the quadrature point evaluation breakdown
\foreach \x in {18, 21}
  \foreach \y/\j in {-3/2, 3/1, 9/0}
{
  % draw a cell work unit
  \path[work group] (\x,\y) rectangle +(2,5);
  % draw a 2 thread configuration
 \path[thread] (\x,\y)
             ++(0.5,1.0)
              +(0,0) rectangle +(1,1) +(0.5,0.5) node[anchor=mid,text=white] {\pgfmathtruncatemacro{\t}{\j*2+1}$t_{\t}$}
             ++(0.0,2.0)
              +(0,0) rectangle +(1,1) +(0.5,0.5) node[anchor=mid,text=white] {\pgfmathtruncatemacro{\t}{\j*2+0}$t_{\t}$};
}
\end{tikzpicture}
%\caption{Action of the residual evaluation kernel on a group of incoming cells. Each cell is displayed as a green,
%  rounded rectangle occupied by the threads which compute the cell information. Each thread computes its values in
%  series, so that thread $t_0$ first computes values at quadrature points for 2 cells, and then computes basis
%  coefficients for 3 cells.}
\label{fig:threadTranspose}
\end{figure}
\end{frame}
%
\begin{frame}
\begin{figure}
\centering
\renewcommand{\arraystretch}{1.5}
\tikzstyle{work group} = [draw,green,rounded corners]
\tikzstyle{thread}     = [red,dashed,rounded corners]
\tikzstyle{batch}      = [blue,rounded corners]
\tikzstyle{seq}        = [brown,thick,decorate,decoration={brace,mirror}]
\tikzstyle{seq2}       = [brown,thick,decorate,decoration={brace}]
\tikzstyle{notation}   = [anchor=south,text width=4cm,text centered]
\begin{tikzpicture}[scale=0.19]
% Draw the basis function evaluation breakdown
\path (-9.0, 17) node[anchor=south,text centered] {\bf Basis Phase};
\foreach \x in {0, 3, 6}
  \foreach \y in {-12, -6, 1, 7}
{
  % draw a cell work unit
  \path[work group] (\x,\y) rectangle +(2,5) +(1, 2.5) node[anchor=mid,text=black] {\tiny $\begin{tabular}{@{}c@{}c@{}} T & T\\T & T\\T & T\end{tabular}$};
}
% Draw the quadrature point evaluation breakdown
\path (35, 17) node[anchor=south,text centered] {\bf Quadrature Phase};
\foreach \x in {18, 21}
  \foreach \y in {-18, -12, -6, 1, 7, 13}
{
  % draw a cell work unit
  \path[work group] (\x,\y) rectangle +(2,5) +(1, 2.5) node[anchor=mid,text=black] {\tiny $\begin{tabular}{@{}c@{}c@{}} T & T\\T & T\end{tabular}$};
}
% Show number of threads
\draw[thread] (2.5,-12.5) rectangle +(3,25)
  ++(1.5,25) node[notation] {\tiny $N_t = 24$};
\draw[thread] (17.5,-18.5) rectangle +(3,37)
  ++(1.5,37) node[notation] {\tiny $N_t = 24$};
% Show number of cells in a batch
\draw[batch] (-1,-13) rectangle +(10,28)
  ++(5,28) node[notation] {\tiny $N_{bc} = 12$};
% Show number of cells in a block
\draw[batch] (17,-19) rectangle +(7,19)
  ++(3.5,0) node[notation,anchor=north] {\tiny $N_{bs} = 6$};
% Show number of sequential basis cells
\draw[seq] (0,-14) -- +(8,0) node[notation,pos=0.5,anchor=north] {\tiny $N_{sbc} = 3$};
% Show number of sequential quadrature cells
\draw[seq2] (18,21) -- +(5,0) node[notation,pos=0.5,anchor=south] {\tiny $N_{sqc} = 2$};
% Show the concurrent blocks
\draw (-2,6.5) -- ++(-5, 0) -- ++(0,-13) node[pos=0.5,anchor=east] {\tiny $N_{bl} = 2$} -- ++(5,0);
\draw (25,9.5) -- ++(5, 0) -- ++(0,-19) node[pos=0.5,anchor=west] {\tiny $N_{bl} = 2$} -- ++(-5,0);
\end{tikzpicture}
%\caption{Diagram of a single cell batch for a computation similar to Fig.~\ref{fig:threadTranspose} so that $N_b = 3$ and
%$N_q = 2$, but now with a vector element $N_{comp} = 2$. We choose $N_bl = 2$ concuurent blocks, so that the batch size
%is $N_{bc} = 12$. Each thread is represented by a $T$, and since we have a thread for each component, $N_t = 24$, with 4
%threads per cell in the quadrature phase, and 6 in the basis phase.}
\label{fig:cellBatch}
\end{figure}
\end{frame}
%% Result slides
\begin{frame}{Performance Expectations}{Element Integration}
\begin{center}\LARGE
  FEM Integration, at the element level,\\
  is also limited by \red{memory bandwidth},\\
  rather than by peak \blue{flop rate}.
\end{center}
\bigskip
\begin{itemize}
  \item We expect bandwidth ratio speedup (3x--6x for most systems)
  \medskip
  \item Input for FEM is a vector of coefficients (auxiliary fields)
  \medskip
  \item Output is a vector of coefficients for the residual
\end{itemize}
\end{frame}
%
\begin{frame}{2D $P_1$ Laplacian Performance}
\begin{center}
\begin{tabular}{c}
  \includegraphics[width=0.8\textwidth]{/PETSc3/papers/gpu/gpu-fem-quadrature-paper/ex52_lap2d_block_gtx580.png} \\
  \Large Reaches \red{100} GF/s by 100K elements
\end{tabular}
\end{center}
\end{frame}
\begin{frame}{2D $P_1$ Laplacian Performance}
\begin{center}
\begin{tabular}{c}
  \includegraphics[width=0.8\textwidth]{/PETSc3/papers/gpu/gpu-fem-quadrature-paper/ex52_time.png} \\
  \Large Linear scaling for both GPU and CPU integration
\end{tabular}
\end{center}
\end{frame}
\begin{frame}{2D $P_1$ Laplacian Performance}{Configuring PETSc}
\$PETSC\_DIR/configure
\begin{itemize}
  \item<1->[] --download-triangle --download-chaco
  \item<2->[] --download-scientificpython --download-fiat --download-generator
  \item<3->[] --with-cuda
  \item<4>[]  --with-cudac='nvcc -m64' --with-cuda-arch=sm\_10
  \item<5>[]  --with-cusp-dir=/PETSc3/multicore/cusp
  \item<5>[]  --with-thrust-dir=/PETSc3/multicore/thrust
  \item<6>[]  --with-cuda-only
  \item<7>[] --with-precision=single
\end{itemize}
\end{frame}
\begin{frame}{2D $P_1$ Laplacian Performance}{Running the example}
\$PETSC\_DIR/src/benchmarks/benchmarkExample.py
\begin{itemize}
  \item[] -{}-daemon -{}-num 52 DMComplex
  \item[] -{}-events IntegBatchCPU IntegBatchGPU IntegGPUOnly
  \item[] -{}-refine 0.0625 0.00625 0.000625 0.0000625 0.00003125 0.000015625 0.0000078125 0.00000390625
  \item[] -{}-order=1 -{}-blockExp 4
  \item[] CPU='dm\_view show\_residual=0 compute\_function batch'
  \item[] GPU='dm\_view show\_residual=0 compute\_function batch gpu gpu\_batches=8'
\end{itemize}
\end{frame}
%
\begin{frame}{2D $P_1$ Rate-of-Strain Performance}
\begin{center}
\begin{tabular}{c}
  \includegraphics[width=0.8\textwidth]{figures/FEM/GPU/block_2D_P1_Elasticity} \\
  \Large Reaches \red{100} GF/s by 100K elements
\end{tabular}
\end{center}
\end{frame}
\begin{frame}{2D $P_1$ Rate-of-Strain Performance}{Running the example}
\$PETSC\_DIR/src/benchmarks/benchmarkExample.py
\begin{itemize}
  \item[] -{}-daemon -{}-num 52 DMComplex
  \item[] -{}-events IntegBatchCPU IntegBatchGPU IntegGPUOnly
  \item[] -{}-refine 0.0625 0.00625 0.000625 0.0000625 0.00003125 0.000015625 0.0000078125 0.00000390625
  \item[] -{}-operator=elasticity -{}-order=1 -{}-blockExp 4
  \item[] CPU='dm\_view op\_type=elasticity show\_residual=0 compute\_function batch'
  \item[] GPU='dm\_view op\_type=elasticity show\_residual=0 compute\_function batch gpu gpu\_batches=8'
\end{itemize}
\end{frame}
%
\begin{frame}{PETSc Multiphysics}

Each block of the Jacobian is evaluated separately:
\begin{itemize}
  \item Reuse single-field code
  \medskip
  \item Vectorize over cells, rather than fields
  \medskip
  \item Retain sparsity of the Jacobian
\end{itemize}
\pause
\medskip
Solver integration is seamless:
\begin{itemize}
  \item Nested Block preconditioners from the command line
  \medskip
  \item Segregated KKT MG smoothers from the command line
  \medskip
  \item Fully composable with AMG, LU, Schur complement, etc.
\end{itemize}
\pause
\bigskip
\begin{center}
  \Large PETSc \magenta{\href{http://www.mcs.anl.gov/petsc/petsc-dev/src/snes/examples/tutorials/ex62.c.html}{ex62}} solves the Stokes
  problem,\break and \magenta{\href{http://www.mcs.anl.gov/petsc/petsc-dev/src/snes/examples/tutorials/ex31.c.html}{ex31}} adds temperature
\end{center}
\end{frame}
%
\begin{frame}{Competing Models}
\begin{center}\Huge
  How should kernels be integrated into libraries?
\end{center}

\medskip

%% Here compare C++/TBB with Generated/CUDA: Errors, Optimization, High-level reasoning (symmetry, zeros)
\begin{columns}
\begin{column}[T]{0.5\textwidth}
\magenta{\href{http://mathema.tician.de/software/pycuda}{CUDA}}+\magenta{\href{http://www.mcs.anl.gov/petsc/petsc-dev/src/snes/examples/tutorials/ex62.c.html}{Code Generation}}
\begin{itemize}
  \item<2-> Explicit vectorization
  \item<3-> Can inspect/optimize code
  \item<3-> Errors easily localized
  \item<4-> Can use high-level reasoning for optimization (\href{https://launchpad.net/ferari}{FErari})
  \item<5-> Kernel fusion is \blue{easy}
\end{itemize}
\end{column}
%
\begin{column}[T]{0.5\textwidth}
\magenta{\href{http://threadingbuildingblocks.org/}{TBB}}+\magenta{\href{http://www.cplusplus.com/reference/stl/}{C++ Templates}}
\begin{itemize}
  \item<2-> Implicit vectorization
  \item<3-> Generated code is hidden
  \item<3-> Notoriously difficult debugging
  \item<4-> Low-level compiler-type optimization
  \item<5-> Kernel fusion is \red{really hard}
\end{itemize}
\end{column}
\end{columns}
\end{frame}

\end{document}
