\documentclass[dvipsnames]{beamer}

\input{tex/talkPreamble.tex}
\usepackage{pgfpages}
%\setbeameroption{show notes on second screen=right}
%\setbeameroption{show only notes}
\usepackage{listings}

\AtBeginSection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[currentsection]
  \end{frame}
}

\AtBeginSubsection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[sectionstyle=show/hide,subsectionstyle=show/shaded/hide]
  \end{frame}
}

\title[FEM-GPU]{Finite Element Integration using CUDA and OpenCL}
\author[M.~Knepley]{Matthew~Knepley, Karl Rupp, Andy Terrel}
\date[GPU-SMP '13]{GPU-SMP 2013\\Changchun, China \quad July 28--Aug 2, 2013\vskip-0.15in}
% - Use the \inst command if there are several affiliations
\institute[UC]{
  Computation Institute\\
  University of Chicago

  \smallskip

  Department of Molecular Biology and Physiology\\
  Rush University Medical Center
}
\subject{PETSc}

\begin{document}

\makeatletter
 \def\beamer@framenotesbegin{% at beginning of slide
   \gdef\beamer@noteitems{}%
   \gdef\beamer@notes{{}}% used to be totally empty.
 }
 \makeatother

\lstset{language=C,basicstyle=\ttfamily\scriptsize,stringstyle=\ttfamily\color{green},commentstyle=\color{brown},morekeywords={MPI_Comm,SNES,KSP,PC,DM,Mat,Vec,IS,PetscSection,PetscInt,PetscScalar,PetscBool,PetscErrorCode},emph={PETSC_COMM_WORLD,PETSC_NULL,SNES_NGMRES_RESTART_PERIODIC},emphstyle=\bf}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.3]{figures/logos/rush_logo.png}\hspace{1.0in}
  \includegraphics[scale=0.24]{figures/logos/uc-logo-official.jpg}
  \end{center}
  \vskip0.4in
\end{frame}
%
\begin{frame}<testing>{Abstract}
Title: Finite Element Integration using CUDA and OpenCL

Abstract:
We present a high performance finite element integration routine for low order elements
in both two and three dimensions, and simplicial and hexahedral cells. We detail the
conversion from CUDA to OpenCL and analyze the performance on a variety of architectures,
including Nvidia GPUs, ATI GPUs, the Intel MIC, and a representative CPU.

\end{frame}

\begin{frame}{Collaborators}
\begin{center}
\begin{tabular}{lc}
  \raisebox{0.6in}{\parbox[c]{14em}{\LARGE \magenta{\href{http://viennacl.sourceforge.net/}{ViennaCL}} Creator\\ANL}} &
  \vbox{
    \hbox{\includegraphics[width=1.2in,height=1.2in]{figures/Collaborators/KarlRupp.jpg}}
    \smallskip
    \hbox{Karl Rupp}
  } \\
  \raisebox{0.6in}{\parbox[c]{14em}{\LARGE \magenta{\href{https://conference.scipy.org/scipy2013/}{SciPy 2013}} Chair\\TACC}} &
  \vbox{
    \hbox{\includegraphics[width=1.2in,height=1.2in]{figures/PETSc/AndyTerrel.jpg}}
    \smallskip
    \hbox{Andy Terrel}
  }
\end{tabular}
\end{center}
\end{frame}

\section*{Introduction}
\begin{frame}{Research Products}
\Large
\begin{itemize}
  \item Efficiently vectorized FEM algorithm

  \item[]<2-3>\quad Traversals are handled by the PETSc library

  \item[]<3>\quad Separates physics from discretization
  \medskip
  \item Open implementation in \magenta{\href{http://www.mcs.anl.gov/petsc}{PETSc}}

  \item[]<4-5>\quad Runs in normal package examples

  \item[]<5>\quad Needed OpenCL, too unstructured for OpenMP
\end{itemize}
\end{frame}

\section{Vectorizing FEM}
%
\begin{frame}{Why is Vectorization Important?}
\Large
For vector length $k$, without vectorization
\begin{center}
we can attain only \red{$\displaystyle\frac{1}{k}$} of peak performance.
\end{center}

\bigskip\pause

For GTX580, $k = 32$
\begin{center}
 so that  unvectorized code runs at \red{3\%} of peak.
\end{center}
\end{frame}
%
\begin{frame}{Why is Vectorization Important?}
\Large
For streaming computations,\\
\quad other factors are less important:
\smallskip
\begin{itemize}
  \item except coalesced (vectorized) loads
  \medskip
  \item little cache reuse
  \medskip
  \item tiling not as important
  \medskip
  \item latency covered by computation
\end{itemize}
\end{frame}
%
\begin{frame}{Why is Vectorization Important?}

Concurrent loads are necessary to saturate the memory bandwidth

\bigskip

\begin{tabular}{lccc}
Architecture             & STREAMS${}^1$ (GB/s) & Peak (GB/s)   & Eff (\%) \\
\hline
NVIDIA GTX 285           &    134         &     159       & 84 \\
NVIDIA GTX 580           &    166         &     192       & 86 \\
AMD HD7970               &    199         &     264       & 75 \\
Dual Intel E5-2670${}^2$ &     80         &     101       & 79 \\
Intel Xeon Phi           &     95         &     220\rlap{${}^3$} & 43 \\
\end{tabular}

\bigskip

${}^1$ Results benefit from autotuning

\medskip

${}^2$ See also \magenta{\href{https://panthema.net/2013/pmbw/Intel-Xeon-E5-2670-64GB/}{https://panthema.net/2013/pmbw/Intel-Xeon-E5-2670-64GB}}

\medskip

${}^3$ This is the ring bus limit, not the processor limit of 320 GB/s
\end{frame}
%
%
\begin{frame}[fragile]{Impediments to Vectorization}{Compiler Complexity}

Compilers cannot vectorize arbitrary code, and users typically do not vectorize

\bigskip

\begin{lstlisting}
for (q = 0; q < N_q; ++q) {
  for (b = 0; b < N_b; ++b) {
    /* Calculate residual for test function res_0 and derivative res_1 */
    b_q  = basis[q*N_b+b];
    db_q = basisDer[q*N_b+b];
    r_b += b_q  * res_0;
    r_b += db_q * res_1;
  }
}
\end{lstlisting}

\bigskip

OpenCL results show large variations, depending on the compiler
\end{frame}
%
\begin{frame}[fragile]{Impediments to Vectorization}{User-specified physics routines}

Vectorization is complicated by hardcoding physics routines

\bigskip

\begin{lstlisting}
for (q = 0; q < N_q; ++q) {
  /* Calculate field and derviative at quadrature point */
  for (b = 0; b < N_b; ++b) {
    b_q  = basis[q*N_b+b];
    db_q = basisDer[q*N_b+b];
    r_b += b_q  * F(u_q, du_q);
    r_b += db_q * G(u_q, du_q);
  }
}
\end{lstlisting}

\bigskip

We avoid hardcoding by adopting a separated model for integration.
\end{frame}
%
\input{slides/FEM/IntegrationModel.tex}
%
\begin{frame}{Impediments to Vectorization}{Code Complexity}

Many levels of blocking are necessary:
\medskip
\begin{itemize}
  \item {\bf Chunk}: Basic tile
  \medskip
  \item {\bf Batch}: Executed in serial % Batch cycle allows overlap of outstanding reads/writes with computation
  \medskip
  \item {\bf Block}: Executed concurrently % Bigger block size means bigger thread block size
\end{itemize}
and are more easily dealt with generically by the library.

\bigskip

We illustrate these sizes in the next section.
\end{frame}
%
\begin{frame}{Impediments to Vectorization}{Memory bandwidth}

\alt<4>{Vectorization over quadrature points increases required bandwidth\\by a factor $N_q$}{Vectorization over basis functions increases required bandwidth\\by a factor $N_b$}

\bigskip

\only<1>{
\begin{figure}
\centering
\tikzstyle{work group} = [draw,green,rounded corners]
\tikzstyle{thread}     = [fill,red]
\begin{tikzpicture}[scale=0.35]
% Draw the basis function evaluation breakdown
\foreach \x in {0, 3, 6, 9, 12, 15}
  \foreach \y in {0, 6}
{
  % draw a cell work unit
  \path[work group] (\x,\y) rectangle +(2,5);
  % draw a 3 thread configuration
 \path[thread] (\x,\y)
             ++(0.5,0.5)
              +(0,0) rectangle +(1,1) +(0.5,0.5) node[anchor=mid,text=white] {$b_2$}
             ++(0.0,1.5)
              +(0,0) rectangle +(1,1) +(0.5,0.5) node[anchor=mid,text=white] {$b_1$}
             ++(0.0,1.5)
              +(0,0) rectangle +(1,1) +(0.5,0.5) node[anchor=mid,text=blue]  {$b_0$};
}
\end{tikzpicture}
\end{figure}
}
\only<2>{
\begin{figure}
\centering
\tikzstyle{work group} = [draw,green,rounded corners]
\tikzstyle{thread}     = [fill,red]
\begin{tikzpicture}[scale=0.35]
% Draw the basis function evaluation breakdown
\foreach \x in {0, 3, 6, 9, 12, 15}
  \foreach \y in {0, 6}
{
  % draw a cell work unit
  \path[work group] (\x,\y) rectangle +(2,5);
  % draw a 3 thread configuration
 \path[thread] (\x,\y)
             ++(0.5,0.5)
              +(0,0) rectangle +(1,1) +(0.5,0.5) node[anchor=mid,text=white] {$b_2$}
             ++(0.0,1.5)
              +(0,0) rectangle +(1,1) +(0.5,0.5) node[anchor=mid,text=blue]  {$b_1$}
             ++(0.0,1.5)
              +(0,0) rectangle +(1,1) +(0.5,0.5) node[anchor=mid,text=white] {$b_0$};
}
\end{tikzpicture}
\end{figure}
}
\only<3>{
\begin{figure}
\centering
\tikzstyle{work group} = [draw,green,rounded corners]
\tikzstyle{thread}     = [fill,red]
\begin{tikzpicture}[scale=0.35]
% Draw the basis function evaluation breakdown
\foreach \x in {0, 3, 6, 9, 12, 15}
  \foreach \y in {0, 6}
{
  % draw a cell work unit
  \path[work group] (\x,\y) rectangle +(2,5);
  % draw a 3 thread configuration
 \path[thread] (\x,\y)
             ++(0.5,0.5)
              +(0,0) rectangle +(1,1) +(0.5,0.5) node[anchor=mid,text=blue]  {$b_2$}
             ++(0.0,1.5)
              +(0,0) rectangle +(1,1) +(0.5,0.5) node[anchor=mid,text=white] {$b_1$}
             ++(0.0,1.5)
              +(0,0) rectangle +(1,1) +(0.5,0.5) node[anchor=mid,text=white] {$b_0$};
}
\end{tikzpicture}
\end{figure}
}
\only<4>{
\begin{figure}
\centering
\tikzstyle{work group} = [draw,green,rounded corners]
\tikzstyle{thread}     = [fill,red]
\begin{tikzpicture}[scale=0.35]
% Draw the basis function evaluation breakdown
\foreach \x in {0, 3, 6, 9, 12, 15}
  \foreach \y in {0, 6}
{
  % draw a cell work unit
  \path[work group] (\x,\y) rectangle +(2,5);
  % draw a 3 thread configuration
 \path[thread] (\x,\y)
             ++(0.5,1.0)
              +(0,0) rectangle +(1,1) +(0.5,0.5) node[anchor=mid,text=white] {$q_1$}
             ++(0.0,2.0)
              +(0,0) rectangle +(1,1) +(0.5,0.5) node[anchor=mid,text=blue]  {$q_0$};
}
\end{tikzpicture}
\end{figure}
}

\end{frame}
%
\begin{frame}{Impediments to Vectorization}{Reductions}
If we vectorize first over quadrature points,
\begin{figure}
\centering
\tikzstyle{work group} = [draw,green,rounded corners]
\tikzstyle{thread}     = [fill,red]
\begin{tikzpicture}[scale=0.35]
% Draw the basis function evaluation breakdown
\foreach \x in {0, 3, 6, 9, 12, 15}
  \foreach \y in {0}
{
  % draw a cell work unit
  \path[work group] (\x,\y) rectangle +(2,5);
  % draw a 3 thread configuration
 \path[thread] (\x,\y)
             ++(0.5,1.0)
              +(0,0) rectangle +(1,1) +(0.5,0.5) node[anchor=mid,text=white] {$q_1$}
             ++(0.0,2.0)
              +(0,0) rectangle +(1,1) +(0.5,0.5) node[anchor=mid,text=blue]  {$q_0$};
}
\end{tikzpicture}
\end{figure}
\pause and then over basis functions
\begin{figure}
\centering
\tikzstyle{work group} = [draw,green,rounded corners]
\tikzstyle{thread}     = [fill,red]
\begin{tikzpicture}[scale=0.35]
% Draw the basis function evaluation breakdown
\foreach \x in {0, 3, 6, 9, 12, 15}
  \foreach \y in {0}
{
  % draw a cell work unit
  \path[work group] (\x,\y) rectangle +(2,5);
  % draw a 3 thread configuration
 \path[thread] (\x,\y)
             ++(0.5,0.5)
              +(0,0) rectangle +(1,1) +(0.5,0.5) node[anchor=mid,text=white] {$b_2$}
             ++(0.0,1.5)
              +(0,0) rectangle +(1,1) +(0.5,0.5) node[anchor=mid,text=white] {$b_1$}
             ++(0.0,1.5)
              +(0,0) rectangle +(1,1) +(0.5,0.5) node[anchor=mid,text=blue]  {$b_0$};
}
\end{tikzpicture}
\end{figure}
\pause for a batch of cells, there must be a \blue{reduction} over quadrature points.
\end{frame}
%
%
\begin{frame}{Thread Transposition}
\begin{figure}
\centering
\tikzstyle{work group} = [draw,green,rounded corners]
\tikzstyle{thread}     = [fill,red]
\tikzstyle{operation}  = [dashed,rounded corners]
\tikzstyle{notation}   = [anchor=south,text width=4cm,text centered]
\begin{tikzpicture}[scale=0.35]
% Draw the basis function evaluation
\draw[operation] (-0.5,-0.5) rectangle +(9,12)
  ++(4.5,12) node[notation] {\scriptsize Map values at quadrature points to coefficients};
% Draw the basis function evaluation breakdown
\foreach \x in {0, 3, 6}
  \foreach \y/\j in {0/1, 6/0}
{
  % draw a cell work unit
  \path[work group] (\x,\y) rectangle +(2,5);
  % draw a 3 thread configuration
 \path[thread] (\x,\y)
             ++(0.5,0.5)
              +(0,0) rectangle +(1,1) +(0.5,0.5) node[anchor=mid,text=white] {\pgfmathtruncatemacro{\t}{\j*3+2}$t_{\t}$}
             ++(0.0,1.5)
              +(0,0) rectangle +(1,1) +(0.5,0.5) node[anchor=mid,text=white] {\pgfmathtruncatemacro{\t}{\j*3+1}$t_{\t}$}
             ++(0.0,1.5)
              +(0,0) rectangle +(1,1) +(0.5,0.5) node[anchor=mid,text=white] {\pgfmathtruncatemacro{\t}{\j*3+0}$t_{\t}$};
}
% Draw arrow for kernel continuation
\draw[<-,ultra thick] (8.5,5.5) -- node[anchor=south] {\scriptsize Continue with kernel} (17.5,5.5);
\draw[dashed] (8.5,11.5) -- (17.5,14.5);
\draw[dashed] (8.5,-0.5) -- (17.5,-3.5);
% Draw the quadrature point evaluation
\draw[operation] (17.5,-3.5) rectangle +(6,18)
  ++(3,18) node[notation] {\scriptsize Evaluate basis and process values at quadrature points};
% Draw the quadrature point evaluation breakdown
\foreach \x in {18, 21}
  \foreach \y/\j in {-3/2, 3/1, 9/0}
{
  % draw a cell work unit
  \path[work group] (\x,\y) rectangle +(2,5);
  % draw a 2 thread configuration
 \path[thread] (\x,\y)
             ++(0.5,1.0)
              +(0,0) rectangle +(1,1) +(0.5,0.5) node[anchor=mid,text=white] {\pgfmathtruncatemacro{\t}{\j*2+1}$t_{\t}$}
             ++(0.0,2.0)
              +(0,0) rectangle +(1,1) +(0.5,0.5) node[anchor=mid,text=white] {\pgfmathtruncatemacro{\t}{\j*2+0}$t_{\t}$};
}
\end{tikzpicture}
%\caption{Action of the residual evaluation kernel on a group of incoming cells. Each cell is displayed as a green,
%  rounded rectangle occupied by the threads which compute the cell information. Each thread computes its values in
%  series, so that thread $t_0$ first computes values at quadrature points for 2 cells, and then computes basis
%  coefficients for 3 cells.}
\label{fig:threadTranspose}
\end{figure}
\end{frame}
%
\begin{frame}
\begin{figure}
\centering
\renewcommand{\arraystretch}{1.5}
\tikzstyle{work group} = [draw,green,rounded corners]
\tikzstyle{thread}     = [red,dashed,rounded corners]
\tikzstyle{batch}      = [blue,rounded corners]
\tikzstyle{seq}        = [brown,thick,decorate,decoration={brace,mirror}]
\tikzstyle{seq2}       = [brown,thick,decorate,decoration={brace}]
\tikzstyle{notation}   = [anchor=south,text width=4cm,text centered]
\begin{tikzpicture}[scale=0.19]
% Draw the basis function evaluation breakdown
\path (-9.0, 17) node[anchor=south,text centered] {\bf Basis Phase};
\foreach \x in {0, 3, 6}
  \foreach \y in {-12, -6, 1, 7}
{
  % draw a cell work unit
  \path[work group] (\x,\y) rectangle +(2,5) +(1, 2.5) node[anchor=mid,text=black] {\tiny $\begin{tabular}{@{}c@{}c@{}} T & T\\T & T\\T & T\end{tabular}$};
}
% Draw the quadrature point evaluation breakdown
\path (35, 17) node[anchor=south,text centered] {\bf Quadrature Phase};
\foreach \x in {18, 21}
  \foreach \y in {-18, -12, -6, 1, 7, 13}
{
  % draw a cell work unit
  \path[work group] (\x,\y) rectangle +(2,5) +(1, 2.5) node[anchor=mid,text=black] {\tiny $\begin{tabular}{@{}c@{}c@{}} T & T\\T & T\end{tabular}$};
}
% Show number of threads
\draw[thread] (2.5,-12.5) rectangle +(3,25)
  ++(1.5,25) node[notation] {\tiny $N_t = 24$};
\draw[thread] (17.5,-18.5) rectangle +(3,37)
  ++(1.5,37) node[notation] {\tiny $N_t = 24$};
% Show number of cells in a batch
\draw[batch] (-1,-13) rectangle +(10,28)
  ++(5,28) node[notation] {\tiny $N_{bc} = 12$};
% Show number of cells in a block
\draw[batch] (17,-19) rectangle +(7,19)
  ++(3.5,0) node[notation,anchor=north] {\tiny $N_{bs} = 6$};
% Show number of sequential basis cells
\draw[seq] (0,-14) -- +(8,0) node[notation,pos=0.5,anchor=north] {\tiny $N_{sbc} = 3$};
% Show number of sequential quadrature cells
\draw[seq2] (18,21) -- +(5,0) node[notation,pos=0.5,anchor=south] {\tiny $N_{sqc} = 2$};
% Show the concurrent blocks
\draw (-2,6.5) -- ++(-5, 0) -- ++(0,-13) node[pos=0.5,anchor=east] {\tiny $N_{bl} = 2$} -- ++(5,0);
\draw (25,9.5) -- ++(5, 0) -- ++(0,-19) node[pos=0.5,anchor=west] {\tiny $N_{bl} = 2$} -- ++(-5,0);
\end{tikzpicture}
%\caption{Diagram of a single cell batch for a computation similar to Fig.~\ref{fig:threadTranspose} so that $N_b = 3$ and
%$N_q = 2$, but now with a vector element $N_{comp} = 2$. We choose $N_bl = 2$ concuurent blocks, so that the batch size
%is $N_{bc} = 12$. Each thread is represented by a $T$, and since we have a thread for each component, $N_t = 24$, with 4
%threads per cell in the quadrature phase, and 6 in the basis phase.}
\label{fig:cellBatch}
\end{figure}
\end{frame}
%
\begin{frame}{Thread Transposition}

\begin{itemize}
  \item Removes reduction
  \medskip
  \item Single pass through memory
  \smallskip
  \begin{itemize}
    \item Operate in unassembled space
    \smallskip
    \item Could do scattered load (better with cache)
    \smallskip
    \item Our cell tiling would aid this
  \end{itemize}
  \medskip
  \item Needs local memory
  \smallskip
  \begin{itemize}
    \item Bounded by $N_b N_q$, good for low order
  \end{itemize}
\end{itemize}
\end{frame}
%
%
\begin{frame}[fragile]{Open Implementation}{Building}

All our runs may be reproduced from the PETSc development branch:
\begin{lstlisting}[language=sh,basicstyle=\footnotesize\tt]
  git clone https://bitbucket.org/petsc/petsc petsc-dev
  cd petsc-dev
  git fetch
  git checkout next
\end{lstlisting}
To run the benchmarks, you configure using
\begin{lstlisting}[language=sh,basicstyle=\footnotesize\tt]
./configure --with-shared-libraries --with-dynamic-loading
  --download-mpich
  --download-scientificpython --download-fiat
  --download-generator
  --download-triangle --download-chaco
\end{lstlisting}
\begin{overprint}
\onslide<2>
and for CUDA you also need
\begin{lstlisting}[language=sh,basicstyle=\footnotesize\tt]
  --with-cudac='nvcc -m64' --with-cuda-only
\end{lstlisting}
\onslide<3>
and for OpenCL you also need
\begin{lstlisting}[language=sh,basicstyle=\footnotesize\tt]
  --with-opencl
\end{lstlisting}
\onslide<4>
and for OpenCL (on Mac) you also need
\begin{lstlisting}[language=sh,basicstyle=\footnotesize\tt]
  --with-opencl-include=/System/Library/Frameworks/
OpenCL.framework/Headers/
  --with-opencl-lib=/System/Library/Frameworks/
OpenCL.framework/OpenCL
\end{lstlisting}
\onslide<5>
To build, use
\begin{lstlisting}[language=sh,basicstyle=\footnotesize\tt]
  make
\end{lstlisting}
\onslide<6>
To build with Python, use
\begin{lstlisting}[language=sh,basicstyle=\footnotesize\tt]
  ./config/builder2.py build
\end{lstlisting}
\end{overprint}
\end{frame}
%
\begin{frame}[fragile]{Open Implementation}{Running}

A representative run for the $P_1$ Laplacian:
\begin{overprint}
\onslide<1>
\begin{lstlisting}[language=sh,basicstyle=\footnotesize\tt]
./src/benchmarks/benchmarkExample.py
  --events IntegBatchCPU IntegBatchGPU IntegGPUOnly
  --num 52 DMComplex
  --refine 0.0625 0.00625 0.000625 0.0000625 0.00003125
           0.000015625 0.0000078125 0.00000390625
  --blockExp 4 --order 1
  CPU='dm_view show_residual=0 compute_function batch'
  GPU='dm_view show_residual=0 compute_function batch gpu
       gpu_batches=8'
\end{lstlisting}
\onslide<2>
which is translated to
\smallskip
\begin{lstlisting}[language=sh,basicstyle=\footnotesize\tt]
./\${PETSC_ARCH}/lib/ex52-obj/ex52
  -refinement_limit 0.0625 -compute_function -batch
  -gpu -gpu_batches 8 -gpu_blocks 16
  -log_summary summary.dat -log_summary_python
  -dm_view -show_residual 0 -preload off
\end{lstlisting}
\end{overprint}
All run parameters are listed in the forthcoming paper.
\end{frame}

\section{Performance}

\begin{frame}{Nvidia GTX285 CUDA}

\begin{figure}
  \centering
  % ./results/gtx285_cuda.py
  \includegraphics[width=4.0in]{/PETSc3/papers/gpu/gpu-fem-quadrature-paper/results/gtx285_cuda.png}
\end{figure}
\end{frame}
%
\begin{frame}{Nvidia GTX285 OpenCL}

\begin{figure}
  \centering
  % ./results/gtx285_opencl.py
  \includegraphics[width=4.0in]{/PETSc3/papers/gpu/gpu-fem-quadrature-paper/results/gtx285_opencl.png}
\end{figure}
\end{frame}
%
\begin{frame}{Nvidia GTX580 CUDA}

\begin{figure}
  \centering
  % ./results/gtx580_cuda.py
  \includegraphics[width=4.0in]{/PETSc3/papers/gpu/gpu-fem-quadrature-paper/results/gtx580_cuda.png}
\end{figure}
\end{frame}
%
\begin{frame}{Nvidia GTX580 OpenCL}

\begin{figure}
  \centering
  % ./results/gtx580_opencl.py
  \includegraphics[width=4.0in]{/PETSc3/papers/gpu/gpu-fem-quadrature-paper/results/gtx580_opencl.png}
\end{figure}
\end{frame}
%
\begin{frame}{Block size variation}{Nvidia GTX580}

\begin{figure}
  \centering
  % ./results/xeon-phi.py
  \includegraphics[width=4.0in]{/PETSc3/papers/gpu/gpu-fem-quadrature-paper/ex52_lap2d_block_gtx580.png}
\end{figure}
\end{frame}
%
\begin{frame}{ATI HD7970}

\begin{figure}
  \centering
  % ./results/hd7970_opencl.py
  \includegraphics[width=4.0in]{/PETSc3/papers/gpu/gpu-fem-quadrature-paper/results/hd7970_flops.png}
\end{figure}
\end{frame}
%
\begin{frame}{Block size variation}{ATI HD7970}

\begin{figure}
  \centering
  % ./results/xeon-phi.py
  \includegraphics[width=4.0in]{/PETSc3/papers/gpu/gpu-fem-quadrature-paper/ex52_lap2d_block_hd7970.png}
\end{figure}
\end{frame}
%
\begin{frame}{Intel Xeon Phi}

\begin{figure}
  \centering
  % ./results/xeon-phi.py
  \includegraphics[width=4.0in]{/PETSc3/papers/gpu/gpu-fem-quadrature-paper/results/xeon-phi.png}
\end{figure}
\end{frame}
%
\begin{frame}{Scaling on the \magenta{\href{http://www.tacc.utexas.edu/user-services/user-guides/longhorn-user-guide}{TACC Longhorn cluster}}}

\begin{figure}
  \centering
  % ./processData.py  --host=longhorn
  \includegraphics[width=4.0in]{/PETSc3/papers/gpu/gpu-fem-quadrature-paper/ex52_lap2d_scal.png}
\end{figure}
Each node uses an Nvidia Quadro FX5800 GPU
\end{frame}

\section*{Conclusions}
\begin{frame}{Conclusions}
\Large
\begin{itemize}
  \item Traversals should be handled by the \blue{library}

  \item[]<2->\quad Allows efficient vectorization

  \item[]<3->\quad Separates physics from discretization

  \item Performance portability requires better compilers

  \item[]<4->\quad Vectorization is somewhat behind

  \item[]<5->\quad MIC programming model is broken
\end{itemize}
\end{frame}
%
\begin{frame}{Competing Models}
\begin{center}\Huge
  How should kernels be integrated into libraries?
\end{center}

\medskip

\begin{columns}
\begin{column}[T]{0.5\textwidth}
\magenta{\href{http://www.mcs.anl.gov/petsc/petsc-dev/src/snes/examples/tutorials/ex52_integrateElement.cu}{CUDA}}/\magenta{\href{http://www.mcs.anl.gov/petsc/petsc-dev/src/snes/examples/tutorials/ex52_integrateElementOpenCL.c}{OpenCL}}
\begin{itemize}
  \item<2-> Explicit vectorization
  \item<3-> Can inspect/optimize code
  \item<3-> Errors easily localized
  \item<4-> Can use high-level reasoning for optimization (\href{https://launchpad.net/ferari}{FErari})
  \item<5-> Kernel fusion is \blue{easy}
\end{itemize}
\end{column}
%
\begin{column}[T]{0.5\textwidth}
\magenta{\href{http://threadingbuildingblocks.org/}{TBB}}+\magenta{\href{http://www.cplusplus.com/reference/stl/}{C++ Templates}}
\begin{itemize}
  \item<2-> Implicit vectorization
  \item<3-> Generated code is hidden
  \item<3-> Notoriously difficult debugging
  \item<4-> Low-level compiler-type optimization
  \item<5-> Kernel fusion is \red{really hard}
\end{itemize}
\end{column}
\end{columns}
\end{frame}

\end{document}
