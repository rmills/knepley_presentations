\documentclass[dvipsnames]{beamer}

\input{tex/talkPreamble.tex}
\usepackage{listings}

\title[Gy\H{o}r]{Scientific Computing from a Library Point-of-View}
\author[M.~Knepley]{Matthew~Knepley}
\date[Gy\H{o}r '12]{Department of Mathematics\\Sz\'echenyi Istv\'an University\\Gy\H{o}r, Hungary \qquad November 20, 2012}
% - Use the \inst command if there are several affiliations
\institute[UC]{
  Computation Institute\\
  University of Chicago

  \smallskip

  Department of Molecular Biology and Physiology\\
  Rush University Medical Center
}
\subject{PETSc}

\begin{document}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.3]{figures/logos/rush_logo.png}\hspace{1.0in}
  \includegraphics[scale=0.24]{figures/logos/uc-logo-official.jpg}
  \end{center}
  \vskip0.4in
\end{frame}
%
\begin{frame}<testing>{abstract}

I will talk about the role that scientific libraries play in allowing computational scientists to both attack problems
with efficient, scalable algorithms, and leverage new computational resources. Good library design, encoding the
relevant mathematical abstractions, allows easy incorporation of multilevel, multiphysics algorithms into existing user
code, as well as massive parallelism and hybrid computing.

\end{frame}
%
\begin{frame}{Impact of Computational Mathematics}\LARGE
The main impact of\\\blue{computational mathematics} is in\\
\bigskip
\pause\Huge
\ design/analysis of algorithms\\
\medskip
\pause
\ for simulation \& data analysis\\
\pause
\bigskip\bigskip\LARGE
This is where CS comes in \ldots
% Code is the transmission mechanism from mathematicians/comp. scientisits to rest of the field
\end{frame}
%
%
\section{Computational Science}
\begin{frame}{Big Idea}\Huge
The best way to create robust,\\
\medskip\pause
efficient and scalable,\\
\medskip\pause
maintainable scientific codes,\\
\pause
\begin{center}
is to use \red{libraries}.
\end{center}
\end{frame}
%
\begin{frame}{Why Libraries?}
\begin{overprint}
\onslide<1-3>
\begin{itemize}\Large
  \item<1-> Hides Hardware Details
  \begin{itemize}
    \item \magenta{\href{http://www.mcs.anl.gov/mpi}{MPI}} does for this for machines and networks
  \end{itemize}
  \bigskip
  \item<2-> Hide Implementation Complexity
  \begin{itemize}
    \item \magenta{\href{http://www.mcs.anl.gov/petsc}{PETSc}} does for this Matrices and Krylov Solvers
  \end{itemize}
  \bigskip
  \item<3-> Accumulates Best Practices
  \begin{itemize}
    \item \magenta{\href{http://www.mcs.anl.gov/petsc}{PETSc}} defaults to classical Gram-Schmidt orthogonalization with
      selective reorthogonalization
  \end{itemize}
\end{itemize}
\onslide<4->
\begin{itemize}\Large
  \item<4-> Improvement without code changes
  \begin{itemize}
    \item \magenta{\href{http://www.mcs.anl.gov/petsc}{PETSc}} time integration library has expanded rapidly, e.g. IMEX
  \end{itemize}
  \bigskip
  \item<5-> Extensiblity
  \begin{itemize}
    \item<6-7> Q: Why is it not just good enough to make a fantastic working code?
    \item<7> A: {\bf Extensibility}\\Users need the ability to change your approach to fit their problem.
    \medskip
    \item<8-> \magenta{\href{http://www.mcs.anl.gov/petsc}{PETSc}} now does \magenta{\href{http://www.mcs.anl.gov/uploads/cels/papers/P2017-0112.pdf}{Multigrid+Block Solvers}}
    \medskip
    \item<9-> \magenta{\href{http://www.mcs.anl.gov/petsc}{PETSc}} now does \magenta{\href{https://bitbucket.org/dalcinl/petiga}{Isogeometric Analysis}}
  \end{itemize}
\end{itemize}
\end{overprint}
\end{frame}
%
\begin{frame}{Early Numerical Libraries}
\begin{itemize}
  \item[71] \magenta{\href{http://www.amazon.com/Handbook-Automatic-Computation-Vol-Mathematischen/dp/0387054146}{Handbook
    for Automatic Computation: Linear Algebra}},\\ \qquad J. H. Wilkinson and C. Reinch
  \begin{itemize}
    \item[73] \magenta{\href{http://www.netlib.org/blas/}{EISPACK}}, Brian Smith et.al.
  \end{itemize}
  \medskip
  \item[79] \magenta{\href{http://www.netlib.org/blas/}{BLAS}}, Lawson, Hanson, Kincaid and Krogh
  \medskip
  \item[90] \magenta{\href{http://www.netlib.org/lapack/}{LAPACK}}, many contributors
  \medskip
  \item[91] \magenta{\href{http://www.mcs.anl.gov/petsc/}{PETSc}}, Gropp and Smith
\end{itemize}
\begin{center}
  All of these packages had their genesis at\\ \magenta{\href{http://www.anl.gov}{Argonne National Laboratory}}/\magenta{\href{http://www.mcs.anl.gov}{MCS}}
\end{center}
\end{frame}
%
\input{slides/GPU/WhyGPU.tex}
%
\subsection{Linear Algebra}
\input{slides/PETSc/GPU/VecCUDA.tex}
\input{slides/PETSc/GPU/MatCUDA.tex}
\input{slides/PETSc/GPU/Solvers.tex}
\input{slides/PETSc/GPU/PFLOTRANExample.tex}
\input{slides/PETSc/GPU/Example.tex}
%
\subsection{FEM Integration}
\input{slides/FEM/InterfaceMaturity.tex}
\input{slides/FEM/IntegrationModel.tex}
\input{slides/PETSc/FEMIntegration.tex}
\begin{frame}{2D $P_1$ Laplacian Performance}
\begin{center}
\begin{tabular}{c}
  \includegraphics[width=0.8\textwidth]{/PETSc3/papers/gpu/gpu-fem-quadrature-paper/ex52_lap2d_block_gtx580.png} \\
  \Large Reaches \red{100} GF/s by 100K elements
\end{tabular}
\end{center}
\end{frame}
\begin{frame}{2D $P_1$ Laplacian Performance}
\begin{center}
\begin{tabular}{c}
  \includegraphics[width=0.8\textwidth]{/PETSc3/papers/gpu/gpu-fem-quadrature-paper/ex52_time.png} \\
  \Large Linear scaling for both GPU and CPU integration
\end{tabular}
\end{center}
\end{frame}
\begin{frame}{2D $P_1$ Laplacian Performance}{Configuring PETSc}
\$PETSC\_DIR/configure
\begin{itemize}
  \item<1->[] --download-triangle --download-chaco
  \item<2->[] --download-scientificpython --download-fiat --download-generator
  \item<3->[] --with-cuda
  \item<4>[]  --with-cudac='nvcc -m64' --with-cuda-arch=sm\_10
  \item<5>[]  --with-cusp-dir=/PETSc3/multicore/cusp
  \item<5>[]  --with-thrust-dir=/PETSc3/multicore/thrust
  \item<6>[]  --with-cuda-only
  \item<7>[] --with-precision=single
\end{itemize}
\end{frame}
\begin{frame}{2D $P_1$ Laplacian Performance}{Running the example}
\$PETSC\_DIR/src/benchmarks/benchmarkExample.py
\begin{itemize}
  \item[] -{}-daemon -{}-num 52 DMComplex
  \item[] -{}-events IntegBatchCPU IntegBatchGPU IntegGPUOnly
  \item[] -{}-refine 0.0625 0.00625 0.000625 0.0000625 0.00003125 0.000015625 0.0000078125 0.00000390625
  \item[] -{}-order=1 -{}-blockExp 4
  \item[] CPU='dm\_view show\_residual=0 compute\_function batch'
  \item[] GPU='dm\_view show\_residual=0 compute\_function batch gpu gpu\_batches=8'
\end{itemize}
\end{frame}
%
\begin{frame}{2D $P_1$ Rate-of-Strain Performance}
\begin{center}
\begin{tabular}{c}
  \includegraphics[width=0.8\textwidth]{figures/FEM/GPU/block_2D_P1_Elasticity} \\
  \Large Reaches \red{100} GF/s by 100K elements
\end{tabular}
\end{center}
\end{frame}
\begin{frame}{2D $P_1$ Rate-of-Strain Performance}{Running the example}
\$PETSC\_DIR/src/benchmarks/benchmarkExample.py
\begin{itemize}
  \item[] -{}-daemon -{}-num 52 DMComplex
  \item[] -{}-events IntegBatchCPU IntegBatchGPU IntegGPUOnly
  \item[] -{}-refine 0.0625 0.00625 0.000625 0.0000625 0.00003125 0.000015625 0.0000078125 0.00000390625
  \item[] -{}-operator=elasticity -{}-order=1 -{}-blockExp 4
  \item[] CPU='dm\_view op\_type=elasticity show\_residual=0 compute\_function batch'
  \item[] GPU='dm\_view op\_type=elasticity show\_residual=0 compute\_function batch gpu gpu\_batches=8'
\end{itemize}
\end{frame}
%
\begin{frame}{General Strategy}
\begin{itemize}\Large
  \item Vectorize
  \medskip
  \item Overdecompose
  \medskip
  \item Cover memory latency with computation
  \begin{itemize}
    \item Multiple cycles of writes in the kernel
  \end{itemize}
  \smallskip
  \item User must \blue{relinquish control of the layout}
\end{itemize}
\bigskip
\magenta{\href{http://arxiv.org/abs/1103.0066}{Finite Element Integration on GPUs}}, ACM TOMS,\\
\quad Andy Terrel and Matthew Knepley.\\
{\bf Finite Element Integration with Quadrature on the GPU}, to SISC,\\
\quad Robert Kirby, Matthew Knepley, Andreas Kl\"ockner, and Andy Terrel.
\end{frame}
%
%
\section{Mathematics}
\begin{frame}[fragile]{Composable System for Scalable Preconditioners}{Stokes and KKT}

The saddle-point matrix is a canonical form for handling constraints:
\begin{itemize}
  \item Incompressibility
  \item Contact
  \item Multi-constituent phase-field models
  \item Optimal control
  \item PDE constrained optimization
\end{itemize}

\smallskip

\includegraphics[width=0.4\textwidth]{./figures/Magma/ridgePlot.pdf}

\begin{textblock}{0.4}[0,1](0.35,0.92)
  \includegraphics[width=8cm]{./figures/Magma/ridgePorosity.jpg}\\
  {\tiny \qquad\qquad Courtesy R.~F. Katz}
\end{textblock}
\begin{textblock}{0.2}[1,0](0.9,0.3)
  {\tiny Courtesy M. Spiegelman}
  \includegraphics[width=3cm]{./figures/Magma/porousChannels.jpg}
\end{textblock}

% SAY: we are actively working on all these problems in PETSc.
\end{frame}
%
\input{slides/MultiField/ComposableOverview.tex}
\input{slides/MultiField/ComposableApproach.tex}
%
%
%%\input{slides/PETSc/OrganizingPrinciple.tex}
%   Reusable pieces allow you to create algorithms not present in the package itself (Ex: Elman PCs, SIMPLE)
%   Add SIMPLE to Stokes Tour
\input{slides/MultiField/StokesOptionsTour.tex}
\input{slides/PETSc/ProgrammingWithOptions.tex}
%
\begin{frame}{Composability}
\Large \blue{Composable} interfaces allow the nested, hierarchical interaction of different components,
\pause
\begin{itemize}
  \item {\bf analysis} (discretization)
  \medskip
  \item {\bf topology} (mesh)
  \medskip
  \item {\bf algebra}  (solver)
\end{itemize}
\pause
so that \red{non-experts} can produce powerful simulations with modern algorithms.
\medskip
\pause
\begin{center}
  \magenta{\href{http://59a2.org/research/}{Jed Brown}} discusses this interplay\\
  in the context of \magenta{\href{}{multilevel solvers}}
\end{center}
\end{frame}
%
%
\section*{Conclusions}
\begin{frame}{Main Points}

\Large
\begin{itemize}
  \item<2-> Libraries encapsulate the Mathematics
  \begin{itemize}
    \item \large Users will give up more Control
  \end{itemize}

  \bigskip

  \item<3-> Multiphysics demands Composable Solvers
  \begin{itemize}
    \item \large Each piece will have to be Optimal
  \end{itemize}
\end{itemize}

\bigskip

\begin{center}
\visible<4>{\emph{Change alone is unchanging\\--- Heraclitus, 544--483 \sc{BC}}}
\end{center}
%
\end{frame}

\end{document}
