\documentclass[dvipsnames]{beamer}

\input{tex/talkPreamble.tex}
\usetikzlibrary{thplex,trees}
\beamertemplatenavigationsymbolsempty

\title[MGK]{The Role of Computer Science in\\Computational Science}
\author[M.~Knepley]{Matthew~Knepley}
\date[Buffalo]{Computer Science Colloquium\\University at Buffalo\\Buffalo, NY \quad April 13, 2017}
% - Use the \inst command if there are several affiliations
\institute[Rice]{
  Computational and Applied Mathematics\\
  Rice University
}
\subject{Me}

\begin{document}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.12]{figures/logos/anl-white-background-modern.jpg}\hspace{1.0in}
  \includegraphics[scale=0.10]{figures/logos/RiceLogo_TMCMYK300DPI.jpg}
  \end{center}
  \vskip0.4in
\end{frame}

\begin{frame}<testing>{Opening Statement}
\end{frame}
%
\begin{frame}<testing>{Abstract}
Title: The Role of Computer Science in Computational Science

Abstract:
Computational Science is the study of algorithms, appropriate for current computing hardware, used to solve scientific
and engineering problems. The computational scientist may focus on algorithm development, improving the accuracy,
efficiency, scalability, or robustness of a given strategy. However, it is equally important to consider the software
implementation, and its extensibility or generalizability, maintainability, and performance. I will speak about the
PETSc library, a community effort that I help lead, which provides scalable parallel linear and nonlinear algebraic
solvers. It is very often used to solve complex, multiphysics problems arising from PDEs, and I will show examples from
biophysics, geophysics, and fluid dynamics. In addition, I will discuss the need for lasting software artifacts as the
basis for computational science.

Solvation mechanics is concerned with the interaction of solute molecules, such as the biomolecules essential for life,
with solvent molecules, water and sometimes ions. These interactions are primarily electrostatic. Mathematical models of
molecular solvation are crucial to the understanding of the physiological function and control of proteins, affecting
the affinity and specificity with which biomolecules bind. The action of solvent molecules in the thin solvation layer
around solute molecules is a key determinant of this behavior. We will present a range of models of varying fidelity for this
process, including thermodynamic measures, compare to existing approaches, and discuss the usefulness of these approaches.
In addition, this illustrates that solvation mechanics, like many computational science problems, is an
interdisciplinary activity requiring mathematical, computational, and software expertise.

The idea is to show this work as an evolution of a model. First, we explore how simple we can make it, then we use the
same techniques (playing with BC) to make it more realistic.

\end{frame}
%
\input{slides/Collaborators/FriendsFields.tex}
%
%% TALK: Show tough problem, this is the point of computational science
\input{slides/TS/TSEx11.tex}
%% TALK: Explain we need topological and geometric complexity, also impacts what analytic methods you can use. We need a
%% robust software infrastructure to enable comparisons (not the same as the reproducibility debate, but related)
%% TALK: I will show three simple but powerful Computer Science principles that can be brough to bear on computational
%% science problems: abstraction (graph theory), orthogonality (GPU implementation), and extensibility (non-conforming meshes)
\section{Scientific Computing}
\subsection{Graph Theory in Simulation}
\input{slides/Plex/ComparisonProblem.tex}
\input{slides/Plex/UnstructuredInterfaceBefore.tex}
\input{slides/Plex/CombinatorialTopology.tex}
\input{slides/Plex/ExampleMeshes.tex}
\input{slides/Plex/HasseInference.tex}
\input{slides/Plex/MeshInterface.tex}
\input{slides/Plex/ExampleOperations.tex}
\input{slides/FEM/IntegrationModel.tex}
\input{slides/FEM/PlexResidual.tex}
%
\subsection{Simulating on Modern Hardware}
%
\input{slides/GPU/KernelConstruction.tex}
\input{slides/GPU/Results/SNESEx12GPU.tex}
\input{slides/GPU/GPUIntegration.tex}
%
\input{slides/AMR/OctreeIntegration.tex}
%
\input{slides/Plex/ComparisonResolution.tex}
%
%
\section{Libraries}
%% SPEAK: Impact
%% SPEAK:   Mathematics is about theorems
%% SPEAK:   Science is about observation and experiments, or demonstrate experimental technique at another lab
%% SPEAK:   Computationalists build usable software
% PETSc slides from Job Talk
\input{slides/PETSc/MattInPETSc.tex}
\begin{frame}
  \input{figures/PETSc/PETScCitations2017.tex}
\end{frame}
%
\begin{frame}{Impact of Computer Science on Science}

\begin{textblock}{0.5}[0.5,0.5](0.5,0.13)\tikz\fill[,decorate,decoration={text along path, text=|\bf\LARGE|Computational Science}] (0,0) arc (250:330:8cm);\end{textblock}
\begin{center}\includegraphics[width=0.8\textwidth]{figures/ScientificComputing/ManhattanBridge.jpg}\end{center}

\note{Computational Science is the Bridge between mathematics and science}

\Large Computational Leaders have always\\\quad embraced the latest technology\\\qquad and been inspired by physical problems,
\begin{overprint}
\onslide<2>
\begin{center}
\begin{tabular}{cc}
\includegraphics[height=0.2\textwidth]{figures/Scientists/LeonhardEuler_2.jpg} & \includegraphics[height=0.2\textwidth]{figures/Hardware/SlideRuleExample.png}
\end{tabular}
\end{center}
\onslide<3>
\begin{center}
\begin{tabular}{cc}
\includegraphics[height=0.2\textwidth]{figures/Hardware/ORNL_Titan.jpg} & \raisebox{2em}{\Huge\red{\bf{PETS}c}}
\end{tabular}
\end{center}
\onslide<4>
\bigskip
\begin{center}
{\Huge Enabling Scientific Discovery}
\end{center}
\end{overprint}
\note[item]{The new technology is massive parallelism and numerical libraries}

\note[item]{Using the Latest Tools to support the Applied Math Mission}

 \note[item]{Tradition is theory/analysis and now computation}
\end{frame}


%% SPEAK: They attacked physical and organizational problems, creating the necessary mathematics, and using the latest technology
% Slide 1: What I find endlessly fascinating about Applied Mathematics is its incorporation of parts of physics into
% mathematics itself, and not _just_ in the application directions. We choose algorithms based upon what we can
% efficiently calculate.
% Slide 2: Logarithms were developed by Napier in the 17th century, but did not come to full flower until Euler began to
% use them and connected them to the exponential function. As we all know, Euler was the consummate applied
% mathematician, and in fact, the first mathematical prize Euler won was for the optimal placement of masts on a ship
% (2nd place).
% Slide 3: Although most people are familiar with the contributions of Wiener and von Neumann, few people today seem to
% recall the contributions of Kantorovich. Not only did he pioneer approximate methods of functional analysis and linear
% programming, but he was one of the first mathematical users of automatic computing. Continuing the applied mathematics
% tradition in St. Petersburg (Leningrad) he developed early parallel programming techniques to compensate for the speed
% of his machines (finished tabulation of Bessel function more quickly the Americans using the MARC and EINIAC). He
% famously reported on ``Functional Analysis and Computational Mathematics'' with S.L. Sobolev and L.A. Lyusternik at the
% Third All-Union Mathematical Congress in 1956, something that sounds modern even today, and also, of course, was
% awarded the 1975 Nobel Prize in Economics.
% Slide 4: Today the landscape of computing is again rapidly changing. The largest computers have billions of concurrent
% processing elements, and the software libraries are capable of encoding incredibly complex systems. I am exhilarated
% by the prospects in applied mathematics and algorithm development, and the potential for...
% Slide 5: Enabling scientific discovery.
%\input{slides/ScientificComputing/Bridge.tex}

\end{document}
