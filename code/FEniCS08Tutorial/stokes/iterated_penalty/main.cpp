// Copyright (C) 2006-2007 Andy R Terrel.
// Licensed under the GNU LGPL Version 2.1.
//
// First added:  2008-03-10
// Last changed: 
//
// This demo solves the Stokes equations, using the iterated
// penalty method of Scott, Vogelius (1986). The sub domains
// for the different boundary conditions used in this
// simulation are computed by the demo program in
// src/demo/mesh/subdomains.

#include <dolfin.h>
#include "IPStokes.h"
#include "L2div.h"

#define MAX_ITER 2
#define DIV_TOL  1e-07

using namespace dolfin;

int main()
{
  // Function for no-slip boundary condition for velocity
  class Noslip : public Function
  {
  public:

    Noslip(Mesh& mesh) : Function(mesh) {}

    void eval(real* values, const real* x) const
    {
      values[0] = 0.0;
      values[1] = 0.0;
    }

  };

  // Function for inflow boundary condition for velocity
  class Inflow : public Function
  {
  public:

    Inflow(Mesh& mesh) : Function(mesh) {}

    void eval(real* values, const real* x) const
    {
      values[0] = -1.0;
      values[1] = 0.0;
    }

  };

  // Read mesh and sub domain markers
  Mesh mesh("dolfin-2.xml.gz");
  MeshFunction<unsigned int> sub_domains(mesh, "subdomains.xml");

  // Create functions for boundary conditions
  Noslip noslip(mesh);
  Inflow inflow(mesh);
  Function zero(mesh, 0.0);

  // Define sub systems for boundary conditions
  SubSystem velocity(0);
  SubSystem pressure(1);

  // No-slip boundary condition for velocity
  DirichletBC bc0(noslip, sub_domains, 0, velocity);

  // Inflow boundary condition for velocity
  DirichletBC bc1(inflow, sub_domains, 1, velocity);

  // Boundary condition for pressure at outflow
  DirichletBC bc2(zero, sub_domains, 2, pressure);

  // Collect boundary conditions
  Array <BoundaryCondition*> bcs(&bc0, &bc1, &bc2);

  // Set up Initial data
  real penalty = 1.0e5, div_u_error;
  Function f(mesh, 0.0);
  Function rho(mesh, penalty);
  Function u;

  IPStokesBilinearForm a(rho);
  Vector wvec;
  Function w(mesh, wvec, a);

  for(int j = 0; j<MAX_ITER; j++) {
    IPStokesLinearForm L(f,w);
    LinearPDE pde(a, L, mesh, bcs);
    pde.solve(u);

    // Update the w
    Vector u_vec = u.vector();
    wvec.vec() += penalty * u_vec.vec();
    
    L2divFunctional div_u(u);
    div_u_error = sqrt(fabs(assemble(div_u, mesh)));
    cout << "Divergence error: " << div_u_error << endl;
    if (div_u_error < DIV_TOL) {
      break;
    }
  }

  // Plotting not available for HO elements
  plot(u);

  // Save solution
  //File ufile("velocity.xml");
  //ufile << u;

}
