from ffc import *

# Reserved variables for forms
(a, L, M) = (None, None, None)

# Reserved variable for element
element = None

# Copyright (c) 2005-2007 Andy R Terrel
# Licensed under the GNU LGPL Version 2.1
#
# First added:  2008-03-10
# Last changed: 
#
# The bilinear form a(v, u) and Linear form L(v) for the Stokes
# equations using a Scott-Vogelius iterated penalty formulation.
#
# Compile this form with FFC: ffc -l dolfin IPStokes.form

shape = "triangle"
Element = VectorElement("Lagrange", shape, 4)

u = TrialFunction(Element)
v = TestFunction(Element)
f = Function(Element)
w = Function(Element)
c = Constant(shape)

a = (dot(grad(v), grad(u)) - c * div(u) * (div(v)))*dx 
L = dot(v, f) * dx +  dot(div(v),div(w))*dx 

compile([a, L, M, element], "IPStokes", "tensor", "dolfin", {'quadrature_points=': False, 'blas': False, 'precision=': '15', 'optimize': False})
