from ffc import *

# Reserved variables for forms
(a, L, M) = (None, None, None)

# Reserved variable for element
element = None

# Copyright (c) 2005-2007 Andy R Terrel
# Licensed under the GNU LGPL Version 2.1
#
# First added:  2008-03-10
# Last changed: 
#
# The bilinear form a(v, u) and Linear form L(v) for the Stokes
# equations using a Scott-Vogelius iterated penalty formulation.
#
# Compile this form with FFC: ffc -l dolfin IPStokes.form

Element = VectorElement("Lagrange", "triangle", 4)
u = Function(Element)

M = div(u)*div(u)*dx

compile([a, L, M, element], "L2div", "tensor", "dolfin", {'quadrature_points=': False, 'blas': False, 'precision=': '15', 'optimize': False})
