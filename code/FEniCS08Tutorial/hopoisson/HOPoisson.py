from ffc import *

# Reserved variables for forms
(a, L, M) = (None, None, None)

# Reserved variable for element
element = None

# Copyright (C) 2005-2007 Anders Logg, Andy R Terrel.
# Licensed under the GNU LGPL Version 2.1.
#
# First added:  2008
#
# The bilinear form a(v, U) and linear form L(v) for
# Poisson's equation.
#
# Compile this form with FFC: ffc -l dolfin Poisson.form.

element = FiniteElement("Lagrange", "triangle", 4)

v = TestFunction(element)
u = TrialFunction(element)
f = Function(element)
g = Function(element)

a = dot(grad(v), grad(u))*dx
L = v*f*dx + v*g*ds

compile([a, L, M, element], "HOPoisson", "tensor", "dolfin", {'quadrature_points=': False, 'blas': False, 'precision=': '15', 'optimize': False})
