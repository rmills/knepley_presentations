\documentclass[dvipsnames]{beamer}

\input{tex/talkPreamble.tex}

\title[Nested Solvers]{Nested and Hierarchical Solvers in PETSc}
\author[M.~Knepley]{Matthew~Knepley {\bf$\in$} PETSc Team}
\date[CS\&E]{SIAM Computational Science \& Engineering\\Boston, MA \quad February 27, 2013}
% - Use the \inst command if there are several affiliations
\institute[UC]{
  Computation Institute\\
  University of Chicago

  \smallskip

  Department of Molecular Biology and Physiology\\
  Rush University Medical Center
}
\subject{PETSc}

\begin{document}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.3]{figures/logos/rush_logo.png}\hspace{1.0in}
  \includegraphics[scale=0.24]{figures/logos/uc-logo-official.jpg}
  \end{center}
  \vskip0.4in
\end{frame}
%
\begin{frame}<testing>{abstract}

Title: Constructing and Configuring Nested and Hierarchical Solvers in PETSc

We look at code architecture for constructing and also configuring deeply
nested solver hierarchies. We are most concerned with
\begin{itemize}
  \item how much code a user has to write
  \item how frequently code has to change
  \item how an implementation chooses among options
\end{itemize}
and the impact tradeoffs here have on the interface design. Our examples
will be drawn from multiphysics PDE simulation, and exhibit block solvers,
algebraic and geometric multigrid, and composition of both linear and
nonlinear solvers. However, this is not an overview of packages capabilities,
but rather a discussion about implementation strategy and user experience.

\end{frame}
%
\input{slides/PETSc/PETScDevelopers.tex}
\begin{frame}{The PETSc Team}

\begin{center}
\begin{tabular}{ccc}
  \vbox{
    \hbox to 0.8in{\bf\Large\hfil IP8 \hfil}
    \bigskip
    \hbox to 0.8in{THURS}
    \smallskip
    \hbox to 0.8in{1:00--1:45}
    \smallskip
    \hbox{Bill Gropp}
  } &
  \vbox{
    \hbox{\includegraphics[width=0.8in,height=0.8in]{figures/PETSc/Barry.jpg}}
    \smallskip
    \hbox{Barry Smith}
  } &
  \vbox{
    \hbox{\includegraphics[width=0.8in,height=0.8in]{figures/PETSc/Satish.jpg}}
    \smallskip
    \hbox{Satish Balay}
  } \\
  \vbox{
    \hbox to 0.8in{\bf\Large\hfil MS186 \hfil}
    \bigskip
    \hbox to 0.8in{THURS}
    \smallskip
    \hbox to 0.8in{10:00--10:25}
    \smallskip
    \hbox{Jed Brown}
  } &
  \vbox{
    \hbox{\includegraphics[width=0.8in,height=0.8in]{figures/PETSc/MatthewKnepley364x364.jpg}}
    \smallskip
    \hbox{Matt Knepley}
  } &
  \vbox{
    \hbox{\includegraphics[width=0.8in,height=0.8in]{figures/PETSc/Lisandro.jpg}}
    \smallskip
    \hbox{Lisandro Dalcin}
  } \\
  \vbox{
    \hbox{\includegraphics[width=0.8in,height=0.80in]{figures/PETSc/Hong.jpg}}
    \smallskip
    \hbox{Hong Zhang}
  } &
  \vbox{
    \hbox to 0.8in{\bf\Large\hfil MS87 \hfil}
    \bigskip
    \hbox to 0.8in{TUES}
    \smallskip
    \hbox to 0.8in{3:00--3:25}
    \smallskip
    \hbox{Mark Adams}
  } &
  \vbox{
    \hbox to 0.8in{\bf\Large\hfil MS255 \hfil}
    \bigskip
    \hbox to 0.8in{FRI}
    \smallskip
    \hbox to 0.8in{2:30--2:55}
    \smallskip
    \hbox{Peter Brune}
  }
\end{tabular}
\end{center}
\end{frame}
%
\section{Linear Examples}
%
%
\input{slides/MultiField/FieldSplitOptions.tex}
%   FS inside MG
\input{slides/PETSc/ProgrammingWithOptions.tex}
\input{slides/PETSc/UserSolve.tex}
%
\section{Nonlinear Examples}
\input{slides/SNES/NPC.tex}
\input{slides/SNES/NPCUseCases.tex}
%
%
\section{Design}
\begin{frame}{Why Can We Do This?}\Large

\magenta{\href{http://www.mcs.anl.gov/petsc/petsc-dev/docs/manualpages/DM/DM.html}{\class{DM}}} object handles
\begin{itemize}
  \item discretization, and
  \item assembly
\end{itemize}
\bigskip
\magenta{\href{http://www.mcs.anl.gov/petsc/petsc-dev/docs/manualpages/DM/DM.html}{\class{DM}}} talks to solver through simple algebraic interface
\begin{itemize}
  \item \magenta{\href{http://www.mcs.anl.gov/petsc/petsc-dev/docs/manualpages/Vec/IS.html}{\class{IS}}}, list of integers

  \item Map between local and global spaces

  \item Maps between total and field spaces
\end{itemize}
\end{frame}
%
\begin{frame}{DM Interface to Solver}{Layout}
\begin{itemize}
  \item<1-> \magenta{\href{http://www.mcs.anl.gov/petsc/petsc-dev/docs/manualpages/DM/DMGetLocalVector.html}{\function{DMGetLocalVector()}}}
  \item<2-> \magenta{\href{http://www.mcs.anl.gov/petsc/petsc-dev/docs/manualpages/DM/DMGetGlobalVector.html}{\function{DMGetGlobalVector()}}}
  \item<3-> \magenta{\href{http://www.mcs.anl.gov/petsc/petsc-dev/docs/manualpages/DM/DMLocalToGlobalBegin.html}{\function{DMLocalToGlobalBegin()}}}, \magenta{\href{http://www.mcs.anl.gov/petsc/petsc-dev/docs/manualpages/DM/DMLocalToGlobalEnd.html}{\function{DMLocalToGlobalEnd()}}}
  \item<4-> \magenta{\href{http://www.mcs.anl.gov/petsc/petsc-dev/docs/manualpages/DM/DMGlobalToLocalBegin.html}{\function{DMGlobalToLocalBegin()}}}, \magenta{\href{http://www.mcs.anl.gov/petsc/petsc-dev/docs/manualpages/DM/DMGlobalToLocalEnd.html}{\function{DMGlobalToLocalEnd()}}}
\end{itemize}
\bigskip
\begin{center}\LARGE
\begin{tabular}{ccc}
  $S_L$ & \only<1-2>{\phantom{$\longrightarrow$}} \only<3>{$\longrightarrow$} \only<4>{$\longleftrightarrow$} & \visible<2->{$S$}
\end{tabular}
\end{center}
\end{frame}
%
\begin{frame}[fragile]{DM Interface to Solver}{Field division}
\begin{itemize}
  \item<1-> \magenta{\href{http://www.mcs.anl.gov/petsc/petsc-dev/docs/manualpages/DM/DMCreateFieldDecomposition.html}{\function{DMCreateFieldDecomposition()}}}
  \item<2-> \magenta{\href{http://www.mcs.anl.gov/petsc/petsc-dev/docs/manualpages/DM/DMCreateSubDM.html}{\function{DMCreateSubDM()}}}
  \item<3-> \magenta{\href{http://www.mcs.anl.gov/petsc/petsc-dev/docs/manualpages/PC/PCFieldSplitSetIS.html}{\function{PCFieldSplitSetIS()}}}
\end{itemize}
\bigskip
\begin{overprint}\LARGE
\onslide<1>
\begin{center}
  Provides IS to Solver\\
\begin{tabular}{clc}
      &            & $S_{u}$ \\
      & $\nearrow$ &  \\
  $S$ & $\longrightarrow$ & $S_{v}$ \\
      & $\searrow$ &  \\
      &  & $S_{p}$
\end{tabular}
\end{center}
\onslide<2>
\begin{center}
  Enables grouping of fields\\
\begin{tabular}{clc}
      &  & \phantom{$S_{u}$} \\
      & \phantom{$\nearrow$} &  \\
  $S$ & $\longrightarrow$ & $S_{u,v}$ \\
      & $\searrow$ &  \\
      &  & $S_{p}$
\end{tabular}
\end{center}
\onslide<3>
\begin{center}
  Need not use a DM {\Large (\verb|-pc_fieldsplit_detect_saddle_point|)}
\end{center}
\end{overprint}
\end{frame}
%
\begin{frame}{DM Interface to Solver}{Multigrid}
\begin{itemize}
  \item<1-> \magenta{\href{http://www.mcs.anl.gov/petsc/petsc-dev/docs/manualpages/DM/DMRefine.html}{\function{DMRefine()}}}, \magenta{\href{http://www.mcs.anl.gov/petsc/petsc-dev/docs/manualpages/DM/DMCoarsen.html}{\function{DMCoarsen()}}}
  \item<2-> \magenta{\href{http://www.mcs.anl.gov/petsc/petsc-dev/docs/manualpages/DM/DMCreateInterpolation.html}{\function{DMCreateInterpolation()}}}
  \item<3-> \magenta{\href{http://www.mcs.anl.gov/petsc/petsc-dev/docs/manualpages/DM/DMCreateInjection.html}{\function{DMCreateInjection()}}}
\end{itemize}
\bigskip
\begin{overprint}\LARGE
\onslide<1>
\begin{center}
  Manages field map between levels\\
\begin{tabular}{clc}
  $S^H_{u}$ & $\longleftrightarrow$ & $S^h_{u}$ \\
  $S^H_{v}$ & $\longleftrightarrow$ & $S^h_{v}$ \\
  $S^H_{p}$ & $\longleftrightarrow$ & $S^h_{p}$
\end{tabular}
\end{center}
\onslide<2>
\begin{center}
  Creates algebraic maps between spaces\\
\begin{tabular}{clc}
  $S^H$ & $\longleftrightarrow$ & $S^h$
\end{tabular}
\end{center}
\onslide<3>
\begin{center}
  Optimization for residual interpolation
\end{center}
\end{overprint}
\end{frame}
%
%% Jed's crazy slide
%
\begin{frame}{What Is Missing?}\LARGE
\begin{itemize}
  \item NASM for unstructured grids (\magenta{\href{http://www.mcs.anl.gov/petsc/petsc-dev/docs/manualpages/DM/DMPlex.html}{\class{DMPlex}}})
  \bigskip
  \item Nonlinear FieldSplit
  \bigskip
  \item Combination of AMG-GMG
  \bigskip
  \item Interface to modeling language (\magenta{\href{http://fenicsproject.org/documentation/ufl/dev/ufl.html}{UFL}})
\end{itemize}
\end{frame}

\end{document}

Start with lots of examples slides:
  Show both Stokes PCs and also Stokes under MG, so need ex43.c
  Is there enough time to run unstructured Stokes with regular refinement and MG? Probably not :)


