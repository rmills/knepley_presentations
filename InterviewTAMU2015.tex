\documentclass[dvipsnames]{beamer}

\input{tex/talkPreamble.tex}
\beamertemplatenavigationsymbolsempty

\title[MGK]{Computational Bioelectrostatics}
\author[M.~Knepley]{Matthew~Knepley}
\date[TAMU]{Numerical Analysis Seminar\\Texas A\&M University\\College Station, TX \quad January 27, 2015}
% - Use the \inst command if there are several affiliations
\institute[UC]{
  Computation Institute\\
  University of Chicago
}
\subject{Me}

\begin{document}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.12]{figures/logos/anl-white-background-modern.jpg}\hspace{1.0in}
  \includegraphics[scale=0.30]{figures/logos/uc-logo-official.jpg}
  \end{center}
  \vskip0.4in
\end{frame}

\begin{frame}<testing>{Opening Statement}
\end{frame}
%
\begin{frame}<testing>{Abstract}
Title: Computational Bioelectrostatics

Abstract:
Mathematical models of molecular solvation are crucial to the understanding of the physiological function and control of
proteins, affecting the affinity and specificity with which biomolecules bind. Moreover, the solution of computational
science problems is an interdisciplinary activity requiring mathematical, computational, and software expertise. I
illustrate this process by considering the problem of protein solvation modeling from computational
biophysics. Beginning with analysis of the relevant equations, I will explain the development of a new operator
approximation for the boundary integral equations and prove bounds for the approximate solvation energy. By examining
the special case of a spherical solute, we can make a finer grained analysis of the approximation, and are able to
derive a much more accurate approximation. Finally, we demonstrate its excellent performance and scalability.

Bio:
Matthew G. Knepley received his B.S. in Physics from Case Western Reserve University in 1994, an M.S. in Computer
Science from the University of Minnesota in 1996, and a Ph.D. in Computer Science from Purdue University in 2000. He
was a Research Scientist at Akamai Technologies in 2000 and 2001. Afterwards, he joined the Mathematics and Computer
Science department at Argonne National Laboratory (ANL), where he was an Assistant Computational Mathematician, and a
Fellow in the Computation Institute at University of Chicago. In 2009, he joined the Computation Institute as a Senior
Research Associate. His research focuses on scientific computation, including fast methods, parallel computing, software
development, numerical analysis, and multi/manycore architectures. He is an author of the widely used PETSc library for
scientific computing from ANL, and is a principal designer of the PyLith library for the solution of dynamic and
quasi-static tectonic deformation problems. He developed the PETSc scalable unstructured mesh support based upon ideas
from combinatorial topology. He was a J. T. Oden Faculty Research Fellow at the Institute for Computation
Engineering and Sciences, UT Austin, in 2008, and won the R\&D 100 Award in 2009, and the SIAM/ACM Prize in
Computational Science and Engineering in 2015 as part of the PETSc team.

\end{frame}
%
\begin{frame}{Computational Science \& Applied Mathematics}\LARGE
% Bring rigor to things that work
Begins with the numerics of BIEs and PDEs,\\                \note<1->{In particular, operator approximation or preconditioning\\}
\medskip
\quad and mathematics of the computation, \visible<2->{is}\\             \note<1->{Here I mean analysis of computation structure for efficiency, scalability, generality\\\bigskip}
\visible<2->{\bigskip
Distilled into\\
\quad high quality numerical libraries, }\visible<2->{and} \\               \note<2->{Still the finest form of communication about computation\\\bigskip}
\visible<3->{\bigskip
Culminates in scientific discovery.}\\                         \note<3->{I am passionate about scientific applications}
\note<1->{Thank you for the introduction/coming. I am grateful for the invitation to speak, and I am excited to share with
you my idea of what constitutes the discipline of Computational Science.\\}
\note<2->{Holistic, not only to acknowledge that the \textit{area} incorporates elements from many discplines, but understand that
to be fully solved, a computational science problem needs to be worked on with many different tools.\\
My grandfather was a carpenter\\He lived in the house he built\\Although it might not have been his primary job, he
dealt with plumbing, dry wall, electrical, roofing, and anything else that was necessary to make the house run smoothly.}
\end{frame}
%
\begin{frame}{Research Areas}\Large
\begin{itemize}
  \item Mathematics
  \smallskip
  \begin{itemize}
    \item Scalable solution of Nonlinear PDE
    \smallskip
    \item Discretization on unstructured meshes
    \smallskip
    \item Massively parallel algorithms
    \smallskip
    \item Fast methods for integral equations
  \end{itemize}
  \bigskip
  \item Applications
  \smallskip
  \begin{itemize}
    \item Bioelectrostatics
    \smallskip
    \item Crustal and Magma Dynamics
    \smallskip
    \item Wave Mechanics
    \smallskip
    \item Fracture Mechanics
  \end{itemize}
\end{itemize}
\end{frame}
%
\newcommand\ganttline[4]{% line, tag, start end
   \node at (0,#1/2+.1) [anchor=base east,] {#2};
   \fill[blue] (#3/\xtick-2000/\xtick,#1/2-.1) rectangle (#4/\xtick-2000/\xtick,#1/2+.1);}
\newcommand\ganttlabel[6]{% year, label, color, yloc, anchor
  \node[#3] at (#1/\xtick+#6/\xtick-2000/\xtick,#4) [anchor=#5] {#2};
  \fill[#3] (#1/\xtick-2000/\xtick,1/2-.1) rectangle (#1/\xtick-2000/\xtick+0.04,7/2+.1);}
%
\begin{frame}
\def\present{2015.2}
\def\xtick{3.5}

{\LARGE\bf Funding}
\setlength{\tabcolsep}{15pt}
\begin{tabular}{cccc}
\includegraphics[width=0.15\textwidth]{figures/logos/logo_DOE} &
\includegraphics[width=0.15\textwidth]{figures/logos/anl-white-background-modern} &
\includegraphics[width=0.15\textwidth]{figures/logos/logo_NSF} &
\raisebox{1.5em}{\includegraphics[width=0.15\textwidth]{figures/logos/logo_ARO}}
\end{tabular}

\smallskip

{\LARGE\bf Community Involvement}
\smallskip
\begin{center}
\begin{tikzpicture}[y=-1cm,scale=1.4]
  \ganttlabel{2000}{2000}{red}{3.7}{north}{0}
  \ganttlabel{2004}{2004}{red}{3.7}{north}{0}
  \ganttlabel{2008}{2008}{red}{3.7}{north}{0}
  \ganttlabel{2012}{2012}{red}{3.7}{north}{0}
  \ganttlabel{2016}{2016}{red}{3.7}{north}{0}
  \ganttline{1}{PETSc}{2001}{\present}
  \ganttline{2}{NSF CIG Rep}{2004}{\present}
  \ganttline{3}{NSF CIG EC}{2010}{2013}
  \ganttline{4}{Rush Medical Center}{2006}{2014}
  \ganttline{5}{Simula Research, NO}{2007}{2011}
  \ganttline{6}{Sz\'echenyi Istv\'an, HU}{2010}{2012}
  \ganttline{7}{GUCAS, CN}{2009}{2010}
\end{tikzpicture}
\end{center}
\end{frame}
%
\begin{frame}{What is PETSc?}
\begin{center}\LARGE PETSc is one of the most popular software libraries in scientific computing.\end{center}
\medskip
\Large I have been a principal architect since 2001,\\
and developed
\large
\begin{itemize}
  \item unstructured meshes (model, algorithms, implementation),
  \medskip
  \item nonlinear preconditioning (model, algorithms),
  \medskip
  \item FEM discretizations (data structures, solvers optimization),
  \medskip
  \item optimizations for multicore and GPU architectures.
\end{itemize}
\end{frame}
%
\input{slides/PETSc/WhatIsPETSc.tex}
\begin{frame}{PETSc}
\input{figures/PETSc/PETScCitations2015.tex}
\end{frame}
%
%
% Read over Columbia Talk

% Hendrik Lenstra: "A math talk without a proof is like a movie without a love scene"

% Show proofs from Biology

  % DONE Proof of energy bounds for diagonal preconditioner for A

  % Proof of BC deformation equivalence to BIBEE
    % Show Kirkwood solution for sphere
    % Theorem is the new BC is equiv and include series solutions in the statement
      % Proof of equiv is on p.3 of Helsing
    % Show that approximations can be seen as deformed BC (this relates PDE and BIE)
    % Graphic that shows it was more accurate

  % NOPE Proof of convergence for Picard for asymmetric solvation
    % Can relate PDE and BIE again through BC

% MAYBE Show short DFT slide

% Show short version of mesh slides, highlight best accomplishments (new paper)

  % Can rephrase mesh operations in graph theoretic terms (show slide about too precise and too general)

  % Can treat function queries with the same infrastructure and topological queries (dual)

  % Can write a generic residual evaluation loop

  % include the ``people using my mesh'' part

% Show short PyLith slides

  % ?Proof of convergence for fault PC in some case

% NOPE Proof of convergence rate for RASM in RBF problem. See paper

% Show engineering challenges for continuum mechanics

\section{Bioelectrostatics}

%% INTRO Approximating Boundary Integrals
\input{slides/Bioelectrostatics/Lysozyme.tex}
\input{slides/Bioelectrostatics/ContinuumModel.tex}
\input{slides/Bioelectrostatics/SecondKindModel.tex}
%
\begin{frame}{Problem}\Large

Boundary element discretizations of the solvation problem:
\bigskip
\begin{itemize}
  \item can be expensive to solve
  \bigskip
  \item are more accurate than required by intermediate design iterations
\end{itemize}
\end{frame}
%
%
\section{Approximate Operators}
\input{slides/Bioelectrostatics/ReactionPotentialDefinition.tex}
\input{slides/Bioelectrostatics/BIBEEIdea.tex}
\input{slides/Bioelectrostatics/BIBEEBoundsStatement.tex}
\input{slides/Bioelectrostatics/BIBEEBoundsProof.tex}
\input{slides/Bioelectrostatics/BIBEEAccuracy.tex}
\input{slides/Bioelectrostatics/GeneralizedBorn.tex}
\input{slides/Bioelectrostatics/CrowdedSolution.tex}
\input{slides/Bioelectrostatics/BIBEEScalability.tex}
%
%
\section{Approximate Boundary Conditions}
\input{slides/Bioelectrostatics/ContinuumModel.tex}
\input{slides/Bioelectrostatics/KirkwoodSolution.tex}
\input{slides/Bioelectrostatics/BIBEEBCStatement.tex}
\input{slides/Bioelectrostatics/BIBEEBCProof.tex}
\input{slides/Bioelectrostatics/BIBEEBCSeries.tex}
\input{slides/Bioelectrostatics/BIBEEBCAsymptotics.tex}
\input{slides/Bioelectrostatics/BIBEEInterpolated.tex}
\input{slides/Bioelectrostatics/BasisAugmentation.tex}
%
\begin{frame}{Resolution}\Large

\note[item]{Between 10 and 100x faster}
Boundary element discretizations of the solvation problem:
\bigskip
\begin{itemize}
  \item can be expensive to solve
  \smallskip
  \begin{itemize}
    \item<2-> {\scriptsize \magenta{\bf\href{http://jcp.aip.org/resource/1/jcpsa6/v130/i10/p104108_s1}{Bounding the
          electrostatic free energies associated with linear continuum models of molecular solvation}}, Bardhan,
      Knepley, Anitescu, JCP, 2009}
  \end{itemize}
  \bigskip
  \item are more accurate than required by intermediate design iterations
  \smallskip
  \begin{itemize}
    \item<3-> {\scriptsize
      \magenta{\bf\href{http://www.degruyter.com/view/j/mlbmb.2012.1.issue/mlbmb-2013-0007/mlbmb-2013-0007.xml?format=INT}{Analysis
          of fast boundary-integral approximations for modeling electrostatic contributions of molecular binding}}, Kreienkamp,
      et al., Molecular-Based Mathematical Biology, 2013}
  \end{itemize}
\end{itemize}
\end{frame}
%
%
\section{Future Directions}
%
\begin{frame}{More Physics}\LARGE
{\bf New Phenomena:}\\
\quad \visible<2->{Dielectric Saturation} \\
\quad \visible<4->{Charge--Hydration Asymmetry} \\
\quad \visible<6->{Solute--Solvent Interface Potential} \\

\bigskip
\bigskip

{\bf New Model:}\\
\quad \visible<3->{Nonlocal Dielectric} \\
\quad \visible<5->{Nonlinear Boundary Condition} \\
\quad \visible<7->{Static Solvation Potential}
\end{frame}
%   They attacked physical and organizational problems,
%      creating the necessary mathematics
%      and using the latest technology
% Slide 1: What I find endlessly fascinating about Applied Mathematics is its incorporation of parts of physics into
% mathematics itself, and not _just_ in the application directions. We choose algorithms based upon what we can
% efficiently calculate.
% Slide 2: Logarithms were developed by Napier in the 17th century, but did not come to full flower until Euler began to
% use them and connected them to the exponential function. AS we all know, Euler was the consummate applied
% mathematician, and nn fact, the first mathematical prize Euler won was for the optimal placement of masts on a ship
% (2nd place).
% Slide 3: Although most people are familiar with the contributions of Wiener and von Neumann, few people today seem to
% recall the contributions of Kantorovich. Not only did he pioneer approximate methods of functional analysis and linear
% programming, but he was one of the first mathematical users of automatic computing. Continuing the applied mathematics
% tradition in St. Petersburg (Leningrad) he developed early parallel programming techniques to compensate for the speed
% of his machines (finished tabulation of Bessel function more quickly the Americans using the MARC and EINIAC). He
% famously reported on with “Functional Analysis and Computational Mathematics” S.L. Sobolev and L.A. Lyusternik at the
% Third All-Union Mathematical Congress in 1956, something that sounds modern even today, and also, of course, was
% awared the 1975 Nobel Prize in Economics.
% Slide 4: Today the landscape of computing is again rapidly changing. The largest computers have billions of concurrent
% processing elements, and the software libraries are capable of encoding incredibly complex systems. I am exhilarated
% by the prospects in applied mathematics and algorithm development, and the potential for...
% Slide 5: Enabling scientific discovery.
\input{slides/ScientificComputing/Bridge.tex}
%
\begin{frame}[plain]

\begin{center}
\Huge\bf Thank You!
\end{center}

\bigskip

\begin{center}
\LARGE \magenta{\href{http://www.cs.uchicago.edu/~knepley}{http://www.cs.uchicago.edu/\textasciitilde knepley}}
\end{center}
\end{frame}

\end{document}
