\begin{frame}{Reorder for Locality}

    Exploits ``nearby'' operations to aggregate computation
\begin{itemize}
  \item Can be \textit{temporal} or \textit{spatial}

  \bigskip

  \item Usually exploits a \blue{cache}

  \bigskip

  \item Difficult to predict/model on a modern processor
\end{itemize}
\end{frame}
%
\begin{frame}{Reorder for Locality}{GPU Differences}

    We have to manage our ``cache'' \red{explicitly}
\begin{itemize}
  \item The NVIDIA 1060C shared memory is only 16K for 32 threads

  \bigskip

  \item We must also manange ``main memory'' explicitly
  \begin{itemize}
    \item Need to move data to/from GPU
  \end{itemize}

  \bigskip

  \item Must be aware of limited precision when reordering

  \bigskip

  \item Can be readily modeled

  \bigskip

  \item Need tools for automatic data movement (marshalling)
\end{itemize}
\end{frame}
%
\begin{frame}{Reorder for Locality}{Example}

    \blue{Data-Aware} Work Queue % talked about with Felipe Cruz/Simon ???/Rio Yakota
\begin{itemize}
  \item A work queue manages many small tasks
  \begin{itemize}
    \item Dependencies are tracked with a DAG

    \item Queue should manage a single computational phase (supertask)
  \end{itemize}

  \bigskip

  \item Nodes also manage an input and output data segment
  \begin{itemize}
    \item Specific classes can have known sizes

    \item Can hold main memory locations for segments
  \end{itemize}

  \bigskip

  \item Framework manages marshalling:
  \begin{itemize}
    \item Allocates contiguous data segments

    \item Calculates segment offsets for tasks

    \item Marshalls (moves) data

    \item Passes offsets to supertask execution
  \end{itemize}
 \end{itemize}
\end{frame}
