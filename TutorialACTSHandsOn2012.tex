\documentclass{beamer}

\input{tex/talkPreamble.tex}

\title[PETSc]{The\\{\bf P}ortable {\bf E}xtensible {\bf T}oolkit for {\bf S}cientific {\bf C}omputing}
\author[M.~Knepley]{Matthew~Knepley}
\date[ACTS '12]{PETSc Hands-On\\13th Workshop on the DOE ACTS Collection\\LBNL, Berkeley, CA \qquad August 14--17, 2012}
% - Use the \inst command if there are several affiliations
\institute[UC]{
\begin{tabular}{cc}
  Mathematics and Computer Science Division & Computation Institute\\
  Argonne National Laboratory               & University of Chicago
\end{tabular}
}

\subject{PETSc}

% If you wish to uncover everything in a step-wise fashion, uncomment the following command: 
%%\beamerdefaultoverlayspecification{<+->}

\begin{document}
%
\frame{
\titlepage
\begin{center}
\includegraphics[scale=0.3]{figures/logos/doe-logo.jpg}\hspace{1.0in}
\includegraphics[scale=0.3]{figures/logos/anl-logo-black.jpg}\hspace{1.0in}
\includegraphics[scale=0.24]{figures/logos/uc-logo-official.jpg}
\end{center}
}
%
%
\begin{frame}{Main Point}

\Huge
We want to enable users to,\\
\bigskip
\pause
assess solver performance,\\
\bigskip
\pause
and optimize solvers\\
\qquad for particular problems.
\end{frame}
%
%
\section{Controlling the Solver}
%
\begin{frame}[fragile]{Controlling PETSc}
\begin{center}\LARGE
  All of PETSc can be controlled by \blue{options}
\end{center}
\begin{itemize}
  \item[] \verb|-ksp_type gmres|
  \item[] \verb|-start_in_debugger|
  \item[] All objects can have a prefix, \verb|-velocity_pc_type jacobi|
\end{itemize}
\pause
\medskip
\begin{center}\LARGE
  All PETSc options can be namespaced
\end{center}
\begin{itemize}
  \item[] \verb|-sub_ksp_type bicg|
  \item[] \verb|-fieldsplit_1_pc_type jacobi|
\end{itemize}
\end{frame}
%
\begin{frame}[fragile]{Examples}

{\LARGE We will illustrate options using}
\bigskip
\begin{center}
  PETSc SNES \magenta{\href{http://www.mcs.anl.gov/petsc/petsc-current/src/snes/examples/tutorials/ex5.c.html}{ex5}}, located at \verb|$PETSC_DIR/src/snes/examples/tutorials/ex5.c|
\end{center}
\bigskip
{\LARGE and}
\bigskip
\begin{center}
  PETSc SNES \magenta{\href{http://www.mcs.anl.gov/petsc/petsc-current/src/snes/examples/tutorials/ex62.c.html}{ex62}}, located at \verb|$PETSC_DIR/src/snes/examples/tutorials/ex62.c|
\end{center}
\end{frame}
%
%
\section{Where do I begin?}
%
\begin{frame}{Nonlinear Systems}
\begin{center}\Huge
I am not going to discuss\\
nonlinear systems today,
\end{center}
\pause
\medskip
\begin{center}\Huge
however if Newton is failing,\\
\medskip
contact \magenta{\href{mailto:petsc-maint@mcs.anl.gov}{petsc-maint@mcs.anl.gov}}
\end{center}
\end{frame}
%
\begin{frame}{What is a Krylov solver?}\Large
A Krylov solver builds a small model of a linear operator $A$, using a subspace defined by
\begin{equation*}
  \mathcal{K}(A,r) = \mathrm{span}\{r, Ar, A^2r, A^3r, \ldots \}
\end{equation*}
where $r$ is the initial residual.
\bigskip

\pause
The small system is sovled directly, and the solution is projected back to the original space.
\end{frame}
%
\begin{frame}{What Should I Know About Krylov Solvers?}
\LARGE
\begin{itemize}
  \item They can handle \textit{low-mode} errors
  \bigskip
  \item They need preconditioners
  \bigskip
  \item They do a lot of inner products
\end{itemize}
\end{frame}
%
\begin{frame}{What is a Preconditioner?}\Large
A preconditioner $M$ changes a linear system,
\begin{equation*}
  M^{-1} A x = M^{-1} b
\end{equation*}
so that the effective operator is $M^{-1} A$, which is hopefully \red{better} for Krylov methods.
\bigskip
\begin{itemize}
  \item<2-> Preconditioner should be inexpensive
  \medskip
  \item<3-> Preconditioner should accelerate convergence
\end{itemize}
\end{frame}
%
\begin{frame}[fragile]{{\bf Always} start with LU}
Always, always start with LU:
\begin{itemize}
  \item No iterative tolerance
  \medskip
  \item (Almost) no condition number dependence
  \medskip
  \item Check for accidental singularity
\end{itemize}
\pause
\bigskip
In parallel, you need a 3rd party package
\begin{itemize}
  \item MUMPS (\verb|--download-mumps|)
  \medskip
  \item SuperLU (\verb|--download-superlu_dist|)
\end{itemize}
\end{frame}
%%     What is LU fails? Use full Schur complement
\begin{frame}[fragile]{What if LU fails?}\Large
LU will fail for
\begin{itemize}
  \item Singular problems
  \medskip
  \item Saddle-point problems
\end{itemize}
\pause
\smallskip
For saddles use \magenta{\href{http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/PC/PCFIELDSPLIT.html}{\texttt{PC\_FIELDSPLIT}}}
\begin{itemize}
  \item Separately solves each field
  \medskip
  \item Decomposition is automatic in PyLith
  \medskip
  \item Autodetect with {\small \verb|-pc_fieldsplit_detect_saddle_point|}
  \medskip
  \item Exact with full Schur complement solve
\end{itemize}
\end{frame}
%
%
\section{How do I improve?}
\subsection{Look at what you have}
%
\begin{frame}[fragile]{What solver did you use?}
Use \verb|-snes_view| or \verb|-ksp_view| to output a description of the solver:
\small
\begin{verbatim}
KSP Object:        (fieldsplit_0_)         1 MPI processes
  type: fgmres
    GMRES: restart=100, using Classical (unmodified) Gram-
      Schmidt Orthogonalization with no iterative refinement
    GMRES: happy breakdown tolerance 1e-30
  maximum iterations=1, initial guess is zero
  tolerances:  relative=1e-09, absolute=1e-50,
    divergence=10000
  right preconditioning
  has attached null space
  using UNPRECONDITIONED norm type for convergence test
\end{verbatim}
\end{frame}
%
\begin{frame}[fragile]{What did the convergence look like?}
Use \verb|-snes_monitor| and \verb|-ksp_monitor|, or \verb|-log_summary|:
\small
\begin{overprint}
\onslide<2>
\smallskip
\begin{verbatim}
  0 SNES Function norm 0.207564 
  1 SNES Function norm 0.0148968 
  2 SNES Function norm 0.000113968 
  3 SNES Function norm 6.9256e-09 
  4 SNES Function norm < 1.e-11
\end{verbatim}
\onslide<3>
\smallskip
\begin{verbatim}
  0 KSP Residual norm 1.61409 
      Residual norms for mg_levels_1_ solve.
      0 KSP Residual norm 0.213376 
      1 KSP Residual norm 0.0192085 
    Residual norms for mg_levels_2_ solve.
    0 KSP Residual norm 0.223226 
    1 KSP Residual norm 0.0219992 
      Residual norms for mg_levels_1_ solve.
      0 KSP Residual norm 0.0248252 
      1 KSP Residual norm 0.0153432 
    Residual norms for mg_levels_2_ solve.
    0 KSP Residual norm 0.0124024 
    1 KSP Residual norm 0.0018736 
  1 KSP Residual norm 0.02282 
\end{verbatim}
\onslide<4>
\smallskip
\begin{verbatim}
Event       Count      Time (sec)     Flops      Total
           Max Ratio  Max     Ratio   Max  Ratio Mflop/s
KSPSetUp      12 1.0 3.6259e-03 1.0 0.00e+00 0.0     0
KSPSolve       3 1.0 4.8937e-01 1.0 8.93e+05 1.0     2
SNESSolve      1 1.0 4.9477e-01 1.0 9.22e+05 1.0     2
\end{verbatim}
\end{overprint}
\end{frame}
%
\begin{frame}[fragile]{Look at timing}
Use \verb|-log_summary|:
\small
\smallskip
\begin{verbatim}
Event        Time (sec)     Flops     --- Global ---  Total
           Max     Ratio   Max  Ratio %T %f %M %L %R  MF/s
VecMDot   1.8904e-03 1.0 2.49e+04 1.0  0  3  0  0  0    13
MatMult   2.1026e-03 1.0 2.65e+05 1.0  0 29  0  0  0   126
PCApply   4.6001e-01 1.0 7.78e+05 1.0 58 84  0  0 64     2
KSPSetUp  3.6259e-03 1.0 0.00e+00 0.0  0  0  0  0  4     0
KSPSolve  4.8937e-01 1.0 8.93e+05 1.0 61 97  0  0 90     2
SNESSolve 4.9477e-01 1.0 9.22e+05 1.0 62100  0  0 92     2
\end{verbatim}
\pause
Use \verb|-log_summary_python| to get this information as a Python module
\end{frame}
%
\subsection{Back off in steps}
%
\begin{frame}[fragile]{Weaken the KSP}

GMRES\quad $\Longrightarrow$\quad BiCGStab
\begin{itemize}
  \item \verb|-ksp_type bcgs|
  \medskip
  \item Less storage
  \medskip
  \item Fewer dot products (less work)
  \medskip
  \item Variants \verb|-ksp_type bcgsl| and \verb|-ksp_type ibcgs|
\end{itemize}
\bigskip
\begin{center}
  Complete \magenta{\href{http://www.mcs.anl.gov/petsc/documentation/linearsolvertable.html}{Table}} of Solvers and Preconditioners
\end{center}
\end{frame}
%
\begin{frame}[fragile]{Weaken the PC}

LU\quad $\Longrightarrow$\quad ILU
\begin{itemize}
  \item \verb|-pc_type ilu|
  \medskip
  \item Less storage and work
\end{itemize}
\pause
\smallskip
In parallel,
\begin{itemize}
  \item Hypre \verb|-pc_type hypre -pc_hypre_type euclid|
  \medskip
  \item Block-Jacobi \verb|-pc_type bjacobi -sub_pc_type ilu|
  \medskip
  \item Additive Schwarz \verb|-pc_type asm -sub_pc_type ilu|
\end{itemize}
\pause
\bigskip
\begin{center}
  Default for MG smoother is Chebychev/SOR(2)
\end{center}
\end{frame}
%
\begin{frame}[fragile]{Algebraic Multigrid (AMG)}
\begin{itemize}
  \item Can solve elliptic problems
  \begin{itemize}
    \item Laplace, elasticity, Stokes
  \end{itemize}
  \medskip
  \item Works for unstructured meshes
  \medskip
  \item \verb|-pc_type gamg|,  \verb|-pc_type ml|, \verb|-pc_type hypre -pc_hypre_type boomeramg|
  \medskip
  \item \red{CRUCIAL} to have an accurate near-null space
  \begin{itemize}
    \item \magenta{\href{http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatSetNearNullSpace.html}{MatSetNearNullSpace()}}
    \item PyLith provides this automatically
  \end{itemize}
  \medskip
  \item Use \verb|-pc_mg_log| to put timing in its own log stage
\end{itemize}
\end{frame}
%
\begin{frame}[fragile]{PC\_FieldSplit}
\begin{itemize}
  \item {\Large Separate solves for block operators}
  \begin{itemize}
    \item Physical insight for subsystems
    \smallskip
    \item Have optimial PCs for simpler equations
    \smallskip
    \item Suboptions \verb|fs_fieldsplit_0_*|
  \end{itemize}
  \bigskip
  \item {\Large Flexibly combine subsolves}
  \begin{itemize}
    \item Jacobi: \verb|fs_pc_fieldsplit_type = additive|
    \smallskip
    \item Gauss-Siedel: \verb|fs_pc_fieldsplit_type = multiplicative|
    \smallskip
    \item Schur complement: \verb|fs_pc_fieldsplit_type = schur|
  \end{itemize}
\end{itemize}
\end{frame}
%
\input{slides/MultiField/StokesOptionsTour.tex}
%
\begin{frame}{Why us FGMRES?}\Large
Flexible GMRES (FGMRES) allows a\\
\blue{different preconditioner} at each step:
\medskip
\begin{itemize}
  \item Takes twice the memory
  \medskip
  \item Needed for iterative PCs
  \medskip
  \item Avoided sometimes with a careful PC choice
\end{itemize}
\end{frame}
%
%
\section{Debugging}
\input{slides/PETSc/CorrectnessDebugging.tex}
\input{slides/PETSc/Debugger.tex}
\input{slides/PETSc/DebuggingTips.tex}
%
%
\section{Examples}
\input{slides/PETSc/Examples/SNESex5.tex}
\input{slides/PETSc/Examples/SNESex62FieldSplit.tex}
\input{slides/PETSc/Examples/SNESex5UserProject.tex}
\end{document}
