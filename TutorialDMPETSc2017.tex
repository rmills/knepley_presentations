% Monday July 18th, 14h00-17h30 Room n^\circ
% Matthew Knepley (Rice University, USA)

% Domain management 60min
%   Structured Grids   30min (RiceOilAndGas2016)
%   Unstructured Grids 30min (ex62 example in ACTS2012)
\documentclass{beamer}

\input{tex/talkPreamble.tex}
\usetikzlibrary{thplex,trees}

\title[PETSc]{The\\{\bf P}ortable {\bf E}xtensible {\bf T}oolkit for {\bf S}cientific {\bf C}omputing}
\author[Matt]{Matthew~Knepley}
\date[PETSc]{DM Tutorial\\PETSc 2017, CU Boulder\\Boulder, CO \qquad June 14, 2017}
% - Use the \inst command if there are several affiliations
\institute[Rice]{
  Computer Science and Engineering\\
  University at Buffalo\\
}
\subject{PETSc}

\begin{document}
\lstset{language=C,basicstyle=\ttfamily\scriptsize,stringstyle=\ttfamily\color{green},commentstyle=\color{brown},morekeywords={MPI_Comm,TS,SNES,KSP,PC,DM,Mat,Vec,VecScatter,IS,PetscSF,PetscSection,PetscObject,PetscInt,PetscScalar,PetscReal,PetscBool,InsertMode,PetscErrorCode},emph={PETSC_COMM_WORLD,PETSC_NULL,SNES_NGMRES_RESTART_PERIODIC},emphstyle=\bf}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.05]{figures/logos/UB_Primary.png}\hspace{0.75in}
  \includegraphics[scale=0.15]{figures/logos/anl-white-background-modern.jpg}
  \end{center}
  \vskip0.4in
\end{frame}
%
\begin{frame}{Main Point}
\Huge
Never believe \textit{anything},\\
\bigskip
\bigskip
\pause
\qquad unless you can run it.
\end{frame}
%
%
\section{DM}
%
\input{slides/DM/Interface.tex}
\input{slides/DM/LocalEvaluation.tex}
%
\subsection{Structured Meshes (DMDA)}
  \input{slides/DA/WhatIsIt.tex}
  \input{slides/DA/LocalEvaluation.tex}
  \input{slides/DA/GhostValues.tex}
  \input{slides/DA/GlobalNumberings.tex}
  \input{slides/DA/LocalNumbering.tex}
  \input{slides/DA/LocalFunction.tex}
  \input{slides/DA/BratuResidual.tex}
  \input{slides/DA/LocalJacobian.tex}
  \input{slides/DA/BratuJacobian.tex}
  \input{slides/DA/Vectors.tex}
  \input{slides/DA/UpdatingGhosts.tex}
  \input{slides/DA/Stencils.tex}
  \input{slides/DA/SetValuesStencil.tex}
  \input{slides/DA/CreatingDA2d.tex}
  \input{slides/DA/Viewing.tex}
  \input{slides/DA/ViewingOperator.tex}
  %\input{slides/DA/ViewingOperatorAction.tex}
  %\input{slides/DA/DebuggingAssembly.tex}
  \input{slides/DA/Function.tex}
%
\subsection{Unstructured Meshes (DMPlex/DMForest)}
  \input{slides/Plex/ComparisonProblem.tex}
  \input{slides/Plex/UnstructuredInterfaceBefore.tex}
  \input{slides/Plex/CombinatorialTopology.tex}
  \input{slides/Plex/ExampleMeshes.tex}
  \input{slides/Plex/MeshInterface.tex}
  \input{slides/Plex/ExampleOperations.tex}
%
\begin{frame}{DMPlex}{What is It?}
\begin{center}\Large
  \class{DMPlex} stands for a DM\\modeling a CW Complex
\end{center}
\begin{itemize}
  \item Handles any kind of mesh
  \begin{itemize}
    \item Simplicial
    \item Hex
    \item Hybrid
    \item Non-manifold
  \end{itemize}
  \medskip
  \item Small interface
  \begin{itemize}
  \item Simple to input a mesh using the API
  \end{itemize}
  \medskip
  \item Accepts mesh generator input
  \begin{itemize}
    \item ExodusII, Triangle, TetGen, LaGriT, Cubit
  \end{itemize}
\end{itemize}
\end{frame}
%
\begin{frame}[fragile]{DMPlex}{How Do I Use It?}
The operations used in SNES ex62 get and set values from a \class{Vec},\\organized by the \class{DM} and \class{PetscSection}
\medskip
\begin{lstlisting}
DMPlexVecGetClosure(
  DM dm, PetscSection section, Vec v, PetscInt point,
  PetscInt *csize, const PetscScalar *values[])
\end{lstlisting}
\begin{itemize}
  \item Element vector on cell
  \item Coordinates on cell vertices
\end{itemize}
\pause
Used in \lstinline!FormFunctionLocal()!,
\begin{lstlisting}
for(c = cStart; c < cEnd; ++c) {
  const PetscScalar *x;

  DMPlexVecGetClosure(dm, PETSC_NULL, X, c, PETSC_NULL, &x);
  for(PetscInt i = 0; i < cellDof; ++i) {
    u[c*cellDof+i] = x[i];
   }
  DMPlexVecRestoreClosure(dm, PETSC_NULL, X, c, PETSC_NULL, &x);
}
\end{lstlisting}
\end{frame}
%
\begin{frame}[fragile]{DMPlex}{How Do I Use It?}
The operations used in SNES ex62 get and set values from a \class{Vec},\\organized by the \class{DM} and \class{PetscSection}
\medskip
\begin{lstlisting}
DMPlexVecSetClosure(
  DM dm, PetscSection section, Vec v, PetscInt point,
  const PetscScalar values[], InsertMode mode)
DMPlexMatSetClosure(
  DM dm, PetscSection section, PetscSection globalSection, Mat A, PetscInt point,
  PetscScalar values[], InsertMode mode)
\end{lstlisting}
\begin{itemize}
  \item Element vector and matrix on cell
\end{itemize}
\smallskip
\pause
Used in \lstinline!FormJacobianLocal()!,
\begin{lstlisting}
for(c = cStart; c < cEnd; ++c) {
  DMPlexMatSetClosure(dm, PETSC_NULL, PETSC_NULL, JacP, c,
                         &elemMat[c*cellDof*cellDof], ADD_VALUES);
}
\end{lstlisting}
\end{frame}
%
\begin{frame}[fragile]{DMPlex}{How Do I Use It?}

{\Large The functions above are built upon}
\begin{lstlisting}
DMPlexGetTransitiveClosure(
  DM dm, PetscInt p, PetscBool useCone,
  PetscInt *numPoints, PetscInt *points[])
\end{lstlisting}
\begin{itemize}
  \item Returns points \textit{and} orientations
  \item Iterate over points to stack up the data in the array
\end{itemize}
\end{frame}
%
\begin{frame}{Adaptive Mesh Refinement}
\begin{itemize}
  \item DM interface with \magenta{\href{http://www.p4est.org/}{p4est}} package from Burstedde and Isaac
  \medskip
  \item PETSc solvers can be used seamlessly
  \medskip
  \item 2015 Gordon Bell Winner for Mantle Convection Simulation
  \medskip
  \item<2> Inversion for basal traction on the full Antarctic ice sheet
\end{itemize}
\begin{overprint}
\onslide<1>
\includegraphics[width=0.9\textwidth]{figures/AMR/earth_3slice.png}\footnote{Isaac}
\onslide<2>
\includegraphics[width=0.95\textwidth]{figures/Ice/anarcticaBasalFriction.png}\footnote{Isaac}
\end{overprint}
\end{frame}
%
%
\begin{frame}{What does a DM do?}
\begin{itemize}
  \item Problem Definition
  \begin{itemize}
    \item Discretization/Dof mapping (\class{PetscSection})
    \item Residual calculation
  \end{itemize}
  \bigskip
  \item Decomposition
  \begin{itemize}
    \item Partitioning, \lstinline!DMCreateSubDM()!
    \item \class{Vec} and \class{Mat} creation
    \item Global $\Longleftrightarrow$ Local mapping
  \end{itemize}
  \bigskip
  \item Hierarchy
  \begin{itemize}
    \item \lstinline!DMCoarsen()! and \lstinline!DMRefine()!
    \item \lstinline!DMInterpolate()! and \lstinline!DMRestrict()!
    \item Hooks for resolution-dependent data
  \end{itemize}
\end{itemize}
\end{frame}
%
\input{slides/PetscSection/WhatIsIt.tex}
\input{slides/PetscSection/WhyUseIt.tex}
\input{slides/PetscSection/SimpleConstruction.tex}
\input{slides/PetscSection/LowLevelInterface.tex}
\input{slides/Plex/StokesDoublet.tex}
\input{slides/PetscSection/SectionHoisting.tex}
%
  \input{slides/FEM/ResidualEvaluation.tex}
  \input{slides/FEM/IntegrationModel.tex}
  % TODO: Switch to lstlsting
  \input{slides/FEM/PlexResidual.tex}
%
%
\section{More Stuff}
\begin{frame}{Things I Will Not Talk About}\LARGE

\begin{itemize}
  \item Communication abstractions
  \begin{itemize}
    \item \cinline{PetscSF} and \cinline{VecScatter}
  \end{itemize}
  \bigskip
  \item Finite elements and finite volumes
  \begin{itemize}
    \item \cinline{PetscFE} and \cinline{PetscFV}
  \end{itemize}
  \bigskip
  \item Non-nested geometric multigrid
  \begin{itemize}
    \item Uses \cinline{DMPlex} and \cinline{Pragmatic}
  \end{itemize}
  \bigskip
  \item Timestepping for second order systems and stiff systems
  \begin{itemize}
    \item \cinline{TSALPHA2} and \cinline{TSBDF}
  \end{itemize}
\end{itemize}
\end{frame}
\end{document}
