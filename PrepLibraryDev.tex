\documentclass[dvipsnames]{beamer}

\input{tex/talkPreamble.tex}
\usepackage{mathtools}       % For \mathllap
\usepackage[boxed]{algorithm}
\usepackage{algpseudocode}   % For \Procedure on algorithm

\title[Lib]{Library Development for Computational Science}
\author[M.~Knepley]{Matthew~Knepley {\bf$\in$} PETSc Team}
\date[Rice]{CAAM 520: Computational Science II\\CAAM, Rice University\\Houston, TX \qquad January 11, 2016}
% - Use the \inst command if there are several affiliations
\institute[Rice]{
  Computational and Applied Mathematics\\
  Rice University
}
\subject{PETSc}

\begin{document}

\lstset{language=C,basicstyle=\ttfamily\scriptsize,stringstyle=\ttfamily\color{green},commentstyle=\color{brown},morekeywords={MPI_Comm,TS,SNES,KSP,PC,DM,Mat,Vec,VecScatter,IS,PetscSF,PetscSection,PetscObject,PetscInt,PetscScalar,PetscReal,PetscBool,InsertMode,PetscErrorCode},emph={PETSC_COMM_WORLD,PETSC_NULL,SNES_NGMRES_RESTART_PERIODIC},emphstyle=\bf}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.12]{figures/logos/anl-white-background-modern.jpg}\hspace{1.0in}
  \includegraphics[scale=0.10]{figures/logos/RiceLogo_TMCMYK300DPI.jpg}
  \end{center}
  \vskip0.4in
\end{frame}

\begin{frame}<testing>{abstract}

Title: Library Development for Computational Science

Abstract:
\end{frame}
%
\input{slides/PETSc/PETScDevelopers.tex}
%
%
\section{Computational Science}
%
\begin{frame}\Huge
High quality numerical libraries\\
\vskip1em
\centering are the best vehicle for\\
\vskip1em
computational communication.
\end{frame}
%
\begin{frame}\Huge
The best way to create robust,\\
\medskip\pause
efficient and scalable,\\
\medskip\pause
maintainable scientific codes,\\
\pause
\begin{center}
is to use \red{libraries}.
\end{center}
\end{frame}
%
\begin{frame}{Why Libraries?}
\begin{overprint}
\onslide<1-3>
\begin{itemize}\Large
  \item<1-> Hide Hardware Details
  \begin{itemize}
    \item \magenta{\href{http://www.mcs.anl.gov/mpi}{MPI}} does for this for machines and networks
  \end{itemize}
  \bigskip
  \item<2-> Hide Implementation Complexity
  \begin{itemize}
    \item \magenta{\href{http://www.mcs.anl.gov/petsc}{PETSc}} does for this Matrices and Krylov Solvers
  \end{itemize}
  \bigskip
  \item<3-> Accumulate Best Practices
  \begin{itemize}
    \item \magenta{\href{http://www.mcs.anl.gov/petsc}{PETSc}} defaults to classical Gram-Schmidt orthogonalization with
      selective reorthogonalization
  \end{itemize}
\end{itemize}
\onslide<4->
\begin{itemize}\Large
  \item<4-> Improvement without code changes
  \begin{itemize}
    \item \magenta{\href{http://www.mcs.anl.gov/petsc}{PETSc}} time integration library has expanded rapidly, e.g. IMEX
  \end{itemize}
  \bigskip
  \item<5-> Extensiblity
  \begin{itemize}
    \item<6-7> Q: Why is it not just good enough to make a fantastic working code?
    \item<7> A: {\bf Extensibility}\\Users need the ability to change your approach to fit their problem.
    \medskip
    \item<8-> \magenta{\href{http://www.mcs.anl.gov/petsc}{PETSc}} now does \magenta{\href{http://www.mcs.anl.gov/uploads/cels/papers/P2017-0112.pdf}{Multigrid+Block Solvers}}
    \medskip
    \item<9-> \magenta{\href{http://www.mcs.anl.gov/petsc}{PETSc}} now does \magenta{\href{https://bitbucket.org/dalcinl/petiga}{Isogeometric Analysis}}
  \end{itemize}
\end{itemize}
\end{overprint}
\end{frame}
%
\begin{frame}{Early Numerical Libraries}
\begin{itemize}
  \item[71] \magenta{\href{http://www.amazon.com/Handbook-Automatic-Computation-Vol-Mathematischen/dp/0387054146}{Handbook
    for Automatic Computation: Linear Algebra}},\\ \qquad J. H. Wilkinson and C. Reinch
  \begin{itemize}
    \item[73] \magenta{\href{http://www.netlib.org/blas/}{EISPACK}}, Brian Smith et.al.
  \end{itemize}
  \medskip
  \item[79] \magenta{\href{http://www.netlib.org/blas/}{BLAS}}, Lawson, Hanson, Kincaid and Krogh
  \medskip
  \item[90] \magenta{\href{http://www.netlib.org/lapack/}{LAPACK}}, many contributors
  \medskip
  \item[91] \magenta{\href{http://www.mcs.anl.gov/petsc/}{PETSc}}, Gropp and Smith
  \medskip
  \item[95] \magenta{\href{http://www.mpich.org/}{MPICH}}, Gropp and Lusk
\end{itemize}
\begin{center}
  All of these packages had their genesis at\\ \magenta{\href{http://www.anl.gov}{Argonne National Laboratory}}/\magenta{\href{http://www.mcs.anl.gov}{MCS}}
\end{center}
\end{frame}
%
\begin{frame}{Why are Top Level Types Important?}

\begin{overprint}
\onslide<1>
Having few important types
\begin{itemize}
  \item minimizes cognitive load
  \medskip
  \item reduces clutter (casts, checks)
  \medskip
  \item prevents loss of type safety from string interface
\end{itemize}
\onslide<2->
Examples:
\begin{itemize}
  \item Using specific solver types in the interface

        \visible<3->{\quad induces lots of messy downcasts and checks}

  \item Using custom TeX macros

        \visible<4->{\quad hinders sharing source with collaborators, publishers}

  \item Data exchange between modules

        \visible<5->{\quad must marshall all top level types in the interface, e.g. std::array}
\end{itemize}
\end{overprint}

\begin{center}
\scriptsize Programming Languages for Scientific Computing, Encyclopedia of App. and Comp. Math., \magenta{\href{http://arxiv.org/abs/1209.1711}{http://arxiv.org/abs/1209.1711}}
\end{center}
\end{frame}
%
\begin{frame}{Why is Composability Important?}{Linear Solvers}

\begin{itemize}
  \item Block Methods/Multiphysics

  \item<2->[Ex] Wathen-Silvester-Elman Stokes preconditioners

  \item Sophisticated smoothers for MG

  \item<3->[Ex] Segregated or distributive smoothers for Stokes

  \item Need recourse to monolithic problem

  \item<4->[Ex] Being able to check using LU

  \item Optimization

  \item<5->[Ex] Use MatNest as an optimized implementation for block methods
\end{itemize}

\end{frame}
%
\begin{frame}{Why is Composability Important?}{Nonlinear Solvers}

\begin{itemize}
  \item Enlarge convergence basin

  \item<2->[Ex] Warm start Newton with Picard

  \item Cleanup noisy Jacobian

  \item<3->[Ex] Right-precondition NGMRES with Newton

  \item Handle localized nonlinearity

  \item<4->[Ex] Left-precondition Newton with NASM

  \item Handle nonlinear field coupling

  \item<5->[Ex] Right-precondition Newton with NASM
\end{itemize}

\end{frame}
%
\begin{frame}{Why is Extensibility Important?}

\begin{itemize}
  \item Incremental complexity

  \item<2->[Ex] Hard core users of \magenta{\href{http://www.fenicsporject.org}{FEniCS}} have to be developers since its built like a compiler

  \item Seeing inside to swap methods

  \item<3->[Ex] PETSc KSP does this for Krylov methods, but what about discretizations, meshes, nonlinear solvers, \ldots

  \item Seeing inside to use results in new ways

  \item<4->[Ex] Error estimators, eigenanalysis
\end{itemize}

\begin{center}
\scriptsize Run-time extensibility and librarization of simulation software, Brown, Knepley and Smith, IEEE CS\&E, 2014, \magenta{\href{http://arxiv.org/abs/1407.2905}{http://arxiv.org/abs/1407.2905}}
\end{center}
\end{frame}
%
\begin{frame}{Why is Configure Testing Important?}

\begin{itemize}
  \item \texttt{.pkg} files can gives locations, but cannot guarantee correct information
  \medskip
  \item New compilers can appear on old architectures (clang) that take different flags % shared library support fails in autoconf often
  \medskip
  \item Libraries can embed compiler inconsistencies
  \medskip
  \item Underlying packages may have inconsistent builds
\end{itemize}

\end{frame}

\end{document}
%
What wrong with iterators? (mesh iterators)
