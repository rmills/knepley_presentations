% 90 min
% Basic Usage: 30min
% - Vectors and Matrices
% - Generic Solver Setup
% - Dynamic Object Configuration
% - Debugging and Profiling
%
% Advanced Usage: 90min
% - Structured Meshes using DMDA
% - Linear Preconditioning
%   - Structured Linear Multigrid
%   - Block Preconditioners
% - Nonlinear Solvers
%   - Nonlinear Preconditioners
% - Timestepping
%
% Things I will not talk about:
%
% - MPI
% - Unstructured meshes using DMPlex
% - Discretization (FEM/FVM) using PetscSection
% - Serial and Parallel Performance
\documentclass{beamer}

\input{tex/talkPreamble.tex}

\title[PETSc]{The\\{\bf P}ortable {\bf E}xtensible {\bf T}oolkit for {\bf S}cientific {\bf C}omputing}
\author[Matt]{Matthew~Knepley}
\date[PETSc18]{PETSc Tutorial\\PETSc User Meeting\\London, UK \qquad June 4, 2018}
% - Use the \inst command if there are several affiliations
\institute[Buffalo]{
  Computer Science and Engineering\\
  University at Buffalo\\
}
\subject{PETSc}

\begin{document}
\lstset{language=C,basicstyle=\ttfamily\scriptsize,stringstyle=\ttfamily\color{green},commentstyle=\color{brown},morekeywords={MPI_Comm,TS,SNES,KSP,PC,DM,Mat,Vec,VecScatter,IS,PetscSF,PetscSection,PetscObject,PetscInt,PetscScalar,PetscReal,PetscBool,InsertMode,PetscErrorCode},emph={PETSC_COMM_WORLD,PETSC_NULL,SNES_NGMRES_RESTART_PERIODIC},emphstyle=\bf}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[height=0.5in]{figures/logos/anl-white-background-modern.jpg}\hspace{1.0in}
  \includegraphics[height=0.5in]{figures/logos/UB_Primary.png}
  \end{center}
  \vskip0.4in
\end{frame}
%
\begin{frame}{Main Point}
\Huge
Never believe \textit{anything},\\
\bigskip
\bigskip
\pause
\qquad unless you can run it.
\end{frame}
%
%
\input{slides/PETSc/PETScDevelopers.tex}
\input{slides/PETSc/Timeline.tex}
\input{slides/PETSc/Tutorial/WhatINeedFromYou.tex}
\input{slides/PETSc/Tutorial/AskQuestions.tex}
\input{slides/PETSc/Tutorial/HowWeCanHelp.tex}
%
%
\section{Getting Started with PETSc}
  \input{slides/PETSc/OriginalImpetus.tex}
  \input{slides/PETSc/RoleOfPETSc.tex}
  \input{slides/PETSc/WhatIsPETSc.tex}
%
  \subsection{Who uses PETSc?}
  \input{slides/PETSc/WhoUsesPETSc.tex}
  \input{slides/PETSc/Limits.tex}
  \input{slides/PyLith/Overview.tex}
  \input{slides/PyLith/MultipleMeshTypes.tex}
  \input{slides/Magma/Overview.tex}
  \input{slides/Fracture/Overview.tex}
  \input{slides/FMM/VortexMethod.tex}
  \input{slides/FMM/GravityAnomaly.tex}
%  \input{slides/GradeTwo/Overview.tex}
  \input{slides/PETSc/Surgery.tex}
%
  \subsection{Stuff for Windows}
  \input{slides/PETSc/MSWindows.tex}
%
  \subsection{How can I get PETSc?}
  \input{slides/PETSc/DownloadingPETSc.tex}
  \input{slides/PETSc/CloningPETSc.tex}
  \input{slides/PETSc/UnpackingPETSc.tex}
  \input{slides/PETSc/Tutorial/Exercise1.tex}
%
  \subsection{How do I Configure PETSc?}
  \input{slides/PETSc/ConfiguringPETSc.tex}
  \input{slides/PETSc/ConfiguringFEM.tex}
  \input{slides/PETSc/AutomaticDownloads.tex}
  \input{slides/PETSc/Tutorial/Exercise2.tex}
%
  \subsection{How do I Build PETSc?}
  \input{slides/PETSc/BuildingPETSc.tex}
  \input{slides/PETSc/Tutorial/Exercise3.tex}
  \input{slides/PETSc/Tutorial/Exercise4.tex}
%
  \subsection{How do I run an example?}
  \input{slides/PETSc/RunningPETSc.tex}
  %% TODO
  %%\input{slides/PETSc/TestingPETSc.tex}
  \input{slides/MPI/UsingMPI.tex}
  \input{slides/MPI/MPIConcepts.tex}
  \input{slides/PETSc/CommonViewingOptions.tex}
  \input{slides/PETSc/CommonMonitoringOptions.tex}
%
  \subsection{How do I get more help?}
  \input{slides/PETSc/GettingMoreHelp.tex}
%
%
\section{PETSc Integration}
%
  \subsection{Initial Operations}
  \input{slides/PETSc/Integration/ApplicationIntegration.tex}
  \input{slides/PETSc/Integration/PETScIntegration.tex}
  \input{slides/PETSc/Integration/Stages.tex}
  \input{slides/PETSc/Integration/Initialization.tex}
  \input{slides/PETSc/Integration/Profiling.tex}
  \input{slides/PETSc/Integration/CommandLineProcessing.tex}
%
  \subsection{Vector Algebra}
  \input{slides/PETSc/Integration/VectorAlgebra.tex}
  \input{slides/PETSc/Integration/ParallelAssembly.tex}
  \input{slides/PETSc/Integration/VectorAssembly.tex}
  \input{slides/PETSc/Integration/MisguidedVectorAssembly.tex}
  \input{slides/PETSc/Integration/EfficientVectorAssembly.tex}
  \input{slides/PETSc/VectorOperations.tex}
  \input{slides/PETSc/Integration/LocalVectors.tex}
  \input{slides/PETSc/Integration/VecGetArray.tex}
%
  \subsection{Matrix Algebra}
  \input{slides/PETSc/Integration/MatrixAlgebra.tex}
  \input{slides/PETSc/Integration/MatrixCreation.tex}
  \input{slides/PETSc/Integration/MatrixPolymorphism.tex}
  \input{slides/PETSc/Integration/MatrixAssembly.tex}
  \input{slides/PETSc/Integration/MisguidedMatrixAssembly.tex}
  \input{slides/PETSc/Integration/ParallelSparseMatrixLayout.tex}
  \input{slides/PETSc/Integration/EfficientMatrixAssembly.tex}
  \input{slides/PETSc/Integration/WhyArePETScMatricesThatWay.tex}
%
  \subsection{Algebraic Solvers}
  \input{slides/PETSc/Experimente.tex}
  %\input{slides/PETSc/Integration/SolverTypes.tex}
  \input{slides/PETSc/Integration/LinearSolvers.tex}
  \input{slides/PETSc/Integration/NonlinearSolvers.tex}
  \input{slides/PETSc/Integration/BasicSolverUsage.tex}
  \input{slides/PETSc/Integration/3rdPartySolvers.tex}
  \input{slides/PETSc/UserSolve.tex}
  \input{slides/Plex/SolverIntegration.tex}
%
  \subsection{Debugging PETSc}
  \input{slides/PETSc/CorrectnessDebugging.tex}
  \input{slides/PETSc/Debugger.tex}
  \input{slides/PETSc/DebuggingTips.tex}
  %%\input{slides/PETSc/Tutorial/Exercise7.tex}
%
  \subsection{Profiling PETSc}
  \input{slides/PETSc/PerformanceDebugging.tex}
  \input{slides/PETSc/UsingStagesAndEvents.tex}
  \input{slides/PETSc/LoggingStages.tex}
  \input{slides/PETSc/LoggingEvents.tex}
  \input{slides/PETSc/LoggingClasses.tex}
  \input{slides/PETSc/MatrixMemoryPreallocation.tex}
  %%\input{slides/PETSc/Tutorial/Exercise8.tex}
%
%
\section{DM}
%
\input{slides/DM/Interface.tex}
\input{slides/DM/LocalEvaluation.tex}
%
\subsection{Structured Meshes (DMDA)}
  \input{slides/DA/WhatIsIt.tex}
  \input{slides/DA/LocalEvaluation.tex}
  \input{slides/DA/GhostValues.tex}
  \input{slides/DA/GlobalNumberings.tex}
  \input{slides/DA/LocalNumbering.tex}
  \input{slides/DA/LocalFunction.tex}
  \input{slides/DA/BratuResidual.tex}
  \input{slides/DA/LocalJacobian.tex}
  \input{slides/DA/BratuJacobian.tex}
  \input{slides/DA/Vectors.tex}
  \input{slides/DA/UpdatingGhosts.tex}
  \input{slides/DA/Stencils.tex}
  \input{slides/DA/SetValuesStencil.tex}
  \input{slides/DA/CreatingDA2d.tex}
  \input{slides/DA/Viewing.tex}
  \input{slides/DA/ViewingOperator.tex}
  %\input{slides/DA/ViewingOperatorAction.tex}
  %\input{slides/DA/DebuggingAssembly.tex}
  %\input{slides/DA/Function.tex}
%
%
\section{Advanced Solvers}
\input{slides/Optimal/MonolithicOrSplit.tex}
%
\subsection{Fieldsplit}
\input{slides/Stokes/Equation.tex}
\input{slides/PETSc/Examples/SNESex62Solutions.tex}
\input{slides/PETSc/Examples/SNESex62Condition.tex}
\input{slides/PETSc/Examples/SNESex62Jacobian.tex}
\input{slides/MultiField/FieldSplitPC.tex}
\input{slides/MultiField/FieldSplitCustomization.tex}
\input{slides/MultiField/FieldSplitOptions.tex}
\input{slides/PETSc/Examples/SNESex62FieldSplit.tex}
\input{slides/PETSc/ProgrammingWithOptions.tex} % FS inside MG
\input{slides/PETSc/Nullspace.tex}
\input{slides/MultiField/FieldSplitNullspace.tex}
%
\subsection{Multigrid}
%
  \input{slides/Optimal/Multigrid.tex}
  \input{slides/Optimal/WhyOptimalAlgorithms.tex}
  \input{slides/Optimal/LinearConvergence.tex}
  \input{slides/Optimal/AMG.tex}
  \input{slides/Optimal/Structured.tex}
  \input{slides/DA/Example2D.tex}
  \input{slides/DA/Example3D.tex}
  %%No increase in iterates for refined problem:
  %%./ex19 -da_refine 3 -ksp_type fgmres -pc_type mg -pc_mg_type full -snes_monitor_short -ksp_monitor_short
  %%./ex19 -da_refine 4 -ksp_type fgmres -pc_type mg -pc_mg_type full -snes_monitor_short -ksp_monitor_short
%
\subsection{Nonlinear Solvers}
%
  \input{slides/PETSc/Integration/3rdPartySolvers.tex}
  \input{slides/SNES/AlwaysUseSNES.tex}
  \input{slides/SNES/FlowControl.tex}
  \input{slides/SNES/Callbacks.tex}
  \input{slides/SNES/Function.tex}
  \input{slides/SNES/Jacobian.tex}
  \input{slides/SNES/Variants.tex}
  \input{slides/SNES/Methods.tex}
  \input{slides/SNES/FiniteDifferenceJacobian.tex}
  \input{slides/SNES/SNESEx19.tex}
  \input{slides/SNES/NoConvergence.tex}
  \input{slides/SNES/NPC.tex}
  \input{slides/SNES/NPCUseCases.tex}
  \input{slides/SNES/NPCEx19.tex}
  \input{slides/KSP/HierarchicalKrylov.tex}
  \input{slides/PETSc/VisualizingSolvers.tex}
%
\subsection{Timestepping}
  \input{slides/TS/Improved.tex}
  \input{slides/TS/Methods.tex}
  \input{slides/TS/ARKIMEX.tex}
  \input{slides/TS/TSEx22.tex}
  \input{slides/TS/TSEx25.tex}
%
\begin{frame}{Second Order TVD Finite Volume Method}{Example}

\magenta{\href{http://www.mcs.anl.gov/petsc/petsc-current/src/ts/examples/tutorials/ex11.c.html}{TS ex11.c}}
\begin{itemize}
  \item \code{\scriptsize ./ex11 -f \$PETSC\_DIR/share/petsc/datafiles/meshes/sevenside.exo}
  \item \code{\scriptsize ./ex11 -f \$PETSC\_DIR/share/petsc/datafiles/meshes/sevenside-quad-15.exo}
  \item \code{\scriptsize ./ex11 -f \$PETSC\_DIR/share/petsc/datafiles/meshes/sevenside.exo -ts\_type rosw}
\end{itemize}
\end{frame}
%
%
\section*{Conclusions}
\begin{frame}{Conclusions}

{\Large PETSc can help you:}
\begin{itemize}
  \item easily construct a code to test your ideas
  \begin{itemize}
    \item<2-> Lots of code construction, management, and debugging tools
  \end{itemize}
  \medskip
  \item scale an existing code to large or distributed machines
  \begin{itemize}
    \item<3-> Using \code{FormFunctionLocal()} and scalable linear algebra
  \end{itemize}
  \medskip
  \item incorporate more scalable or higher performance algorithms
  \begin{itemize}
    \item<4-> Such as domain decomposition, fieldsplit, and multigrid
  \end{itemize}
  \medskip
  \item tune your code to new architectures
  \begin{itemize}
    \item<5-> Using profiling tools and specialized implementations
  \end{itemize}
\end{itemize}
\end{frame}

\ifx \mattStuff \undefined

\else
  \relax
\fi
\end{document}
