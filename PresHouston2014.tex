\documentclass[dvipsnames]{beamer}

\input{tex/talkPreamble.tex}
\usepackage{mathtools}       % For \mathllap
\usepackage[boxed]{algorithm}
\usepackage{algpseudocode}   % For \Procedure on algorithm

\title[NPC]{Nonlinear Preconditioning in PETSc}
\author[M.~Knepley]{Matthew~Knepley {\bf$\in$} PETSc Team}
\date[UH '14]{Seminar\\University of Houston\\Houston, TX \qquad June 18, 2014}
% - Use the \inst command if there are several affiliations
\institute[UC]{
  Computation Institute\\
  University of Chicago

  \smallskip

  Department of Molecular Biology and Physiology\\
  Rush University Medical Center
}
\subject{PETSc}

\begin{document}

\lstset{language=C,basicstyle=\ttfamily\scriptsize,stringstyle=\ttfamily\color{green},commentstyle=\color{brown},morekeywords={MPI_Comm,TS,SNES,KSP,PC,DM,Mat,Vec,VecScatter,IS,PetscSF,PetscSection,PetscObject,PetscInt,PetscScalar,PetscReal,PetscBool,InsertMode,PetscErrorCode},emph={PETSC_COMM_WORLD,PETSC_NULL,SNES_NGMRES_RESTART_PERIODIC},emphstyle=\bf}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.3]{figures/logos/rush_logo.png}\hspace{1.0in}
  \includegraphics[scale=0.24]{figures/logos/uc-logo-official.jpg}
  \end{center}
  \vskip0.4in
\end{frame}

\begin{frame}<testing>{abstract}

Title: Nonlinear Preconditioning in PETSc

Abstract:
A common pitfall in large, complex simulations is a failure of convergence in the nonlinear solver, typically Newton's
method or partial linearization with Picard's method. We present a framework for nonlinear preconditioning, based upon
the composition of nonlinear solvers. We can define both left and right variants of the preconditioning operation, in
analogy to the linear case. Although we currently lack a convincing convergence theory, we will present a powerful and
composable interface for these algorithms, as well as examples which demonstrate their effectiveness.
\end{frame}
%
\input{slides/PETSc/PETScDevelopers.tex}

\section{Algorithmics}
%
\input{slides/SNES/BasicSystem.tex}
\input{slides/SNES/LeftPrec.tex}
\input{slides/SNES/NPrecAdditions.tex}
\input{slides/SNES/AdditiveNPrec.tex}
\input{slides/SNES/LeftNPrec.tex}
\input{slides/SNES/MultiplicativeNPrec.tex}
\input{slides/SNES/RightNPrec.tex}
\input{slides/SNES/NPrecTable.tex}
%
\input{slides/SNES/NRICH.tex}
\input{slides/SNES/NRICH-LP.tex}
\input{slides/SNES/LineSearchVariants.tex}
\input{slides/SNES/NGMRES.tex}
\input{slides/SNES/NK.tex}
\input{slides/SNES/LP-NK.tex}
\input{slides/SNES/RP-NK.tex}
\input{slides/SNES/FAS.tex}
%
\begin{frame}{Other Nonlinear Solvers}\Large
\setlength{\leftmargini}{3em}
\begin{itemize}
  \item[NASM] Nonlinear Additive Schwarz
  \bigskip
  \item[NGS]  Nonlinear Gauss-Siedel
  \bigskip
  \item[NCG]  Nonlinear Conjugate Gradients
  \bigskip
  \item[QN]   Quasi-Newton methods
\end{itemize}
\end{frame}
%
%
\section{Experiments}
%
\subsection{Composition}
\input{slides/SNES/Ex16Equations.tex}
\input{slides/SNES/SNESEx16.tex}
\input{slides/SNES/NPCEx16.tex}
%
\subsection{Multilevel}
\input{slides/SNES/Ex19Equations.tex}
\input{slides/SNES/SNESEx19.tex}
\input{slides/SNES/NPCEx19.tex}

\end{document}

\subsection*{Magma}
\input{slides/Magma/Overview.tex}
\input{slides/Magma/Equations.tex}
%
\begin{frame}[fragile]{Early Newton convergence}
\begin{semiverbatim}\scriptsize
0 TS dt 0.01 time 0
    0 SNES Function norm 5.292194079127e-03 
      Linear pressure_ solve converged due to CONVERGED_RTOL its 10
      0 KSP Residual norm 4.618093146920e+00 
      Linear pressure_ solve converged due to CONVERGED_RTOL its 10
      1 KSP Residual norm 3.018153330707e-03 
      Linear pressure_ solve converged due to CONVERGED_RTOL its 11
      2 KSP Residual norm 4.274869628519e-13 
    Linear solve converged due to CONVERGED_RTOL its 2
    1 SNES Function norm 2.766906985362e-06 
      Linear pressure_ solve converged due to CONVERGED_RTOL its 8
      0 KSP Residual norm 2.555890235972e-02 
      Linear pressure_ solve converged due to CONVERGED_RTOL its 8
      1 KSP Residual norm 1.638293944976e-07 
      Linear pressure_ solve converged due to CONVERGED_RTOL its 8
      2 KSP Residual norm 1.771928779400e-14 
    Linear solve converged due to CONVERGED_RTOL its 2
    2 SNES Function norm 1.188754322734e-11 
  Nonlinear solve converged due to CONVERGED_FNORM_RELATIVE its 2
1 TS dt 0.01 time 0.01
\end{semiverbatim}
\end{frame}
%
\begin{frame}[fragile]{Later Newton convergence}
\begin{semiverbatim}\scriptsize
0 TS dt 0.01 time 0.63
    0 SNES Function norm 9.366565251786e-03 
      Linear pressure_ solve converged due to CONVERGED_RTOL its 16
      Linear pressure_ solve converged due to CONVERGED_RTOL its 16
      Linear pressure_ solve converged due to CONVERGED_RTOL its 16
    Linear solve converged due to CONVERGED_RTOL its 2
    1 SNES Function norm 4.492625910272e-03 
    Linear solve converged due to CONVERGED_RTOL its 2
    2 SNES Function norm 3.666181450068e-03 
    Linear solve converged due to CONVERGED_RTOL its 2
    3 SNES Function norm 2.523116582272e-03 
    Linear solve converged due to CONVERGED_RTOL its 2
    4 SNES Function norm 3.022638159491e-04 
    Linear solve converged due to CONVERGED_RTOL its 2
    5 SNES Function norm 9.761317324448e-06 
    Linear solve converged due to CONVERGED_RTOL its 2
    6 SNES Function norm 1.147944474432e-08 
    Linear solve converged due to CONVERGED_RTOL its 2
    7 SNES Function norm 8.729160299009e-14 
  Nonlinear solve converged due to CONVERGED_FNORM_RELATIVE its 7
1 TS dt 0.01 time 0.64
\end{semiverbatim}
\end{frame}
%
\begin{frame}[fragile]{Newton failure}
\begin{semiverbatim}\scriptsize
0 TS dt 0.01 time 0.64
Time 0.64 L_2 Error: 0.494811 [0.0413666, 0.491642, 0.0376071]
    0 SNES Function norm 9.682733054059e-03 
    Linear solve converged due to CONVERGED_RTOL iterations 2
    1 SNES Function norm 6.841434267123e-03 
    Linear solve converged due to CONVERGED_RTOL iterations 3
    2 SNES Function norm 4.412420553822e-03 
    Linear solve converged due to CONVERGED_RTOL iterations 5
    3 SNES Function norm 3.309326919835e-03 
    Linear solve converged due to CONVERGED_RTOL iterations 6
    4 SNES Function norm 3.022494350289e-03 
    Linear solve converged due to CONVERGED_RTOL iterations 7
    5 SNES Function norm 2.941050948582e-03 
    Linear solve converged due to CONVERGED_RTOL iterations 7
   \vdots
    9 SNES Function norm 2.631941422878e-03 
    Linear solve converged due to CONVERGED_RTOL iterations 7
   10 SNES Function norm 2.631897334054e-03 
    Linear solve converged due to CONVERGED_RTOL iterations 10
   11 SNES Function norm 2.631451174722e-03 
    Linear solve converged due to CONVERGED_RTOL iterations 15
   \vdots
\end{semiverbatim}
\end{frame}
%%   Show good FAS convergence late
\begin{frame}[fragile]{FAS convergence}
\begin{semiverbatim}\scriptsize
0 TS dt 0.01 time 0.64
    0 SNES Function norm 9.682733054059e-03 
      2 SNES Function norm 4.412420553822e-03 
            2 SNES Function norm 8.022096211721e-15 
          1 SNES Function norm 2.773743832538e-04 
            1 SNES Function norm 5.627093528843e-11 
          1 SNES Function norm 4.405884464849e-10 
        2 SNES Function norm 8.985059910030e-08 
          1 SNES Function norm 4.672651281994e-15 
            0 SNES Function norm 3.160322858961e-15 
          0 SNES Function norm 4.672651281994e-15 
        1 SNES Function norm 1.046571008046e-14 
      2 SNES Function norm 1.804845173803e-02 
        2 SNES Function norm 2.776600115290e-12 
          0 SNES Function norm 1.354009326059e-12 
            0 SNES Function norm 5.881604627760e-13 
          0 SNES Function norm 1.354011456281e-12 
        0 SNES Function norm 2.776600115290e-12 
      2 SNES Function norm 9.640723411562e-05 
    1 SNES Function norm 9.640723411562e-05 
    2 SNES Function norm 1.057876040732e-08 
    3 SNES Function norm 5.623618219189e-11 
1 TS dt 0.01 time 0.65
\end{semiverbatim}
\end{frame}
