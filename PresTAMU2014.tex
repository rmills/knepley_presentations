\documentclass[dvipsnames]{beamer}

\input{tex/talkPreamble.tex}
\usepackage{mathtools}       % For \mathllap
\usepackage[boxed]{algorithm}
\usepackage{algpseudocode}   % For \Procedure on algorithm

\title[DFT]{A Computational Viewpoint on Classical Density Functional Theory}
\author[M.~Knepley]{Matthew~Knepley}
\date[TAMU]{Numerical Analysis Seminar\\Texas A\&M University\\College Station, TX \quad December 3, 2014}
% - Use the \inst command if there are several affiliations
\institute[UC]{
  Computation Institute\\
  University of Chicago
}
\subject{PETSc}

\begin{document}

\lstset{language=C,basicstyle=\ttfamily\scriptsize,stringstyle=\ttfamily\color{green},commentstyle=\color{brown},morekeywords={MPI_Comm,TS,SNES,KSP,PC,DM,Mat,Vec,VecScatter,IS,PetscSF,PetscSection,PetscObject,PetscInt,PetscScalar,PetscReal,PetscBool,InsertMode,PetscErrorCode},emph={PETSC_COMM_WORLD,PETSC_NULL,SNES_NGMRES_RESTART_PERIODIC},emphstyle=\bf}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.12]{figures/logos/anl-white-background-modern.jpg}\hspace{1.0in}
  \includegraphics[scale=0.30]{figures/logos/uc-logo-official.jpg}
  \end{center}
  \vskip0.4in
\end{frame}

\begin{frame}<testing>{abstract}
Classical Density Functional Theory (CDFT) has become a powerful tool for investigating the physics of molecular scale
biophysical systems. It can accurately represent the entropic contribution to the free energy. When augmented with an
electrostatic contribution, it can treat complex systems such as ion channels. We will look at the basic computational
steps involved in the Rosenfeld formulation of CDFT, using the Reference Fluid Density (RFD) method of Gillespie. We
present a scalable and accurate implementation using a hybrid numerical-analytic Fourier method. This method enables 3D
simulation of realistic biological systems on a desktop or small cluster.
\end{frame}
%

\begin{frame}{Collaborators}
\begin{center}
\begin{tabular}{lcc}
  \raisebox{0.6in}{\parbox[c]{9em}{\LARGE BIBEE\\Researchers}} &
  \multicolumn{2}{c}{\vbox{
    \hbox{\includegraphics[width=1.2in,height=1.2in]{figures/Collaborators/JayBardhan.png}}
    \smallskip
    \hbox{Jaydeep Bardhan}
  }} \\
  \raisebox{0.6in}{\parbox[c]{10em}{\LARGE Classical DFT\\Researchers}} &
  \vbox{
    \hbox{\includegraphics[width=1.2in,height=1.2in]{figures/Collaborators/DirkGillespie.jpg}}
    \smallskip
    \hbox{Dirk Gillespie}
  } &
  \vbox{
    \hbox{\includegraphics[width=1.2in,height=1.2in]{figures/Collaborators/BobEisenberg.jpg}}
    \smallskip
    \hbox{Bob Eisenberg}
  }
\end{tabular}
\end{center}
\end{frame}
%
\input{slides/IonChannel/BiologicalIonChannels.tex}
%
%
\section{CDFT Intro}
%
\input{slides/CDFT/WhatisCDFT.tex}
%
%
\section{Model}
%
\input{slides/CDFT/EquilibriumModel.tex}
%
\begin{frame}{Details}\Large
The theory and implementation are detailed in

\bigskip

Knepley, Karpeev, Davidovits, Eisenberg, Gillespie,\\
\magenta{\href{http://jcp.aip.org/resource/1/jcpsa6/v132/i12/p124101_s1}{An Efficient Algorithm for Classical Density
    Functional Theory in Three Dimensions: Ionic Solutions}},\\
JCP, 2012.
\end{frame}
%
\subsection{Hard Sphere Repulsion}
\input{slides/CDFT/RosenfeldHardSphereModel.tex}
\input{slides/CDFT/HardSphereBasis.tex}
\input{slides/CDFT/HardSphereBasisConvolution.tex}
\input{slides/CDFT/HardSphereBasisProblem.tex}
\input{slides/CDFT/HardSphereSpectralQuadrature.tex}
\input{slides/CDFT/HardSphereNumericalStability.tex}
%
\subsection{Bulk Fluid Electrostatics}
\input{slides/CDFT/BulkFluidComputation.tex}
%
\subsection{Reference Fluid Density Electrostatics}
\begin{frame}{Reference Fluid Density (RFD) Electrostatics}

Expand around $\rho_i^{\mathrm{ref}}\left(\vx\right)$, an inhomogeneous reference density profile:
\begin{align*}
  \mu^{SC}_i&\left[\left\{\rho_k\left(\vy\right)\right\}\right] \approx
    \mu^{SC}_i\left[\left\{\rho_k^{\mathrm{ref}}\left(\vy\right)\right\}\right] \\
  &- kT\sum_i \int c_i^{\left(1\right)}\left[\left\{\rho_k^{\mathrm{ref}}\left(\vy\right)\right\};\vx\right]
      \Delta\rho_i\left(\vx\right) d^3x \\
  &- \frac{kT}{2} \sum_{i,j} \iint
    c_{ij}^{\left(2\right)}\left[\left\{\rho_k^{\mathrm{ref}}\left(\vy\right)\right\};\vx,\vx'\right]
      \Delta\rho_i\left(\vx\right) \Delta\rho_j\left(\vx'\right) d^3x\,d^3x'
\end{align*}
with
\begin{align*}
   \Delta\rho_i\left(\vx\right) = \rho_i\left(\vx\right) - \rho_i^{\mathrm{ref}}\left(\vx\right)
\end{align*}
\end{frame}
%
\begin{frame}{Reference Fluid Density (RFD) Electrostatics}
\begin{align*}
  \rho^{\mathrm{ref}}_i\left[\left\{\rho_k\left(\vx'\right)\right\};\vx\right] = \frac{3}{4\pi R_{SC}^{3}\left(\vx\right)}
    \int_{\left\vert\vx' - \vx\right\vert \leq R_{SC}\left(\vx\right)} \alpha_i\left(\vx'\right) \rho_i\left(\vx'\right) d^3x'
\end{align*}
\pause
\Large
Choose $\alpha_i$ so that the reference density is
\begin{itemize}
  \item charge neutral, and

  \item has the same ionic strength as $\rho_i$
\end{itemize}
\pause
This can model gradient flow
\end{frame}
%
\begin{frame}{Reference Fluid Density (RFD) Electrostatics}
We can rewrite this expression as an averaging operation:
\begin{align*}
  \rho^{ref}(\vx) = \int \rho(\vx') \frac{\theta\left(|\vx' - \vx| - R_{SC}(\vx)\right)}{\frac{4\pi}{3} R^3_{SC}(\vx)} dx'
\end{align*}
where
\begin{align*}
  R_{SC}(\vx) = \frac{\sum_i \tilde\rho_i(\vx) R_i}{\sum_i \tilde\rho_i(\vx)} + \frac{1}{2\Gamma(\vx)}
\end{align*}
\pause
We close the system using
\begin{align*}
  \Gamma_{\mathrm{SC}}\left[\rho\right](\vx) = \Gamma_{\mathrm{MSA}}\left[\rho^{\mathrm{ref}}(\rho)\right](\vx).
\end{align*}
\end{frame}
%
\begin{frame}{Reference Fluid Density (RFD) Electrostatics}
Efficient Evaluation:
\begin{itemize}
  \item Full integral $\mathcal{O}(N^2)$ with vectorization\\
        \magenta{\href{http://dx.doi.org/10.1007/978-3-642-16405-7_30}{Accurate Evaluation of Local Averages on GPGPUs}}, Karpeev, Knepley, Brune, LNESS, 2013
  \medskip
  \item FFT + Interpolation\\
        \magenta{\href{http://www.mcs.anl.gov/~brune/text/thesis.pdf}{Fast Numerical Methods and Biological Problems}}, Brune, 2011
\end{itemize}
\pause
Complexity in
\begin{align*}
  \mathcal{O}(N_R N \log N)
\end{align*}
using
\begin{align*}
  N_R \le \frac{\log R_{\mathrm{max}} - \log R_{\mathrm{min}}}{\log\left(1 + \sqrt{\frac{8 \epsilon}{R_{\mathrm{max}}
        ||\nabla\rho||_2 + 10 ||\rho||_2}}\right)}
\end{align*}
where we have used Young's inequality to produce the denominator from the interpolation estimate.
\end{frame}
%
%
\section{Verification}
%
\begin{frame}{Consistency checks}
\begin{itemize}\Large
  \item Check $n_\alpha$ of constant density against analytics
  \bigskip
  \item Check that $n_3$ is the combined volume fraction
  \bigskip
  \item Check that wall solution has only 1D variation
\end{itemize}
\end{frame}
%
\begin{frame}{Sum Rule Verification}{Hard Spheres}
\begin{align*}
  \beta P^{HS}_{\mathrm{bath}} = \sum_i \rho_i(R_i)
\end{align*}
where
\begin{align*}
  P^{HS}_{\mathrm{bath}} = \frac{6kT}{\pi} \left( \frac{\xi_{0}}{\Delta} + \frac{3\xi_{1}\xi_{2}}{\Delta^{2}}
    + \frac{3\xi_{2}^{3}}{\Delta^{3}} \right)
\end{align*}
using auxiliary variables
\begin{align*}
  \xi_{n} &= \frac{\pi}{6} \sum_{j} \rho^{bath}_j \sigma_{j}^{n} \qquad n \in \{0,\ldots,3\} \\
  \Delta  &= 1-\xi_{3}
\end{align*}
\end{frame}
%
\begin{frame}{Sum Rule Verification}{Hard Spheres}
\begin{overprint}
\onslide<1>
Relative accuracy and Simulation time for $R = 0.1\mathrm{nm}$
\onslide<2>
Volume fraction ranges from $10^{-5}$ to $0.4$ (very difficult for MC/MD)
\end{overprint}
\begin{center}
\includegraphics[width=\textwidth]{figures/DFT/sumRule.png}
\end{center}
\pause
% domain is divided into cubes which are 0.05nm x 0.05nm x 0.00625nm.
\end{frame}
%
\begin{frame}{Ionic Fluid Verification}{Charged Hard Spheres}
\begin{center}
\begin{tabular}{ll}
$R_{\mathrm{cation}}$ & $0.1\mathrm{nm}$ \\
$R_{\mathrm{anion}}$  & $0.2125\mathrm{nm}$ \\
Concentration       & $1\mathrm{M}$ \\
Domain              & $2\times2\times6\,\mathrm{nm}^3$ and periodic \\
Uncharged hard wall & $z=0$ \\
Grid                & \alt<2>{$21\times21\times{}$\red{161}}{$21\times21\times161$}
\end{tabular}
\end{center}
\end{frame}
%
\begin{frame}{Ionic Fluid Verification}{Charged Hard Spheres}

\begin{center}
Cation Concentrations for 1M concentration\\
\includegraphics[width=\textwidth]{figures/DFT/table1_2_cation.png}
\end{center}
\end{frame}
%
\begin{frame}{Ionic Fluid Verification}{Charged Hard Spheres}

\begin{center}
Anion Concentrations for 1M concentration\\
\includegraphics[width=\textwidth]{figures/DFT/table1_2_anion.png}
\end{center}
\end{frame}
%
\begin{frame}{Ionic Fluid Verification}{Charged Hard Spheres}

\begin{center}
Mean Electrostatic Potential for 1M concentration\\
\includegraphics[width=\textwidth]{figures/DFT/table1_2_phi.png}
\end{center}
\end{frame}
%
\begin{frame}{Ionic Fluid Verification}{Charged Hard Spheres}\Large

These results were first reported in 1D in\\
{\magenta{\href{http://iopscience.iop.org/0953-8984/17/42/002/}{Density functional theory of the electrical double layer: the RFD functional}},\\
J. Phys.: Condens. Matter 17, 6609, 2005}.
\end{frame}
%
\begin{frame}{Main Points}
Real Space vs. Fourier Space
\begin{itemize}
  \item $\mathcal{O}(N^2)$ vs. $\mathcal{O}(N \lg N)$

  \item Accurate quadrature only available in Fourier space
\end{itemize}
\pause
Electrostatics
\begin{itemize}
  \item Bulk Fluid (BF) model can be qualitatively wrong

  \item Reference Fluid Density (RFD) model demands complex algorithm
\end{itemize}
\pause
Solver convergence
\begin{itemize}
  \item Picard was more robust

  \item Newton rarely entered the quadratic regime

  \item Still no multilevel alternative (interpolation?)
\end{itemize}
\end{frame}
%
\begin{frame}{Conclusion}
The theory and implementation are detailed in\\
{\magenta{\href{http://jcp.aip.org/resource/1/jcpsa6/v132/i12/p124101_s1}{An Efficient Algorithm for Classical Density Functional Theory in Three Dimensions: Ionic Solutions}}, JCP, 2012}.
\begin{overprint}
\onslide<1>
\begin{center}\includegraphics[width=\textwidth]{figures/DFT/rhoK__40R__150K__1e-4Ca.png}\end{center}
\onslide<2>
\begin{center}\includegraphics[width=\textwidth]{figures/DFT/Delta_muHSK__40R__150K__1e-4Ca.png}\end{center}
\end{overprint}
\note{Problem: 1D DFT is great, predicts I-V curves for 100+ solutions including multivlaent species, but cannot see details inside the channel.}
\end{frame}
%
\begin{frame}{Hydrodynamics}
Recall that for electrostatics, we have
\begin{align*}
  \mu^{\mathrm{SC}}_i = \mu^{\mathrm{ES},bath}_i - \sum_j \int_{|\vx-\vx'| \leq R_{ij}}
    \left( c^{(2)}_{ij}(\vx,\vx') + \psi_{ij}(\vx,\vx') \right) \Delta\rho_j(\vx')\,d^3x'
\end{align*}
where
\begin{align*}
  c^{(2)}_{ij}\left(\vx,\vx'\right) + \psi_{ij}\left(\vx,\vx'\right) = \frac{z_i z_j e^2}{8\pi\epsilon}
    \Bigg(&\frac{|\vx-\vx'|}{2\lambda_i\lambda_j} - \frac{\lambda_i + \lambda_j}{\lambda_i\lambda_j} +\\
    &\frac{1}{|\vx-\vx'|} \left(\frac{\left(\lambda_i - \lambda_j\right)^2}{2\lambda_i\lambda_j} + 2\right) \Bigg)
\end{align*}
for the interaction kernel
\begin{align*}
  \frac{1}{|\vx-\vx'|}
\end{align*}
\end{frame}
%
\begin{frame}{Hydrodynamics}
A similar expression for hydrodynamics would have the same form
\begin{align*}
  \mu^{\mathrm{HSC}}_i = \mu^{\mathrm{HD},bath}_i - \sum_j \int_{|\vx-\vx'| \leq R_{ij}}
    \left( c^{(2)}_{ij}(\vx,\vx') + \psi_{ij}(\vx,\vx') \right) \Delta\rho_j(\vx')\,d^3x'
\end{align*}
where now
\begin{align*}
  c^{(2)}_{ij}\left(\vx,\vx'\right) + \psi_{ij}\left(\vx,\vx'\right) = \frac{1}{8\pi} \sum_k \frac{C_k(\vx,\vx')}{|\vx-\vx'|^k}
\end{align*}
for the interaction kernel
\begin{align*}
  \frac{1}{|\vx-\vx'|} \left( 1 + \frac{\vx \vx'}{|\vx-\vx'|^2} \right)
\end{align*}
\end{frame}

\end{document}

- Mean Field Electrostatics
  - White Bear paper
    - Show complexity for computation
  - Paper on GPU implementation
  - Paper on Spectral Quadrature
- RFD
  - Citation of Dirk/Deszo paper
  - Dirk paper with all the fits to experiment

\only<1>{\begin{textblock}{0.5}[0.5,0.5](0.8,0.5)\includegraphics[width=\textwidth]{figures/DFT/ryanodineReceptor.jpg}\end{textblock}}
