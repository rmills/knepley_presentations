\documentclass[dvipsnames]{beamer}

\input{tex/talkPreamble.tex}
\usepackage{pgfpages}
%\setbeameroption{show notes on second screen=right}
%\setbeameroption{show only notes}
\usepackage{listings}

\AtBeginSection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[currentsection]
  \end{frame}
}

\AtBeginSubsection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[sectionstyle=show/hide,subsectionstyle=show/shaded/hide]
  \end{frame}
}

\title[CompSci]{The Process of Computational Science}
\author[M.~Knepley]{Matthew~Knepley}
\date[Columbia]{Applied Physics and Applied Mathematics\\School of Engineering and Applied Science, Columbia University\\New York, NY \quad February 18, 2013\vskip-0.15in}
% - Use the \inst command if there are several affiliations
\institute[UC]{
  Computation Institute\\
  University of Chicago

  \smallskip

  Department of Molecular Biology and Physiology\\
  Rush University Medical Center
}
\subject{PETSc}

\begin{document}

\makeatletter
 \def\beamer@framenotesbegin{% at beginning of slide
   \gdef\beamer@noteitems{}%
   \gdef\beamer@notes{{}}% used to be totally empty.
 }
 \makeatother

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.3]{figures/logos/rush_logo.png}\hspace{1.0in}
  \includegraphics[scale=0.24]{figures/logos/uc-logo-official.jpg}
  \end{center}
  \vskip0.4in
\end{frame}
%
\begin{frame}<testing>{Abstract}
Title: The Process of Computational Science

Abstract:
The solution of computational science problems is an interdisciplinary activity requiring mathematical, computational,
and software expertise. I will give a view of the full process of computational science, illustrating each step with
examples from computational mechanics and computational biophysics. Beginning with analysis of the relevant equations, I
will explain the development of a new operator approximation for boundary integral equations in bioelectrostatic
modeling of protein solvation. Then considering the mathematical structures involved in the computation itself, I
examine residual computation in both classical Density Functional Theory for hard sphere gases in Biophysics and
computational mechanics in Earth Science. The distillation of analytical and numerical results into high quality
numerical libraries and application to scientific discovery in collaboration with physical scientists finish the
cycle, illustrated by several examples from my work on the PETSc library from Argonne National Laboratory.

Bio:
Matthew G. Knepley received his B.S. in Physics from Case Western Reserve University in 1994, an M.S. in Computer
Science from the University of Minnesota in 1996, and a Ph.D. in Computer Science from Purdue University in 2000. He
was a Research Scientist at Akamai Technologies in 2000 and 2001. Afterwards, he joined the Mathematics and Computer
Science department at Argonne National Laboratory (ANL), where he was an Assistant Computational Mathematician, and a
Fellow in the Computation Institute at University of Chicago. In 2009, he joined the Computation Institute as a Senior
Research Associate. He is an author of the widely used PETSc library for scientific computing from ANL, and is a
principal designer of the PyLith library for the solution of dynamic and quasi-static tectonic deformation problems. He
was a J. T. Oden Faculty Research Fellow at the Institute for Computation Engineering and Sciences, UT Austin, in 2008,
and won the R\&D 100 Award in 2009 as part of the PETSc team.

\end{frame}

\begin{frame}{Computational Science}\Huge
My approach to\\
\medskip
\quad Computational Science is\\
\bigskip\pause
\begin{center}\bf\blue{Holistic}\end{center}
\note<1->{Thank you for the introduction/coming. I am grateful for the invitation to speak, and I am excited to share with
you my idea of what constitutes the discipline of Computational Science.\\}
\note<2->{Holistic, not only to acknowledge that the \textit{area} incorporates elements from many discplines, but understand that
to be fully solved, a computational science problem needs to be worked on with many different tools.\\
My grandfather was a carpenter\\He lived in the house he built\\Although it might not have been his primary job, he
dealt with plumbing, dry wall, electrical, roofing, and anything else that was necessary to make the house run smoothly.}
\end{frame}
\begin{frame}{Computational Science}\LARGE
% Bring rigor to things that work
starting with the numerics of PDEs,\\                         \note<1->{In particular, operator approximation or preconditioning\\}
\medskip
\quad and mathematics of the computation,\pause\\             \note<1->{Here I mean analysis of computation structure for efficiency, scalability, generality\\\bigskip}
\bigskip
through the distillation into\\
\quad high quality numerical libraries,\pause\\               \note<2->{Still the finest form of communication about computation\\\bigskip}
\bigskip
to scientific discovery through computing.\\                  \note<3->{I am passionate about scientific applications}
\end{frame}

\newcommand\ganttline[4]{% line, tag, start end
   \node at (0,#1/2+.1) [anchor=base east,] {#2};
   \fill[blue] (#3/\xtick-2000/\xtick,#1/2-.1) rectangle (#4/\xtick-2000/\xtick,#1/2+.1);}
\newcommand\ganttlabel[6]{% year, label, color, yloc, anchor
  \node[#3] at (#1/\xtick+#6/\xtick-2000/\xtick,#4) [anchor=#5] {#2};
  \fill[#3] (#1/\xtick-2000/\xtick,1/2-.1) rectangle (#1/\xtick-2000/\xtick+0.04,4/2+.1);}

\begin{frame}{Community Involvement}
\def\present{2013.2}
\def\xtick{2.2}
%      Papers by discipline
% Geosciences       102
% CFD                74
% Nano-simulations   51
% Biology -- Medical 49
% Fusion             18
% Software Packages that use or interface to PETSc 51

{\bf\Large PETSc Citations}
\includegraphics[width=\textwidth]{figures/PETSc/PetscCitations.png}

\begin{tikzpicture}[y=-1cm,scale=1.6]
  \ganttlabel{2000}{2000}{red}{2.2}{north}{0}
  \ganttlabel{2004}{2004}{red}{2.2}{north}{0}
  \ganttlabel{2008}{2008}{red}{2.2}{north}{0}
  \ganttlabel{2012}{2012}{red}{2.2}{north}{0}
  \ganttline{1}{PETSc}{2001}{\present}
  \ganttline{2}{CIG Rep}{2004}{\present}
  \ganttline{3}{CIG EC}{2010}{\present}
  \ganttline{4}{Rush}{2006}{\present}
\end{tikzpicture}
\note{BIBEE and DFT at Rush}

\setlength{\tabcolsep}{15pt}
\begin{tabular}{cccc}
\includegraphics[width=0.15\textwidth]{figures/logos/logo_DOE} &
\includegraphics[width=0.15\textwidth]{figures/logos/anl-white-background-modern} &
\includegraphics[width=0.15\textwidth]{figures/logos/logo_NSF} &
\raisebox{1.5em}{\includegraphics[width=0.15\textwidth]{figures/logos/logo_ARO}}
\end{tabular}
\end{frame}

\section{Operator Approximation}

\begin{frame}{Collaborators}
\begin{center}
\begin{tabular}{lcc}
  \raisebox{0.6in}{\parbox[c]{9em}{\LARGE BIBEE\\Researchers}} &
  \multicolumn{2}{c}{\vbox{
    \hbox{\includegraphics[width=1.2in,height=1.2in]{figures/Collaborators/JayBardhan.png}}
    \smallskip
    \hbox{Jaydeep Bardhan}
  }} \\
  \raisebox{0.6in}{\parbox[c]{10em}{\LARGE Classical DFT\\Researchers}} &
  \vbox{
    \hbox{\includegraphics[width=1.2in,height=1.2in]{figures/Collaborators/DirkGillespie.jpg}}
    \smallskip
    \hbox{Dirk Gillespie}
  } &
  \vbox{
    \hbox{\includegraphics[width=1.2in,height=1.2in]{figures/Collaborators/BobEisenberg.jpg}}
    \smallskip
    \hbox{Bob Eisenberg}
  }
\end{tabular}
\end{center}
\end{frame}
%
\input{slides/Bioelectrostatics/Lysozyme.tex}
\input{slides/Bioelectrostatics/ContinuumModel.tex}
\input{slides/Bioelectrostatics/SecondKindModel.tex}
\input{slides/Bioelectrostatics/ReactionPotentialDefinition.tex}
%
\begin{frame}{Problem}\LARGE

Boundary element discretizations of the solvation problem (Eq.~\ref{eq:sigma}):
\medskip
\begin{itemize}
  \item can be expensive to solve
  \medskip
  \item are more accurate than required by intermediate design iterations
\end{itemize}
\end{frame}
%
\input{slides/Bioelectrostatics/BIBEEIdea.tex}
\input{slides/Bioelectrostatics/BIBEEBoundsStatement.tex}
\input{slides/Bioelectrostatics/BIBEEBoundsProof.tex}
\input{slides/Bioelectrostatics/BIBEEAccuracy.tex}
\input{slides/Bioelectrostatics/BIBEEScalability.tex}
%
\begin{frame}{Resolution}\LARGE

\note[item]{Between 10 and 100x faster}
Boundary element discretizations of the solvation problem:
\medskip
\begin{itemize}
  \item can be expensive to solve
  \begin{itemize}
    \item {\scriptsize \magenta{\bf\href{http://jcp.aip.org/resource/1/jcpsa6/v130/i10/p104108_s1}{Bounding the electrostatic free energies associated with linear continuum models of molecular solvation}}, JCP, 2009}
  \end{itemize}
  \medskip
  \item are more accurate than required by intermediate design iterations
  \begin{itemize}
    \item Accuracy is not tunable
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Evolution of BIBEE}
\begin{itemize}
  \item Sharp bounds for solvation energy
  \medskip
  \item Exploration of behavior in simplified geometries
  \begin{itemize}
    \item {\scriptsize \magenta{\bf\href{http://jcp.aip.org/resource/1/jcpsa6/v135/i12/p124107_s1}{Mathematical Analysis of the BIBEE Approximation for Molecular Solvation: Exact Results for Spherical
  Inclusions}}, JCP, 2011}

    \item Represent BIBEE as a deformed boundary condition

    \item Fully developed series solution

    \item Improve accuracy by combining CFA and P approximations
  \end{itemize}
  \medskip
  \item Application to protein-ligand binding
  \begin{itemize}
    \item {\scriptsize \magenta{\bf\href{http://www.degruyter.com/view/j/mlbmb.2012.1.issue/mlbmb-2013-0007/mlbmb-2013-0007.xml?format=INT}{Analysis of fast boundary-integral approximations for modeling electrostatic contributions of molecular binding}}, Molecular-Based Mathematical Biology, 2013}
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Future of BIBEE}
\begin{itemize}
  \item Framework for systematic exploration
  \begin{itemize}
    \item Both analytical and computational foundation
  \end{itemize}
  \medskip
  \item Reduced-basis Method with analytic solutions
  \begin{itemize}
    \item Tested in protein binding paper above

    \item The spatial high frequency part is handled by BIBEE/P\\topology is not important

    \item The spatial low frequency part is handled by analytic solutions\\insensitive to bumpiness

    \item {\scriptsize \magenta{\bf\href{http://iopscience.iop.org/1749-4699/5/1/014006/}{Computational science and re-discovery: open-source implementations of ellipsoidal harmonics for problems in potential theory}}, CSD, 2012.}
  \end{itemize}
  \medskip
  \item Extend to other kernels, e.g. Yukawa
  \medskip
  \item Extend to full multilevel method
\end{itemize}
\end{frame}

\section{Residual Evaluation}

\begin{frame}{Collaborators}
\note{With the possible exception of Laurent Lafforgue, we all have collaborators}
\begin{center}
\begin{tabular}{lcc}
  \raisebox{0.6in}{\parbox[c]{9em}{\LARGE PETSc\\Developers}} &
  \vbox{
    \hbox{\includegraphics[width=1.2in,height=1.2in]{figures/PETSc/Barry.jpg}}
    \smallskip
    \hbox{Barry Smith}
  } &
  \vbox{
    \hbox{\includegraphics[width=1.2in,height=1.2in]{figures/PETSc/Jed.jpg}}
    \smallskip
    \hbox{Jed Brown}
  } \\
  \raisebox{0.6in}{\parbox[c]{8em}{\LARGE Former UC Students}} &
  \vbox{
    \hbox{\includegraphics[width=1.2in,height=1.2in]{figures/PETSc/AndyTerrel.jpg}}
    \smallskip
    \hbox{Andy Terrel}
  } &
  \vbox{
    \hbox{\includegraphics[width=1.2in,height=1.2in]{figures/PETSc/PeterBrune.jpg}}
    \smallskip
    \hbox{Peter Brune}
  }
\end{tabular}
\end{center}
\end{frame}

\input{slides/Plex/ComparisonProblem.tex}
\input{slides/Plex/UnstructuredInterfaceBefore.tex}
\input{slides/Plex/CombinatorialTopology.tex}
\input{slides/Plex/ExampleMeshes.tex}
\input{slides/Plex/MeshInterface.tex}
\input{slides/Plex/ExampleOperations.tex}

\begin{frame}<testing>
Slide with more complex operations
\begin{itemize}
  \item ghost cells
  \item cohesive cells
  \item submesh extraction
\end{itemize}
\end{frame}

\begin{frame}<testing>
Slide on cohesive cell insertion (alternate Brad's pictures with graph representations in TikZ)
\end{frame}

\input{slides/FEM/ResidualEvaluation.tex}
\input{slides/FEM/IntegrationModel.tex}
% TODO: Switch to lstlsting
\input{slides/FEM/PlexResidual.tex}

\begin{frame}{GPU Integration}
\begin{textblock}{0.37}[1,0](1.07,0.2)\includegraphics[width=\textwidth]{./figures/Hardware/NvidiaC2070.jpeg}\end{textblock}
{\Large Porting to the GPU meant changing
\smallskip

\blue{only} the element integration function:}
\medskip
\begin{itemize}
  \item Has the same flexibility as CPU version
  \medskip
  \item Multiple threads execute each cell integral
  \medskip
  \item Achieves \red{100 GF/s} for 2D $P_1$ Laplacian
  \medskip
  \item Code is
  available \magenta{\href{https://bitbucket.org/petsc/petsc-dev/src/a86497f023abdabc7031d8e16494be6c96d5e91d/src/snes/examples/tutorials/ex52_integrateElement.cu?at=default}{here}}
  \medskip
  \item {\magenta{\href{http://arxiv.org/abs/1103.0066}{Finite Element Integration on GPUs}}, TOMS, 2013}
  \item {{\bf{Finite Element Integration with Quadrature on the GPU}}, PLC, 2013}
\end{itemize}

\end{frame}

\begin{frame}[fragile]{Solver Integration: No New Code}

\begin{overprint}
\onslide<1>
\magenta{\href{http://www.mcs.anl.gov/petsc/petsc-as/snapshots/petsc-dev/src/snes/examples/tutorials/ex62.c.html}{ex62}}: $P_2/P_1$ Stokes Problem on Unstructured Mesh
\medskip

Full Schur Complement
\begin{semiverbatim}\scriptsize
-ksp_type fgmres -pc_type fieldsplit -pc_fieldsplit_type schur
-pc_fieldsplit_schur_factorization_type full
  -fieldsplit_velocity_ksp_type gmres -fieldsplit_velocity_pc_type lu
  -fieldsplit_pressure_ksp_rtol 1e-10 -fieldsplit_pressure_pc_type jacobi
\end{semiverbatim}
\onslide<2>
\magenta{\href{http://www.mcs.anl.gov/petsc/petsc-as/snapshots/petsc-dev/src/snes/examples/tutorials/ex62.c.html}{ex62}}: $P_2/P_1$ Stokes Problem on Unstructured Mesh
\medskip

SIMPLE
\begin{semiverbatim}\scriptsize
-ksp_type fgmres -pc_type fieldsplit -pc_fieldsplit_type schur
-pc_fieldsplit_schur_factorization_type full
  -fieldsplit_velocity_ksp_type gmres -fieldsplit_velocity_pc_type lu
  -fieldsplit_pressure_ksp_rtol 1e-10 -fieldsplit_pressure_pc_type jacobi
  -fieldsplit_pressure_inner_ksp_type preonly
    -fieldsplit_pressure_inner_pc_type jacobi
  -fieldsplit_pressure_upper_ksp_type preonly
    -fieldsplit_pressure_upper_pc_type jacobi
\end{semiverbatim}
\onslide<3>
\magenta{\href{http://www.mcs.anl.gov/petsc/petsc-as/snapshots/petsc-dev/src/snes/examples/tutorials/ex31.c.html}{ex31}}: $P_2/P_1$ Stokes Problem with Temperature on Unstructured Mesh
\medskip

Additive Schwarz + Full Schur Complement
\begin{semiverbatim}\scriptsize
-ksp_type fgmres -pc_type fieldsplit -pc_fieldsplit_0_fields 0,1
-pc_fieldsplit_1_fields 2 -pc_fieldsplit_type additive
  -fieldsplit_0_ksp_type fgmres -fieldsplit_0_pc_type fieldsplit
  -fieldsplit_0_pc_fieldsplit_type schur
  -fieldsplit_0_pc_fieldsplit_schur_factorization_type full
    -fieldsplit_0_fieldsplit_velocity_ksp_type preonly
    -fieldsplit_0_fieldsplit_velocity_pc_type lu
    -fieldsplit_0_fieldsplit_pressure_ksp_rtol 1e-10
    -fieldsplit_0_fieldsplit_pressure_pc_type jacobi
  -fieldsplit_temperature_ksp_type preonly
  -fieldsplit_temperature_pc_type lu
\end{semiverbatim}
\onslide<4>
\magenta{\href{http://www.mcs.anl.gov/petsc/petsc-as/snapshots/petsc-dev/src/snes/examples/tutorials/ex31.c.html}{ex31}}: $P_2/P_1$ Stokes Problem with Temperature on Unstructured Mesh
\medskip

Least-Squares Commutator + Upper Schur Comp. + Full Schur Comp.
\begin{semiverbatim}\scriptsize
-ksp_type fgmres -pc_type fieldsplit -pc_fieldsplit_0_fields 0,1
-pc_fieldsplit_1_fields 2 -pc_fieldsplit_type schur
-pc_fieldsplit_schur_factorization_type upper
  -fieldsplit_0_ksp_type fgmres -fieldsplit_0_pc_type fieldsplit
  -fieldsplit_0_pc_fieldsplit_type schur
  -fieldsplit_0_pc_fieldsplit_schur_factorization_type full
    -fieldsplit_0_fieldsplit_velocity_ksp_type preonly
    -fieldsplit_0_fieldsplit_velocity_pc_type lu
    -fieldsplit_0_fieldsplit_pressure_ksp_rtol 1e-10
    -fieldsplit_0_fieldsplit_pressure_pc_type jacobi
  -fieldsplit_temperature_ksp_type gmres
  -fieldsplit_temperature_pc_type lsc
\end{semiverbatim}
\end{overprint}

\begin{overprint}
\onslide<1>
\begin{equation*}
\begin{pmatrix}
      I        &  0 \\
    B^T A^{-1}  &  I
\end{pmatrix}
\begin{pmatrix}
\hat A & 0 \\
    0  & \hat S
\end{pmatrix}
\begin{pmatrix}
 I & A^{-1} B \\
 0 & I
\end{pmatrix}
\end{equation*}
\onslide<2>
\begin{equation*}
\begin{pmatrix}
      I        &  0 \\
    B^T D_A^{-1}  &  I
\end{pmatrix}
\begin{pmatrix}
\hat A & 0 \\
    0  & \hat S
\end{pmatrix}
\begin{pmatrix}
 I & D_A^{-1} B \\
 0 & I
\end{pmatrix}
\end{equation*}
\onslide<3>
\begin{equation*}
\begin{pmatrix}
  \begin{pmatrix}
         I        &  0 \\
     B^T A^{-1}  &  I
  \end{pmatrix}
  \begin{pmatrix}
    \hat A & 0 \\
         0 & \hat S
  \end{pmatrix}
  \begin{pmatrix}
    I & A^{-1} B \\
    0 & I
  \end{pmatrix}
  & 0 \\
  0 & L_T
\end{pmatrix}
\end{equation*}
\onslide<4>
\begin{equation*}
\begin{pmatrix}
  \begin{pmatrix}
         I        &  0 \\
     B^T A^{-1}  &  I
  \end{pmatrix}
  \begin{pmatrix}
    \hat A & 0 \\
         0 & \hat S
  \end{pmatrix}
  \begin{pmatrix}
    I & A^{-1} B \\
    0 & I
  \end{pmatrix}
  & G \\
  0 & \hat S_{\mathrm{LSC}}
\end{pmatrix}
\end{equation*}
\end{overprint}

\end{frame}
%
\input{slides/Plex/ComparisonResolution.tex}
%
\begin{frame}{Future Work}\LARGE

\begin{itemize}
  \item Unify FEM and FVM residual evaulation
  \bigskip
  \item Batched integration on accelerators
  \bigskip
  \item Integrate auxiliary fields
  \bigskip
  \item Incorporate cell problems for coefficients
\end{itemize}
\end{frame}

\section{Applications}
\input{slides/PyLith/Overview2.tex}
\input{slides/PyLith/Capabilities.tex}
%
\begin{frame}{Classical DFT in Three Dimensions}

\parbox{0.5\textwidth}{\raggedright
I wrote the first \magenta{\href{https://bitbucket.org/knepley/cdft/wiki/Home}{3D Classical DFT}} with true hard
sphere chemical potential using fundamental measure theory. It used an $\mathcal{O}(N \log N)$ algorithm based upon the
FFT. We examined the physics of ion channels, such as the ryanodine receptor.
\green{\href{https://ftp.rush.edu/users/molebio/Dirk/papers/DFT ES functional PRE 03.pdf}{Advanced electrostatics}} 
allowed \green{\href{https://ftp.rush.edu/users/molebio/Dirk/papers/RyR Ca selectivity energetics BJ 08.pdf}{prediction}} of
I-V curves for 100+ solutions, including polyvalent species.
}

\bigskip

The implementation is detailed in {\scriptsize \magenta{\href{http://jcp.aip.org/resource/1/jcpsa6/v132/i12/p124101_s1}{An Efficient Algorithm for Classical Density Functional Theory in Three Dimensions: Ionic Solutions}}, JCP, 2012}.

\only<1>{\begin{textblock}{0.5}[0.5,0.5](0.8,0.5)\includegraphics[width=\textwidth]{figures/DFT/ryanodineReceptor.jpg}\end{textblock}}
\only<2>{\begin{textblock}{0.5}[0.5,0.5](0.8,0.5)\includegraphics[width=\textwidth]{figures/DFT/Fig1b.jpg}\end{textblock}}
\only<3>{\begin{textblock}{0.5}[0.5,0.5](0.8,0.35)\includegraphics[width=\textwidth]{figures/DFT/rhoK__40R__150K__1e-4Ca.png}\end{textblock}}
\only<3>{\begin{textblock}{0.5}[0.5,0.5](0.8,0.65)\includegraphics[width=\textwidth]{figures/DFT/Delta_muHSK__40R__150K__1e-4Ca.png}\end{textblock}}

  \note{Problem: 1D DFT is great, predicts I-V curves for 100+ solutions including multivlaent species, but cannot see
  details inside the channel. Need 3D, but too expensive.}
\end{frame}
%
\input{slides/Plex/PeopleUsingPlex.tex}
\input{slides/PETSc/PeopleUsingComposableSolvers.tex}
%

\input{slides/ScientificComputing/Bridge.tex}

\section*{Additional Slides}
%
\begin{frame}
\begin{center}\Huge Additional Slides\end{center}
\end{frame}

\input{slides/PETSc/ProgrammingWithOptions.tex}

\begin{frame}{Nonlinear Preconditioning}
\begin{itemize}
  \item Major Point: Composable structures for computation reduce system complexity and generate real application benefits

  \item Minor Point: Numerical libraries are communication medium for scientific results

  \item Minor Point: Optimal solvers can be constructed on the fly to suit the problem

  \item Slides for Stokes PCs

  \item Slide with programming with options
\end{itemize}
\end{frame}

\begin{frame}{Nonlinear Preconditioning}
\begin{itemize}
  \item NPC in PETSc

  \item Paper with Barry and Peter

  \item Cite Peter and Jed paper for use cases
\end{itemize}
\end{frame}

\begin{frame}{Parallel Fast Multipole Method}
\begin{itemize}
  \item Using mesh partitioner to develop schedule removes load balance barrier

  \item Partitioner can be proved to work with Teng's result

  \item Simple parallelization can be proved to work with overlap

  \item Ex: Work with May, 512 GPU paper
\end{itemize}
\end{frame}

\begin{frame}{GPU Computing}
\begin{itemize}
  \item Papers with Andy about FEM Integration

  \item Paper with PETSc about solvers

  \item Conferences with Yuen
\end{itemize}
\end{frame}

\end{document}

Prof. L. Polvani
Cell: 212-854-7331

Ideas:

   Composability?

Hasse Diagram Ideas for structuring computation

  - Composable framework for mesh operations

    - Distribution of meshes, functions (composition of PetscSection objects)

  - Universal residual calculation (dimension/element/cell independent)

  - Dual graph organizes functions

    - FieldSplit composable framework for preconditioning

  - Addition of cohesive cells, FV ghost cells

  - Extraction of faults, partial boundaries

  - Ex: PyLith, HiFlow, Blaise, Spiegelman

  - FErari optimization of function evaluation

  - Composable nonlinear solvers

  - Thread-transposition for GPU FEM

Biological

  - Eigenvalue bounds for preconditioner (1\% accuracy)

  - Split analytical/computational approach uses analytical solutions combined with projection, preconditioning, and BEM

  - Split analytical/computational DFT formulation, using spectral quadrature

    Future Work:

      Nonlinear FieldSplit: This means breaking up the residual evaluation process



Major Theme: Composable structures for computation reduce system complexity and generate real application benefits



Marc:

 - Khaki's and Button-down

 - Marc, Michael, Guillamue, Chris, Lorenzo, Adam,   Marianetti, Tiffany  (Kathryn Johnson, MRI for machine)


Lamont changes: Moving to open search every year (48 postdocs)
