\documentclass[dvipsnames]{beamer}

\input{tex/talkPreamble.tex}
\usepackage{pgfpages}
%\setbeameroption{show notes on second screen=right}
%\setbeameroption{show only notes}
\usepackage{listings}

\AtBeginSection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[currentsection]
  \end{frame}
}

\AtBeginSubsection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[sectionstyle=show/hide,subsectionstyle=show/shaded/hide]
  \end{frame}
}

\title[CompSci]{The Process of Computational Science}
\author[M.~Knepley]{Matthew~Knepley}
\date[Orsay '13]{Maison de la Simulation\\Saclay, France \quad June 15, 2013\vskip-0.15in}
% - Use the \inst command if there are several affiliations
\institute[UC]{
  Computation Institute\\
  University of Chicago

  \smallskip

  Department of Molecular Biology and Physiology\\
  Rush University Medical Center
}
\subject{PETSc}

\begin{document}

\makeatletter
 \def\beamer@framenotesbegin{% at beginning of slide
   \gdef\beamer@noteitems{}%
   \gdef\beamer@notes{{}}% used to be totally empty.
 }
 \makeatother

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.3]{figures/logos/rush_logo.png}\hspace{1.0in}
  \includegraphics[scale=0.24]{figures/logos/uc-logo-official.jpg}
  \end{center}
  \vskip0.4in
\end{frame}
%
\begin{frame}<testing>{Abstract}
Title: The Process of Computational Science

Abstract:
The solution of computational science problems is an interdisciplinary activity requiring mathematical, computational,
and software expertise. I will give a view of the full process of computational science, illustrating each step with
examples from computational mechanics and computational biophysics. Beginning with analysis of the relevant equations, I
will explain the development of a new operator approximation for boundary integral equations in bioelectrostatic
modeling of protein solvation. Then considering the mathematical structures involved in the computation itself, I
examine residual computation in both classical Density Functional Theory for hard sphere gases in Biophysics and
computational mechanics in Earth Science. The distillation of analytical and numerical results into high quality
numerical libraries and application to scientific discovery in collaboration with physical scientists finish the
cycle, illustrated by several examples from my work on the PETSc library from Argonne National Laboratory.

Bio:
Matthew G. Knepley received his B.S. in Physics from Case Western Reserve University in 1994, an M.S. in Computer
Science from the University of Minnesota in 1996, and a Ph.D. in Computer Science from Purdue University in 2000. He
was a Research Scientist at Akamai Technologies in 2000 and 2001. Afterwards, he joined the Mathematics and Computer
Science department at Argonne National Laboratory (ANL), where he was an Assistant Computational Mathematician, and a
Fellow in the Computation Institute at University of Chicago. In 2009, he joined the Computation Institute as a Senior
Research Associate. He is an author of the widely used PETSc library for scientific computing from ANL, and is a
principal designer of the PyLith library for the solution of dynamic and quasi-static tectonic deformation problems. He
was a J. T. Oden Faculty Research Fellow at the Institute for Computation Engineering and Sciences, UT Austin, in 2008,
and won the R\&D 100 Award in 2009 as part of the PETSc team.

\end{frame}

\begin{frame}{Computational Science}\Huge
My approach to\\
\medskip
\quad Computational Science is\\
\bigskip\pause
\begin{center}\bf\blue{Holistic}\end{center}
\note<1->{Thank you for the introduction/coming. I am grateful for the invitation to speak, and I am excited to share with
you my idea of what constitutes the discipline of Computational Science.\\}
\note<2>{Holistic, not only to acknowledge that the \textit{area} incorporates elements from many discplines, but understand that
to be fully solved, a computational science problem needs to be worked on with many different tools.}
\note<2>{Computational Science has the same problem that many disciplines have had before becoming indpendent of their
  predecessors. It uses pieces of many existing disciplines, but for different ends. It is often claimed that
  computational science ``belongs'' in other departments, and that the pieces which do not fit there are ``not
  research'' but merely ``implementation''. I would like to show how all the disparate pieces fit together to solve
  interesting comp. sci. problems, illustrating our particular strategies and goals.}
\note<2>{My grandfather was a carpenter. He lived in the house he built. Although it might not have been his primary job, he
dealt with plumbing, dry wall, electrical, roofing, and anything else that was necessary to make the house run
smoothly.}
\end{frame}
\begin{frame}{Computational Science}\LARGE
% Bring rigor to things that work
starting with the numerics of PDEs,\\                         \note<1->{In particular, operator approximation or preconditioning\\}
\medskip
\quad and mathematics of the computation,\pause\\             \note<1->{Here I mean analysis of computation structure for efficiency, scalability, generality\\\bigskip}
\bigskip
through the distillation into\\
\quad high quality numerical libraries,\pause\\               \note<2->{Still the finest form of communication about computation\\\bigskip}
\bigskip
to scientific discovery through computing.\\                  \note<3->{I am passionate about scientific applications}
\end{frame}

\newcommand\ganttline[4]{% line, tag, start end
   \node at (0,#1/2+.1) [anchor=base east,] {#2};
   \fill[blue] (#3/\xtick-2000/\xtick,#1/2-.1) rectangle (#4/\xtick-2000/\xtick,#1/2+.1);}
\newcommand\ganttlabel[6]{% year, label, color, yloc, anchor
  \node[#3] at (#1/\xtick+#6/\xtick-2000/\xtick,#4) [anchor=#5] {#2};
  \fill[#3] (#1/\xtick-2000/\xtick,1/2-.1) rectangle (#1/\xtick-2000/\xtick+0.04,4/2+.1);}

\begin{frame}{Community Involvement}
\def\present{2013.2}
\def\xtick{2.2}
%      Papers by discipline
% Geosciences       102
% CFD                74
% Nano-simulations   51
% Biology -- Medical 49
% Fusion             18
% Software Packages that use or interface to PETSc 51

{\bf\Large PETSc Citations}
\includegraphics[width=\textwidth]{figures/PETSc/PetscCitations.png}

\begin{tikzpicture}[y=-1cm,scale=1.6]
  \ganttlabel{2000}{2000}{red}{2.2}{north}{0}
  \ganttlabel{2004}{2004}{red}{2.2}{north}{0}
  \ganttlabel{2008}{2008}{red}{2.2}{north}{0}
  \ganttlabel{2012}{2012}{red}{2.2}{north}{0}
  \ganttline{1}{PETSc}{2001}{\present}
  \ganttline{2}{CIG Rep}{2004}{\present}
  \ganttline{3}{CIG EC}{2010}{\present}
  \ganttline{4}{Rush}{2006}{\present}
\end{tikzpicture}
\note{BIBEE and DFT at Rush}

\setlength{\tabcolsep}{15pt}
\begin{tabular}{cccc}
\includegraphics[width=0.15\textwidth]{figures/logos/logo_DOE} &
\includegraphics[width=0.15\textwidth]{figures/logos/anl-white-background-modern} &
\includegraphics[width=0.15\textwidth]{figures/logos/logo_NSF} &
\raisebox{1.5em}{\includegraphics[width=0.15\textwidth]{figures/logos/logo_ARO}}
\end{tabular}
\end{frame}

\section{Operator Approximation}

\begin{frame}{Collaborators}
\begin{center}
\begin{tabular}{lcc}
  \raisebox{0.6in}{\parbox[c]{9em}{\LARGE BIBEE\\Researchers}} &
  \multicolumn{2}{c}{\vbox{
    \hbox{\includegraphics[width=1.2in,height=1.2in]{figures/Collaborators/JayBardhan.png}}
    \smallskip
    \hbox{Jaydeep Bardhan}
  }} \\
  \raisebox{0.6in}{\parbox[c]{10em}{\LARGE Classical DFT\\Researchers}} &
  \vbox{
    \hbox{\includegraphics[width=1.2in,height=1.2in]{figures/Collaborators/DirkGillespie.jpg}}
    \smallskip
    \hbox{Dirk Gillespie}
  } &
  \vbox{
    \hbox{\includegraphics[width=1.2in,height=1.2in]{figures/Collaborators/BobEisenberg.jpg}}
    \smallskip
    \hbox{Bob Eisenberg}
  }
\end{tabular}
\end{center}
\end{frame}

\begin{frame}{Bioelectrostatics}{The Natural World}
\includegraphics[width=4in]{figures/Bioelectrostatics/lysozymeSurfaceCharge.pdf}
\begin{center}Lysozyme\end{center}
\end{frame}
%
\begin{frame}{Bioelectrostatics}{Physical Model}
\includegraphics[width=4in]{figures/Bioelectrostatics/continuumModel.pdf}
\end{frame}
%
\begin{frame}{Bioelectrostatics}{Mathematical Model}
\note[item]{BIBEE arose from a purely mathematical inquiry about why a popular ad hoc model, GB, works so well}
\begin{eqnarray}\label{eq:sigma}
  \sigma(\vr) + \hat\epsilon \int_\Gamma \frac{\partial}{\partial n(\vr)} \frac{\sigma(\vr') d^2\vr'}{4\pi ||\vr-\vr'||}
  &=& -\hat\epsilon \sum^Q_{k=1} \frac{\partial}{\partial n(\vr)} \frac{q_k}{4\pi ||\vr-\vr_k||} \\
  \left(\mathcal{I} + \hat\epsilon \mathcal{D}^* \right) \sigma(\vr) &=&
\end{eqnarray}
\note[item]{The integral is the Cauchy Principal Value, and sigma is the surface charge on the protein}
where we define
\begin{equation*}
  \hat\epsilon = \frac{1}{2} \frac{\epsilon_1 - \epsilon_2}{\epsilon_1 + \epsilon_2} < 0
\end{equation*}
\end{frame}
%
\begin{frame}{Bioelectrostatics}{Mathematical Model}
\note[item]{The reaction potential is the part of the potential we did not know about to begin with}
The \textit{reaction} potential is given by
\begin{equation*}
  \phi^R(\vr) = \int_\Gamma \frac{\sigma(\vr') d^2\vr'}{4\pi\epsilon_1 ||\vr-\vr'||}
\end{equation*}
which defines the electrostatic part of the solvation free energy
\note[item]{In the linear response model}
\begin{eqnarray*}
  \Delta G_{es} &=& \frac{1}{2} q^T \phi^R \\ 
               &=& \frac{1}{2} q^T L q \\ 
               &=& \frac{1}{2} q^T C A^{-1} B q \\ 
\end{eqnarray*}
\note[item]{$L_{ij}$ is the potential induced by solvent polarization at $r_i$ by a unit charge at $r_j$}
\note[item]{$B$ maps charges to normal displacement field, rhs operator}
\note[item]{$C$ maps induced charge to volume potential at point charges}
\note[item]{$A^{-1}$ get induced charge from surface normal displacement}
\end{frame}

\begin{frame}{Problem}\LARGE

Boundary element discretizations of the solvation problem (Eq.~\ref{eq:sigma}):
\medskip
\begin{itemize}
  \item can be expensive to solve, and\\hard to precondition
  \medskip
  \item are more accurate than required by intermediate design iterations
\end{itemize}
\end{frame}

\begin{frame}{BIBEE}{Approximate $\mathcal{D}^*$ by a diagonal operator}
\begin{center}\Large \red{B}oundary \red{I}ntegral-\red{B}ased \red{E}lectrostatics \red{E}stimation\end{center}

\note[item]{Assumes charge does not act on itself, constant normal field on $\Gamma$}
{\bf Coulomb Field Approximation}: uniform normal field
\begin{equation}
  \left(1 - \frac{\hat\epsilon}{2} \right) \sigma_{CFA} = Bq
\end{equation}

\note[item]{Considers only the small piece of $\Gamma$ around $r$}
{\bf Preconditioning}: consider only local effects
\begin{equation}
                                           \sigma_{P}   = Bq
\end{equation}

{\bf Lower Bound}: no good physical motivation
\begin{equation}
  \left(1 + \frac{\hat\epsilon}{2} \right) \sigma_{LB}  = Bq
\end{equation}
\end{frame}

\begin{frame}{Energy Bounds: First Step}{Replace $C$ with $B$}
\note{Borgis did BIBEE/CFA in PDE form (starting point for his variational CFA), but did not see that the approximation is a bound.}
We will need the single layer operator $\mathcal{S}$
\begin{equation*}
  \mathcal{S}\tau(\vr) = \int \frac{\tau(\vr') d^2\vr'}{4\pi ||\vr - \vr'||}
\end{equation*}
\end{frame}
%
\begin{frame}{Energy Bounds: First Step}{Replace $C$ with $B$}
The potential at the boundary $\Gamma$ given by
\begin{equation*}
  \phi^{Coulomb}(\vr) = C^T q
\end{equation*}
can also be obtained by solving an exterior Neumann problem for $\tau$,
\begin{eqnarray*}
  \phi^{Coulomb}(\vr) &=& \mathcal{S}\tau \\
                     &=& \mathcal{S} (\mathcal{I} - 2 \mathcal{D}^*)^{-1} (\frac{2}{\hat\epsilon} Bq) \\
                     &=& \frac{2}{\hat\epsilon} \mathcal{S} (\mathcal{I} - 2 \mathcal{D}^*)^{-1} Bq
\end{eqnarray*}
so that the solvation energy is given by
\begin{equation*}
  \frac{1}{2} q^T C A^{-1} B q = \frac{1}{\hat\epsilon} q^T B^T (\mathcal{I} - 2 \mathcal{D}^*)^{-T} \mathcal{S}
  (\mathcal{I} + \hat\epsilon \mathcal{D}^*)^{-1} Bq
\end{equation*}
\end{frame}

\begin{frame}{Energy Bounds: Second Step}{Quasi-Hermiticity}
It is well known that (\magenta{\href{http://books.google.com/books/about/Boundary_Integral_Equations.html?id=_Gy56YLlGXEC}{Hsaio and Wendland}})
\begin{equation*}
  \mathcal{S} \mathcal{D}^* = \mathcal{D} \mathcal{S}
\end{equation*}
and
\begin{equation*}
  \mathcal{S} = \mathcal{S}^{1/2} \mathcal{S}^{1/2}
\end{equation*}
which means that we can define a Hermitian operator $H$ similar to $\mathcal{D}^*$
\begin{equation*}
  H = \mathcal{S}^{1/2} \mathcal{D}^* \mathcal{S}^{-1/2}
\end{equation*}
leading to an energy
\begin{equation*}
  \frac{1}{2} q^T C A^{-1} B q = \frac{1}{\hat\epsilon} q^T B^T \mathcal{S}^{1/2} (\mathcal{I} - 2 H)^{-1}
  (\mathcal{I} + \hat\epsilon H)^{-1} \mathcal{S}^{1/2} Bq
\end{equation*}
\end{frame}

\begin{frame}{Energy Bounds: Third Step}{Eigendecomposition}
The spectrum of $\mathcal{D}^*$ is in $[-\frac{1}{2}, \frac{1}{2})$, and the energy is
\begin{equation*}
  \frac{1}{2} q^T C A^{-1} B q = \sum_i \frac{1}{\hat\epsilon} \left( 1 - 2 \lambda_i \right)^{-1} \left( 1 + \hat\epsilon \lambda_i \right)^{-1} x^2_i
\end{equation*}
where
\begin{equation*}
  H = V \Lambda V^T
\end{equation*}
and
\begin{equation*}
  \vx = V^T \mathcal{S}^{1/2} B q
\end{equation*}
\end{frame}

\begin{frame}{Energy Bounds: Diagonal Approximations}
\note{BIBEE/P is a lower bound only for spheres and prolate spheroids, which possess operators $D^*$ with only nonpositive eigenvalues\\\bigskip}

The BIBEE approximations yield the following bounds
\begin{eqnarray}
  \frac{1}{2} q^T C A^{-1}_{CFA} B q &=& \sum_i \frac{1}{\hat\epsilon} \left( 1 - 2 \lambda_i \right)^{-1} \left( 1 - \frac{\hat\epsilon}{2} \right)^{-1} x^2_i \\
  \frac{1}{2} q^T C A^{-1}_{P}   B q &=& \sum_i \frac{1}{\hat\epsilon} \left( 1 - 2 \lambda_i \right)^{-1} x^2_i \\ 
  \frac{1}{2} q^T C A^{-1}_{LB}  B q &=& \sum_i \frac{1}{\hat\epsilon} \left( 1 - 2 \lambda_i \right)^{-1} \left( 1 + \frac{\hat\epsilon}{2} \right)^{-1} x^2_i
\end{eqnarray}
where we note that
\begin{equation*}
  | \hat\epsilon | < \frac{1}{2}
\end{equation*}

\note{Because $\hat\epsilon < 0$, each term in the sum is negative, and
\begin{equation*}
  \left(1 - \frac{\hat\epsilon}{2}\right) \ge \left(1 + \hat\epsilon \lambda_i \right) \forall i
\end{equation*}
}
\end{frame}
%
\begin{frame}{Energy Bounds: Diagonal Approximations}
\begin{center}\scriptsize Electrostatic solvation free energies of met-enkephalin structures\end{center}
\begin{center}
\includegraphics[]{figures/Bioelectrostatics/MEK_Energy_Plot.pdf}
\end{center}
\begin{center}\scriptsize Snapshots taken from a 500-ps MD simulation at 10-ps intervals.\end{center}
\end{frame}

\begin{frame}{BIBEE Scalabiltiy}
\begin{center}
\includegraphics[width=4in]{figures/Bioelectrostatics/lysozymeParallelScaling.pdf}
\end{center}
\end{frame}

\begin{frame}{Resolution}\LARGE

\note[item]{Between 10 and 100x faster}
Boundary element discretizations of the solvation problem:
\medskip
\begin{itemize}
  \item can be expensive to solve, and\\hard to precondition
  \begin{itemize}
    \item {\scriptsize \magenta{\bf\href{http://jcp.aip.org/resource/1/jcpsa6/v130/i10/p104108_s1}{Bounding the electrostatic free energies associated with linear continuum models of molecular solvation}}, JCP, 2009}
    \item BIBEE-FMM (uses \magenta{\href{http://bitbucket.org/jbardhan/kifmm3d}{kifmm3d}})
  \end{itemize}
  \medskip
  \item are more accurate than required by intermediate design iterations
  \begin{itemize}
    \item Accuracy is not tunable
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Evolution of BIBEE}
\begin{itemize}
  \item Sharp bounds for solvation energy
  \medskip
  \item Exploration of behavior in simplified geometries
  \begin{itemize}
    \item {\scriptsize \magenta{\bf\href{http://jcp.aip.org/resource/1/jcpsa6/v135/i12/p124107_s1}{Mathematical Analysis of the BIBEE Approximation for Molecular Solvation: Exact Results for Spherical
  Inclusions}}, JCP, 2011}

    \item Represent BIBEE as a deformed boundary condition

    \item Fully developed series solution

    \item Improve accuracy by combining CFA and P approximations
  \end{itemize}
  \medskip
  \item Application to protein-ligand binding
  \begin{itemize}
    \item {\scriptsize \magenta{\bf\href{http://www.degruyter.com/view/j/mlbmb.2012.1.issue/mlbmb-2013-0007/mlbmb-2013-0007.xml?format=INT}{Analysis of fast boundary-integral approximations for modeling electrostatic contributions of molecular binding}}, Molecular-Based Mathematical Biology, 2013}
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Future of BIBEE}
\begin{itemize}
  \item Framework for systematic exploration
  \begin{itemize}
    \item Both analytical and computational foundation
  \end{itemize}
  \medskip
  \item Reduced-basis Method with analytic solutions
  \begin{itemize}
    \item Tested in protein binding paper above

    \item The spatial high frequency part is handled by BIBEE/P\\topology is not important

    \item The spatial low frequency part is handled by analytic solutions\\insensitive to bumpiness

    \item {\scriptsize \magenta{\bf\href{http://iopscience.iop.org/1749-4699/5/1/014006/}{Computational science and re-discovery: open-source implementations of ellipsoidal harmonics for problems in potential theory}}, CSD, 2012.}
  \end{itemize}
  \medskip
  \item Extend to other kernels, e.g. Yukawa
  \medskip
  \item Extend to full multilevel method
\end{itemize}
\end{frame}

\section{Residual Evaluation}

\begin{frame}{Collaborators}
\note{With the possible exception of Laurent Lafforgue, we all have collaborators}
\begin{center}
\begin{tabular}{lcc}
  \raisebox{0.6in}{\parbox[c]{9em}{\LARGE PETSc\\Developers}} &
  \vbox{
    \hbox{\includegraphics[width=1.2in,height=1.2in]{figures/PETSc/Barry.jpg}}
    \smallskip
    \hbox{Barry Smith}
  } &
  \vbox{
    \hbox{\includegraphics[width=1.2in,height=1.2in]{figures/PETSc/Jed.jpg}}
    \smallskip
    \hbox{Jed Brown}
  } \\
  \raisebox{0.6in}{\parbox[c]{8em}{\LARGE Former UC Students}} &
  \vbox{
    \hbox{\includegraphics[width=1.2in,height=1.2in]{figures/PETSc/AndyTerrel.jpg}}
    \smallskip
    \hbox{Andy Terrel}
  } &
  \vbox{
    \hbox{\includegraphics[width=1.2in,height=1.2in]{figures/PETSc/PeterBrune.jpg}}
    \smallskip
    \hbox{Peter Brune}
  }
\end{tabular}
\end{center}
\end{frame}

\input{slides/Plex/ComparisonProblem.tex}
\input{slides/Plex/UnstructuredInterfaceBefore.tex}
\input{slides/Plex/CombinatorialTopology.tex}
\input{slides/Plex/ExampleMeshes.tex}
\input{slides/Plex/MeshInterface.tex}
\input{slides/Plex/ExampleOperations.tex}

\begin{frame}<testing>
Slide with more complex operations
\begin{itemize}
  \item ghost cells
  \item cohesive cells
  \item submesh extraction
\end{itemize}
\end{frame}

\begin{frame}<testing>
Slide on cohesive cell insertion (alternate Brad's pictures with graph representations in TikZ)
\end{frame}

\input{slides/FEM/ResidualEvaluation.tex}
\input{slides/FEM/IntegrationModel.tex}
% TODO: Switch to lstlsting
\input{slides/FEM/PlexResidual.tex}

\begin{frame}{GPU Integration}
\begin{textblock}{0.37}[1,0](1.07,0.2)\includegraphics[width=\textwidth]{./figures/Hardware/NvidiaC2070.jpeg}\end{textblock}
{\Large Porting to the GPU meant changing
\smallskip

\blue{only} the element integration function:}
\medskip
\begin{itemize}
  \item Has the same flexibility as CPU version
  \medskip
  \item Multiple threads execute each cell integral
  \medskip
  \item Achieves \red{100 GF/s} for 2D $P_1$ Laplacian
  \medskip
  \item Code is
  available \magenta{\href{https://bitbucket.org/petsc/petsc-dev/src/a86497f023abdabc7031d8e16494be6c96d5e91d/src/snes/examples/tutorials/ex52_integrateElement.cu?at=default}{here}}
  \medskip
  \item {\magenta{\href{http://arxiv.org/abs/1103.0066}{Finite Element Integration on GPUs}}, TOMS, 2013}
  \item {{\bf{Finite Element Integration with Quadrature on the GPU}}, PLC, 2013}
\end{itemize}

\end{frame}

\begin{frame}[fragile]{Solver Integration: No New Code}

\begin{overprint}
\onslide<1>
\magenta{\href{http://www.mcs.anl.gov/petsc/petsc-as/snapshots/petsc-dev/src/snes/examples/tutorials/ex62.c.html}{ex62}}: $P_2/P_1$ Stokes Problem on Unstructured Mesh
\medskip

Full Schur Complement
\begin{semiverbatim}\scriptsize
-ksp_type fgmres -pc_type fieldsplit -pc_fieldsplit_type schur
-pc_fieldsplit_schur_factorization_type full
  -fieldsplit_velocity_ksp_type gmres -fieldsplit_velocity_pc_type lu
  -fieldsplit_pressure_ksp_rtol 1e-10 -fieldsplit_pressure_pc_type jacobi
\end{semiverbatim}
\onslide<2>
\magenta{\href{http://www.mcs.anl.gov/petsc/petsc-as/snapshots/petsc-dev/src/snes/examples/tutorials/ex62.c.html}{ex62}}: $P_2/P_1$ Stokes Problem on Unstructured Mesh
\medskip

SIMPLE
\begin{semiverbatim}\scriptsize
-ksp_type fgmres -pc_type fieldsplit -pc_fieldsplit_type schur
-pc_fieldsplit_schur_factorization_type full
  -fieldsplit_velocity_ksp_type gmres -fieldsplit_velocity_pc_type lu
  -fieldsplit_pressure_ksp_rtol 1e-10 -fieldsplit_pressure_pc_type jacobi
  -fieldsplit_pressure_inner_ksp_type preonly
    -fieldsplit_pressure_inner_pc_type jacobi
  -fieldsplit_pressure_upper_ksp_type preonly
    -fieldsplit_pressure_upper_pc_type jacobi
\end{semiverbatim}
\onslide<3>
\magenta{\href{http://www.mcs.anl.gov/petsc/petsc-as/snapshots/petsc-dev/src/snes/examples/tutorials/ex31.c.html}{ex31}}: $P_2/P_1$ Stokes Problem with Temperature on Unstructured Mesh
\medskip

Additive Schwarz + Full Schur Complement
\begin{semiverbatim}\scriptsize
-ksp_type fgmres -pc_type fieldsplit -pc_fieldsplit_0_fields 0,1
-pc_fieldsplit_1_fields 2 -pc_fieldsplit_type additive
  -fieldsplit_0_ksp_type fgmres -fieldsplit_0_pc_type fieldsplit
  -fieldsplit_0_pc_fieldsplit_type schur
  -fieldsplit_0_pc_fieldsplit_schur_factorization_type full
    -fieldsplit_0_fieldsplit_velocity_ksp_type preonly
    -fieldsplit_0_fieldsplit_velocity_pc_type lu
    -fieldsplit_0_fieldsplit_pressure_ksp_rtol 1e-10
    -fieldsplit_0_fieldsplit_pressure_pc_type jacobi
  -fieldsplit_temperature_ksp_type preonly
  -fieldsplit_temperature_pc_type lu
\end{semiverbatim}
\onslide<4>
\magenta{\href{http://www.mcs.anl.gov/petsc/petsc-as/snapshots/petsc-dev/src/snes/examples/tutorials/ex31.c.html}{ex31}}: $P_2/P_1$ Stokes Problem with Temperature on Unstructured Mesh
\medskip

Least-Squares Commutator + Upper Schur Comp. + Full Schur Comp.
\begin{semiverbatim}\scriptsize
-ksp_type fgmres -pc_type fieldsplit -pc_fieldsplit_0_fields 0,1
-pc_fieldsplit_1_fields 2 -pc_fieldsplit_type schur
-pc_fieldsplit_schur_factorization_type upper
  -fieldsplit_0_ksp_type fgmres -fieldsplit_0_pc_type fieldsplit
  -fieldsplit_0_pc_fieldsplit_type schur
  -fieldsplit_0_pc_fieldsplit_schur_factorization_type full
    -fieldsplit_0_fieldsplit_velocity_ksp_type preonly
    -fieldsplit_0_fieldsplit_velocity_pc_type lu
    -fieldsplit_0_fieldsplit_pressure_ksp_rtol 1e-10
    -fieldsplit_0_fieldsplit_pressure_pc_type jacobi
  -fieldsplit_temperature_ksp_type gmres
  -fieldsplit_temperature_pc_type lsc
\end{semiverbatim}
\end{overprint}

\begin{overprint}
\onslide<1>
\begin{equation*}
\begin{pmatrix}
      I        &  0 \\
    B^T A^{-1}  &  I
\end{pmatrix}
\begin{pmatrix}
\hat A & 0 \\
    0  & \hat S
\end{pmatrix}
\begin{pmatrix}
 I & A^{-1} B \\
 0 & I
\end{pmatrix}
\end{equation*}
\onslide<2>
\begin{equation*}
\begin{pmatrix}
      I        &  0 \\
    B^T D_A^{-1}  &  I
\end{pmatrix}
\begin{pmatrix}
\hat A & 0 \\
    0  & \hat S
\end{pmatrix}
\begin{pmatrix}
 I & D_A^{-1} B \\
 0 & I
\end{pmatrix}
\end{equation*}
\onslide<3>
\begin{equation*}
\begin{pmatrix}
  \begin{pmatrix}
         I        &  0 \\
     B^T A^{-1}  &  I
  \end{pmatrix}
  \begin{pmatrix}
    A & 0 \\
         0 & S
  \end{pmatrix}
  \begin{pmatrix}
    I & A^{-1} B \\
    0 & I
  \end{pmatrix}
  & 0 \\
  0 & L_T
\end{pmatrix}
\end{equation*}
\onslide<4>
\begin{equation*}
\begin{pmatrix}
  \begin{pmatrix}
         I        &  0 \\
     B^T A^{-1}  &  I
  \end{pmatrix}
  \begin{pmatrix}
    \hat A & 0 \\
         0 & \hat S
  \end{pmatrix}
  \begin{pmatrix}
    I & A^{-1} B \\
    0 & I
  \end{pmatrix}
  & G \\
  0 & \hat S_{\mathrm{LSC}}
\end{pmatrix}
\end{equation*}
\end{overprint}

\end{frame}

\begin{frame}{Resolution}
Traditional PDE codes:
\medskip
\begin{itemize}
  \item<1-> Cannot compare different discretizations
  \begin{itemize}
    \item {\scriptsize\magenta{\bf\href{http://www.springerlink.com/content/1352h6578v745011/}{Automated FEM Discretizations for the Stokes Equation}}, BIT, 2008}

    \item {\scriptsize\green{\href{http://doi:10.1137/08073901X}{Efficient Assembly of H(div) and H(curl) Conforming Finite Elements}}, SISC, 2009}
  \end{itemize}
  \medskip
  \item<2-> Compare different mesh types
  \begin{itemize}
    \item {\scriptsize\magenta{\bf\href{http://onlinelibrary.wiley.com/doi/10.1002/jgrb.50217/abstract}{A Domain Decomposition Approach to Implementing Fault Slip in Finite-Element Models of Quasi-static and Dynamic Crustal Deformation}}, JGR, 2013}
  \end{itemize}
  \medskip
  \item<3-> Run 1D, 2D, and 3D problems
  \begin{itemize}
    \item {\scriptsize {\bf Ibid.}}
  \end{itemize}
  \medskip
  \item<4-> Enabling an optimal solver without programming
  \begin{itemize}
    \item {\scriptsize {\bf Ibid.}}

    \item {\scriptsize\magenta{\bf\href{http://www.mcs.anl.gov/uploads/cels/papers/P2017-0112.pdf}{Composable linear
    solvers for multiphysics}}, IPDPS, 2012}

    \item {\scriptsize\green{\href{http://www.sciencedirect.com/science/article/pii/S0031920110002098}{On the rise of strongly tilted mantle plume tails}}, PEPI, 2011}
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Future Work}\LARGE

\begin{itemize}
  \item Unify FEM and FVM residual evaulation
  \bigskip
  \item Batched integration on accelerators
  \bigskip
  \item Integrate auxiliary fields
  \bigskip
  \item Incorporate cell problems for coefficients
\end{itemize}
\end{frame}

\section{Applications}

\begin{frame}{PyLith}
\note{I think the biggest impact of PyLith has been that more people are creating models for crustal deformation with 3-D fault geometry and 3-D physical properties in quasi-static models of coseismic, postseismic, and interseismic crustal deformation. This improves the accuracy of the models, permitting better constraints on the rheologies of the lower crust and upper mantle and on earthquake slip.}

\begin{tabular}{lc}
\raisebox{1in}{\multirow{2}{*}{\parbox[c]{3.3in}{\Large\raggedright \magenta{\href{http://www.geodynamics.org/cig/software/pylith/user_resources}{PyLith}} is an open source, parallel
  simulator for crustal deformation problems developed by \magenta{\href{http://people.cs.uchicago.edu/~knepley/}{myself}},\\ \magenta{\href{https://profile.usgs.gov/baagaard}{Brad
  Aagaard}}, and \magenta{\href{http://www.geodynamics.org/cig/Members/willic3}{Charles Williams}}. PyLith employs a
  finite element discretization on unstructured meshes and is built on the \magenta{\href{http://www.mcs.anl.gov/petsc}{PETSc}} libraries from
  ANL.}}} & \vbox{
    \hbox{\includegraphics[width=1.2in,height=1.2in]{figures/Collaborators/BradAagaard.jpg}}
    \smallskip
    \hbox{Brad Aagaard}
  } \\
  & \vbox{
      \hbox{\includegraphics[width=1.2in,height=1.2in]{figures/Collaborators/CharlesWilliams.jpg}}
      \smallskip
      \hbox{Charles Williams}
    }
\end{tabular}
\end{frame}
%
\begin{frame}{PyLith}
\begin{itemize}
  \item<1-> Multiple problems
  \begin{itemize}
    \item<2-> Dynamic rupture

    \item<3-> Quasi-static relaxation
  \end{itemize}

  \item<4-> Multiple models
  \begin{itemize}
    \item<4-> Fault constitutive models

    \item<5-> Nonlinear visco-elastic-plastic

    \item<5-> Finite deformation
  \end{itemize}

  \item<6-> Multiple Meshes
  \begin{itemize}
    \item<6-> 1D, 2D, 3D

    \item<6-> Hex and tet meshes
  \end{itemize}
\end{itemize}
\only<2-3>{\begin{textblock}{0.5}[0.5,0.5](0.6,0.7)\includegraphics[width=\textwidth]{/PETSc3/cig/kinrup/figs/tpv13-2d_mesh.png}\end{textblock}}
\note[item]{\scriptsize Finite-element mesh comprised of quadrilateral cells for SCEC spontaneous rupture benchmark TPV13-2D. The
  discretization size is 100 m on the fault surface and increases at a geometric rate of 2\% with distance from the
  fault. We employ this same spatial variation of the discretization size in the 3-D model.}
\only<3>{\begin{textblock}{0.5}[0.5,0.5](0.85,0.45)\includegraphics[width=\textwidth]{/PETSc3/cig/pylith-git/doc/userguide/benchmarks/strikeslip/figs/error_tet4_1000m.png}\end{textblock}}
\only<4-5>{\begin{textblock}{0.5}[0.5,0.5](0.8,0.42)\includegraphics[width=\textwidth]{figures/PyLith/tpv13_ruptime_comparison.pdf}\end{textblock}}
\note[item]{\scriptsize Rupture time contours (0.5 s interval) for SCEC spontaneous rupture benchmark TPV13. (a) Effect of
  discretization size and (b) demonstration of code verifi- cation via excellent agreement among PyLith and three other
  dynamic rupture modeling codes [Harris et al., 2011]. The contours for PyLith and Kaneko (spectral element code) are
  nearly identical.}
\only<5>{\begin{textblock}{0.5}[0.5,0.5](0.80,0.9)\includegraphics[width=\textwidth]{/PETSc3/cig/kinrup/figs/savageprescott_soln.png}\end{textblock}}
\note[item]{\scriptsize Deformation (exaggerated by a factor of 5000) 95\% of the way through earthquake cycle 10 of the Savage and
  Prescott benchmark, which involves viscoelastic relaxation over multiple earthquake cycles on a vertical, strike-slip
  fault. The coordinates are in units of elastic layer thickness and the displacement field is in units of coseismic
  slip. The locking depth is one-half of the thickness of the elastic layer. We refine the hexahedral mesh by a factor
  of three near the center of the domain. Figure 8 compares profiles along y=0 with the analytic solution.}
\only<6->{\begin{textblock}{0.3}[0.5,0.5](0.65,0.35)\includegraphics[height=\textwidth]{figures/PyLith/example_tri3.png}\end{textblock}}
\only<7->{\begin{textblock}{0.3}[0.5,0.5](0.75,0.5)\includegraphics[height=\textwidth]{figures/PyLith/example_quad4.png}\end{textblock}}
\only<8->{\begin{textblock}{0.3}[0.5,0.5](0.85,0.7)\includegraphics[height=\textwidth]{figures/PyLith/example_tet4.png}\end{textblock}}
\only<9->{\begin{textblock}{0.3}[0.5,0.5](0.95,0.85)\includegraphics[height=\textwidth]{figures/PyLith/example_hex8.png}\end{textblock}}
\end{frame}

\begin{frame}{Classical DFT in Three Dimensions}

\parbox{0.5\textwidth}{\raggedright
I wrote the first \magenta{\href{https://bitbucket.org/knepley/cdft/wiki/Home}{3D Classical DFT}} with true hard
sphere chemical potential using fundamental measure theory. It used an $\mathcal{O}(N \log N)$ algorithm based upon the
FFT. We examined the physics of ion channels, such as the ryanodine receptor.
\green{\href{https://ftp.rush.edu/users/molebio/Dirk/papers/DFT ES functional PRE 03.pdf}{Advanced electrostatics}} 
allowed \green{\href{https://ftp.rush.edu/users/molebio/Dirk/papers/RyR Ca selectivity energetics BJ 08.pdf}{prediction}} of
I-V curves for 100+ solutions, including polyvalent species.
}

\bigskip

The implementation is detailed in {\scriptsize \magenta{\href{http://jcp.aip.org/resource/1/jcpsa6/v132/i12/p124101_s1}{An Efficient Algorithm for Classical Density Functional Theory in Three Dimensions: Ionic Solutions}}, JCP, 2012}.

\only<1>{\begin{textblock}{0.5}[0.5,0.5](0.8,0.5)\includegraphics[width=\textwidth]{figures/DFT/ryanodineReceptor.jpg}\end{textblock}}
\only<2>{\begin{textblock}{0.5}[0.5,0.5](0.8,0.5)\includegraphics[width=\textwidth]{figures/DFT/Fig1b.jpg}\end{textblock}}
\only<3>{\begin{textblock}{0.5}[0.5,0.5](0.8,0.35)\includegraphics[width=\textwidth]{figures/DFT/rhoK__40R__150K__1e-4Ca.png}\end{textblock}}
\only<3>{\begin{textblock}{0.5}[0.5,0.5](0.8,0.65)\includegraphics[width=\textwidth]{figures/DFT/Delta_muHSK__40R__150K__1e-4Ca.png}\end{textblock}}

  \note{Problem: 1D DFT is great, predicts I-V curves for 100+ solutions including multivlaent species, but cannot see
  details inside the channel. Need 3D, but too expensive.}
\end{frame}

\begin{frame}{People Using My Mesh}

\vskip-1in
\begin{overprint}
\onslide<1-3>
\magenta{\href{https://www.math.lsu.edu/~bourdin/Fracture.html}{Blaise Bourdin}}
\begin{itemize}
  \item Full variational formulation
  \begin{itemize}
    \item Phase field for crack

    \item Linear or quadratic penalty
  \end{itemize}
  \item Cracks are \red{not prescribed} 
  \begin{itemize}
    \item \alt<2>{\blue{Arbitrary crack geometry}}{Arbitrary crack geometry}
    \item \alt<3>{\blue{Arbitrary crack intersections}}{Arbitrary crack intersections}
  \end{itemize}
  \item Multiple materials and composite toughness
\end{itemize}
\only<2-3>{\begin{textblock}{0.3}[0.5,0.5](0.9,0.45)\includegraphics[width=\textwidth]{figures/Fracture/BranchingCrack}\end{textblock}}
\only<3>{\begin{textblock}{0.8}[0.5,0.5](0.40,0.85)\includegraphics[width=\textwidth]{figures/Fracture/3DCrackSystem}\end{textblock}}

\onslide<4->
\magenta{\href{http://www.hiflow3.org/}{HiFlow3}}
\begin{itemize}
  \item Multi-purpose finite element software
  \item Arose from EMCL at Karlsruhe Institute of Technology 
  \item<5-> Flow behavior in the human respiratory system
\end{itemize}
\only<5->{\begin{textblock}{0.5}[0.5,0.5](0.30,0.75)\includegraphics[width=\textwidth]{figures/Plex/Applications/HiFlow3_Lungs.png}\end{textblock}}
\only<6>{\begin{textblock}{0.45}[0.5,0.5](0.82,0.75)\includegraphics[width=\textwidth]{figures/Plex/Applications/HiFlow3_Lungs_Closeup.png}\end{textblock}}
\end{overprint}
\end{frame}

\begin{frame}{People Using PETSc Composable Solvers}

\vskip-1.2in
\begin{overprint}
\onslide<1>
TerraFERMA (Cian Wilson and Marc Spiegelman, Columbia)
\begin{itemize}
  \item Magma Dynamics

  \item Flexible model builder

  \item Finite element

  \item Nested FieldSplit solver
\end{itemize}

\onslide<2-3>
Sam Weatherley and Richard Katz (Oxford)
\begin{itemize}
  \item Magma Dynamics

  \item Finite volume

  \item Nested FieldSplit solver

  \item Small scale parallel ($10^2$--$10^3$)
\end{itemize}

\onslide<4>
PTatin (Dave May, ETHZ)
\begin{itemize}
  \item Lithospheric and Mantle dynamics

  \item Finite element

  \item Lagrangian particles

  \item Nested FieldSplit solver

  \item Large scale parallel ($10^3$--$10^5$)
\end{itemize}
\end{overprint}

\only<1->{\begin{textblock}{0.5}[0.5,0.5](0.82,0.50)\includegraphics[width=\textwidth]{figures/Magma/PorosityAndCompaction.pdf}\end{textblock}}
\only<2 >{\begin{textblock}{0.5}[0.5,0.5](0.30,0.80)\includegraphics[width=\textwidth]{figures/Magma/ridgePlot.pdf}\end{textblock}}
\only<3->{\begin{textblock}{0.95}[0.5,0.5](0.55,0.80)\includegraphics[width=\textwidth]{figures/Magma/ridgePorosity.jpg}\end{textblock}}
\only<4->{\begin{textblock}{0.35}[0.5,0.5](0.30,0.8)\includegraphics[width=\textwidth]{figures/NonNewtonianFluids/DrunkenSeaman.pdf}\end{textblock}}
\end{frame}

\begin{frame}{Impact of Mathematics on Science}

\begin{textblock}{0.5}[0.5,0.5](0.5,0.13)\tikz\fill[,decorate,decoration={text along path, text=|\bf\LARGE|Computational Science}] (0,0) arc (250:330:8cm);\end{textblock}
\begin{center}\includegraphics[width=0.8\textwidth]{figures/ScientificComputing/ManhattanBridge.jpg}\end{center}

\note{Computational Science is the Bridge between mathematics and science}

\Large Computational Leaders have always\\\quad embraced the latest technology\\\qquad and been inspired by physical problems.
\begin{overprint}
\onslide<1>
\begin{center}
\begin{tabular}{cc}
\includegraphics[height=0.2\textwidth]{figures/Scientists/LeonhardEuler.jpg} & \includegraphics[height=0.2\textwidth]{figures/Hardware/SlideRuleExample.png}
\end{tabular}
\end{center}
\onslide<2>
\begin{center}
\begin{tabular}{cc}
\includegraphics[height=0.2\textwidth]{figures/scientists/NorbertWiener.jpg} & \includegraphics[height=0.2\textwidth]{figures/scientists/JohnVonNeumann.jpg}
\end{tabular}
\end{center}
\onslide<3>
\begin{center}
\begin{tabular}{cc}
\includegraphics[height=0.2\textwidth]{figures/Hardware/ORNL_Titan.jpg} & \raisebox{2em}{\Huge\red{\bf{PETS}c}}
\end{tabular}
\end{center}
\onslide<4>
\bigskip
\begin{center}
{\Huge Enabling Scientific Discovery}
\end{center}
\end{overprint}
\note[item]{The new technology is massive parallelism and numerical libraries}

\note[item]{Using the Latest Tools to support the Applied Math Mission}

 \note[item]{Tradition is theory/analysis and now computation}
\end{frame}

\section*{Additional Slides}
%
\begin{frame}
\begin{center}\Huge Additional Slides\end{center}
\end{frame}

\input{slides/PETSc/ProgrammingWithOptions.tex}

\begin{frame}{Nonlinear Preconditioning}
\begin{itemize}
  \item Major Point: Composable structures for computation reduce system complexity and generate real application benefits

  \item Minor Point: Numerical libraries are communication medium for scientific results

  \item Minor Point: Optimal solvers can be constructed on the fly to suit the problem

  \item Slides for Stokes PCs

  \item Slide with programming with options
\end{itemize}
\end{frame}

\begin{frame}{Nonlinear Preconditioning}
\begin{itemize}
  \item NPC in PETSc

  \item Paper with Barry and Peter

  \item Cite Peter and Jed paper for use cases
\end{itemize}
\end{frame}

\begin{frame}{Parallel Fast Multipole Method}
\begin{itemize}
  \item Using mesh partitioner to develop schedule removes load balance barrier

  \item Partitioner can be proved to work with Teng's result

  \item Simple parallelization can be proved to work with overlap

  \item Ex: Work with May, 512 GPU paper
\end{itemize}
\end{frame}

\begin{frame}{GPU Computing}
\begin{itemize}
  \item Papers with Andy about FEM Integration

  \item Paper with PETSc about solvers

  \item Conferences with Yuen
\end{itemize}
\end{frame}

\end{document}
