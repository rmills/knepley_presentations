\documentclass{beamer}

\input{tex/talkPreamble.tex}

\title[FEM]{Finite Element Assembly on Arbitrary Meshes}
\author[M.~Knepley]{Matthew~Knepley}
\date[KAUST]{Department of Applied Mathematics and Computational Science\\King Abdullah University of Science and Technology\\Apr 5, 2010}
% - Use the \inst command if there are several affiliations
\institute[UC]{
  Computation Institute\\
  University of Chicago

  \smallskip

  Department of Molecular Biology and Physiology\\
  Rush University Medical Center
}
\subject{FEM Implementation}

\begin{document}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.3]{figures/logos/rush_logo.png}\hspace{1.0in}
  \includegraphics[scale=0.24]{figures/logos/uc-logo-official.jpg}
  \end{center}
  \vskip0.4in
\end{frame}

\section{Introduction}
%
\begin{frame}<testing>
\frametitle{Abstract}
The implementation of modern scientific codes has become a daunting endeavor. Very often, application scientists are
unwilling or unable to make use of the latest theoretical advances due to the complexity of programming. We will examine
computations arising from both finite element discretization and the fast multipole method in order to illustrate how
better computational abstractions can alleviate the programmability burden while simultaneously providing improved
performance. In particular, we will examine operator assembly and mesh distribution from FEM methods, and a scalable
parallelization strategy for FMM, to show that better mathematical abstractions produce more flexible and efficient code.
\end{frame}
%
\begin{frame}{Collaborators}

\begin{itemize}
  \item Automated FEM
  \begin{itemize}
    \item Andy Terrel (UT Austin)
    \item Ridgway Scott (UChicago)
    \item Rob Kirby (Texas Tech)
  \end{itemize}

  \item Sieve
  \begin{itemize}
    \item Dmitry Karpeev (ANL)
    \item Peter Brune (UChicago)
    \item Anders Logg (Simula)
  \end{itemize}

  \item PyLith
  \begin{itemize}
    \item Brad Aagaard (USGS)
    \item Charles Williams (NZ)
  \end{itemize}
\end{itemize}
\end{frame}
%
%
\section{Mesh Distribution}
\begin{frame}{Main Point}

\Huge
Rethinking meshes\\
\bigskip
\pause
produces a simple FEM interface\\
\bigskip
\pause
and good code reuse.
\end{frame}
%
\input{slides/ScientificComputing/Problems.tex}
%
\subsection{Sieve}
\input{slides/Sieve/Overview.tex}
\input{slides/Sieve/TopologicalDatabase.tex}
\input{slides/Sieve/DoubletNavigation.tex}
\input{slides/Sieve/SieveDef.tex}
\input{slides/MeshDistribution/MeshDual.tex}
%
\subsection{Section}
\input{slides/Section/DoubletSectionNavigation.tex}
\input{slides/Section/SectionDef.tex}
%
\subsection{Completion}
\input{slides/Overlap/DoubletOverlap.tex}
\input{slides/Overlap/Completion.tex}
\input{slides/Overlap/CompletionUses.tex}
\input{slides/Section/Implementation/Completion.tex}
\input{slides/Section/Implementation/CompletionDiagram.tex}
\input{slides/Section/Implementation/SectionHierarchy.tex}
%
\subsection{Distribution}
\input{slides/MeshDistribution/SectionDistribution.tex}
\input{slides/MeshDistribution/SieveDistribution.tex}
\input{slides/MeshDistribution/MeshDistribution.tex}
\input{slides/MeshDistribution/MeshPartition.tex}
\input{slides/Completion/DoubletDistribution.tex}
\input{slides/MeshDistribution/DistributionExample2D.tex}
\input{slides/MeshDistribution/DistributionExample3D.tex}
%
\subsection{Interfaces}
\input{slides/Sieve/OverviewII.tex}
\input{slides/Sieve/GlobalAndLocal.tex}
\input{slides/Sieve/HierarchicalInterfaces.tex}
\input{slides/Sieve/UnstructuredInterfaceBefore.tex}
\input{slides/Sieve/CombinatorialTopology.tex}
\input{slides/Sieve/UnstructuredInterfaceAfter.tex}
%
%
\section{Unifying Paradigm}
\input{slides/Sieve/HierarchicalAbstractions.tex}
%
\subsection{DA}
\input{slides/DA/LocalEvaluation.tex}
\input{slides/DA/GhostValues.tex}
\input{slides/DA/LocalFunction.tex}
\input{slides/DA/BratuResidual.tex}
\input{slides/DA/LocalJacobian.tex}
\input{slides/DA/Vectors.tex}
\input{slides/DA/UpdatingGhosts.tex}
%
\subsection{Mesh}
\input{slides/Mesh/LocalEvaluation.tex}
%% Code for Laplacian is in ResdiualCode slide
\input{slides/PyLith/MultipleMeshTypes.tex}
%
\subsection{DMMG}
\input{slides/DM/LocalEvaluation.tex}
\input{slides/Optimal/DMMGIntegrationWithSNES.tex}
\input{slides/Optimal/Structured.tex}
%
\subsection{PCFieldSplit}
\input{slides/MultiField/LocalEvaluation.tex}
% Hopefully show code from BFBT or Spiegelman
\input{slides/MultiField/Preconditioning.tex}
%
%
\section{Finite Element Assembly}
\input{slides/FEM/MathematicsPuzzle.tex}
\input{slides/FEM/FEMComponents.tex}
%
\subsection{Layout}
\input{slides/FEM/SectionAllocation.tex}
%
\subsection{Integration}
\input{slides/FEM/FIAT.tex}
\input{slides/FEM/FIATIntegration.tex}
\input{slides/FEM/DofKind.tex}
\input{slides/FFC/FFC.tex}
%
\subsection{Assembly}
% Talk about partial assembly of operators
\input{slides/FEM/ResidualCode.tex}
\input{slides/FEM/BoundaryConditions.tex}
\input{slides/FEM/DirichletValues.tex}
\input{slides/FEM/DualBasisApplication.tex}
\input{slides/FEM/DirichletAssembly.tex}
%% Rework this slide to discuss how we handle these difficulties
%%\input{slides/FEM/ArbitraryElementDifficulties.tex}
%
%
\subsection{Examples}
\input{slides/PyLith/Overview.tex}
\input{slides/PyLith/MultipleMeshTypes.tex}
\input{slides/PyLith/CohesiveCells.tex}
\input{slides/PyLith/MeshSplitting.tex}
\input{slides/PyLith/StrikeSlipBenchmark.tex}
%
\input{slides/Fracture/Overview.tex}
%
%
\section*{Conclusions}
\begin{frame}
\frametitle{Conclusions}
\begin{center}
    {\Large Better mathematical abstractions\\bring concrete benefits}
\end{center}

\begin{itemize}
  \item Vast reduction in complexity
  \begin{itemize}
    \item Dimension and mesh independent code

    \item Complete serial code reuse
  \end{itemize}

  \bigskip

  \item Opportunites for optimization
  \begin{itemize}
    \item Higher level operations missed by traditional compilers

    \item Single communication routine to optimize
  \end{itemize}

  \bigskip

  \item Expansion of capabilities
  \begin{itemize}
    \item Arbitrary elements

    \item Unstructured multigrid

    \item Multilevel algorithms
  \end{itemize}
\end{itemize}
\end{frame}
\input{slides/FEniCSTutorial/References.tex}
\input{slides/PETSc/Experimente.tex}

\ifx\@restTalk\@empty

\else
  \relax
\fi

\end{document}
