\documentclass[dvipsnames]{beamer}

\input{tex/talkPreamble.tex}

\beamertemplatenavigationsymbolsempty

\title[Magma]{Computational Considerations for Magma Dynamics}
\author[M.~Knepley]{Matthew~Knepley and Tobin Isaac}
\date[MIM]{Melt in the Mantle Workshop\\Isaac Newton Centre\\Cambridge University, UK \qquad February 19, 2016}
% - Use the \inst command if there are several affiliations
\institute[Rice]{
  Computational and Applied Mathematics\\
  Rice University\\
  \smallskip
  Computation Institute\\
  University of Chicago
}
\subject{Me}

\begin{document}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.24]{figures/logos/uc-logo-official.jpg}\hspace{1.0in}
  \includegraphics[scale=0.10]{figures/logos/RiceLogo_TMCMYK300DPI.jpg}
  \end{center}
  \vskip0.4in
\end{frame}
%
\begin{frame}<testing>{Abstract}
  60 min

  Abstract:

  The optimal solver for a given problem depends not only on the equations being solved, but the boundary conditions,
  discretization, parameters, problem regime, and machine architecture. This interdependence means that \textit{a priori} 
  selection of a solver is a fraught activity and should be avoided at all costs. While there are many packages which
  allow flexible selection and (some) combination of linear solvers, this understanding has not yet penetrated the world
  of nonlinear solvers. We will briefly discuss techniques for combining nonlinear solvers, theoretical underpinnings,
  and show concrete examples from magma dynamics.

  The same considerations which are present for solver selection should also be taken into account when choosing a
  discretization. However, scientific software seems even less likely to allow the user freedom here than in the
  nonlinear solver regime. We will discuss tradeoffs involved in choosing a discretization of the magma dynamics
  problem, and demonstrate how a flexible mechanism might work using examples from the PETSc libraries from Argonne
  National Laboratory.
\end{frame}
%
\begin{frame}{Collaborators}
\begin{center}
\begin{tabular}{lc}
  \raisebox{0.6in}{\parbox[c]{15em}{\LARGE Magma Dynamics\\Collaboration}} &
  \vbox{
    \hbox{\includegraphics[width=1.2in,height=1.2in]{figures/Collaborators/RichardKatz.jpg}}
    \smallskip
    \hbox{\ \ \ Richard Katz}
  } \\
  \raisebox{0.6in}{\parbox[c]{10em}{\LARGE Timestepping\\Collaboration}} &
  \vbox{
    \hbox{\includegraphics[width=1.2in,height=1.2in]{figures/Collaborators/JedBrown.jpg}}
    \smallskip
    \hbox{\ \ \ \ \ Jed Brown}
  }
\end{tabular}
\end{center}
\end{frame}
\begin{frame}{Collaborators}
\begin{center}
\begin{tabular}{lc}
  \raisebox{0.6in}{\parbox[c]{20em}{\LARGE Adaptive Mesh Refinement\\Collaboration}} &
  \vbox{
    \hbox{\includegraphics[width=1.2in,height=1.2in]{figures/PETSc/TobyIssac.jpg}}
    \smallskip
    \hbox{\ \ \ Toby Isaac}
  }
\end{tabular}
\end{center}
\end{frame}
%
\begin{frame}{Main Point}
\begin{center}
  \LARGE What am I going to talk about?
\end{center}
\Large
\bigskip
\begin{itemize}
  \item Nonlinear Solvers for the Mechanical Model
  \medskip
  \item Discretization of the Mechanical Model
  \medskip
  \item \alt<2>{\sout{Meshing for the Mechanical Model}}{Meshing for the Mechanical Model}
\end{itemize}
\end{frame}
%
\begin{frame}{Main Point}
\begin{center}
  \LARGE Why is this important?
\end{center}
\Large
\bigskip
\begin{itemize}
  \item \blue{Comparison} is essential for making informed algorithmic choices
  \medskip
  \item Comparison in a \red{single code} seems necessary
  \medskip
  \item Current codes make it difficult to compare meshes, discretizations, and multilevel solvers
\end{itemize}
\end{frame}
%
\section{Nonlinear Solvers}
%
\begin{frame}{Main Points}\LARGE

We can construct nonlinear solvers that are\\
\quad more robust than Newton.\\

\pause
\bigskip

We can construct nonlinear solvers that are\\
\quad more rapidly convergent than Newton.\\

\pause
\bigskip

Picard is never really acceptable.
\end{frame}
%
% TODO: Add simple slide about NPC?
% TODO: Add explanation slide about nonlinear solver choices?
% TODO: Add simple slide about NDI predictions?
% TODO: Problems with Newton convergence when using enthalpy method might be alleviated using NGMRES+NK
%
\subsection{Problem Definition}
\input{slides/Magma/Equations.tex}
\input{slides/Magma/Benchmarks.tex}
%
\subsection{Newton for pure FEM Formulation}
\input{slides/Magma/BEuler.tex}
\input{slides/Magma/NK.tex}
\input{slides/Magma/NKConvEarly.tex}
\input{slides/Magma/NKConvLater.tex}
\input{slides/Magma/NKConvFail.tex}
%
\subsection{Composition Strategies}
\input{slides/SNES/BasicSystem.tex}
\input{slides/SNES/LeftPrec.tex}
\input{slides/SNES/AdditiveNPrec.tex}
\input{slides/SNES/LeftNPrec.tex}
\input{slides/SNES/MultiplicativeNPrec.tex}
\input{slides/SNES/RightNPrec.tex}
\input{slides/SNES/NPrecTable.tex}
%
\subsection{Solvers for pure FEM Formulation}
\input{slides/Magma/NK-NCG.tex}
\input{slides/Magma/NK-NCGConv.tex}
\input{slides/Magma/FAS-NK.tex}
\input{slides/Magma/FAS-NKConv.tex}
\input{slides/Magma/FAS-NGS.tex}
\input{slides/Magma/FAS-NGSConv.tex}
%
\subsection{Solvers for FEM+FVM Formulation}
%% Nonlinear solves are harder for discretizations with little diffusion, likely due to greater localization
\input{slides/Magma/SplitTS.tex}
\input{slides/Magma/MIMEX.tex}
\input{slides/Magma/NK-FV.tex}
\input{slides/Magma/NK-FVConvEarly.tex}
\input{slides/Magma/NK-FVConvLater.tex}
\input{slides/Magma/FAS-NK-FV.tex}
\input{slides/Magma/FAS-NK-FVConv.tex}
\input{slides/Magma/FAS-NGS-FV.tex}
\input{slides/Magma/FAS-NGS-FVConv.tex}
%
\section{Discretization}
%
\begin{frame}\Huge
\begin{center}
  Can't you just pick\\
\bigskip
the \textit{right} discretization?
\end{center}
\end{frame}
%
\begin{frame}{Continuous Galerkin}
\begin{center}\Large
  Can't you just use CG for everything?
\end{center}
\bigskip
\begin{itemize}
  \item<1-> Good idea for elliptic equations\\
            Mass and momentum conservation
  \medskip
  \item<2-> Bad for porosity advection (very diffusive)\\
            Easy to see in uniform advection of a porous block
  \medskip
  \item<3-> Solitary wave benchmark does not test diffusivity\\because you can make the wave stand almost still
  \medskip
  \item<4-> Shear band benchmark does not test diffusivity\\but localization will be artificially arrested
\end{itemize}
\end{frame}
%
\begin{frame}{Discontinuous Galerkin}
\begin{center}\Large
  Can't you just use DG for everything?
\end{center}
\bigskip
\begin{overprint}
\onslide<1-2>
\begin{itemize}
  \item<1-> Good idea for advection
  \medskip
  \item<2-> Bad idea for conservation of mass and momentum\\
            Forcing continuity results (morally) in penalization\\
            creating hard-to-solve systems of equations, resulting in \ldots
\end{itemize}
\onslide<3>
\begin{itemize}
  \item Good idea for advection
  \medskip
  \item Bad idea for conservation of mass and momentum\\
        Forcing continuity results (morally) in penalization\\
        creating hard-to-solve systems of equations, resulting in \ldots
\end{itemize}
\medskip
\begin{center}\includegraphics[width=1in]{figures/PETSc/SadDave.jpg}\end{center}
\onslide<4>
\begin{itemize}
  \item Good idea for advection
  \medskip
  \item Bad idea for conservation of mass and momentum\\
        Forcing continuity results (morally) in penalization
  \medskip
  \item Bad idea for efficiency\\
        \magenta{\href{http://onlinelibrary.wiley.com/doi/10.1002/zamm.201400274/full}{Lehmann, Luk\'a\u{c}ov\'a-Medvid'ov\'a, Kaus and Popov}}, ZAMM, 2015\\
        DG has higher cost for equivalent accuracy, resulting in \ldots
\end{itemize}
\onslide<5>
\begin{itemize}
  \item Good idea for advection
  \medskip
  \item Bad idea for conservation of mass and momentum\\
        Forcing continuity results (morally) in penalization
  \medskip
  \item Bad idea for efficiency\\
        DG has higher cost for equivalent accuracy, resulting in \ldots
\end{itemize}
\vspace{-3ex}
\begin{center}\includegraphics[width=1in]{figures/PETSc/SadDave.jpg}\end{center}
\end{overprint}
\end{frame}
%
\begin{frame}{Discontinuous Galerkin}
\begin{center}\Large
  DG makes a lot of sense for the advection equation
\end{center}
\bigskip
\begin{itemize}
  \item Not diffusive
  \medskip
  \item Handles sharp fronts
  \medskip
  \item Can be high order accurate
  \medskip
  \item Implicit formulation makes sense
  \medskip
  \item Needs a limiter to prevent oscillation
\end{itemize}
\end{frame}
%
\begin{frame}{Finite Volume}
\begin{center}\Large
  FV also makes sense for the advection equation
\end{center}
\bigskip
\begin{itemize}
  \item Not diffusive
  \medskip
  \item Handles sharp fronts
  \medskip
  \item Can be low order accurate
  \medskip
  \item Implicit formulation does not make sense
  \medskip
  \item Needs a limiter to prevent oscillation
\end{itemize}
\end{frame}
%
\begin{frame}{Composability}
\begin{center}\Large
  Can we mix discretizations?
\end{center}
\bigskip
\begin{itemize}
  \item FEniCS/Firedrake can do CG+DG
  \medskip
  \item Deal.II can do CG+DG
  \medskip
  \item PETSc can do CG+FV
\end{itemize}
\end{frame}
%
\input{slides/Magma/DiscretizationFEM.tex}
\input{slides/Magma/DiscretizationFVM.tex}
%
\begin{frame}{Adaptive Mesh Refinement}
\begin{center}\LARGE
  Should we use AMR?
\end{center}
\bigskip
\begin{itemize}
  \item Uniform refinement is scalable, but
  \medskip
  \begin{itemize}
    \item<2-> AMR achieves higher accuracy on fixed resources, or
    \medskip
    \item<3-> AMR uses less memory for fixes accuracy
  \end{itemize}
  \medskip
  \item<4-> AMR could affect the nonlinear conditioning
  \medskip
  \item<5-> AMR complicates multilevel solves
\end{itemize}
\end{frame}
%
\begin{frame}{AMR Implementation}
\begin{center}\LARGE
  Can we use AMR?
\end{center}
\bigskip
\begin{itemize}
  \item LibMesh can do AMR
  \medskip
  \item Deal.II can do AMR
  \medskip
  \item PETSc can do AMR
\end{itemize}
\end{frame}
%
\begin{frame}{AMR Shear Bands}
\includegraphics[width=0.95\textwidth]{magmaAMR.pdf}
\end{frame}
%
%
\section{Conclusions}
%
\begin{frame}{Conclusions}\Huge
\ \ \ We need composable,\\
\pause
\medskip
\ \ \ \quad extensible systems\\
\pause
\medskip
\ \ \ \quad to match numerics\\
\pause
\medskip
\ \ \ \quad\quad to the physics.
\end{frame}
%
%
\section{Non-asymptotic Convergence}
%
\subsection{Convergence Rates}
\input{slides/NDI/RateOfConvergence.tex}
\input{slides/NDI/NDI.tex}
\input{slides/NDI/Newton.tex}
\input{slides/SNES/NPCLeftVsRight.tex}
\input{slides/NDI/RP.tex}
\input{slides/NDI/NonAbelian.tex}
%
\subsection{Convergence Theory}
\input{slides/NDI/ComposedRates.tex}
\input{slides/NDI/MultidimensionalInductionTheorem.tex}
\input{slides/NDI/ComposedNewtonConv.tex}
\input{slides/NDI/AsymmetryExample.tex}
%
\section{AMR}
\input{slides/AMR/OverviewDMAMR.tex}
\input{slides/AMR/OverviewP4est.tex}
%
\begin{frame}[fragile]{Integrating solvers with p4est (or any format)}
  \begin{itemize}
    \item
      \textcolor{blue}{Comparison} of numerical methods (discretizations
      (FE/FV), solvers (FAS/NASM/etc.))
      requires \textcolor{red}{implementation} of numerical methods
    \item
      Implementations for exotic formats can be expensive (man hours) and
      error prone
    \item
      Numerical methods we are interested in are naturally described with
      operations on unstructured meshes (DMPlex)
  \end{itemize}
  \pause
\begin{cprog}
  DoSomething_p4est (DM dmp4est)
  {
    if (dmp4est->ops->dosomething) { /* optimized implementation */
      (dmp4est->ops->dosomething) (dmp4est);
    } else {
      DM dmplex;

      DMConvert (dmp4est, &dmplex);
      DoSomething (dmplex);
      DMDestroy (&dmplex);
    }
  }
\end{cprog}
\end{frame}
%
\input{slides/AMR/NonconformalPlex.tex}
\input{slides/AMR/HangingNodes.tex}

\end{document}
