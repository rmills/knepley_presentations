\documentclass{beamer}

\input{tex/talkPreamble.tex}

\title[PETSc]{The\\{\bf P}ortable {\bf E}xtensible {\bf T}oolkit for {\bf S}cientific {\bf C}omputing}
\author[M.~Knepley]{Matthew~Knepley}
\date[JSG '11]{PETSc Tutorial\\Jackson School of Geosciences\\University of Texas at Austin, TX \qquad September 7, 2011}
% - Use the \inst command if there are several affiliations
\institute[UC]{
\begin{tabular}{cc}
  Mathematics and Computer Science Division & Computation Institute\\
  Argonne National Laboratory               & University of Chicago
\end{tabular}
}

\subject{PETSc}

% If you wish to uncover everything in a step-wise fashion, uncomment the following command: 
%%\beamerdefaultoverlayspecification{<+->}

\begin{document}

\frame{
\titlepage
\begin{center}
\includegraphics[scale=0.3]{figures/logos/doe-logo.jpg}\hspace{1.0in}
\includegraphics[scale=0.3]{figures/logos/anl-logo-black.jpg}\hspace{1.0in}
\includegraphics[scale=0.24]{figures/logos/uc-logo-official.jpg}
\end{center}
}

\section{Getting Started with PETSc}
%
  \subsection{What is PETSc?}
\begin{frame}{Unit Objectives}

{\Large
  \begin{itemize}
    \item Introduce PETSc

    \medskip

    \item Download, Configure, Build, and Run an Example

    \medskip

    \item Empower students to learn more about PETSc
  \end{itemize}
}
\end{frame}
  \input{slides/PETSc/Tutorial/WhatINeedFromYou.tex}
  \input{slides/PETSc/Tutorial/AskQuestions.tex}
  \input{slides/PETSc/Tutorial/HowWeCanHelp.tex}
%
\begin{frame}{Tutorial Repositories}

\href{http://petsc.cs.iit.edu/petsc/tutorials/SimpleTutorial}{\magenta{http://petsc.cs.iit.edu/petsc/tutorials/SimpleTutorial}}
\begin{itemize}
  \item Very simple

  \item Shows how to create your own project

  \item Uses multiple languages
\end{itemize}
\end{frame}
%
  \input{slides/PETSc/OriginalImpetus.tex}
  \input{slides/PETSc/RoleOfPETSc.tex}
  \input{slides/PETSc/WhatIsPETSc.tex}
  \input{slides/PETSc/Timeline.tex}
  \input{slides/PETSc/PETScDevelopers.tex}
%
  \subsection{Who uses PETSc?}
  \input{slides/PETSc/WhoUsesPETSc.tex}
  \input{slides/PETSc/Limits.tex}
  \input{slides/PyLith/Overview.tex}
  \input{slides/PyLith/MultipleMeshTypes.tex}
  \input{slides/Magma/Overview.tex}
  %% TODO: Put links to videos on Fracture page
  \input{slides/Fracture/Overview.tex}
  \input{slides/FMM/VortexMethod.tex}
  \input{slides/FMM/GravityAnomaly.tex}
  \input{slides/GradeTwo/Overview.tex}
  \input{slides/PETSc/Surgery.tex}
%
  \subsection{Stuff for Windows}
  \input{slides/PETSc/MSWindows.tex}
%
  \subsection{How can I get PETSc?}
  \input{slides/PETSc/DownloadingPETSc.tex}
  \input{slides/PETSc/CloningPETSc.tex}
  \input{slides/PETSc/UnpackingPETSc.tex}
  \input{slides/PETSc/Tutorial/Exercise1.tex}
%
  \subsection{How do I Configure PETSc?}
  \input{slides/PETSc/ConfiguringPETSc.tex}
  \input{slides/PETSc/ConfiguringSieve.tex}
  \input{slides/PETSc/AutomaticDownloads.tex}
  \input{slides/PETSc/Tutorial/Exercise2.tex}
%
  \subsection{How do I Build PETSc?}
  \input{slides/PETSc/BuildingPETSc.tex}
  \input{slides/PETSc/Tutorial/Exercise3.tex}
  \input{slides/PETSc/Tutorial/Exercise4.tex}
%
  \subsection{How do I run an example?}
  \input{slides/PETSc/RunningPETSc.tex}
  \input{slides/PETSc/RunningPETScWithPython.tex}
  \input{slides/MPI/UsingMPI.tex}
  \input{slides/MPI/MPIConcepts.tex}
  \input{slides/MPI/AlternativeMemoryModels.tex}
  \input{slides/PETSc/CommonViewingOptions.tex}
  \input{slides/PETSc/CommonMonitoringOptions.tex}
  \input{slides/PETSc/Tutorial/Exercise5.tex}
  \input{slides/PETSc/Tutorial/Exercise6.tex}
%
  \subsection{How do I get more help?}
  \input{slides/PETSc/GettingMoreHelp.tex}
%
%
\section{PETSc Integration}
%
  \subsection{Initial Operations}
  \input{slides/PETSc/Integration/ApplicationIntegration.tex}
  \input{slides/PETSc/Integration/PETScIntegration.tex}
  \input{slides/PETSc/Integration/Stages.tex}
  \input{slides/PETSc/Integration/Initialization.tex}
  \input{slides/PETSc/Integration/Profiling.tex}
  \input{slides/PETSc/Integration/CommandLineProcessing.tex}
%
  \subsection{Vector Algebra}
  \input{slides/PETSc/Integration/VectorAlgebra.tex}
  \input{slides/PETSc/Integration/ParallelAssembly.tex}
  \input{slides/PETSc/Integration/VectorAssembly.tex}
  \input{slides/PETSc/Integration/MisguidedVectorAssembly.tex}
  \input{slides/PETSc/Integration/EfficientVectorAssembly.tex}
  \input{slides/PETSc/VectorOperations.tex}
  \input{slides/PETSc/Integration/LocalVectors.tex}
  \input{slides/PETSc/Integration/VecGetArray.tex}
%
  \subsection{Matrix Algebra}
  \input{slides/PETSc/Integration/MatrixAlgebra.tex}
  \input{slides/PETSc/Integration/MatrixCreation.tex}
  \input{slides/PETSc/Integration/MatrixPolymorphism.tex}
  \input{slides/PETSc/Integration/MatrixAssembly.tex}
  \input{slides/PETSc/Integration/MisguidedMatrixAssembly.tex}
%%\input{part2/slides/ParallelSparseMatrix}
  \input{slides/PETSc/Integration/EfficientMatrixAssembly.tex}
  \input{slides/PETSc/Integration/WhyArePETScMatricesThatWay.tex}
%
  \subsection{Algebraic Solvers}
  \input{slides/PETSc/Experimente.tex}
  \input{slides/PETSc/Integration/SolverTypes.tex}
  \input{slides/PETSc/Integration/LinearSolvers.tex}
  \input{slides/PETSc/Integration/NonlinearSolvers.tex}
  \input{slides/PETSc/Integration/BasicSolverUsage.tex}
  \input{slides/PETSc/Integration/3rdPartySolvers.tex}
%
  \subsection{More Abstractions}
  \input{slides/FEM/HigherLevelAbstractions.tex}
  \input{slides/PETSc/Integration/WaysToUsePETSc}
%
%
\section{Common PETSc Usage}
%
  \subsection{Principles and Design}
  \input{slides/PETSc/PETScPyramid.tex}
  \input{slides/SNES/FlowControl.tex}
  \input{slides/PETSc/LevelsOfAbstraction.tex}
  \input{slides/PETSc/OODesign.tex}
%%  \input{slides/PETSc/DesignPrinciples.tex}
%%  \input{slides/PETSc/Collectivity.tex}
  \input{slides/PETSc/WhatIsNotInPETSc.tex}
%%  \input{slides/PETSc/PetscObject.tex}
%
  \subsection{Debugging PETSc}
  \input{slides/PETSc/CorrectnessDebugging.tex}
  \input{slides/PETSc/Debugger.tex}
  \input{slides/PETSc/DebuggingTips.tex}
  \input{slides/PETSc/Tutorial/Exercise7.tex}
%
  \subsection{Profiling PETSc}
  \input{slides/PETSc/PerformanceDebugging.tex}
  \input{slides/PETSc/UsingStagesAndEvents.tex}
  \input{slides/PETSc/LoggingStages.tex}
  \input{slides/PETSc/LoggingEvents.tex}
  \input{slides/PETSc/LoggingClasses.tex}
  \input{slides/PETSc/MatrixMemoryPreallocation.tex}
  \input{slides/PETSc/Tutorial/Exercise8.tex}
%
  \subsection{Serial Performance}
  \input{slides/Model/Importance.tex}
  \input{slides/Model/ComplexityAnalysis.tex}
  \input{slides/Model/PerformanceCaveats.tex}
  \input{slides/Model/AXPYAnalysis.tex}
  \input{slides/Performance/Streams.tex}
  \input{slides/Performance/SMVAnalysis.tex}
  \input{slides/Performance/ImprovingSerialPerformance.tex}
  \input{slides/FEM/PerformanceTradeoffs.tex}
%
%
\section{Advanced PETSc}
%
  \subsection{SNES}
  \input{slides/SNES/FlowControl.tex}
  \input{slides/SNES/Callbacks.tex}
  \input{slides/PETSc/TopologyAbstractions.tex}
  \input{slides/PETSc/AssemblyAbstractions.tex}
  \input{slides/SNES/Function.tex}
  \input{slides/SNES/Jacobian.tex}
  \input{slides/SNES/Variants.tex}
  \input{slides/SNES/FiniteDifferenceJacobian.tex}
  \input{slides/SNES/DrivenCavity.tex}
%
  \subsection{DMDA}
  \input{slides/DA/WhatIsIt.tex}
  \input{slides/DA/LocalEvaluation.tex}
  \input{slides/DA/GhostValues.tex}
  \input{slides/DA/GlobalNumberings.tex}
  \input{slides/DA/LocalNumbering.tex}
  \input{slides/DA/LocalFunction.tex}
  \input{slides/DA/BratuResidual.tex}
  \input{slides/DA/LocalJacobian.tex}
  \input{slides/DA/BratuJacobian.tex}
  \input{slides/DA/DAIsMoreThanAMesh.tex}
  \input{slides/DA/Vectors.tex}
  \input{slides/DA/UpdatingGhosts.tex}
  \input{slides/DA/Stencils.tex}
  \input{slides/DA/SetValuesStencil.tex}
  \input{slides/DA/CreatingDA2d.tex}
%
%
%\section{Marc Garbey's Problem}
%\input{slides/Domain}
%\input{slides/Equation}
%\input{slides/InitialTry}
%\input{slides/AddPETScFeatures}
%\input{slides/MultigridFixes}
%\input{slides/DMMGRefinement}
%\input{slides/MeshIndependence}
%\input{slides/ViewingDA}
%\input{slides/ForcingFunction}
%\input{slides/DirichletSolution}
%\input{slides/NeumannSolution}
%\input{slides/MultigridOptions}
%
%
\section{Future Plans}
\begin{frame}{Things To Check Out}
\begin{itemize}
  \item \code{PCFieldSplit} for multiphysics
  \medskip
  \item GPU acceleration for \class{Vec}, \class{Mat}, \class{KSP}
  \medskip
  \item \class{PCMG} for multilevel solvers
  \medskip
  \item \class{DMMesh} (Sieve) for topology automation
  \medskip
  \item Deal{\bf II} and FEniCS for FEM automation
  \medskip
  \item \magenta{\href{https://bitbucket.org/petfmm/petfmm-dev}{PetFMM}} and \magenta{\href{http://code.google.com/p/petrbf/}{PetRBF}} for particle methods
\end{itemize}
\end{frame}
%
  \subsection{PCFieldSplit}
  \input{slides/MultiField/LocalEvaluation.tex}
  %% Hopefully show code from mp.c
  %% From Barry's talk:
  %%   Can do all PCs with only actions of individual components
  %%   Can't remember a good point
  \input{slides/MultiField/Preconditioning.tex}
  \input{slides/MultiField/FieldSplitOptions.tex}
%
  \subsection{PETSc-GPU}
  \input{slides/GPU/PETScGPU.tex}
  \input{slides/PETSc/GPU/VecCUDA.tex}
  \input{slides/PETSc/GPU/MatCUDA.tex}
  \input{slides/PETSc/GPU/Solvers.tex}
  \input{slides/PETSc/GPU/Example.tex}
  \input{slides/PETSc/GPU/PFLOTRANExample.tex}
  \input{slides/GPU/Results/LinearSolve_GeForce9400M.tex}
  \input{slides/GPU/Results/LinearSolve_TeslaM2050.tex}
%
  \subsection{DM}
  %% FIX FOR NEW FORMAT
  \input{slides/DM/LocalEvaluation.tex}
  \input{slides/Optimal/DMMGIntegrationWithSNES.tex}
  \input{slides/Optimal/Structured.tex}
  \input{slides/PETSc/ProgrammingWithOptions.tex}

  \subsection{Mesh}
  \input{slides/Mesh/LocalEvaluation.tex}
  %% Show code for Laplacian in bratu.cxx
  \input{slides/PyLith/MultipleMeshTypes.tex}
  \input{slides/PyLith/CohesiveCells.tex}
  %% Dirichlet conditions
%
  \subsection{FEniCS Tools}
  \input{slides/FIAT/FIAT.tex}
  \input{slides/FFC/FFC.tex}
%
  \subsection{PetFMM}
  \input{slides/FMM/Applications.tex}
  \input{slides/FMM/PetFMM.tex}
%
%
\section{Conclusions}
\begin{frame}
\frametitle{Conclusions}

  PETSc can help you
\begin{itemize}
  \item easily construct a code to test your ideas
  \begin{itemize}
    \item<2-> Lots of code construction, management, and debugging tools
  \end{itemize}

  \medskip

  \item scale an existing code to large or distributed machines
  \begin{itemize}
    \item<3-> Using \code{FormFunctionLocal()} and scalable linear algebra
  \end{itemize}

  \medskip

  \item incorporate more scalable or higher performance algorithms
  \begin{itemize}
    \item<4-> Such as domain decomposition or multigrid
  \end{itemize}

  \medskip

  \item tune your code to new architectures
  \begin{itemize}
    \item<5-> Using profiling tools and specialized implementations
  \end{itemize}
\end{itemize}
\end{frame}

\ifx\@restTalk\@empty

\else
  \relax
\fi
\end{document}
