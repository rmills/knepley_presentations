\documentclass[dvipsnames]{beamer}

\input{tex/talkPreamble.tex}

\title[Distribution]{Constructing Parallel Data Distributions}
\author[M.~Knepley]{Matthew~Knepley, Michael Lange, and Gerard~Gorman}
\date[ICL16]{Applied Modeling and Computation Group Seminar\\Earth Science and Engineering\\Imperial College, London \quad October 10, 2016}
% - Use the \inst command if there are several affiliations
\institute[Rice]{
  Computational and Applied Mathematics\\
  Rice University
}
\subject{Plex}

\begin{document}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.12]{figures/logos/anl-white-background-modern.jpg}\hspace{1.0in}
  \includegraphics[scale=0.10]{figures/logos/RiceLogo_TMCMYK300DPI.jpg}
  \end{center}
  \vskip0.4in
\end{frame}
%
\begin{frame}<testing>{Abstract}\small
A parallel data distribution is usually treated as a fait accompli, specified before the real action starts, of writing MPI code
for actual data movement. However this has led to code with many severe drawbacks. It is hard to maintain, since separate
versions are written for each different distribution pattern. It is hard to optimize since no single kernel is in evidence. It is
also inextensible, since a user has a difficult time making a new distribution from an old one. The PETSc VecScatter interface
is a good example of this type of API, where all the work on specification is reserved for the user.

We will present an interface which treats distributions as first class objects. This allows us to derive new distributions mechanically
from previous distributions. It also allows us to write small kernels which handle all the processing and communication. We
will show illustrative examples from the world of finite elements.

This is joint work with Michael Lange and Gerard Gorman.
\end{frame}
%
\begin{frame}{Mesh and Field Distribution}
\begin{center}\Huge
  Suppose I want to distribute a mesh...
\end{center}
\end{frame}
%
\begin{frame}{Mesh and Field Distribution}\Large
\begin{itemize}
  \item Decide which cells go to which processes (partition)
  \medskip
  \item Tell processes how many cells are coming
  \medskip
  \item Send cell descriptions
  \medskip
  \item Mark \textit{ghost} vertices/edges/faces
  \medskip
  \item Locally renumber/reorder
\end{itemize}
\end{frame}
%
\begin{frame}{Mesh and Field Distribution}
\begin{center}\Huge
  Suppose I want to distribute a field...
\end{center}
\end{frame}
%
\begin{frame}{Mesh and Field Distribution}\Large
\begin{itemize}
  \item Associate field values with parts of mesh
  \medskip
  \item Tell processes how many field values are coming
  \medskip
  \item Send field values
  \medskip
  \item Mark \textit{ghost} values
  \medskip
  \item Locally renumber/reorder
\end{itemize}
\end{frame}
%
\begin{frame}{Mesh and Field Distribution}
\begin{center}\Huge
  Suppose I want to communicate ghost values...
\end{center}
\end{frame}
%
\begin{frame}{Mesh and Field Distribution}\Large
\begin{itemize}
  \item Construct map from owned values to ghost values
  \medskip
  \item Tell processes how many owned values are coming
  \medskip
  \item Send field values
\end{itemize}
\end{frame}
%
\begin{frame}{Mesh and Field Distribution}
\begin{center}\Huge
  Suppose I want to use a\\cell overlap...
\end{center}
\end{frame}
%
\begin{frame}{Mesh and Field Distribution}
\begin{center}\Huge
  Suppose I want many-to-many instead of one-to-many...
\end{center}
\end{frame}
%
\begin{frame}{Mesh and Field Distribution}
\begin{center}\Huge
  Suppose I want\\Jacobian preallocation\\instead of ghost values...
\end{center}
\end{frame}
%
\begin{frame}{Main Point}

\Huge
Parallel data distributions\\
\bigskip
\pause
\quad can use a single interface\\
\bigskip
\pause
\qquad to first class objects.
\end{frame}
%
\section{Section and SF}
\input{slides/PetscSection/SectionHoisting.tex}
%
\begin{frame}{Distribution Automation}\Huge

We have two main tools:

\bigskip
\bigskip
\pause

\begin{center}
  PetscSection and PetscSF
\end{center}
\end{frame}
%
\begin{frame}{PetscSection}\LARGE

A PetscSection is a multimap
\begin{align*}
  \mathrm{int} \longrightarrow \left\{ \mathrm{int} \right\}
\end{align*}

\bigskip

\begin{overprint}
\onslide<2>
which can represent {\bf mesh topology}
\begin{align*}
  \mathrm{mesh\ point} \longrightarrow \left\{ \mathrm{mesh\ point} \right\}
\end{align*}
\onslide<3>
which can represent {\bf mesh partition}
\begin{align*}
  \mathrm{process} \longrightarrow \left\{ \mathrm{mesh\ point} \right\}
\end{align*}
\onslide<4>
which can represent {\bf data layout}
\begin{align*}
  \mathrm{mesh\ point} \longrightarrow \left\{ \mathrm{dof} \right\}
\end{align*}
\onslide<5>
which can represent {\bf Jacobian adjacency}
\begin{align*}
  \mathrm{dof} \longrightarrow \left\{ \mathrm{dof} \right\}
\end{align*}
\end{overprint}
\end{frame}
%
\begin{frame}{PetscSF}\LARGE

A PetscSF is a \textit{star-forest}
\bigskip
\begin{align*}
  \mathrm{local\ dof\ (root)} \longrightarrow \left\{ \mathrm{(local\ dof,\ process)\ (leaf)} \right\}
\end{align*}
\end{frame}
%
\begin{frame}{PetscSF}
\begin{center}
  \includegraphics[height=0.8\textheight]{figures/PetscSF/SFExample.pdf}
\end{center}
\end{frame}
%
% TODO Show SFBcast and SFReduce
%
\section{Moving Data}
%
\begin{frame}{Data Description}\LARGE
All data is understood as a function with
\medskip
\begin{center}
  a domain consisting of \textit{points}
\end{center}
\medskip
and
\medskip
\begin{center}
  a range consisting of \textit{dofs}
\end{center}
\end{frame}
%
\begin{frame}{Moving Data}\LARGE
  In order to move data, we need
\bigskip
\begin{itemize}
  \item a migration PetscSF
  \begin{itemize}\Large
    \item roots are old point distribution
    \item leaves are new point distribution
  \end{itemize}
  \medskip
  \item a PetscSection mapping points to dofs
  \begin{itemize}\Large
    \item in the old point distribution
  \end{itemize}
\end{itemize}
\end{frame}
%
\begin{frame}{Moving Data}\LARGE

To redistribute the data, we
\bigskip
\begin{itemize}
  \item redistribute the section
  \medskip
  \item derive a dof migration SF
  \smallskip
  \begin{itemize}\Large
    \item Pushfoward of point migration SF over Section
  \end{itemize}

  \item redistribute the values
\end{itemize}
\end{frame}
%
\subsection{$P_1$ Example}
\begin{frame}[fragile]{Example}{Field Data}\Large

Suppose that we have a $P_1$ discretization\\
\smallskip
\qquad and want to distribute the data.
\bigskip
\begin{overprint}
\onslide<1>
\begin{center}
\begin{tikzpicture}[scale=2]

\node (v0) [shape=circle,color=blue] at (-1.0,  0) {2};
\node (v1) [shape=circle,color=blue] at ( 0.0, -1) {3};
\node (v2) [shape=circle,color=blue] at ( 0.0,  1) {4};
\node (v3) [shape=circle,color=blue] at ( 1.0,  0) {5};
\node (c0) [shape=circle,color=blue] at (-0.3,  0) {0};
\node (c1) [shape=circle,color=blue] at ( 0.3,  0) {1};

\draw[color=gray,line width=0.5pt] (v0) -- (v1) -- (v2) -- (v0);
\draw[color=gray,line width=0.5pt] (v1) -- (v3) -- (v2) -- (v1);

\end{tikzpicture}
\end{center}
\onslide<2>
\begin{center}
\begin{tikzpicture}[scale=2]

\node (d0) [shape=circle,draw=black,color=blue] at (-1,  0) {0};
\node (d1) [shape=circle,draw=black,color=blue] at ( 0, -1) {1};
\node (d2) [shape=circle,draw=black,color=blue] at ( 0,  1) {2};
\node (d3) [shape=circle,draw=black,color=blue] at ( 1,  0) {3};

\draw[color=gray,line width=0.5pt] (d0) -- (d1) -- (d2) -- (d0);
\draw[color=gray,line width=0.5pt] (d1) -- (d3) -- (d2) -- (d1);

\end{tikzpicture}
\end{center}
\onslide<3>
\begin{center}
\begin{tikzpicture}[scale=2]

\node (d0) [shape=circle,draw=black,color=red] at (-1,  0) {5.0};
\node (d1) [shape=circle,draw=black,color=red] at ( 0, -1) {1.0};
\node (d2) [shape=circle,draw=black,color=red] at ( 0,  1) {3.0};
\node (d3) [shape=circle,draw=black,color=red] at ( 1,  0) {8.0};

\draw[color=gray,line width=0.5pt] (d0) -- (d1) -- (d2) -- (d0);
\draw[color=gray,line width=0.5pt] (d1) -- (d3) -- (d2) -- (d1);

\end{tikzpicture}
\end{center}
\onslide<4>
\bigskip
\begin{center}
\begin{tabular}{lccc}
         & \multicolumn{2}{c}{Section}         & Vec \\
  Proc 0 & $\blue{2} \to \{\red{1}, 0\}$ & $\blue{3} \to \{\red{1}, 1\}$ & $\{5.0, 1.0, 3.0, 8.0\}$ \\
         & $\blue{4} \to \{\red{1}, 2\}$ & $\blue{5} \to \{\red{1}, 3\}$ & \\
  Proc 1 &                               &                               & $\{\}$
\end{tabular}
\end{center}
\end{overprint}
\end{frame}
%
\begin{frame}[fragile]{Partition}\Large

\begin{overprint}
\onslide<1>
We create a partition of the cells,
\begin{center}
\begin{tikzpicture}[scale=2]

\node (c00) [shape=circle,color=brown] at (-0.8, 0) {0};
\node (c11) [shape=circle,color=green] at ( 0.8, 0) {1};

\draw[color=gray,line width=0.5pt] (-1.5,  0) -- (-0.5, -1) -- (-0.5, 1) -- (-1.5,  0);
\draw[color=gray,line width=0.5pt] ( 0.5, -1) -- ( 1.5,  0) -- ( 0.5, 1) -- ( 0.5, -1);

\end{tikzpicture}
\end{center}
\onslide<2>
which induces a vertex partition
\begin{center}
\begin{tikzpicture}[scale=2]

\node (v00) [shape=circle,color=brown] at (-1.5,  0) {2};
\node (v01) [shape=circle,color=brown] at (-0.5, -1) {3};
\node (v02) [shape=circle,color=brown] at (-0.5,  1) {4};
\node (c00) [shape=circle,color=brown] at (-0.8,  0) {0};
\node (v11) [shape=circle,color=green] at ( 0.5, -1) {3};
\node (v12) [shape=circle,color=green] at ( 0.5,  1) {4};
\node (v13) [shape=circle,color=green] at ( 1.5,  0) {5};
\node (c11) [shape=circle,color=green] at ( 0.8,  0) {1};

\draw[color=gray,line width=0.5pt] (v00) -- (v01) -- (v02) -- (v00);
\draw[color=gray,line width=0.5pt] (v11) -- (v13) -- (v12) -- (v11);

\end{tikzpicture}
\end{center}
\onslide<3>
which induces a dof partition
\begin{center}
\begin{tikzpicture}[scale=2]

\node (d00) [shape=circle,color=brown] at (-1.5,  0) {0};
\node (d01) [shape=circle,color=brown] at (-0.5, -1) {1};
\node (d02) [shape=circle,color=brown] at (-0.5,  1) {2};
\node (d11) [shape=circle,color=green] at ( 0.5, -1) {1};
\node (d12) [shape=circle,color=green] at ( 0.5,  1) {2};
\node (d13) [shape=circle,color=green] at ( 1.5,  0) {3};

\draw[color=gray,line width=0.5pt] (d00) -- (d01) -- (d02) -- (d00);
\draw[color=gray,line width=0.5pt] (d11) -- (d13) -- (d12) -- (d11);

\end{tikzpicture}
\end{center}
\onslide<4>
and can be represented by a Section and IS
\medskip
\begin{center}
\begin{tabular}{lccc}
         & \multicolumn{2}{c}{Section}         & IS \\
  Proc 0 & $0 \to \{\blue{3}, 0\}$ & $1 \to \{\blue{3}, 3\}$ & $\{\brown{0, 1, 2}, \green{1, 2, 3}\}$ \\
  Proc 1 &                         &                         & $\{\}$
\end{tabular}
\end{center}
\end{overprint}
\end{frame}
%
\begin{frame}{Bootstrap}\Large

\begin{overprint}
\onslide<1>
Create a PetscSF that assumes\\
\qquad everyone communicates:
\bigskip
\begin{center}
\begin{tabular}{cc}
Proc 0          & Proc 1 \\
\hline
$0 \to (p0, 0)$ & $0 \to (p0, 1)$ \\
$1 \to (p1, 0)$ & $1 \to (p1, 1)$
\end{tabular}
\end{center}
\onslide<2>
Create a PetscSF that assumes\\
\qquad a broadcast from Proc 0:
\bigskip
\begin{center}
\begin{tabular}{cc}
Proc 0          & Proc 1 \\
\hline
$0 \to (p0, 0)$ & $0 \to (p0, 1)$
\end{tabular}
\end{center}
\end{overprint}
\end{frame}
%
\begin{frame}[fragile]{Partition Inversion}\Large
Start with partition info at senders,
\medskip
\begin{center}
\begin{tabular}{lccc}
         & \multicolumn{2}{c}{Section}            & IS \\
  Proc 0 & $0 \to \{\blue{3}, 0\}$ & $1 \to \{\blue{3}, 3\}$ & $\{\brown{0, 1, 2}, \green{1, 2, 3}\}$ \\
  Proc 1 & $0 \to \{0, 0\}$        & $1 \to \{0, 0\}$        & $\{\}$
\end{tabular}
\end{center}

\pause
\bigskip

but need partition info at receivers.
\medskip
\begin{center}
\begin{tabular}{lccc}
         & \multicolumn{2}{c}{Section}         & IS \\
  Proc 0 & $0 \to \{\brown{3}, 0\}$ & $1 \to \{\brown{0}, 0\}$ & $\{\blue{0, 1, 2}\}$ \\
  Proc 1 & $0 \to \{\green{3}, 0\}$ & $1 \to \{\green{0}, 0\}$ & $\{\blue{1, 2, 3}\}$
\end{tabular}
\end{center}

\pause
\bigskip

\begin{cprog}
  DistributeData(procSF, partSec, MPIU_2INT, part, invpartSec, invpart);
\end{cprog}
\end{frame}
%
\begin{frame}[fragile]{Data Distribution}\Large

\begin{overprint}
\onslide<1>
Now the serial data
\medskip
\begin{center}
\begin{tabular}{lccc}
         & \multicolumn{2}{c}{Section}         & Vec \\
  Proc 0 & $\blue{2} \to \{\red{1}, 0\}$ & $\blue{3} \to \{\red{1}, 1\}$ & $\{5.0, 1.0, 3.0, 8.0\}$ \\
         & $\blue{4} \to \{\red{1}, 2\}$ & $\blue{5} \to \{\red{1}, 3\}$ & \\
  Proc 1 &                               &                               & \{\}
\end{tabular}
\end{center}
\onslide<2>
using the same call on a new SF,
\medskip
\begin{cprog}
  DistributeData(pointSF, dataSec, MPIU_SCALAR, data, newdataSec, newdata);
\end{cprog}
Notice the local renumbering of vertices.
\end{overprint}
\medskip
can be distributed
\medskip
\begin{center}
\begin{tabular}{lccc}
         & \multicolumn{2}{c}{Section}         & Vec \\
  Proc 0 & $\brown{1} \to \{\red{1}, 0\}$ & $\brown{2} \to \{\red{1}, 1\}$ & $\{5.0, 1.0, 3.0\}$ \\o
         & $\brown{3} \to \{\red{1}, 2\}$ & & \\
  Proc 1 & $\green{1} \to \{\red{1}, 0\}$ & $\green{2} \to \{\red{1}, 1\}$ & $\{1.0, 3.0, 8.0\}$ \\
         & $\green{3} \to \{\red{1}, 2\}$ & & 
\end{tabular}
\end{center}
\end{frame}
%
\begin{frame}[fragile]{Data Distribution}\Large
Data distribution has three phases:
\begin{enumerate}
  \item Distribute the Section
  \smallskip
  \item Create a new SF for the data
  \smallskip
  \item Distribute the data with SFBcast()
\end{enumerate}

\bigskip

\begin{cprog}
DistributeData(sf, secSource, dtype, dataSource, secTarget, dataTarget) {
  PetscSFDistributeSection(sf, secSource, remoteOff, secTarget);
  PetscSFCreateSectionSF(sf, secSource, remoteOff, secTarget, sfDof);
  PetscSFBcast(sfDof, dtype, dataSource, dataTarget);
}
\end{cprog}
\end{frame}
%
\begin{frame}[fragile]{Data Distribution}{Phase I}\Large

\begin{overprint}
\onslide<1>
The serial Section
\begin{center}
\begin{tabular}{lcc}
         & \multicolumn{2}{c}{Serial Section} \\
  Proc 0 & $\blue{2} \to \{\red{1}, 0\}$ & $\blue{3} \to \{\red{1}, 1\}$ \\
         & $\blue{4} \to \{\red{1}, 2\}$ & $\blue{5} \to \{\red{1}, 3\}$ \\
  Proc 1 &                  &
\end{tabular}
\end{center}
\onslide<2>
to give the parallel Section
\begin{center}
\begin{tabular}{cccc}
 \multicolumn{2}{c}{Serial Section}  & \multicolumn{2}{c}{Parallel Section} \\
 $\blue{2} \to \{\red{1}, 0\}$ & $\blue{3} \to \{\red{1}, 1\}$ & $\brown{1} \to \{\red{1}, 0\}$ & $\brown{2} \to \{\red{1}, 1\}$ \\
 $\blue{4} \to \{\red{1}, 2\}$ & $\blue{5} \to \{\red{1}, 3\}$ & $\brown{3} \to \{\red{1}, 2\}$ & \\
                               &                               & $\green{1} \to \{\red{1}, 0\}$ & $\green{2} \to \{\red{1}, 1\}$ \\
                               &                               & $\green{3} \to \{\red{1}, 2\}$ & 
\end{tabular}
\end{center}
\end{overprint}
is pushed over the point SF
\begin{center}
\begin{tabular}{cccc}
\multicolumn{2}{c}{Proc 0}  & \multicolumn{2}{c}{Proc 1} \\
\hline
$\brown{0} \to (p0, \blue{0})$ & $\brown{1} \to (p0, \blue{2})$ & $\green{0} \to (p0, \blue{1})$ & $\green{1} \to (p0, \blue{3})$ \\
$\brown{2} \to (p0, \blue{3})$ & $\brown{3} \to (p0, \blue{4})$ & $\green{2} \to (p0, \blue{4})$ & $\green{3} \to (p0, \blue{5})$
\end{tabular}
\end{center}
\end{frame}
%
\begin{frame}[fragile]{Data Distribution}{Phase II}\Large

\begin{overprint}
\onslide<1>
The point SF
\medskip
\begin{center}
\begin{tabular}{cccc}
\multicolumn{2}{c}{Proc 0}  & \multicolumn{2}{c}{Proc 1} \\
\hline
$\brown{0} \to (p0, \blue{0})$ & $\brown{1} \to (p0, \blue{2})$ & $\green{0} \to (p0, \blue{1})$ & $\green{1} \to (p0, \blue{3})$ \\
$\brown{2} \to (p0, \blue{3})$ & $\brown{3} \to (p0, \blue{4})$ & $\green{2} \to (p0, \blue{4})$ & $\green{3} \to (p0, \blue{5})$
\end{tabular}
\end{center}
\onslide<2>
to give a dof SF
\medskip
\begin{center}
\begin{tabular}{cccc}
\multicolumn{2}{c}{Proc 0}  & \multicolumn{2}{c}{Proc 1} \\
\hline
$\red{0} \to (p0, \red{0})$ & $\red{1} \to (p0, \red{1})$ & $\red{0} \to (p0, \red{1})$ & $\red{1} \to (p0, \red{2})$ \\
$\red{2} \to (p0, \red{2})$ &                             & $\red{2} \to (p0, \red{3})$ & 
\end{tabular}
\end{center}
\end{overprint}
\medskip
is expanded using the Section
\medskip
\begin{center}
\begin{tabular}{lccc}
         & \multicolumn{3}{c}{Section} \\
  Proc 0 & $\brown{1} \to \{\red{1}, 0\}$ & $\brown{2} \to \{\red{1}, 1\}$ & $\brown{3} \to \{\red{1}, 2\}$ \\
  Proc 1 & $\green{1} \to \{\red{1}, 0\}$ & $\green{2} \to \{\red{1}, 1\}$ & $\green{3} \to \{\red{1}, 2\}$
\end{tabular}
\end{center}
\end{frame}
%
\begin{frame}[fragile]{Data Distribution}{Phase III}\Large

\begin{overprint}
\onslide<1>
Now the serial data
\medskip
\begin{center}
\begin{tabular}{lc}
         &  Serial Vec \\
  Proc 0 & $\{5.0, 1.0, 3.0, 8.0\}$ \\
  Proc 1 & $\{\}$
\end{tabular}
\end{center}
\onslide<2>
to the parallel data distribution.
\medskip
\begin{center}
\begin{tabular}{lcc}
         &  Serial Vec & Parallel Vec \\
  Proc 0 & $\{5.0, 1.0, 3.0, 8.0\}$ & $\{5.0, 1.0, 3.0\}$ \\
  Proc 1 & $\{\}$                   & $\{1.0, 3.0, 8.0\}$
\end{tabular}
\end{center}
\end{overprint}
\medskip
is sent using the dof SF
\medskip
\begin{center}
\begin{tabular}{cccc}
\multicolumn{2}{c}{Proc 0}  & \multicolumn{2}{c}{Proc 1} \\
\hline
$\red{0} \to (p0, \red{0})$ & $\red{1} \to (p0, \red{1})$ & $\red{0} \to (p0, \red{1})$ & $\red{1} \to (p0, \red{2})$ \\
$\red{2} \to (p0, \red{2})$ &                             & $\red{2} \to (p0, \red{3})$ & 
\end{tabular}
\end{center}
\end{frame}
%
\begin{frame}[fragile]{PetscSection Distribution}\Large
Section distribution has three phases:
\begin{enumerate}
  \item Distribute the number of point dofs
  \smallskip
  \item Distribute the old dof offsets (used in dof SF)
  \smallskip
  \item Construct the Section locally
\end{enumerate}

\bigskip

\begin{cprog}
DistributeSection(sf, secSource, remoteOff, secTarget) {
  /* Calculate domain (chart) from local SF points */
  PetscSFBcast(sf, secSource.dof, secTarget.dof);
  /* Use remote offsets in PetscSFCreateSectionSF() */
  PetscSFBcast(sf, secSource.off, remoteOff);
  PetscSectionSetUp(secTarget);
}
\end{cprog}
\end{frame}
%
\begin{frame}[fragile]{PetscSF Distribution}
\begin{cprog}
DistributeSF(sfSource, sfMig, sfTarget) {
  PetscSFGetGraph(sfMig, Nr, Nl, leaves, NULL);
  for (p = 0; p < Nl; ++p) { /* Make bid to own all points we received */
    lowners[p].rank  = rank;
    lowners[p].index = leaves ? leaves[p] : p;
  }
  for (p = 0; p < Nr; ++p) { /* Flag so that MAXLOC ignores root value */
    rowners[p].rank  = -3;
    rowners[p].index = -3;
  }
  PetscSFReduce(sfMig, lowners, rowners, MAXLOC);
  PetscSFBcast(sfMig, rowners, lowners);
  for (p = 0, Ng = 0; p < Nl; ++p) {
    if (lowners[p].rank != rank) {
      ghostPoints[Ng] = leaves ? leaves[p] : p;
      remotePoints[Ng].rank  = lowners[p].rank;
      remotePoints[Ng].index = lowners[p].index;
      Ng++;
    }
  }
  PetscSFSetGraph(sfTarget, Np, Ng, ghostPoints, remotePoints);
}
\end{cprog}
\end{frame}
%
\begin{frame}[fragile]{Example}{Jacobian Connectivity}\Large

\begin{overprint}
\onslide<1>
We can use mesh topology to locally determine the nonzero pattern of the Jacobian,
\begin{center}
\begin{tabular}{lccc}
         & \multicolumn{2}{c}{Serial Section}   & IS \\
  Proc 0 & $\red{0} \to \{\magenta{3}, 0\}$ & $\red{1} \to \{\magenta{4},  3\}$ & \{\magenta{0, 1, 2, 0, 1, 2, 3,}  \\
         & $\red{2} \to \{\magenta{4}, 7\}$ & $\red{3} \to \{\magenta{3}, 11\}$ & \magenta{0, 1, 2, 3, 1, 2, 3}\} \\
  Proc 1 &                                  &                                   &
\end{tabular}
\end{center}
\onslide<2>
it can be distributed.
\begin{center}
\begin{tabular}{lccc}
         & \multicolumn{2}{c}{Serial Section}   & IS \\
  Proc 0 & $\red{0} \to \{\magenta{3}, 0\}$ & $\red{1} \to \{\magenta{4},  3\}$ & \{\magenta{0, 1, 2, 0, 1, 2, 3,}  \\
         & $\red{2} \to \{\magenta{4}, 7\}$ &                                   & \magenta{0, 1, 2}\} \\
  Proc 1 & $\red{0} \to \{\magenta{4}, 0\}$ & $\red{1} \to \{\magenta{4},  4\}$ & \{\magenta{0, 1, 2, 3,}  \\
         & $\red{2} \to \{\magenta{3}, 7\}$ &                                   & \magenta{0, 1, 2, 3, 1, 2, 3}\} \\
\end{tabular}
\end{center}
\end{overprint}
\smallskip
and using the same call on the dof SF,
\medskip
\begin{cprog}
  DistributeData(dofSF, adjSec, MPIU_INT, adj, newadjSec, newadj);
\end{cprog}
\end{frame}
%
\subsection{Redistribution Example}
\begin{frame}[fragile]{Example}{Redistribution}\Large

Suppose that we start with distributed $P_1$\\
\smallskip
\qquad and want to redistribute the data.
\bigskip
\begin{overprint}
\onslide<1>
\begin{center}
\begin{tikzpicture}[scale=2]

\node (v00) [shape=circle,color=brown] at (-1.5,  0) {1};
\node (v01) [shape=circle,color=brown] at (-0.5, -1) {2};
\node (v02) [shape=circle,color=brown] at (-0.5,  1) {3};
\node (c00) [shape=circle,color=brown] at (-0.8,  0) {0};
\node (v11) [shape=circle,color=green] at ( 0.5, -1) {1};
\node (v12) [shape=circle,color=green] at ( 0.5,  1) {2};
\node (v13) [shape=circle,color=green] at ( 1.5,  0) {3};
\node (c11) [shape=circle,color=green] at ( 0.8,  0) {0};

\draw[color=gray,line width=0.5pt] (v00) -- (v01) -- (v02) -- (v00);
\draw[color=gray,line width=0.5pt] (v11) -- (v13) -- (v12) -- (v11);

\end{tikzpicture}
\end{center}
\onslide<2>
\begin{center}
\begin{tikzpicture}[scale=2]

\node (d00) [shape=circle,draw=black,color=brown] at (-1.5,  0) {0};
\node (d01) [shape=circle,draw=black,color=brown] at (-0.5, -1) {1};
\node (d02) [shape=circle,draw=black,color=brown] at (-0.5,  1) {2};
\node (d11) [shape=circle,draw=black,color=green] at ( 0.5, -1) {0};
\node (d12) [shape=circle,draw=black,color=green] at ( 0.5,  1) {1};
\node (d13) [shape=circle,draw=black,color=green] at ( 1.5,  0) {2};

\draw[color=gray,line width=0.5pt] (d00) -- (d01) -- (d02) -- (d00);
\draw[color=gray,line width=0.5pt] (d11) -- (d13) -- (d12) -- (d11);

\end{tikzpicture}
\end{center}
\onslide<3>
\begin{center}
\begin{tikzpicture}[scale=2]

\node (d00) [shape=circle,draw=black,color=red] at (-1.5,  0) {5.0};
\node (d01) [shape=circle,draw=black,color=red] at (-0.5, -1) {1.0};
\node (d02) [shape=circle,draw=black,color=red] at (-0.5,  1) {3.0};
\node (d11) [shape=circle,draw=black,color=red] at ( 0.5, -1) {1.0};
\node (d12) [shape=circle,draw=black,color=red] at ( 0.5,  1) {3.0};
\node (d13) [shape=circle,draw=black,color=red] at ( 1.5,  0) {8.0};

\draw[color=gray,line width=0.5pt] (d00) -- (d01) -- (d02) -- (d00);
\draw[color=gray,line width=0.5pt] (d11) -- (d13) -- (d12) -- (d11);

\end{tikzpicture}
\end{center}
\onslide<4>
\bigskip
\begin{center}
\begin{tabular}{lccc}
         & \multicolumn{2}{c}{Local Section}                             & Vec \\
  Proc 0 & $\brown{1} \to \{\red{1}, 0\}$ & $\brown{2} \to \{\red{1}, 1\}$ & $\{5.0, 1.0, 3.0\}$ \\
         & $\brown{3} \to \{\red{1}, 2\}$ & & \\
  Proc 1 & $\green{1} \to \{\red{1}, 0\}$ & $\green{2} \to \{\red{1}, 1\}$ & $\{1.0, 3.0, 8.0\}$ \\
         & $\green{3} \to \{\red{1}, 2\}$ & & \\
\end{tabular}
\end{center}
\onslide<5>
\bigskip
\begin{center}
\begin{tabular}{lccc}
         & \multicolumn{2}{c}{Global Section}                            & Vec \\
  Proc 0 & $\brown{1} \to \{\red{1}, 0\}$ & $\brown{2} \to \{\red{1}, 1\}$ & $\{5.0, 1.0, 3.0\}$ \\
         & $\brown{3} \to \{\red{1}, 2\}$ & & \\
  Proc 1 & $\green{1} \to \{\red{1}, -2\}$ & $\green{2} \to \{\red{1}, -3\}$ & $\{8.0\}$ \\
         & $\green{3} \to \{\red{1}, 3\}$ & & \\
\end{tabular}
\end{center}
\onslide<6>
\bigskip
Point SF
\smallskip
\begin{center}
\begin{tabular}{cccc}
\multicolumn{2}{c}{Proc 0}  & \multicolumn{2}{c}{Proc 1} \\
\hline
 & & $\green{1} \to (p0, \brown{2})$ & $\green{2} \to (p0, \brown{3})$ \\
\end{tabular}
\end{center}
\onslide<7>
\bigskip
Dof SF
\smallskip
\begin{center}
\begin{tabular}{cccc}
\multicolumn{2}{c}{Proc 0}  & \multicolumn{2}{c}{Proc 1} \\
\hline
 & & $\red{0} \to (p0, \red{1})$ & $\red{1} \to (p0, \red{2})$ \\
\end{tabular}
\end{center}
\end{overprint}
\end{frame}
%
\begin{frame}[fragile]{Partitioning}\Large
Again start with partition info at senders,
\medskip
\begin{center}
\begin{tabular}{lccc}
         & \multicolumn{2}{c}{Section}            & IS \\
  Proc 0 & $0 \to \{0, 0\}$ & $1 \to \{4, 0\}$ & $\{\brown{0, 1, 2, 3}\}$ \\
  Proc 1 & $0 \to \{4, 0\}$ & $1 \to \{0, 4\}$ & $\{\green{0, 1, 2, 3}\}$
\end{tabular}
\end{center}

\pause
\bigskip

and get partition info at receivers.
\medskip
\begin{center}
\begin{tabular}{lccc}
         & \multicolumn{2}{c}{Section}         & IS \\
  Proc 0 & $0 \to \{0, 0\}$ & $1 \to \{4, 0\}$ & $\{\green{0, 1, 2, 3}\}$ \\
  Proc 1 & $0 \to \{4, 0\}$ & $1 \to \{0, 4\}$ & $\{\brown{0, 1, 2, 3}\}$
\end{tabular}
\end{center}

\pause
\bigskip

using the same call and generic bootstrap SF.
\begin{cprog}
  DistributeData(procSF, partSec, MPIU_2INT, part, invpartSec, invpart);
\end{cprog}
\end{frame}
%
\begin{frame}[fragile]{Data Distribution}{Phase I}\Large

\begin{overprint}
\onslide<1>
The local Section
\begin{center}
\begin{tabular}{lcc}
         & \multicolumn{2}{c}{Local Section} \\
  Proc 0 & $\brown{1} \to \{\red{1}, 0\}$ & $\brown{2} \to \{\red{1}, 1\}$ \\
         & $\brown{3} \to \{\red{1}, 2\}$ & \\
  Proc 1 & $\green{1} \to \{\red{1}, 0\}$ & $\green{2} \to \{\red{1}, 1\}$ \\
         & $\green{3} \to \{\red{1}, 2\}$ & \\
\end{tabular}
\end{center}
\onslide<2>
to give the new local Section
\begin{center}
\begin{tabular}{cccc}
 \multicolumn{2}{c}{Old Section}  & \multicolumn{2}{c}{New Section} \\
 $\brown{1} \to \{1, 0\}$ & $\brown{2} \to \{1, 1\}$ & $\brown{1} \to \{1, 0\}$ & $\brown{2} \to \{1, 1\}$ \\
 $\brown{3} \to \{1, 2\}$ &                          & $\brown{3} \to \{1, 2\}$ & \\
 $\green{1} \to \{1, 0\}$ & $\green{2} \to \{1, 1\}$ & $\green{1} \to \{1, 0\}$ & $\green{2} \to \{1, 1\}$ \\
 $\green{3} \to \{1, 2\}$ &                          & $\green{3} \to \{1, 2\}$ & 
\end{tabular}
\end{center}
\end{overprint}
is pushed over the migration point SF
\begin{center}
\begin{tabular}{cccc}
\multicolumn{2}{c}{Proc 0}  & \multicolumn{2}{c}{Proc 1} \\
\hline
$\brown{0} \to (p1, \green{0})$ & $\brown{1} \to (p1, \green{1})$ & $\green{0} \to (p0, \brown{0})$ & $\green{1} \to (p0, \brown{1})$ \\
$\brown{2} \to (p1, \green{2})$ & $\brown{3} \to (p1, \green{3})$ & $\green{2} \to (p0, \brown{2})$ & $\green{3} \to (p0, \brown{3})$
\end{tabular}
\end{center}
\end{frame}
%
\begin{frame}[fragile]{Data Distribution}{Phase II}\Large

\begin{overprint}
\onslide<1>
The point SF
\medskip
\begin{center}
\begin{tabular}{cccc}
\multicolumn{2}{c}{Proc 0}  & \multicolumn{2}{c}{Proc 1} \\
\hline
$\brown{0} \to (p1, \green{0})$ & $\brown{1} \to (p1, \green{1})$ & $\green{0} \to (p0, \brown{0})$ & $\green{1} \to (p0, \brown{1})$ \\
$\brown{2} \to (p1, \green{2})$ & $\brown{3} \to (p1, \green{3})$ & $\green{2} \to (p0, \brown{2})$ & $\green{3} \to (p0, \brown{3})$
\end{tabular}
\end{center}
\onslide<2>
to give a dof SF
\medskip
\begin{center}
\begin{tabular}{cccc}
\multicolumn{2}{c}{Proc 0}  & \multicolumn{2}{c}{Proc 1} \\
\hline
$\red{0} \to (p1, \red{0})$ & $\red{1} \to (p1, \red{1})$ & $\red{0} \to (p0, \red{0})$ & $\red{1} \to (p0, \red{1})$ \\
$\red{2} \to (p1, \red{2})$ &                             & $\red{2} \to (p0, \red{2})$ & 
\end{tabular}
\end{center}
\end{overprint}
\medskip
is expanded using the Section
\medskip
\begin{center}
\begin{tabular}{lccc}
         & \multicolumn{3}{c}{Local Section} \\
  Proc 0 & $\brown{1} \to \{\red{1}, 0\}$ & $\brown{2} \to \{\red{1}, 1\}$ & $\brown{3} \to \{\red{1}, 2\}$ \\
  Proc 1 & $\green{1} \to \{\red{1}, 0\}$ & $\green{2} \to \{\red{1}, 1\}$ & $\green{3} \to \{\red{1}, 2\}$ \\
\end{tabular}
\end{center}
\end{frame}
%
\begin{frame}[fragile]{Data Distribution}{Phase III}\Large

\begin{overprint}
\onslide<1>
Now the original data
\medskip
\begin{center}
\begin{tabular}{lc}
         &  Old Vec \\
  Proc 0 & $\{5.0, 1.0, 3.0\}$ \\
  Proc 1 & $\{1.0, 3.0, 8.0\}$
\end{tabular}
\end{center}
\onslide<2>
to the new data distribution.
\medskip
\begin{center}
\begin{tabular}{lcc}
         &  Old Vec & New Vec \\
  Proc 0 & $\{5.0, 1.0, 3.0\}$ & $\{1.0, 3.0, 8.0\}$ \\
  Proc 1 & $\{1.0, 3.0, 8.0\}$ & $\{5.0, 1.0, 3.0\}$
\end{tabular}
\end{center}
\end{overprint}
\medskip
is sent using the dof SF
\medskip
\begin{center}
\begin{tabular}{cccc}
\multicolumn{2}{c}{Proc 0}  & \multicolumn{2}{c}{Proc 1} \\
\hline
$\red{0} \to (p1, \red{0})$ & $\red{1} \to (p1, \red{1})$ & $\red{0} \to (p0, \red{0})$ & $\red{1} \to (p0, \red{1})$ \\
$\red{2} \to (p1, \red{2})$ &                             & $\red{2} \to (p0, \red{2})$ & 
\end{tabular}
\end{center}
\end{frame}
%
%\subsection{$P_3$ Example}
%
% Can we do this by really running the code with correct output options?
% Link everything in talk to the questions at the beginning (refer back). We solve each thing in turn.
%
%
\section*{Conclusions}
%
\begin{frame}{Automation}\Huge

\begin{overprint}
\onslide<1>
We can automatically generate\\
\smallskip
\ \ complex, parallel\\
\smallskip
\ \ communication patterns\\
\smallskip
\ \ \ \ for structured data
\onslide<2>
\begin{center}
  Section + SF $\Longrightarrow$ SF
\end{center}
\onslide<3>
\begin{center}
  Partition + Bootstrap SF $\Longrightarrow$ Migration Point SF
\end{center}
\onslide<4>
\begin{center}
  Dof Section + Point SF $\Longrightarrow$ Migration Dof SF
\end{center}
\onslide<5>
\begin{center}
  Adjacency + Dof SF $\Longrightarrow$ Migration Adjacency SF
\end{center}
\onslide<6>
\begin{center}
  Topology + Point SF $\Longrightarrow$ Migration Topology SF
\end{center}
\onslide<7>
\begin{center}
  Simple Spec (FE, FV, FD)  $\Longrightarrow$ Dof Section
\end{center}
\end{overprint}
\end{frame}
%
\input{slides/PetscSection/SectionHoisting.tex}
%
\begin{frame}{Advantages}

\begin{itemize}\LARGE
  \item \blue{Composable Abstractions}
  \medskip
  \item Independent of
  \begin{itemize}\Large
    \item Dimension
    \smallskip
    \item Cell Shape
    \smallskip
    \item Discretization
  \end{itemize}
  \medskip
  \item Localizes Optimization
  \medskip
  \item Extensible by User
\end{itemize}
\end{frame}
%
\begin{frame}{Enabled Features}

\begin{itemize}\LARGE
  \item Parallel Mesh Loads
  \medskip
  \item Parallel Load Balancing
  \medskip
  \item Arbitrary Mesh Overlap
  \medskip
  \item Arbitrary datatype support (DMNetwork)
\end{itemize}
\end{frame}
%
\begin{frame}[plain]

\begin{center}
\Huge\bf Thank You!
\end{center}

\bigskip

\begin{center}
\LARGE \magenta{\href{http://www.caam.rice.edu/~mk51}{http://www.caam.rice.edu/\textasciitilde mk51}}
\end{center}
\end{frame}

\end{document}
