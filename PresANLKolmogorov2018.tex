\documentclass[dvipsnames]{beamer}

\input{tex/talkPreamble.tex}
\setbeamercovered{invisible}

% Figure this out
\newcounter{ccount}
\newcounter{num2}
\newcounter{num_prev}

\title[KST]{Understanding Multivariate Computation\\using the Kolmogorov Superposition Theorem}
\author[M.~Knepley]{Matthew~Knepley and Jonas~Actor}
\date[ANL18]{MCS Seminar, Argonne National Laboratory\\Chicago, IL \quad September 19, 2018}
% - Use the \inst command if there are several affiliations
\institute[Buffalo]{
  Computer Science and Engineering\\
  University at Buffalo
}
\subject{KST}

\begin{document}
\beamertemplatenavigationsymbolsempty

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.12]{figures/logos/anl-white-background-modern.jpg}\hspace{1.0in}
  \includegraphics[scale=0.05]{figures/logos/UB_Primary.png}
  \end{center}
\end{frame}
%
\begin{frame}<testing>{Abstract}\small
The Curse of Dimensionality constrains computational methods for high-dimensional problems. Many methods to overcome
these constraints, including neural networks, projection-pursuit, radial basis functions, and ridge functions, can be
explained as approximations of the Kolmogorov Superposition Theorem (KST). This theorem proves the existence of a
representation of a multivariate continuous function as the superposition of a small number of univariate
functions. These univariate functions are defined on a series of grids that are refined during the construction
process. Unfortunately, KST is difficult to use directly because the resulting representation is highly nonsmooth. At
best, the functions involved in the superpositions are Lipschitz continuous and not differentiable, and the best known
constructions are merely Holder continuous. We describe the first known algorithm to construct a Lipschitz KST inner
function. The resulting inner function induced by the spatial decomposition is independent of the multivariate function
being represented, depending only on the spatial dimensions of the domain. This construction could potentially be the
basis for understanding compact approximations of high-dimensional functions.
\end{frame}
%
\input{slides/Collaborators/KST.tex}
%
% SPEAK: CofD is a problem when we are approximating multidimensional functions, but embedded in this description is an
% assumption that we often take for granted. This is the idea that the function increase on any neighborhood can be
% bounded. If this is true, I can figure out what neighborhood size I need and approximate by a constant. The number of
% neighborhoods I need grows exponentially, which is CoD. This property of thie function is exactly Lipschitz continuity.
\input{slides/Approximation/CurseOfDimensionality.tex}
%
\begin{frame}{Main Question}
  \begin{center}\Huge
    Do functions of three variables\\
    exist at all?\footnote[frame]{\tiny P\'olya and Szeg\"o, Problems and Theorems of Analysis, 1925 (German), transl. 1945, reprinted 1978}
  \end{center}
\end{frame}
%
%% TODO Slide with other approx strategies
%
\begin{frame}{Nomography}
  \begin{center}\LARGE {\bf Q}: Can any function of three variables be expressed using functions of only two variables?\footnote[frame]{\tiny Hilbert, G\"ottinger Nachrichten, 1900}\end{center}
  \pause
  \begin{center}\Large Ex: Cardano's Formula for roots of a cubic equation\end{center}
  \pause
  \begin{center}\LARGE {\bf A}: Any \textit{continuous} function of three variables can be expressed using
    \textit{continuous} functions of only two variables.\footnote[frame]{\tiny Arnol'd, Dokl. Akad. Nauk SSSR 114:5, 1957}\end{center}
\end{frame}
%
\begin{frame}{Nomography}
  \begin{center}\LARGE {\bf Q}: Can any function of three variables be expressed using only univariate functions and
    addition?\end{center}
  \pause
  \begin{center}\LARGE {\bf A}: Yes\footnote[frame]{\tiny Kolmogorov, Dokl. Akad. Nauk SSSR 114:5, 1957}, for any continuous $f: [0,1]^n \rightarrow \R$
    \Large
    \begin{align*}
      f(x_1,\dots,x_n) = \sum^{2n}_{q=0} \chi\left(\,\sum^n_{p=1} \psi_{p,q}(x_p) \right).
    \end{align*}
  \end{center}
\end{frame}
%
\begin{frame}{What's going on here?}
  \Large\vspace{-1.5cm}
  \begin{align*}
    \tikzmark{f}f(x_1,\dots,x_n) =  \tikzmark{sum2} \sum^{2n}_{q=0} \tikzmark{chi} \chi_q \tikzmark{comp} \left( \, \tikzmark{sum1}\sum^n_{p=1} \tikzmark{psi}\psi_{p,q}(x_p) \right)
  \end{align*}
%
\begin{tikzpicture}[
  remember picture,
  overlay,
  expl/.style={draw=black,fill=riceGrey!20,rounded corners,inner sep=1.5ex,minimum width=9cm},
  % arrow/.style={draw=black,fill=riceGrey!30,line width=3pt,->,>=latex}
  arrow/.style={draw=black,line width=3pt,->,>=latex}
]
\node<2-2>[expl]
  (fex)
  at (6,-2.5cm)
  {Original function $f : [0,1]^n \rightarrow \R$};
\node<3-3>[expl]
  (psiex)
  at (6,-2.5cm)
  {Inner function $\psi_{p,q} : [0,1] \rightarrow \R$};
\node<4-4>[expl]
  (sumex)
  at (6,-2.5cm)
  {Addition};
\node<5-5>[expl]
  (chiex)
  at (6,-2.5cm)
  {Outer function $\chi: \R \rightarrow \R$};
\node<6-6>[expl]
  (compex)
  at (6,-2.5cm)
  {Function composition};
\draw<2-2>[draw=black, line width=3pt]
  (fex) to[out=90,in=270] ([xshift=1.35cm,yshift=-2ex]{pic cs:f});
\draw<2-2>[draw=black,line width =3pt]
  ([xshift=0cm,yshift=-2ex]{pic cs:f}) -- ([xshift=2.7cm,yshift=-2ex]{pic cs:f});
\draw<3-3>[draw=black,line width=3pt]
  (psiex) to[out=90,in=270] ([xshift=3.25ex,yshift=-2ex]{pic cs:psi});
\draw<3-3>[draw=black,line width =3pt]
  ([xshift=0ex,yshift=-2ex]{pic cs:psi}) -- ([xshift=6.5ex,yshift=-2ex]{pic cs:psi});
\draw<4-4>[arrow]
  (sumex) to[out=90,in=270] ([xshift=2ex,yshift=-3.5ex]{pic cs:sum1});
\draw<4-4>[arrow]
  (sumex) to[out=90,in=270] ([xshift=2ex,yshift=-3.5ex]{pic cs:sum2});
\draw<5-5>[arrow]
  (chiex) to[out=90,in=280] ([xshift=1ex,yshift=-1.6ex]{pic cs:chi});
% \draw<6-6>[arrow]
%   (compex) to[out=90,in=220] ([xshift=1.1ex,yshift=-2.5ex]{pic cs:comp});
\draw<6-6>[draw=black,line width=3pt]
  (compex) to[out=90,in=270] ([xshift=0.35cm,yshift=-4.5ex]{pic cs:psi});
\draw<6-6>[line width=3pt]
  ([xshift=-1.3cm,yshift=-4.5ex]{pic cs:psi}) -- ([xshift=2cm,yshift=-4.5ex]{pic cs:psi});
\end{tikzpicture}
\end{frame}
%
\begin{frame}{2D: $n = 2$}\Large
  \begin{overprint}
    \onslide<1>
    \begin{alignat*}{2}
      f(x, y) &= \chi_0 \big(\psi_{x,0}(x) &&{}+ \psi_{y,0}(y)\big) \\
              &+ \chi_1 \big(\psi_{x,1}(x) &&{}+ \psi_{y,1}(y)\big) \\
              &+ \chi_2 \big(\psi_{x,2}(x) &&{}+ \psi_{y,2}(y)\big) \\
              &+ \chi_3 \big(\psi_{x,3}(x) &&{}+ \psi_{y,3}(y)\big) \\
              &+ \chi_4 \big(\psi_{x,4}(x) &&{}+ \psi_{y,4}(y)\big)
    \end{alignat*}
    \onslide<2>
    \begin{alignat*}{2}
      f(x, y) &= \chi_0 \big(\lambda_x \psi(x)             &&{}+ \lambda_y \psi(y)\big) \\
              &+ \chi_1 \big(\lambda_x \psi(x + \epsilon)  &&{}+ \lambda_y \psi(y + \epsilon)\big) \\
              &+ \chi_2 \big(\lambda_x \psi(x + 2\epsilon) &&{}+ \lambda_y \psi(y + 2\epsilon)\big) \\
              &+ \chi_3 \big(\lambda_x \psi(x + 3\epsilon) &&{}+ \lambda_y \psi(y + 3\epsilon)\big) \\
              &+ \chi_4 \big(\lambda_x \psi(x + 4\epsilon) &&{}+ \lambda_y \psi(y + 4\epsilon)\big)
    \end{alignat*}

    \bigskip

    \begin{center}Single $\psi$ function (Sprecher)\end{center}
    \onslide<3>
    \begin{alignat*}{3}
      f(x, y) &= \chi \big(\lambda_x \psi(x)             &&{}+ \lambda_y \psi(y)\big) &&\\
              &+ \chi \big(\lambda_x \psi(x + \epsilon)  &&{}+ \lambda_y \psi(y + \epsilon)  &&{}+ \delta\big) \\
              &+ \chi \big(\lambda_x \psi(x + 2\epsilon) &&{}+ \lambda_y \psi(y + 2\epsilon) &&{}+ 2\delta\big) \\
              &+ \chi \big(\lambda_x \psi(x + 3\epsilon) &&{}+ \lambda_y \psi(y + 3\epsilon) &&{}+ 3\delta\big) \\
              &+ \chi \big(\lambda_x \psi(x + 4\epsilon) &&{}+ \lambda_y \psi(y + 4\epsilon) &&{}+ 4\delta\big)
    \end{alignat*}

    \bigskip

    \begin{center}Single $\chi$ function (Lorentz) and $\psi$ function (Sprecher)\end{center}
  \end{overprint}
\end{frame}
%
\begin{frame}{Universality}
  \begin{center}\LARGE
  \begin{align*}
    \Psi^q(x_1,\dots,x_n) = \sum_{p=1}^n \psi_{p,q}(x_p)
  \end{align*}

  \bigskip
  
  \Huge is independent of $f$, so that
  \vspace{-0.25em}
  \begin{align*}
    KST : f \to \chi
  \end{align*}
  \end{center}
\end{frame}
%
%
\section{Constructive KST}
%
\begin{frame}{Topographic KST}
  \begin{overprint}
    \onslide<1>
    \begin{center}\Large
      \begin{align*}
        f(x,y) = \text{ elevation in Chugash Mountains at }(x,y)
      \end{align*}
    \end{center}
    \onslide<2>
    \begin{center}\Large
      \begin{align*}
        f(\text{Cleave Creek Glacier}) = \text{ ? }
      \end{align*}
    \end{center}
  \end{overprint}
  \bigskip
  \begin{overprint}
    \onslide<1>
  \begin{figure}
    \centering
    \begin{tikzpicture}
      \node[anchor=south west,inner sep=0] (image) at (0,0) {\adjincludegraphics[trim={0 {0.0\height} 0 {0.4\height}},clip,width=0.9\textwidth]{figures/Topography/ChugashMountains}};
      % \begin{scope}[x={(image.south east)},y={(image.north west)}]
      %     \draw[help lines,xstep=.5,ystep=.5] (0,0) grid (1.001,1.001);
      % \end{scope}
    \end{tikzpicture}
  \end{figure}
    \onslide<2>
  \begin{figure}
    \centering
    \begin{tikzpicture}
      \node[anchor=south west,inner sep=0] (image) at (0,0) {\adjincludegraphics[trim={0 {0.0\height} 0 {0.4\height}},clip,width=0.9\textwidth]{figures/Topography/ChugashMountains}};
      \begin{scope}[x={(image.south east)},y={(image.north west)}]
          % \draw[help lines,xstep=.5,ystep=.5] (0,0) grid (1.001,1.001);
          \draw [fill, color=red] (0.785,0.4) ellipse (0.01 and 0.045) ;
      \end{scope}
    \end{tikzpicture}
  \end{figure}
  \end{overprint}
  \tiny{\blfootnote{\href{https://viewer.nationalmap.gov/basic/}{https://viewer.nationalmap.gov/basic/}}}
\end{frame}
%
\begin{frame}{Topographic KST}
  \onslide<1-2>{ \vspace{0.8cm} }
  \onslide<3->{  \vspace{-1.3cm} }
  \begin{figure}
    \centering
  \begin{tikzpicture}
      \node[anchor=south west,inner sep=0] (image) at (0,0) {\adjincludegraphics[trim={0 {0.0\height} 0 {0.4\height}},clip,width=0.9\textwidth]{figures/Topography/ChugashMountains}};
      \begin{scope}[x={(image.south east) },y={(image.north west)}]
          \draw[help lines,xstep=.5,ystep=.5] (0,0) grid (1.001,1.001);
          \foreach \x in {1,2} {
              \setcounter{ccount}{\x}
              \node [anchor=north,above] at (\x/2 - 0.25,1) {\Alph{ccount}};
          }
          \foreach \y in {1,2} { \node [anchor=east] at (0, 1.0 - \y/2 + 0.25) {\y}; }
          \onslide<1-2>{
              \draw [fill, color=red] (0.785,0.4) ellipse (0.01 and 0.045) ;
          }

          \onslide<3->{
              \draw[help lines, <->] (0,-1.2) -- (1,-1.2);
              \foreach \x in {0,...,3}{
                  \setcounter{ccount}{\x}
                  \ifnum\value{ccount}=1
                      \draw[ultra thick, |-|, draw=red] (0.1+0.2*\x,-1.2) -- (0.3+0.2*\x, -1.2);
                  \else
                      \draw[ultra thick, |-|] (0.1+0.2*\x,-1.2) -- (0.3+0.2*\x, -1.2);
                  \fi
              }
          }
          \onslide<3-3>{
              \draw[draw = red, ultra thick] (0,0) -- (0.5,0) -- (0.5,0.5) -- (0,0.5) -- cycle;
              \draw[draw = red, ultra thick, ->] (0.25,-0.05) -- (0.4, -1.15);
              % \draw[draw=red, ultra thick, |-|] (0.3,-1.2) -- (0.5, -1.2) ;
              % \node [right] at (0.345,-0.6) {$ \begin{aligned} \Psi^q(\text{27 Mile Glacier}) &= \sum_{p=1}^n \psi_{p,q}(\text{27 Mile Glacier)} \\&= \text{A2} \end{aligned}$ } ;
              \node [right] at (0.345,-0.6) {$ \Psi^q(\text{27 Mile Glacier}) = \text{A2}$ } ;

          }
          \onslide<4->{
              % \draw[help lines, <->] (0.01, -1.25) -- (0.01, -0.2) ;
              \foreach \x in {0,...,3}{
                  \reinitrand[first=\x, last=100, counter=num, seed=8]  \rand
                  \setcounter{ccount}{\x}
                  \ifnum\value{ccount}=1
                      \draw[ultra thick, draw=red] (0.1+0.2*\x, -1 + 0.007*\value{num}) -- (0.3+0.2*\x, -1 + 0.007*\value{num}) ;
                      \node [above] at (0.4, -1 + 0.007*\value{num} + 0.05) {$f(\text{27 Mile Glacier}) = \chi($A2$)$};
                  \else
                      \draw[thick] (0.1+0.2*\x, -1 + 0.007*\value{num}) -- (0.3+0.2*\x, -1 + 0.007*\value{num}) ;
                  \fi
              }
          }
      \end{scope}
  \end{tikzpicture}
\end{figure}
\onslide<1-1>{
\vspace{-3.5cm}
\begin{center}
\begin{tabular}{llcll}
Tsina Glacier   & A1 && Tsina River   & B1 \\
27 Mile Glacier & A2 && Chugash Mtns. & B2
\end{tabular}
\end{center}
}
\onslide<2-2>{
\vspace{-2cm}
\begin{center}
\begin{tabular}{lllcll}
Elevation (ft) & A1 & 5400 && B1 &  400 \\
               & A2 & 4800 && B2 & 5700
\end{tabular}
\end{center}
}
\end{frame}
%
\begin{frame}{Topographic KST}
  \begin{figure}
    \centering
  \begin{tikzpicture}
      \node[anchor=south west,inner sep=0] (image) at (0,0) {\adjincludegraphics[trim={0 {0.0\height} 0 {0.4\height}},clip,width=0.9\textwidth]{figures/Topography/ChugashMountains}};
      \begin{scope}[x={(image.south east) },y={(image.north west)}]
          % \draw[help lines,xstep=.5,ystep=.5] (0,0) grid (1.001,1.001);
          \draw[line width = 12.5, draw=white,xstep=.5,ystep=.5] (0,0) grid (1.001,1.001);

          \foreach \x in {1,2} {
              \setcounter{ccount}{\x}
              \node [anchor=north,above] at (\x/2 - 0.25,1) {\Alph{ccount}};
          }
          \foreach \y in {1,2} { \node [anchor=east] at (0, 1.0 - \y/2 + 0.25) {\y}; }

          \onslide<2->{
              \draw[help lines, <->] (0,-1.2) -- (1,-1.2);
              \reinitrand[first=1, last=100, counter=num, seed=8]  \rand
              \foreach \x in {0,...,3}{
                  \draw[ultra thick, |-|] (0.1+0.2*\x,-1.2) -- (0.26+0.2*\x, -1.2);
                  % \draw[help lines, |-|] (0.26+0.2*\x, -1.2) -- (0.3+0.2*\x, -1.2);
                  \setcounter{ccount}{\x}
                  \reinitrand[first=\x, last=100, counter=num2, seed=8]  \rand
                  % \setcounter{num2}{\rand{arabic}{num}}
                  \ifnum\value{ccount}=0
                      \draw[thick] (0.1+0.2*\x, -1 + 0.007*\value{num2}) -- (0.26+0.2*\x, -1 + 0.007*\value{num2});
                      \setcounter{num_prev}{\value{num2}}
                  \else
                      \draw[thick] (0.06+0.2*\x, -1+0.007*\value{num_prev}) -- (0.1+0.2*\x, -1 + 0.007*\value{num2}) -- (0.26+0.2*\x, -1 + 0.007*\value{num2}) ;
                      \setcounter{num_prev}{\value{num2}}
                  \fi
              }
          }

          \onslide<3->{
              \foreach \x in {0,...,2}{
                  \draw[draw opacity =0, fill opacity = 0.3, fill=red] (0.26+0.2*\x,-1.6) -- (0.3+0.2*\x, -1.6) -- (0.3+0.2*\x, -0.2) -- (0.26+0.2*\x, -0.2) -- cycle;
              }
          }

      \end{scope}
  \end{tikzpicture}
\end{figure}
\end{frame}
%
%
\section{Abstract KST}
%
\begin{frame}{Kolmogorov Strategy}\LARGE
  \begin{itemize}
    \item Approximate with \textit{constants}
    \medskip
    \item Insert \textit{gaps} to preserve continuity
    \medskip
    \item \textit{Duplicate and shift} to cover domain
  \end{itemize}
  \bigskip
  \begin{overprint}
  \onslide<2>
  \begin{tabular}{ll}
    $\Psi$: & balances continuity against \\
            & discriminating between different points
  \end{tabular}
  \onslide<3>
  \begin{tabular}{ll}
    $\Psi$: & regularity vs. point separation \\
    $\chi$ & assigns values to shifted sums of our \\
           & $R^{2n+1}$ embedding to match $f$
  \end{tabular}
  \onslide<4>
  \begin{tabular}{ll}
    $\Psi$: & regularity vs. point separation \\
    $\chi$: & approximates $f$
  \end{tabular}
  \end{overprint}
\end{frame}
%
\subsection{Spatial Decomposition}
\begin{frame}{Spatial Decomposition}{Single town}
  \begin{center}\Large
    Cartesian product $\mathcal{S}$ of a set of intervals $\mathcal{I}$\\that nearly cover $[0, 1]$
  \end{center}
  \medskip
  \begin{figure}
  \centering
  \begin{tikzpicture}[scale=0.8]
    \draw [ultra thick, fill=black, fill opacity = 0.2] (0,0) -- (2.3, 0) -- (2.3,2.3) -- (0,2.3) -- (0,0) ;
    \draw [ultra thick, fill=black, fill opacity = 0.2] (0,2.5) -- (2.3, 2.5) -- (2.3,4.8) -- (0,4.8) -- (0,2.5) ;
    \draw [ultra thick, fill=black, fill opacity = 0.2] (2.5,0) -- (2.5, 2.3) -- (4.8,2.3) -- (4.8,0) -- (2.5,0) ;
    \draw [ultra thick, fill=black, fill opacity = 0.2] (2.5,2.5) -- (4.8, 2.5) -- (4.8,4.8) -- (2.5,4.8) -- (2.5,2.5) ;
    \draw [ultra thick, color=black] (0,-1) -- (2.3, -1) ;
    \draw [ultra thick, color=black] (2.5,-1) -- (4.8,-1) ;
    \draw [ultra thick, color=black] (-1,0) -- (-1,2.3) ;
    \draw [ultra thick, color=black] (-1,2.5) -- (-1,4.8) ;
    % \node  at (-1,-1) {$k=1$};
  \end{tikzpicture}
  \end{figure}
\end{frame}
%
\begin{frame}{Spatial Decomposition}{Refined towns}
  \vspace{-1cm}
  \begin{center}\Large
    \begin{gather*}
      \mathcal{S}^k: \text{ Near partition of } [0,1]^d \\
      \text{diam}(\mathcal{S}^k) \rightarrow 0 \text{ as } k \rightarrow \infty
    \end{gather*}
  \end{center}
  \medskip
  \begin{figure}
  \centering
  \begin{tikzpicture}[scale=0.8]
    \draw [ultra thick, fill=black, fill opacity = 0.2] (0,0) -- (2, 0) -- (2,2.3) -- (0,2.3) -- (0,0) ;
    \draw [ultra thick, fill=black, fill opacity = 0.2] (0,2.5) -- (2, 2.5) -- (2,4.8) -- (0,4.8) -- (0,2.5) ;
    \draw [ultra thick, fill=black, fill opacity = 0.2] (2.2,0) -- (2.2, 2.3) -- (4.8,2.3) -- (4.8,0) -- (2.2,0) ;
    \draw [ultra thick, fill=black, fill opacity = 0.2] (2.2,2.5) -- (4.8, 2.5) -- (4.8,4.8) -- (2.2,4.8) -- (2.2,2.5) ;
    \draw [ultra thick, fill=black, fill opacity = 0.2] (5,0) -- (6, 0) -- (6,2.3) -- (5,2.3) -- (5,0) ;
    \draw [ultra thick, fill=black, fill opacity = 0.2] (5,2.5) -- (6, 2.5) -- (6,4.8) -- (5,4.8) -- (5,2.5) ;
    \draw [ultra thick, fill=black, fill opacity = 0.2] (2.2,5) -- (2.2, 6) -- (4.8,6) -- (4.8,5) -- (2.2,5) ;
    \draw [ultra thick, fill=black, fill opacity = 0.2] (0,5) -- (0, 6) -- (2, 6) -- (2,5) -- (0,5) ;
    \draw [ultra thick, fill=black, fill opacity = 0.2] (5,5) -- (5,6) -- (6,6) -- (6,5) -- (5,5) ;

    % \draw [ultra thick, color=black] (0,-1) -- (2.3, -1) ;
    % \draw [ultra thick, color=black] (2.5,-1) -- (4.8,-1) ;
    % \draw [ultra thick, color=black] (-1,0) -- (-1,2.3) ;
    % \draw [ultra thick, color=black] (-1,2.5) -- (-1,4.8) ;
    % \node  at (-1,-1) {$k=1$};
  \end{tikzpicture}
  \end{figure}
\end{frame}
%
\begin{frame}{Spatial Decomposition}{Cover gaps}
  \begin{center}\Large
    $2n+1$ copies $\mathcal{I}_q$ of the same set of intervals $\mathcal{I}$\\shifted by $\epsilon$
  \end{center}
  \medskip
  \begin{figure}
  \centering
  \begin{tikzpicture}
    \draw [help lines, |-|] (0, 1.2) -- (0.6, 1.2) ;
    \node [below] at (0,-0.4) {-1};
    \node [below] at (2.4,-0.4) {0} ;
    \node [below] at (4.8, -0.4) {1} ;
    \node [below] at (7.2, -0.4) {2} ;
    \draw [help lines] (0,   0.9) -- (0,   -0.3) ;
    \draw [help lines] (2.4, 2.8) -- (2.4, -0.3) ;
    \draw [help lines] (4.8, 2.8) -- (4.8, -0.3) ;
    \draw [help lines] (7.2, 2.8) -- (7.2, -0.3) ;
    \node [above] at (0.3, 1.3) {$\frac{1}{4}$} ;
    \foreach \q in {0,...,4}
      \draw [line width = 3] (\q*0.6, \q*0.6) -- (4.8 + \q*0.6, \q*0.6) ;
  \end{tikzpicture}
  \end{figure}
\end{frame}
%
\begin{frame}{Spatial Decomposition}{Overlapping towns}
  \begin{center}\Large
    Cartesian product $\mathcal{S}_q$ of shifted intervals $\mathcal{I}_q$
  \end{center}
  \medskip
  \begin{figure}
  \centering
  \begin{tikzpicture}[scale=1.5]
  \draw [help lines] (-1.5, 0) -- (2.5, 0) ;
  \draw [help lines] (0, -1.5) -- (0, 2.5) ;
  \draw [thick, fill=black, fill opacity=0.2](0*0.25 -1,0*0.25 -1) -- (0*0.25 -1 ,0*0.25+1) -- (0*0.25+1, 0*0.25+1) -- (0*0.25+1,0*0.25 -1) -- (0*0.25 - 1, 0*0.25 -1);
  \draw [thick, fill=black, fill opacity=0.2](1*0.25 -1,1*0.25 -1) -- (1*0.25 -1 ,1*0.25+1) -- (1*0.25+1, 1*0.25+1) -- (1*0.25+1,1*0.25 -1) -- (1*0.25 - 1, 1*0.25 -1);
  \draw [thick, fill=black, fill opacity=0.2](2*0.25 -1,2*0.25 -1) -- (2*0.25 -1 ,2*0.25+1) -- (2*0.25+1, 2*0.25+1) -- (2*0.25+1,2*0.25 -1) -- (2*0.25 - 1, 2*0.25 -1);
  \draw [thick, fill=black, fill opacity=0.2](3*0.25 -1,3*0.25 -1) -- (3*0.25 -1 ,3*0.25+1) -- (3*0.25+1, 3*0.25+1) -- (3*0.25+1,3*0.25 -1) -- (3*0.25 - 1, 3*0.25 -1);
  \draw [thick, fill=black, fill opacity=0.2](4*0.25 -1,4*0.25 -1) -- (4*0.25 -1 ,4*0.25+1) -- (4*0.25+1, 4*0.25+1) -- (4*0.25+1,4*0.25 -1) -- (4*0.25 - 1, 4*0.25 -1);
  \draw [help lines] (-1,1.5) -- (-0.75,1.5);
  \draw [help lines] (-1, 1.55) -- (-1, 1.45);
  \draw [help lines] (-0.75, 1.55) -- (-0.75, 1.45);
  \node [above] at (-0.875, 1.5) {$\frac{1}{4}$};
  \end{tikzpicture}
  \end{figure}
\end{frame}
%
\begin{frame}{Spatial Decomposition}{Refined overlapping towns}
  \vspace{-1cm}
  \begin{center}\Large
    \begin{align*}
      \text{diam}(\mathcal{S}^k_q) \rightarrow 0 \text{ as } k \rightarrow \infty
    \end{align*}
  \end{center}
  \medskip
  \begin{figure}
  \centering
  \begin{tikzpicture}[scale=1.5]
    \draw [help lines] (-1.5, 0) -- (2.5, 0) ;
    \draw [help lines] (0, -1.5) -- (0, 2.5) ;
    \foreach \q in {0,...,4}{
      \draw [thick, fill=black, fill opacity=0.2](-1 + \q*0.25,-1 + \q*0.25) -- (-1 +\q*0.25, -0.1 +\q*0.25) -- (-0.1+\q*0.25,-0.1+\q*0.25) -- (-0.1+\q*0.25,-1+\q*0.25) --cycle;
      \draw [thick, fill=black, fill opacity=0.2](0.1+\q*0.25,0.1+\q*0.25) -- (1+\q*0.25,0.1+\q*0.25) -- (1+\q*0.25,1+\q*0.25) -- (0.1+\q*0.25,1+\q*0.25) -- cycle;
      \draw [thick, fill=black, fill opacity=0.2](1+\q*0.25,-0.1+\q*0.25) -- (1+\q*0.25,-1+\q*0.25) -- (0.1+\q*0.25,-1+\q*0.25) -- (0.1+\q*0.25,-0.1+\q*0.25) -- cycle;
      \draw [thick, fill=black, fill opacity=0.2](-0.1+\q*0.25,1+\q*0.25) -- (-1+\q*0.25,1+\q*0.25) -- (-1+\q*0.25,0.1+\q*0.25) -- (-0.1+\q*0.25,0.1+\q*0.25) -- cycle;
    }
  \end{tikzpicture}
  \end{figure}
\end{frame}
%
\begin{frame}{Gap Requirement}
  \begin{center}\Large
    Every $x \in [0,1]$ in \textbf{All But One} of $\mathcal{I}_q$
  \end{center}
  \medskip
  \begin{figure}
  \centering
  \begin{tikzpicture}
  \draw [help lines, |-|] (0, 1.2) -- (0.6, 1.2) ;
  \draw [help lines] (0,   0.9) -- (0,   -0.3) ;
  \draw [help lines] (2.4, 2.8) -- (2.4, -0.3) ;
  \draw [help lines] (4.8, 2.8) -- (4.8, -0.3) ;
  \draw [help lines] (7.2, 2.8) -- (7.2, -0.3) ;
  \node [above] at (0.3, 1.3) {$\frac{1}{4}$} ;
  \foreach \q in {0,...,4} {
    \draw [line width = 3] (\q*0.6, \q*0.6) -- (2.2 + \q*0.6, \q*0.6) ;
    \draw [line width = 3] (2.6 + \q*0.6, \q*0.6) -- (4.8 + \q*0.6, \q*0.6) ;
    }
  \draw [line width = 3, color = red] (2.4,-0.6) -- (2.4, 3) -- (4.8, 3.0) -- (4.8, -0.6) -- cycle;
  \node [below] at (0,-0.4) {-1};
  \node [below] at (2.4,-0.4) {0} ;
  \node [below] at (4.8, -0.4) {1} ;
  \node [below] at (7.2, -0.4) {2} ;
  \end{tikzpicture}
  \end{figure}
  \begin{center}\Large
    \textbf{All But One} of the intervals implies\\
    \textbf{More than Half} of the squares
  \end{center}
\end{frame}
%
\subsection{Inner Functions}
\begin{frame}{Inner Function $\psi$}
  At each refinement level $k$:
  \begin{itemize}
    \setlength{\itemindent}{2em}
    \item Assign a value of $\psi^k$ at the lower left corner of each square
    \item Value is fixed for all future $k$
  \end{itemize}
  \medskip
  \begin{figure}
  \centering
  \scalebox{0.75}{
  \begin{tikzpicture}
  % \draw [thick] (0,1.1) -- (3.15,1.1) -- (4.15, 2.1) -- (4.7333, 2.1) -- (6.2333, 3.6) --(10, 3.6);
  \draw [black] (4.15,2.1) circle [radius=0.1];
  \draw [black] (6.2333, 3.6) circle [radius=0.1];
  \draw [black] (0,1.1) circle [radius=0.1];
  \draw [help lines] (0,0.1) -- (10,0.1) ;
  \draw [help lines] (0,0.95) -- (0,0.1);
  \draw [help lines] (6.2333, 3.45) -- (6.2333,0.1);
  \draw [help lines] (4.15, 1.95) -- (4.15, 0.1);
  \draw [line width = 6] (0,0) -- (3,0) -- (3.3,0.2) -- (0.3, 0.2)  -- cycle;
  \draw [line width = 6] (6.0833,0) -- (10,0) -- (10.3,0.2) -- (6.3833,0.2) -- cycle ;
  \draw [line width = 6] (4,0) -- (4.5833,0) -- (4.8833, 0.2) -- (4.3,0.2) -- cycle;
  \draw [help lines] (-1.2, 0.1) -- (-1.2,3.8) ;
  \draw [help lines] (-1.35, 3.65) -- (-1.2,3.8) -- (-1.05, 3.65) ;
  \node [left] at (-1.4, 2) {$\psi^k$};
  % \node [above] at (0.4,1.2) {$\psi_{k}$} ;
  \end{tikzpicture}
  }
  \end{figure}
\end{frame}
%
\begin{frame}{Inner Function $\psi$}
  \begin{center}\Large
    $\psi^k$ (near) constant on squares, linear on gaps\\
    $\psi = \lim_{k \rightarrow \infty} \psi^k$ uniformly
  \end{center}
  \medskip
  \begin{figure}
    \centering
    \scalebox{0.85}{
    \begin{tikzpicture}
    \draw [thick] (0,1.1) -- (3.15,1.1) -- (4.15, 2.1) -- (4.7333, 2.1) -- (6.2333, 3.6) --(10, 3.6);
    \draw [help lines] (0,0) -- (10,0) ;
    \draw [line width = 6] (0,0) -- (3,0) -- (3.3,0.2) -- (0.3, 0.2)  -- cycle;
    \draw [line width = 6] (6.0833,0) -- (10,0) -- (10.3,0.2) -- (6.3833,0.2) -- cycle ;
    \draw [line width = 6] (4,0) -- (4.5833,0) -- (4.8833, 0.2) -- (4.3,0.2) -- cycle;
    \node [above] at (0.4,1.2) {$\psi^k$} ;
    \end{tikzpicture}
    }
  \end{figure}
  \smallskip
  \Large
  Small gaps $\rightarrow$ steep $\psi^k$, but\\
  Large gaps can violate \textbf{All But One}
\end{frame}
%
\begin{frame}{Disjoint Image Condition}
  \vspace{-1cm}
  \begin{center}\Large
    \begin{align*}
       \forall S,\,S' \in \mathcal{S}^k, \Psi(S) \cap \Psi(S') = \emptyset
    \end{align*}
  \end{center}
  \smallskip
  \begin{figure}
  \centering
  \begin{tikzpicture}[scale=0.65]
    \draw [ultra thick, fill=black, fill opacity = 0.2] (0,0) -- (2.3, 0) -- (2.3,2.3) -- (0,2.3) -- (0,0) ;
    \draw [ultra thick, fill=black, fill opacity = 0.2] (0,2.5) -- (2.3, 2.5) -- (2.3,4.8) -- (0,4.8) -- (0,2.5) ;
    \draw [ultra thick, fill=black, fill opacity = 0.2] (2.5,0) -- (2.5, 2.3) -- (4.8,2.3) -- (4.8,0) -- (2.5,0) ;
    \draw [ultra thick, fill=black, fill opacity = 0.2] (2.5,2.5) -- (4.8, 2.5) -- (4.8,4.8) -- (2.5,4.8) -- (2.5,2.5) ;
    \draw [help lines, <->] (-5,-3) -- (9,-3);
    \draw [ultra thick, |-|] (-4.3, -3) -- (-1.7, -3);
    \draw [ultra thick, |-|] (-1,-3) -- (1.6,-3);
    \draw [ultra thick, |-|] (2.3,-3) -- (4.9,-3) ;
    \draw [ultra thick, |-|] (5.6,-3) -- (8.2, -3);
    \node at (1.15,1.15) {$S^k_{11}$};
    \node at (3.65,1.15) {$S^k_{21}$};
    \node at (1.15,3.65) {$S^k_{12}$};
    \node at (3.65,3.65) {$S^k_{22}$};
    \node at (-3,-3.5) {$\Psi(S^k_{11})$};
    \node at (0.3,-3.5) {$\Psi(S^k_{21})$};
    \node at (3.6,-3.5) {$\Psi(S^k_{12})$};
    \node at (6.9,-3.5) {$\Psi(S^k_{22})$};
    \draw [help lines, ->] (-0.2,1.15) -| (-3,-2.8);
    \draw [help lines, ->] (5,3.65) -| (6.9,-2.8) ;
    \draw [help lines] (-0.2,3.65) -| (-0.7, -2);
    \draw [help lines, ->] (-0.7,-2) -| (3.6,-2.8) ;
    \draw [help lines] (3.65,-0.2) |- (0.3,-1);
    \draw [help lines, ->] (0.3,-1) -- (0.3,-2.8);
    \draw [help lines] (-0.2,1.5) -- (-0.2,0.8) ;
    \draw [help lines] (-0.2,3.3) -- (-0.2,4) ;
    \draw [help lines] (5,4) -- (5, 3.3) ;
    \draw [help lines] (4,-0.2) -- (3.3,-0.2) ;
    % \node [right] at (9.8,-3.15) {$\Psi^q(\R^2) = \R$} ;
  \end{tikzpicture}
  \end{figure}
\end{frame}
%
\begin{frame}{Kolmogorov Requirements for the Inner Function}
  \Large
  \textbf{Refinement:}\\
  \qquad $\text{diam}(\mathcal{S}^k_q) \to 0 \text{ as } k \to \infty$\\
  \smallskip
  \textbf{More Than Half:}\\
  \qquad $\forall x \in [0,1]^n, x \in \mathcal{S}^k_q$ for at least $n+1$ of the $q$'s\\
  \smallskip
  \textbf{Disjoint Image:}\\
  \qquad $\forall S,\,S' \in \mathcal{S}^k_q, \Psi(S) \cap \Psi(S') = \emptyset$\\
  \smallskip
  \textbf{Monotonicity:}\\
  \qquad Function $\psi$ is strictly monotonic increasing
\end{frame}
%
\begin{frame}{Regularity of Inner Function}
  \Large
  \begin{center}
    KST trades smoothness for variables
  \end{center}
  \bigskip
  \begin{itemize}
    \item KST not feasible for $\psi_{p,q} \in C^1([0,1])$\footnote[frame]{\tiny Vituskin, DAN, 95:701--704, 1954.}
    \bigskip
    \item Possible to construct $\psi_{p,q} \in \text{Lip}([0,1])$\footnote[frame]{\tiny Fridman, DAN, 177:1019--1022, 1967.}
    \bigskip
    \item Only known construction $\psi_{p,q} \in \text{H\"older}([0,1])$
  \end{itemize}
\end{frame}
%
%
\section{Concrete KST}
\subsection{Sprecher Construction}
\begin{frame}{Sprecher Construction $\widehat{\psi}$ \footnote[frame]{\tiny Sprecher, J. Constr. Approx. 1995}}
  \Large
  \begin{itemize}
    \item Inner function $\widehat{\psi}$ is the uniform limit $\displaystyle \lim_{k \rightarrow\infty} \widehat{\psi}^k$
    \bigskip
    \item Fix values at numbers with $k$ digits\\
    in base-$\gamma$ expansion
    \medskip
    \begin{itemize}\Large
      \item Almost flat for most points
      \smallskip
      \item Large increase for expansions ending in $\gamma-1$
    \end{itemize}
    \bigskip
    \item Linearly interpolate between fixed values
  \end{itemize}
\end{frame}
%
\begin{frame}{Sprecher Construction $\widehat{\psi}$ \footnote[frame]{\tiny Sprecher, J. Constr. Approx. 1995}}
  \Large
  \begin{align*}
    \text{Radix } \gamma \ge 2d+1
  \end{align*}
  \bigskip
  \begin{align*}
    \mathcal{D}^k &= \left\{ \frac{i}{\gamma^k} \,\,:\quad i=0,\dots,\gamma^k \right\} \\
                  &= \left\{ \vphantom{\frac{i}{\gamma^k}} 0.i_0 i_1 \dots i_k \,\,:\quad i_\ell \in [0,\gamma-1],\,\ell \in [0,k] \right\}
  \end{align*}
  \bigskip
  \begin{align*}
    \beta(k) = \frac{n^k -1}{n-1}
  \end{align*}
\end{frame}
%
% TODO
%\begin{frame}{Sprecher Construction $\widehat{\psi}$\footnote[frame]{\tiny Sprecher, J. Constr. Approx. 1995}}{Grid for $\gamma = 10$}
%  \begin{figure}
%    \centering
%    \begin{tikzpicture}
%    \end{tikzpicture}
%  \end{figure}
%\end{frame}
%
\begin{frame}{K\"oppen Construction $\widehat{\psi}$ \footnote[frame]{\tiny K\"oppen, ICANN 2002, LNCS 2415, 2002}}
  \large
  \begin{align*}
    \widehat{\psi}^k(d_k) = \begin{cases}
      d_k & k=1 \\
      \widehat{\psi}^{k-1}\left( d_k - \frac{i_k}{\gamma^k} \right) + \frac{i_k}{\gamma^{\beta(k)}} & k > 1,\,i_k < \gamma-1 \\
      \frac{1}{2} \left(\widehat{\psi}^{k}\left( d_k - \frac{1}{\gamma^k} \right) +  \widehat{\psi}^{k-1}\left( d_k + \frac{1}{\gamma^k} \right) \right) & k > 1,\,i_k = \gamma-1
    \end{cases}
  \end{align*}
  \medskip
  \Large
  \begin{center}
    Interpolate linearly to extend $\widehat{\psi}^k$ from $\mathcal{D}^k$ to $[0,1]$
    \smallskip
    \begin{align*}
      \widehat{\psi} = \lim_{k \rightarrow \infty} \widehat{\psi}^k
    \end{align*}
  \end{center}
\end{frame}
%
\begin{frame}{K\"oppen Construction $\widehat{\psi}$ \footnote[frame]{\tiny K\"oppen, ICANN 2002, LNCS 2415, 2002}}
  \begin{figure}
    \centering
    \includegraphics[height=0.80\textheight]{figures/Krep/KoppenInner}
  \end{figure}
\end{frame}
%
\begin{frame}{K\"oppen Construction $\widehat{\psi} \footnote[frame]{\tiny K\"oppen, ICANN 2002, LNCS 2415, 2002}, k=7$}
  \begin{figure}
    \centering
    \includegraphics[height=0.80\textheight]{figures/Krep/KoppenInner_k7}
  \end{figure}
\end{frame}
%
\begin{frame}{K\"oppen Construction $\widehat{\psi}$}{Approximability}
  \Large
  While $\widehat{\psi}$ satisfies all Kolomogorov requirements\footnote[frame]{\tiny Braun and Griebel, Constr. Approx., 30:653-675, 2009},\\
  \smallskip
  it's not locally Lipschitz on \textbf{any} open interval $I \subset [0,1]$.

  \bigskip

  In fact, $\psi \in \text{H\"older}_\alpha([0,1])$ for $\alpha = \log_{2n+2} 2$,
  \medskip
  \begin{align*}
    |\psi(x) - \psi(y)| < \epsilon \quad \text{if} \quad |x - y| < \epsilon^{1/\alpha}.
  \end{align*}
  \medskip
  For $n = 2$, $\alpha^{-1} \approx 2.5$, but\\
  for $n = 10$ we have $\alpha^{-1} \approx 4.5$.
\end{frame}
%
\subsection{Lipschitz Construction}
\begin{frame}{Reasoning for Lipschitz Construction}
  \Large
  KST inner functions are\\
  \bigskip
  \qquad\qquad \dots strictly monotonically increasing\\
  \bigskip
  \qquad\qquad \dots so they are of Bounded Variation\\
  \bigskip
  \qquad\qquad \dots so they define rectifiable curves\\
  \bigskip
  \qquad\qquad \dots which have Lipschitz reparameterizations.
\end{frame}
%
\begin{frame}{Lipschitz Reparameterization}
  \Large
  \begin{center}
    Reparameterization $\sigma: [0,1] \rightarrow [0,1]$
    \medskip
    \begin{align*}
      \sigma(x) &= \frac{\text{ arclength of }\widehat{\psi} \text{ from }0 \text{ to }x }{\text{ total arclength of }\widehat{\psi} \text{ from } 0 \text{ to }1}\\
      ~&~\\
      \psi(x)   &= \widehat{\psi}\left( \sigma^{-1}(x) \right)
    \end{align*}
  \end{center}
\end{frame}
%
\begin{frame}{Lipschitz Reparameterization $\psi, k = 7$}
  \begin{figure}
    \centering
    \includegraphics[height=0.85\textheight]{figures/Krep/LipschitzInner_k7}
  \end{figure}
\end{frame}
%
\begin{frame}{KST Proof with Lipschitz $\psi$}
  \Large
  \begin{itemize}
    \item $\psi^k = \widehat{\psi^k} \circ (\sigma^k)^{-1}$ converges uniformly to $\psi$
    \bigskip
    \item $\psi$ satisfies the Kolmogorov requirements
    \bigskip
    \item $\psi \in \text{Lip}_{2}([0,1])$
  \end{itemize}
\end{frame}
%
\begin{frame}{Comparing $\widehat{\mathcal{S}}^k$ and $\mathcal{S}^k$}
  \centering
  \begin{figure}
    \centering
    \begin{tikzpicture}
      \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[width=\framewidth,height=0.8\textheight]{figures/Krep/HolderVsLipschitzInner_Sk}};
      \begin{scope}[x={(image.south east) },y={(image.north west)}]
        \node [right] at (-0.08, 0.33) {Uniform};
        \node [right] at (-0.08, 0.67) {$\sigma$(Uniform)};
      \end{scope}
    \end{tikzpicture}
  \end{figure}
\end{frame}
%
\subsection{Outer Functions}
\begin{frame}{Kolmogorov Outer Function}
  \Large
  Define a residual
  \begin{align*}
    f_r = f_{r-1} - \sum_{q=0}^{2d} \chi_{r-1} \circ \Psi^q
  \end{align*}
  and choose $k_r$, with $r \in \N$, so that the oscillation of
  \begin{align*}
    f_{r-1} - \sum_{q=0}^{2d} \chi_{r-1} \circ \Psi^q
  \end{align*}
  is bounded by $\frac{\|f_{r-1}\|_\infty}{d+1}$. Then by a Fixed Point Theorem,
  \begin{align*}
    \chi = \lim_{r \rightarrow \infty} \chi_r.
  \end{align*}
\end{frame}
%
\begin{frame}{Outer Function Construction}
  \Large
  \begin{center} 
    $\chi_r: \R \rightarrow \R$ interpolates
    \bigskip
    \begin{align*}
      \left\{\left(\Psi^q(\mathbf{d_{k_r}}), \frac{1}{d+1}f(\mathbf{d_{k_r}})\right) \,\,:\,\, \mathbf{d_{k_r}} \in \prod_{p=1}^d \sigma(\mathcal{D}_{k_r} + q \varepsilon)\right\}
    \end{align*}
    % $$\left \lvert \sum_{q=0}^{2d} \chi_r( \Psi^q(\mathbf{d_{k_r}})) - f(\mathbf{d}_{k_r}) \right \rvert \le \frac{\norm{f_{r-1}}_\infty}{d+1} \rightarrow 0$$
  \end{center}
  % Given $f$, need to find $\chi \in C([0,1]) \rightarrow \R$ such that $$f = \sum_{q=0}^{2d} \chi \circ \Psi^q$$
\end{frame}
%
% TODO For H\"older $\psi$, only need polynomial $P(d)$ points \footnote{\tiny{ Griebel and Braun, ICHPSC 2009}}
%
%
\section{Conclusions}
%
\begin{frame}{Conclusions}

\begin{itemize}\LARGE
  \item KST leverages superpositions for dimension reduction
  \bigskip
  \item Practical KST computation requires Lipschitz inner functions
  \bigskip
  \item Need to develop adaptive theory for outer function construction
\end{itemize}
\end{frame}
%
\begin{frame}[plain]

\begin{center}
\Huge\bf Thank You!
\end{center}

\bigskip

\begin{center}
\LARGE \magenta{\href{http://cse.buffalo.edu/~knepley}{http://cse.buffalo.edu/\textasciitilde knepley}}
\end{center}
\end{frame}

\end{document}
