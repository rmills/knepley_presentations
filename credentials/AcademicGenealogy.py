#!/usr/bin/env python

tree = [('Matthew Gregg Knepley', 'Purdue University', 2000,
         [('Ahmed Hamdy Mohamed Sameh', 'University of Illinois at Urbana-Champaign', 1968,
           [('Alfredo Ang', 'University of Illinois at Urbana-Champaign', 1959,
             [('Nathan Newmark', 'University of Illinois at Urbana-Champaign', 1934,
               [('H. Malcolm Westergaard', 'Technische Universit\\"at M\\"unchen', 1921,
                 [('Sebastian Finsterwalder', 'Eberhard-Karls-Universit\\"at T\\"ubingen', 1886,
                   []),
                  ('Ludwig F\\"oppl', 'Georg-August-Universit\\"at G\\"ottingen', 1912,
                   [('David Hilbert', 'Georg-August-Universit\\"at G\\"ottingen', 1885,
                     [('Carl Louis Ferdinand Lindemann', 'Friedrich-Alexander-Universit\\"at Erlangen-N\\"urnberg', 1873,
                       [('Christian Felix Klein', 'Rheinische Friedrich-Wilhelms-Universit\\"at Bonn', 1868,
                         [('Julius Pl\\"ucker', 'Philipps-Universit\\"at Marburg', 1823,
                           [('Christian Ludwig Gerling', 'Georg-August-Universit\\"at G\\"ottingen', 1812,
                             [('Carl Friedrich Gau\ss', 'Universit\\"at Helmstedt', 1799,
                               [('Johann Friedrich Pfaff', 'Georg-August-Universit\\"at G\\"ottingen', 1786,
                                 [('Abraham Gotthelf K\\"astner', 'Universit\\"at Leipzig', 1739,
                                   [('Christian August Hausen', 'Martin-Luther-Universit\\"at Halle-Wittenberg', 1713,
                                     [('Johann Andreas Planer', 'Martin-Luther-Universit\\"at Halle-Wittenberg', 1709,
                                       [('Johann Pasch', 'Martin-Luther-Universit\\"at Halle-Wittenberg', 1683,
                                         [('Michael Walther, Jr.', 'Martin-Luther-Universit\\"at Halle-Wittenberg', 1661,
                                           [('Johann Andreas Quenstedt', 'Martin-Luther-Universit\\"at Halle-Wittenberg', 1644,
                                             [('Christoph Notnagel', 'Martin-Luther-Universit\\"at Halle-Wittenberg', 1630,
                                               [('Ambrosius Rhodius', 'Martin-Luther-Universit\\"at Halle-Wittenberg', 1610,
                                                 [('Melchior J\\"ostel', 'Martin-Luther-Universit\\"at Halle-Wittenberg', 1600,
                                                   [('Valentinus Otho', 'Martin-Luther-Universit\\"at Halle-Wittenberg', 1570,
                                                     [('Georg Joachim von Leuchen', 'Rheticus Martin-Luther-Universit\\"at Halle-Wittenberg', 1535,
                                                      [('Miko{\l}aj Kopernik (Nicolaus Copernicus)',' Uniwersytet Jagiello\\`nski, Universit\\\'a di Bologna, Universit\\\'a degli Studi di Ferrara, Universit\\\'a di Padova', 1499,
                                                        [])])])])])])])])])])])])])])])]),
                          ('Rudolf Otto Sigismund Lipschitz', 'Universit\\"at Berlin', 1853,
                           [('Gustav Peter Lejeune Dirichlet', 'Rheinische Friedrich-Wilhelms-Universit\\"at Bonn', 1827,
                             [('Simeon Denis Poisson', '\\\'Ecole Polytechnique', 1800,
                               [('Joseph Louis Lagrange', 'Universit\\\'a di Torino', 1754,
                                 [('Leonhard Euler', 'Universit\\"at Basel', 1726,
                                   [('Johann Bernoulli', 'Universit\\"at Basel', 1694,
                                     [('Jacob Bernoulli', 'Universit\\"at Basel', 1684,
                                       [('Nicolas Malebranche', 'Unknown', 1672,
                                         [('Gottfried Wilhelm Leibniz', 'Universit\\"at Leipzig, Acad\\\'emie royale des sciences de Paris', 1666,
                                           [('Christiaan Huygens', 'Universiteit Leiden', 1647,
                                             [('Frans van Schooten, Jr.', 'Universiteit Leiden', 1635,
                                               [('Marin Mersenne', 'Universit\\\'e Paris IV-Sorbonne', 1611,
                                                 [])])])])])])])])])]),
                              ('Jean-Baptiste Joseph Fourier', '\\\'Ecole Normale Sup\\\'erieure', 1795,
                               [('Joseph Louis Lagrange', 'Universit\\\'a di Torino', 1754,
                                 []),
                                ('Pierre-Simon Laplace', 'Universit\\\'e de Caen Basse Normandie', 1773,
                                 [('Jean Le Rond d\\\'Alembert', 'Acad\\\'emie des Sciences Paris', 1741,
                                   [])]),
                                ('Gaspard Monge', 'Le coll\\`ege de la Trinit\\\'e \\`a Lyon', 1764,
                                 [])])])])])])])])])])])])])]

def drawSubtree(f, node, tab = '', opts = ''):
  name,uni,year,children = node
  f.write('node {\\vbox{\\begin{center} %s\\\\%s %d\\end{center}}} [%s]' % (name, uni, year, opts))
  for child in children:
    f.write('\n%schild {' % tab)
    drawSubtree(f, child, tab+'  ')
    f.write('%s}' % tab)
  f.write('\n')
  return

def outputLaTeX(tree, filename):
  with open(filename, 'w') as f:
    f.write('''\
\\documentclass{article}
\\usepackage{amsfonts, amsmath, subfigure, multirow}
\\usepackage{hyperref}
\\usepackage{graphicx}
\\usepackage{array}
\\usepackage{xfrac}

\\usepackage{tikz}
\\usepackage{pgflibraryshapes}
\\usetikzlibrary{backgrounds}
\\usetikzlibrary{arrows}
\\input{../figures/tikzColors}

\\usepackage[papersize={22in,34in},left=1in,right=17in,centering]{geometry}

\\begin{document}

\\begin{figure}
\\centering
\\begin{tikzpicture}[show background rectangle]
''')
    for root in tree:
      f.write('\\path[level/.style={sibling distance=10cm},level 9/.style={sibling distance=20cm}]\n')
      drawSubtree(f, root, opts = 'grow\'=up')
      f.write(';\n')
    f.write('''
\\end{tikzpicture}
\\caption{Academic Tree for Matthew Gregg Knepley}
\\end{figure}
\\end{document}
''')
  return

outputLaTeX(tree, 'AcademicGenealogy.tex')
