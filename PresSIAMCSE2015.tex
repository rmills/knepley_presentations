\documentclass[dvipsnames]{beamer}

\input{tex/talkPreamble.tex}
\beamertemplatenavigationsymbolsempty

\title[Magma]{Computational Magma Dynamics}
\author[M.~Knepley]{Matthew~Knepley {\bf$\in$} PETSc Team}
\date[CS\&E]{SIAM Computational Science \& Engineering\\Salt Lake City, UT \quad March 16, 2015}
% - Use the \inst command if there are several affiliations
\institute[UC]{
  Computation Institute\\
  University of Chicago
}
\subject{Magma}

\begin{document}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.12]{figures/logos/anl-white-background-modern.jpg}\hspace{1.0in}
  \includegraphics[scale=0.35]{figures/logos/ci_site_logo.png}\hspace{1.0in}
  \includegraphics[scale=0.30]{figures/logos/uc-logo-official.jpg}
  \end{center}
  \vskip0.4in
\end{frame}
%
\begin{frame}<testing>{abstract}

Title: Scalable Nonlinear Solvers for Magma Dynamics

We present new nonlinear solvers for magma dynamics problems based upon the nonlinear preconditioning framework in
PETSc. We demonstrate their effectiveness on model problems, and examine a possible direction for convergence analysis.
\end{frame}
%
\begin{frame}{Collaborators}
\begin{center}
\begin{tabular}{lc}
  \raisebox{0.6in}{\parbox[c]{15em}{\LARGE Magma Dynamics\\Collaboration}} &
  \vbox{
    \hbox{\includegraphics[width=1.2in,height=1.2in]{figures/Collaborators/RichardKatz.jpg}}
    \smallskip
    \hbox{\ \ \ Richard Katz}
  } \\
  \raisebox{0.6in}{\parbox[c]{10em}{\LARGE Timestepping\\Collaboration}} &
  \vbox{
    \hbox{\includegraphics[width=1.2in,height=1.2in]{figures/Collaborators/JedBrown.jpg}}
    \smallskip
    \hbox{\ \ \ \ \ Jed Brown}
  }
\end{tabular}
\end{center}
\end{frame}
%
\begin{frame}{Main Point}
\begin{center}
  \LARGE What am I going to talk about?
\end{center}
\Large
\bigskip
\begin{itemize}
  \item Magma Dynamics Model
  \medskip
  \item FEM formulation and solver
  \medskip
  \item FEM+FVM formulation and solver
\end{itemize}
\end{frame}
%
\begin{frame}{Main Point}
\begin{center}
  \LARGE Why is this important?
\end{center}
\Large
\bigskip
\begin{itemize}
  \item It is difficult to compare meshes, discretizations, and multilevel solvers
  \medskip
  \item \blue{Comparison} is essential for making informed algorithmic choices
  \medskip
  \item Comparison in a \red{single code} seems necessary
\end{itemize}
\end{frame}
%
%
\section{Problem Definition}
%
%%\input{slides/Magma/Overview.tex}
\input{slides/Magma/Equations.tex}
\input{slides/Magma/Benchmarks.tex}
%
\section{Solvers for FEM Formulation}
%
\begin{frame}{Mesh}\LARGE

With PETSc's DMPlex, we can use
\medskip
\begin{itemize}
  \item Simplices,
  \smallskip
  \item Hexes,
  \smallskip
  \item 2D and 3D,
\end{itemize}
\medskip
changing nothing but mesh creation.

\bigskip

\begin{center}\normalsize I do this in \magenta{\href{http://www.mcs.anl.gov/petsc/petsc-current/src/snes/examples/tutorials/ex62.c.html}{SNES ex62}}\end{center}
\end{frame}
%
\begin{frame}[fragile]{Output}\LARGE

PETSc Viewers can output
\medskip
\begin{overprint}
\onslide<1>
\begin{itemize}
  \item Meshes
  \smallskip
  \item Solutions,
  \smallskip
  \item Auxiliary and derived fields,
\end{itemize}
\onslide<2>
\begin{semiverbatim}\scriptsize
    -dm_view hdf5:sol_fv_1.h5
    -magma_view_solution hdf5:sol_fv_1.h5::append
    -compaction_vec_view hdf5:sol_fv_1.h5:HDF5_VIZ:append
\end{semiverbatim}
\end{overprint}

\medskip
to HDF5/Xdmf using simple options.
\end{frame}
%
\input{slides/Magma/DiscretizationFEM.tex}
%
\input{slides/Magma/BEuler.tex}
\input{slides/Magma/NK.tex}
\input{slides/Magma/NKConvEarly.tex}
\input{slides/Magma/NKConvLater.tex}
\input{slides/Magma/NKConvFail.tex}
\input{slides/Magma/NK-NCG.tex}
\input{slides/Magma/NK-NCGConv.tex}
\input{slides/Magma/FAS-NK.tex}
\input{slides/Magma/FAS-NKConv.tex}
\input{slides/Magma/FAS-NGS.tex}
\input{slides/Magma/FAS-NGSConv.tex}
%
%
\section{Solvers for FEM+FVM Formulation}
%
% - What did we have to do? (look at ChangeSets)
%   - Tabulation at FV (face) and FEM (cell) quad points
%   - Ghost cells
%   - Geometry and faces and cells and must respect periodicity
%   - Need different preallocation since topological adjacencies are different
%   - Need residual evaluation broken up into implicit and explicit parts
%   - FAS for combined system
%     - nonlinear if we feedback phi to momentum
%
\input{slides/Magma/DiscretizationFVM.tex}
\input{slides/Magma/SplitTS.tex}
\input{slides/Magma/MIMEX.tex}
\input{slides/Magma/NK-FV.tex}
\input{slides/Magma/NK-FVConvEarly.tex}
\input{slides/Magma/NK-FVConvLater.tex}
\input{slides/Magma/FAS-NK-FV.tex}
\input{slides/Magma/FAS-NK-FVConv.tex}
\input{slides/Magma/FAS-NGS-FV.tex}
\input{slides/Magma/FAS-NGS-FVConv.tex}
%
%% Slide with comparison of phi conservation (graph and plot of difference in phi after a certain time)
%% Slide of difference between FV and FEM porosity solutions
%% Slide with Mobius problem
%
\section*{Conclusions}
\begin{frame}
\begin{center}
  \Huge What does this mean?
\end{center}
\bigskip
\Large
\begin{overprint}
\onslide<1>
\begin{itemize}
  \item PETSc allows comparsion between\\
  Meshes,\\
  \quad Discretizations, and\\
  \qquad Solvers
  \medskip
  \item Can allow more robust, scalable solves
  \medskip
  \item Can allow better physical fidelity
\end{itemize}
\onslide<2>
There are a bunch of unanswered analytical questions:
\begin{itemize}
  \item Is the hybrid method stable? Working on a proof.
  \medskip
  \item Can we use the Implicit-Input/Explicit-Output scheme to increase the timestep?
  \medskip
  \item What is the accuracy/degree of freedom?
\end{itemize}
\end{overprint}
\medskip
\begin{center}\magenta{\href{http://www.mcs.anl.gov/petsc}{http://www.mcs.anl.gov/petsc}}\end{center}
\end{frame}

\end{document}
