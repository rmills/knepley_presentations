\documentclass{beamer}

\input{tex/talkPreamble.tex}

\title[SC]{Implementation for Scientific Computing:\\FEM and FMM}
\author[M.~Knepley]{Matthew~Knepley}
\date[Gyor '10]{Department of Mathematics and Computer Sciences\\Sz\'echenyi Istv\'an University, Gy\"or, Hungary\\October 1, 2010}
% - Use the \inst command if there are several affiliations
\institute[UC]{
  Computation Institute\\
  University of Chicago
}

\subject{FEM Implementation}

% Delete this, if you do not want the table of contents to pop up at the beginning of each subsection:
\AtBeginSection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[currentsection,hideothersubsections]
  \end{frame}
}

\AtBeginSubsection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[sectionstyle=show/hide,subsectionstyle=show/shaded/hide]
  \end{frame}
}

% If you wish to uncover everything in a step-wise fashion, uncomment the following command: 
%%\beamerdefaultoverlayspecification{<+->}

\begin{document}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.3]{figures/logos/rush_logo.png}\hspace{1.0in}
  \includegraphics[scale=0.24]{figures/logos/uc-logo-official.jpg}
  \end{center}
  \vskip0.4in
\end{frame}

\section{Introduction}

\begin{frame}<testing>
\frametitle{Abstract}
The implementation of modern scientific codes has become a daunting endeavor. Very often, application scientists are
unwilling or unable to make use of the latest theoretical advances due to the complexity of programming. We will examine
computations arising from both finite element discretization and the fast multipole method in order to illustrate how
better computational abstractions can alleviate the programmability burden while simultaneously providing improved
performance. In particular, we will examine operator assembly and mesh distribution from FEM methods, and a scalable
parallelization strategy for FMM, to show that better mathematical abstractions produce more flexible and efficient code.
\end{frame}

\begin{frame}<testing>
This talk is intended to highlight often overlooked influences of mathematics on computer science and scientific
computing. Optimization (in the technical sense) is routinely used to solve engineering problems, but seldom employed to
improve the software itself. Moreover, the role of mathematics in creating useful programming abstractions is often
ignored due to the difficulty in quantifying its beneficial effect.
\end{frame}

\begin{frame}<testing>
Questions:
\begin{itemize}
  \item[TM]: Arnold-Winther is not the first stable discretization for elasticity?
  \begin{itemize}
    \item I believe it is the first stable \emph{mixed} element

    \item It is the first stable element with purely polynomial basis functions (no other stabilization)

    \item Why are mixed discretizations superior to primitive variables?
  \end{itemize}

  \item[TM]: Industry does not need these new elements.

  \item[TM]: FEniCS does not handle isoparametric elements.
  \begin{itemize}
    \item I told him that it could, but this was FFC's job not FIAT
  \end{itemize}

  \item[LD]: Fischera corner is not the correct exact solution?
  \begin{itemize}
    \item Look in his book. Peter just made up a solution.
  \end{itemize}

  \item[LD]: Can FErari handle isoparametric elements?
  \begin{itemize}
    \item I told him that it could, as multilinear forms
  \end{itemize}

  \item[LD]: How does Sieve/FIAT handle hanging nodes?
  \begin{itemize}
    \item The local correction is directly applied (just like Wolfgang does it)

    \item Make an example of Sieve finding neighbors of quadrilateral and hexahedral hanging nodes (use sheets on table)
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}<testing>

Talk with Leszek:
\begin{itemize}
  \item Ask about Solver (talk wtih Jason) interface with direct solvers (SuperLU\_dist was crap on HP)
  \begin{itemize}
    \item Direct solver (uses it for Fischera problem)

    \item Maxwell dispersion controls p, accuracy limits h

    \item Formulate integral equation or wave equation (Fast Integral Rezanoff in Germany) (Olaf Steinbach Graz 3D BEM code with adaptivity)
  \end{itemize}

  \item Ask about hanging nodes

  \item Show orientation information on board

  \item Get 3Dhp code from Jason

  \item Nodal based data structure

  \item Algorithms on ONLY refinement trees (same as our Sieve algorithms) two-level cells -> nodes -> dof

  \item Cells <--> cells (like dual)

  \item Constraint information on nodes
  \begin{itemize}
    \item Explicit face connection for hanging node
  \end{itemize}

  \item Constrained approx techniques for dof (Projection Based Interpolation in 3D A. Buffia)

  \item Can we do recursive compression of the full sieve

  \item We can use the hp3D implemenetation (like UHM) underneath the Sieve interface
  \begin{itemize}
    \item This sounds like a wothwhile project
  \end{itemize}

  \item Must try problem with two connecting faces with different splits

  \item Has 60 levels in 2D and 15 in 3D
\end{itemize}
\end{frame}

\input{slides/ScientificComputing/Opportunity.tex}
\input{slides/ScientificComputing/Realization.tex}
%
\begin{frame}{Collaborators}

\begin{itemize}
  \item Automated FEM
  \begin{itemize}
    \item Andy Terrel (UT Austin)
    \item Ridgway Scott (UChicago)
    \item Rob Kirby (Texas Tech)
  \end{itemize}

  \item Sieve
  \begin{itemize}
    \item Dmitry Karpeev (ANL)
    \item Peter Brune (UChicago)
    \item Anders Logg (Simula)
  \end{itemize}

  \item FMM
  \begin{itemize}
    \item Lorena Barba (BU)
    \item Felipe Cruz (Bristol)
    \item Rio Yokota (BU)
  \end{itemize}
\end{itemize}
\end{frame}
%
\section{Operator Assembly}
\begin{frame}{Main Point}

\Huge
A familiar problem,\\
\qquad FEM assembly,\\
\bigskip
\pause
is recast to allow\\
\bigskip
\pause
automatic optimization.
\end{frame}
\subsection{Problem Statement}
\input{slides/FEM/FormDecomposition.tex}
\input{slides/FEM/ElementMatrixFormation.tex}
\input{slides/FEM/ElementMatrixComputation.tex}
\input{slides/FErari/AbstractProblem.tex}
\subsection{Plan of Attack}
\input{slides/FErari/ComplexityReducingRelations.tex}
\input{slides/FErari/BinaryAlgorithm.tex}
\input{slides/FErari/Coplanarity.tex}
\subsection{Results}
\input{slides/FErari/Overview.tex}
\input{slides/FErari/Results.tex}
\subsection{Mixed Integer Linear Programming}
\input{slides/FErari/MINLPModeling.tex}
\input{slides/FErari/MINLPFormulation.tex}
\input{slides/FErari/MINLPFormulationSparse.tex}
\input{slides/FErari/MINLPResults.tex}
%
\section{Mesh Distribution}
\begin{frame}{Main Point}

\Huge
Rethinking meshes\\
\bigskip
\pause
produces a simple FEM interface\\
\bigskip
\pause
and good code reuse.
\end{frame}
%
\input{slides/ScientificComputing/Problems.tex}
%
\subsection{Sieve}
\input{slides/Sieve/Overview.tex}
\input{slides/Sieve/DoubletNavigation.tex}
\input{slides/MeshDistribution/MeshDual.tex}
\input{slides/Section/DoubletSectionNavigation.tex}
\input{slides/Overlap/DoubletOverlap.tex}
\input{slides/Overlap/Completion.tex}
\input{slides/Overlap/CompletionUses.tex}
%
\subsection{Distribution}
\input{slides/MeshDistribution/MeshDistribution.tex}
\input{slides/MeshDistribution/MeshPartition.tex}
\input{slides/Completion/DoubletDistribution.tex}
\input{slides/MeshDistribution/SectionDistribution.tex}
\input{slides/MeshDistribution/SieveDistribution.tex}
%% Annotate example slides with meaning of colors:
%%   blue:    sieve point (on arrow means remote point)
%%   green:   rank point
%%   magenta: data value
\input{slides/MeshDistribution/DistributionExample2D.tex}
\input{slides/MeshDistribution/DistributionExample3D.tex}
%
\subsection{Interfaces}
\input{slides/Sieve/OverviewII.tex}
\input{slides/Sieve/GlobalAndLocal.tex}
%%\input{slides/Sieve/HierarchicalInterfaces.tex}
\input{slides/Sieve/UnstructuredInterfaceBefore.tex}
\input{slides/Sieve/CombinatorialTopology.tex}
\input{slides/Sieve/UnstructuredInterfaceAfter.tex}
%%\input{slides/Sieve/HierarchicalAbstractions.tex}
%
\subsection{Full Assembly}
% Talk about partial assembly of operators
\input{slides/FEM/ResidualCode2.tex}
%%\input{slides/FEM/BoundaryConditions.tex}
%%\input{slides/FEM/DirichletValues.tex}
%%\input{slides/FEM/DualBasisApplication.tex}
\input{slides/PyLith/Overview.tex}
\input{slides/PyLith/MultipleMeshTypes.tex}
\input{slides/Fracture/Overview.tex}

\section{Parallel FMM}
\begin{frame}{Main Point}

\Huge
Using estimates and proofs,\\
\bigskip
\pause
a simple software architecture,\\
\bigskip
\pause
achieves good scaling\\
\bigskip
and adaptive load balance.
\end{frame}
%
\subsection{Short Introduction to FMM}
\input{slides/FMM/Applications.tex}
\input{slides/FMM/Basics.tex}
\input{slides/FMM/SpatialDecomposition.tex}
\input{slides/FMM/ControlFlow.tex}
%
\subsection{Parallelism}
\input{slides/FMM/FMMInSieve.tex}
\input{slides/FMM/ControlFlow.tex}
\input{slides/FMM/ParallelTreeImplementation.tex}
\input{slides/FMM/ParallelTreeGraph.tex}
%%\input{slides/FMM/ParallelDecomposition.tex}
%%\input{slides/FMM/ParallelDataMovement.tex}
\input{slides/FMM/ParallelLoadBalance.tex}
\input{slides/FMM/ParallelDecompositionSpiral.tex}
%
\subsection{PetFMM}
\input{slides/FMM/PetFMM.tex}
\input{slides/FMM/PetFMM_Performance.tex}

\section*{Conclusions}
\begin{frame}
\frametitle{Conclusions}
\begin{center}
    {\Large Better mathematical abstractions\\bring concrete benefits}
\end{center}

\begin{itemize}
  \item Vast reduction in complexity
  \begin{itemize}
    \item Dimension and mesh independent code

    \item Complete serial code reuse
  \end{itemize}

  \bigskip

  \item Opportunites for optimization
  \begin{itemize}
    \item Higher level operations missed by traditional compilers

    \item Single communication routine to optimize
  \end{itemize}

  \bigskip

  \item Expansion of capabilities
  \begin{itemize}
    \item Arbitrary elements

    \item Unstructured multigrid

    \item Multilevel algorithms
  \end{itemize}
\end{itemize}
\end{frame}


\section*{Further Work}
\subsection{FEM}
\input{slides/FEM/FIAT.tex}
\input{slides/FFC/FFC.tex}
\subsection{UMG}
\input{slides/Poisson/ReentrantRefinement.tex}
\input{slides/Optimal/NonOptimalMesh.tex}
\input{slides/UnstructuredMG/WhyMultigridII.tex}
\input{slides/UnstructuredMG/UMGRequirements.tex}
\input{slides/UnstructuredMG/FBC.tex}
\input{slides/UnstructuredMG/DecimationAlgorithm.tex}
\input{slides/UnstructuredMG/SieveAdvantages.tex}
\input{slides/UnstructuredMG/ExampleProblem3D.tex}
\input{slides/UnstructuredMG/GMGPerformanceIII.tex}
\input{slides/UnstructuredMG/HierarchyQualityII.tex}
\subsection{PyLith}
\input{slides/PyLith/Overview.tex}
\input{slides/PyLith/MultipleMeshTypes.tex}
\input{slides/PyLith/CohesiveCells.tex}
\input{slides/PyLith/MeshSplitting.tex}
\input{slides/PyLith/StrikeSlipBenchmark.tex}

\end{document}
