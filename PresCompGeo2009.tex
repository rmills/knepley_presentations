\documentclass{beamer}

\mode<presentation>
{
  %%\usetheme[right]{Hannover}
  %%\usetheme{Goettingen}
  %%\usetheme{Dresden}
  \usetheme{Warsaw}
  \useoutertheme{infolines}
  \useinnertheme{rounded}

  \setbeamercovered{transparent}
}

\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage{times}
\usepackage[T1]{fontenc} % Note that the encoding and the font should match. If T1 does not look nice, try deleting this line
\usepackage{amsfonts, amsmath, subfigure, multirow}
\usepackage{hyperref}
\newlength\LL \settowidth\LL{1000}

%\usepackage{beamerseminar}
\usepackage{graphicx}

\usepackage{array}

\newcommand{\uKern}{\mathbb{K}}

\newcommand{\D}{\mathcal{D}}\newcommand{\E}{\mathcal{E}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\I}{\mathcal{I}}
\newcommand{\N}{\mathcal{N}}
\newcommand{\bigO}{\mathcal{O}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\kb}{\tt}
\newcommand{\blue}{\textcolor{blue}}
\newcommand{\green}{\textcolor{green}}
\newcommand{\red}{\textcolor{red}}
\newcommand{\brown}{\textcolor{brown}}
\newcommand{\cyan}{\textcolor{cyan}}
\newcommand{\magenta}{\textcolor{magenta}}
\newcommand{\yellow}{\textcolor{yellow}}
\newcommand{\mini}{\mathop{\rm minimize}}
\newcommand{\st}{\mbox{subject to }}
\newcommand{\lap}[1]{\Delta #1}
\newcommand{\grad}[1]{\nabla #1}
\renewcommand{\div}[1]{\nabla \cdot #1}
\def\code#1{{\tt #1}}
\def\Update#1{\frametitle{Code Update} \begin{center}\Huge Update to {Revision \green #1}\end{center}}

\title[GPU]{Tree-based methods on GPUs}
\author[M.~Knepley]{Felipe~Cruz\inst1 and Matthew~Knepley\inst2${}^,$\inst3}
\date[GUCAS]{International Workshop on\\Modern Computational Geoscience Frontiers\\GUCAS, Beijing \qquad July 1, 2009}
% - Use the \inst command if there are several affiliations
\institute[UC]{
  \inst1 Department of Mathematics \hfill \inst2 Computation Institute\\
  University of Bristol            \hfill University of Chicago

  \smallskip

  \inst3 Department of Molecular Biology and Physiology\\
  Rush University Medical Center
}

\subject{Multicore FMM}

% Delete this, if you do not want the table of contents to pop up at the beginning of each subsection:
\AtBeginSection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[currentsection,hideothersubsections]
  \end{frame}
}

\AtBeginSubsection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[sectionstyle=show/hide,subsectionstyle=show/shaded/hide]
  \end{frame}
}

% If you wish to uncover everything in a step-wise fashion, uncomment the following command: 
%%\beamerdefaultoverlayspecification{<+->}

\begin{document}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.3]{figures/logos/rush_logo.png}\hspace{1.0in}
  \includegraphics[scale=0.24]{figures/logos/uc-logo-official.jpg}
  \end{center}
  \vskip0.4in
\end{frame}

\begin{frame}<testing>
\frametitle{Abstract}
We examine the performance of the Fast Multipole Method on the Nvidia Tesla machine. The inherent bottleneck imposed by
the tree structure is ameliorated by a refactoring of the algorithm which exposes the fine-grained dependency
structure. Examples are shown for problems arising from vortex methods for fluids.
\end{frame}
%
\begin{frame}<testing>
\frametitle{Main Idea}
Although computational results are the \emph{sine quibus non} for scientific computing, in this talk I want to
concentrate on the interface. Enabling applications depends more on programmability than on performance I contend. In
this, I am following Robert van de Geijn and his FLAME philosophy. Our extenstion of the FLASH interface to tree-based
solvers will greatly expand the universe of optimal methods available on multicore architectures.
\end{frame}
%
%
\section{Short Introduction to FMM}
%
\begin{frame}{FMM Applications for Geoscience}
    FMM can accelerate both integral and boundary element methods for:
\begin{itemize}
  \item Laplace

  \item Stokes

  \item Elasticity
\end{itemize}

\visible<2->{
    Advantages
\begin{itemize}
  \item Mesh-free

  \item $\bigO(N)$ time

  \item GPU and distributed parallelism

  \item Memory is greatly reduced in 3D for BEM
\end{itemize}
}
\end{frame}
%
\begin{frame}{FMM Applications for Geoscience}

    Constant coefficient versions can \blue{precondition} full equations:
\begin{itemize}
  \item Work by Dave May at ETH
  \begin{itemize}
    \item Solve Stokes

    \item Scale identity by viscosity magnitude
  \end{itemize}

  \bigskip

  \item Advantages over MG
  \begin{itemize}
    \item No grids have to be created

    \item No iterative problems
  \end{itemize}
\end{itemize}
\end{frame}
%
\begin{frame}{Stokes Flow}{Vorticity Formulation}

    In vorticity form, the Stokes equation conserves vorticity
\begin{equation*}
  \frac{\partial \omega}{\partial t} + u\cdot \nabla \omega
  = \frac{ {\rm D}\omega}{ {\rm D} t} = 0
\end{equation*}
and we can recover the velocity using the Biot-Savart law
\begin{eqnarray*}
  u(x,t) &=& \int(\nabla\times\mathbb{G})(x-x^{\prime})\omega(x^{\prime},t)dx^{\prime} \\
         &=& \int\uKern(x-x^{\prime})\omega(x^{\prime},t)d x^{\prime}=(\uKern\ast\omega)(x,t)
\end{eqnarray*}
where $\mathbb{G}$ is the Green function for the Poisson equation.
\end{frame}
%
\begin{frame}{Stokes Flow}{RBF Expansion}

    We expand the vorticity
\begin{equation*}
  \omega(x, t) \approx \omega_{\sigma}(x, t) = \sum_i^N \gamma_i \zeta_{\sigma}(x, x_i)
\end{equation*}
in a basis of radial functions
\begin{equation*}
  \zeta_{\sigma}(x,y) = \frac{1}{2\pi\sigma^2} \exp{\left( \frac{-|x-y|^2}{2\sigma^2} \right) }
\end{equation*}
resulting in the following kernel
\begin{equation*}
  \uKern_{\sigma}(x) =
       \frac{1}{2 \pi |x|^{2}}(-x_2,x_1) \left(1-\exp\left(-\frac{|x|^{2}}{2\sigma^{2}}\right)\right).
\end{equation*}
\end{frame}
%
\begin{frame}{Stokes Flow}{$N$-body Formulation}

    Thus the velocity evaluation is an $N$-body summation:
\begin{equation*}
  u_{\sigma}(x,t) = \sum_{j=1}^{N}
  \gamma_{j}\;\uKern_{\sigma}(x-x_{j}).
\end{equation*}

\end{frame}
%
\input{slides/FMM/Basics.tex}
\input{slides/FMM/PetFMM.tex}
\input{slides/FMM/PetFMM_Performance.tex}
%
\subsection{Spatial Decomposition}
\input{slides/FMM/SpatialDecomposition.tex}
%
\subsection{Data Decomposition}
\input{slides/FMM/Sections.tex}
%
%
\section{Multicore Interfaces}
\input{slides/FMM/Complexity_GG.tex}
%
\subsection{GPU Programming}
\input{slides/GPU/GPUvsCPU.tex}
\input{slides/GPU/GeneralProgramming.tex}
\input{slides/GPU/Modularity.tex}
\input{slides/GPU/Locality.tex}
%% TODO: Precision
%
%%\subsection{FLASH}
%%\input{slides/Automation/FLASH_Design.tex}
%%\input{slides/Automation/FLASH_Cholesky.tex}
%
\subsection{PetFMM}
\input{slides/FMM/PetFMM_GPU.tex}
\input{slides/FMM/PetFMM_GPU_Classes.tex}
% TODO: Add in Reduce task to this slide
\input{slides/FMM/PetFMM_Tasks.tex}
\input{slides/FMM/PetFMM_Transform.tex}
\input{slides/FMM/PetFMM_Reduce.tex}
\input{slides/FMM/PetFMM_GPU_Timing.tex}
\input{slides/FMM/PetFMM_Timing.tex}
%
%
\section{Multicore Implementation}
%
\subsection{Complexity Analysis}
\input{slides/FMM/Complexity_GG.tex}
%
\subsection{Redesign}
\input{slides/FMM/ParticlesPerCell.tex}
\input{slides/FMM/MissingConcurrency.tex}
\input{slides/FMM/MissingBandwidth.tex}
%
%
\section*{Conclusions}
\begin{frame}
\frametitle{What's Important?}
\begin{center}
    {\Large Interface improvements bring concrete benefits}
\end{center}

\begin{itemize}
  \item Facilitated code reuse
  \begin{itemize}
    \item Serial code was largely reused

    \item Test infrastructure completely reused
  \end{itemize}

  \bigskip

  \item Opportunites for performance improvement
  \begin{itemize}
    \item Overlapping computations

    \item Better task scheduling
  \end{itemize}

  \bigskip

  \item Expansion of capabilities
  \begin{itemize}
    \item Could now combine distributed and multicore implementations

    \item Could replace local expansions with cheaper alternatives
  \end{itemize}
\end{itemize}
\end{frame}

\end{document}
